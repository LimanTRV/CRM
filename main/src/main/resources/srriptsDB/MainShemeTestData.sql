
INSERT INTO main.crm_user(login, user_password, first_name, last_name, email, is_active, is_deleted) VALUES ('bloh','123', 'Joshua', 'Bloh', 'bloh@java.com', TRUE, FALSE);
INSERT INTO main.crm_user(login, user_password, first_name, last_name, email, is_active, is_deleted) VALUES ('admin','admin', 'Иосиф', 'Джугашвили', 'iosif@baku.com', TRUE, FALSE);
INSERT INTO main.crm_user(login, user_password, first_name, last_name, email, is_active, is_deleted) VALUES ('user','user', 'Карлос', 'Кастанеда', 'huan@kaktus.com', TRUE, FALSE);
INSERT INTO main.crm_user(login, user_password, first_name, last_name, email, is_active, is_deleted) VALUES ('guest','guest', 'Kasper', 'Ghost', 'ghost@busters.com', TRUE, FALSE);

INSERT INTO main.role( name, code, is_deleted) VALUES ('Admin', 'A', FALSE );
INSERT INTO main.role( name, code, is_deleted) VALUES ('Teacher', 'T', FALSE );
INSERT INTO main.role( name, code, is_deleted) VALUES ('Center admin', 'CA', FALSE );
INSERT INTO main.role( name, code, is_deleted) VALUES ('Viewer', 'V', FALSE );
INSERT INTO main.role( name, code, is_deleted) VALUES ('Student', 'S', FALSE );

INSERT INTO main.user_role( crm_user_id, role_id) VALUES (2, 1);
INSERT INTO main.user_role( crm_user_id, role_id) VALUES (1, 2);
INSERT INTO main.user_role( crm_user_id, role_id) VALUES (3, 4);
INSERT INTO main.user_role( crm_user_id, role_id) VALUES (4, 5);


INSERT INTO main.resource( code, name, url) VALUES ('L', 'Lesson', '/lessons');
INSERT INTO main.resource( code, name, url) VALUES ('LV', 'Lesson view', '/lessons/view');
INSERT INTO main.resource( code, name, url)VALUES ('T', 'Task', '/task');
INSERT INTO main.resource( code, name, url)VALUES ('P', 'Profile', '/profile');



INSERT INTO main.operation( code, operation_name, is_deleted) VALUES ('A','Add', FALSE);
INSERT INTO main.operation( code, operation_name, is_deleted) VALUES ('D', 'Delete', FALSE);
INSERT INTO main.operation( code, operation_name, is_deleted) VALUES ('E','Edit', FALSE);
INSERT INTO main.operation( code, operation_name, is_deleted) VALUES ('V', 'View', FALSE);


INSERT INTO main.student(crm_user_id) VALUES (1);
INSERT INTO main.student(crm_user_id) VALUES (2);
INSERT INTO main.student(crm_user_id) VALUES (3);
INSERT INTO main.study_group(name, description, course_type) VALUES ('группа 1', 'java-разработчики', 'java');
INSERT INTO main.study_group(name, description, course_type) VALUES ('группа 2', 'php-разработчики', 'php');
INSERT INTO main.study_group(name, description, course_type) VALUES ('группа 3', 'c++-разработчики', 'c++');
INSERT INTO main.study_group(name, description, course_type) VALUES ('группа 4', 'android-разработчики', 'android');

INSERT INTO main.student_to_group(student_id, study_group_id) VALUES (1, 1);
INSERT INTO main.student_to_group(student_id, study_group_id) VALUES (1, 2);
INSERT INTO main.student_to_group(student_id, study_group_id) VALUES (1, 3);
INSERT INTO main.student_to_group(student_id, study_group_id) VALUES (1, 4);
INSERT INTO main.student_to_group(student_id, study_group_id) VALUES (2, 1);
INSERT INTO main.student_to_group(student_id, study_group_id) VALUES (2, 2);
INSERT INTO main.student_to_group(student_id, study_group_id) VALUES (2, 3);
INSERT INTO main.student_to_group(student_id, study_group_id) VALUES (2, 4);
INSERT INTO main.student_to_group(student_id, study_group_id) VALUES (3, 1);
INSERT INTO main.student_to_group(student_id, study_group_id) VALUES (3, 2);
INSERT INTO main.student_to_group(student_id, study_group_id) VALUES (3, 3);
INSERT INTO main.student_to_group(student_id, study_group_id) VALUES (3, 4);



