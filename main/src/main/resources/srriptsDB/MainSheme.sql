CREATE SCHEMA main;

CREATE TABLE main.crm_user
(
  id serial NOT NULL,
  login character varying(255) NOT NULL,
  user_password character varying(255) NOT NULL,
  first_name character varying(255) NOT NULL,
  last_name character varying(255) NOT NULL,
  email character varying(255) NOT NULL,
  is_active boolean NOT NULL,
  is_deleted boolean NOT NULL default FALSE,
  CONSTRAINT pk_user PRIMARY KEY (id),
  CONSTRAINT app_user_usr_email_key UNIQUE (usr_email)
);

CREATE TABLE main.role
(
  id serial NOT NULL,
  name character varying(255) NOT NULL,
  code character varying(255) NOT NULL,
  is_deleted boolean NOT NULL default FALSE,
  CONSTRAINT pk_role PRIMARY KEY (id)
);

CREATE TABLE main.user_role
(
  id serial NOT NULL,
  crm_user_id integer NOT NULL,
  role_id integer NOT NULL,
  CONSTRAINT pk_user_role PRIMARY KEY (id),
  CONSTRAINT fk_crm_user_role FOREIGN KEY (crm_user_id)
  REFERENCES main.crm_user (id) MATCH SIMPLE
  ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT fk_user_role_role FOREIGN KEY (role_id)
  REFERENCES main.role (id) MATCH SIMPLE
  ON UPDATE NO ACTION ON DELETE NO ACTION
);

CREATE TABLE main.student
(
  id serial NOT NULL,
  is_deleted boolean NOT NULL default FALSE,
  crm_user_id integer,
  CONSTRAINT pk_student PRIMARY KEY (id),
  CONSTRAINT fk_student_crm_user FOREIGN KEY (crm_user_id)
  REFERENCES main.crm_user (id) MATCH SIMPLE
  ON UPDATE NO ACTION ON DELETE NO ACTION
);

CREATE TABLE main.operation
(
  id serial NOT NULL,
  code character varying(255),
  operation_name character varying(255),
  is_deleted boolean NOT NULL default FALSE,
  CONSTRAINT pk_operation PRIMARY KEY (id)
);

CREATE TABLE main.resource
(
  id serial NOT NULL,
  code character varying(255),
  name character varying(255),
  url character varying(255),
  is_deleted boolean NOT NULL default FALSE,
  CONSTRAINT pk_resource PRIMARY KEY (id)
);

CREATE TABLE main.operation_resource
(
  id serial NOT NULL,
  resource_id integer NOT NULL,
  operation_id integer NOT NULL,
  CONSTRAINT pk_operation_resource PRIMARY KEY (id),
  CONSTRAINT fk_operation_resource_resource FOREIGN KEY (resource_id)
  REFERENCES main.resource (id) MATCH SIMPLE
  ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT fk_operation_resource_operation FOREIGN KEY (operation_id)
  REFERENCES main.operation (id) MATCH SIMPLE
  ON UPDATE NO ACTION ON DELETE NO ACTION
);

CREATE TABLE main.role_resource
(
  id serial NOT NULL,
  resource_id integer NOT NULL,
  role_id integer NOT NULL,
  CONSTRAINT pk_role_resource PRIMARY KEY (id),
  CONSTRAINT fk_role_resource_resource FOREIGN KEY (resource_id)
  REFERENCES main.resource (id) MATCH SIMPLE
  ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT fk_role_resource_role FOREIGN KEY (role_id)
  REFERENCES main.role (id) MATCH SIMPLE
  ON UPDATE NO ACTION ON DELETE NO ACTION
);

CREATE TABLE main.study_group
(
  id serial NOT NULL,
  name character varying(255),
  description character varying(2000),
  course_type character varying(1000),
  is_deleted boolean NOT NULL default FALSE,
  CONSTRAINT pk_study_group PRIMARY KEY (id)
);

CREATE TABLE main.student_to_group
(
  id serial NOT NULL,
  student_id integer NOT NULL,
  study_group_id integer NOT NULL,
  CONSTRAINT pk_student_to_group PRIMARY KEY (id),
  CONSTRAINT fk_student_to_group_study_group FOREIGN KEY (study_group_id)
  REFERENCES main.study_group (id) MATCH SIMPLE
  ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT fk_student_to_group_student FOREIGN KEY (student_id)
  REFERENCES main.student (id) MATCH SIMPLE
  ON UPDATE NO ACTION ON DELETE NO ACTION
);