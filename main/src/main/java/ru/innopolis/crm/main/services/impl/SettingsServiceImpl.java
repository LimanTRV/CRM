package ru.innopolis.crm.main.services.impl;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

import ru.innopolis.crm.main.models.dao.SettingsOptionDao;
import ru.innopolis.crm.main.models.dao.exceptions.DaoException;
import ru.innopolis.crm.main.models.entity.SettingsOption;
import ru.innopolis.crm.main.services.SettingsService;
import ru.innopolis.crm.main.services.exceptions.ServiceException;

/**
 *
 */
@Service
public class SettingsServiceImpl implements SettingsService
{

	private static final Logger LOGGER = Logger.getLogger(SettingsServiceImpl.class);

	private SettingsOptionDao settingsOptionDao;

	@Override
	public List<SettingsOption> getAll() throws ServiceException
	{
		try
		{
			return settingsOptionDao.getAll();
		}
		catch (DaoException e)
		{
			LOGGER.error(e);
			throw new ServiceException();
		}
	}

	@Override
	public SettingsOption getById(long id) throws ServiceException
	{
		try
		{
			return settingsOptionDao.getById(id);
		}
		catch (DaoException e)
		{
			LOGGER.error(e);
			throw new ServiceException();
		}
	}

	@Override
	public SettingsOption getByCode(String code) throws ServiceException
	{
		try
		{
			return settingsOptionDao.getByCode(code);
		}
		catch (DaoException e)
		{
			LOGGER.error(e);
			throw new ServiceException();
		}
	}

	@Override
	public int insert(SettingsOption option) throws ServiceException
	{
		try
		{
			return settingsOptionDao.insert(option);
		}
		catch (DaoException e)
		{
			LOGGER.error(e);
			throw new ServiceException();
		}
	}

	@Override
	public int update(SettingsOption option) throws ServiceException
	{
		try
		{
			return settingsOptionDao.update(option);
		}
		catch (DaoException e)
		{
			LOGGER.error(e);
			throw new ServiceException();
		}
	}

	@Override
	public int delete(SettingsOption option) throws ServiceException
	{
		try
		{
			return settingsOptionDao.delete(option);
		}
		catch (DaoException e)
		{
			LOGGER.error(e);
			throw new ServiceException();
		}
	}

	@Autowired
	public void setSettingsOptionDao(SettingsOptionDao settingsOptionDao)
	{
		this.settingsOptionDao = settingsOptionDao;
	}
}
