package ru.innopolis.crm.main.models.dao.impl;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import ru.innopolis.crm.main.models.dao.OperationDao;
import ru.innopolis.crm.main.models.dao.ResourceDao;
import ru.innopolis.crm.main.models.entity.Operation;
import ru.innopolis.crm.main.models.entity.Resource;

import javax.sql.DataSource;
import java.sql.*;
import java.util.*;

/**
 * Implements <code>ResourceDao</code> interface
 *
 * @author Artem Panasyuk, Farit Fattakhov, Roman Taranov
 */
@Repository
public class ResourceDaoImpl implements ResourceDao
{

	private static final Logger LOG = Logger.getLogger(Resource.class);

	private DataSource dataSource;

	@Autowired
	public void setDataSource(DataSource dataSource)
	{
		this.dataSource = dataSource;
	}

	private static final String SELECT_ALL = "select * from main.resource where not is_deleted";

	private static final String SELECT_BY_ID = "select * from main.resource where not is_deleted and id = ?";

	private static final String SELECT_BY_CODE = "select * from main.resource where not is_deleted and code = ?";

	private static final String INSERT = "insert into main.resource (code, name, url, is_deleted) VALUES (?, ?, ?, ?)";

	private static final String UPDATE = "update main.resource set code=?, name=?, url=?, is_deleted=? where id=?";

	private static final String DELETE = "update main.resource set is_deleted=true where id=?";

	@Override
	public Resource getById(Long resourceId)
	{
		Resource resource = null;
		try
		{
			Connection connection = dataSource.getConnection();
			PreparedStatement preparedStatement = connection.prepareStatement(SELECT_BY_ID);
			preparedStatement.setLong(1, resourceId);
			ResultSet resultSet = preparedStatement.executeQuery();

			if (resultSet.next())
			{
				resource = createResource(resultSet);
			}
		}
		catch (SQLException e)
		{
			LOG.error("SQLException:" + e.getMessage());
		}
		return resource;
	}

	@Override
	public Resource getByCode(String resourceCode)
	{
		Resource resource = null;
		try
		{
			Connection connection = dataSource.getConnection();
			PreparedStatement preparedStatement = connection.prepareStatement(SELECT_BY_CODE);
			preparedStatement.setString(1, resourceCode);
			ResultSet resultSet = preparedStatement.executeQuery();

			if (resultSet.next())
			{
				resource = createResource(resultSet);
			}
		}
		catch (SQLException e)
		{
			LOG.error("SQLException:" + e.getMessage());
		}
		return resource;
	}

	@Override
	public List<Resource> getAll()
	{
		List<Resource> list = new ArrayList<>();
		try
		{
			Connection connection = dataSource.getConnection();
			Statement statement = connection.createStatement();
			ResultSet resultSet = statement.executeQuery(SELECT_ALL);

			while (resultSet.next())
			{
				list.add(createResource(resultSet));
			}
		}
		catch (SQLException e)
		{
			LOG.error("SQLException:" + e.getMessage());
		}
		return list;
	}

	@Override
	public void create(Resource resource)
	{
		try
		{
			Connection connection = dataSource.getConnection();
			PreparedStatement preparedStatement = connection.prepareStatement(INSERT, Statement.RETURN_GENERATED_KEYS);
			preparedStatement.setString(1, resource.getCode());
			preparedStatement.setString(2, resource.getName());
			preparedStatement.setString(3, resource.getUrl());
			preparedStatement.setBoolean(4, resource.isDeleted());

			preparedStatement.executeUpdate();
			ResultSet generatedKeys = preparedStatement.getGeneratedKeys();
			if (generatedKeys.next())
			{
				resource.setId(generatedKeys.getLong(1));
				LOG.info("New Resource inserted with id: " + resource.getId());
			}
			else
			{
				LOG.info("Resource with id: " + resource.getId() + "is existed");
			}
		}
		catch (SQLException e)
		{
			LOG.error("SQLException:" + e.getMessage());
		}
	}

	@Override
	public void update(Resource resource)
	{
		try
		{
			Connection connection = dataSource.getConnection();
			PreparedStatement preparedStatement = connection.prepareStatement(UPDATE, Statement.RETURN_GENERATED_KEYS);

			preparedStatement.setString(1, resource.getCode());
			preparedStatement.setString(2, resource.getName());
			preparedStatement.setString(3, resource.getUrl());
			preparedStatement.setBoolean(4, resource.isDeleted());
			preparedStatement.setLong(5, resource.getId());

			preparedStatement.executeUpdate();

		}
		catch (SQLException e)
		{
			LOG.error("SQLException:" + e.getMessage());
		}
	}

	@Override
	public void delete(Resource resource)
	{
		try
		{
			Connection connection = dataSource.getConnection();
			PreparedStatement preparedStatement = connection.prepareStatement(DELETE, Statement.RETURN_GENERATED_KEYS);
			preparedStatement.setLong(1, resource.getId());

			preparedStatement.executeUpdate();
		}
		catch (SQLException e)
		{
			LOG.error("SQLException:" + e.getMessage());
		}
	}

	/**
	 * Creates new Resource from given result set
	 *
	 * @param resultSet - result of a select from table in DB
	 * @return - entity corresponding to the selected row
	 * @throws SQLException
	 */
	private Resource createResource(ResultSet resultSet) throws SQLException
	{
		return new Resource(
				resultSet.getLong("id"),
				resultSet.getString("code"),
				resultSet.getString("name"),
				resultSet.getString("url"),
				resultSet.getBoolean("is_deleted")
		);
	}

}
