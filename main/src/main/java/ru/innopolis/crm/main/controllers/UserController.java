package ru.innopolis.crm.main.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;
import ru.innopolis.crm.main.models.entity.Role;
import ru.innopolis.crm.main.models.entity.Student;
import ru.innopolis.crm.main.models.entity.User;
import ru.innopolis.crm.main.services.RoleService;
import ru.innopolis.crm.main.services.StudentService;
import ru.innopolis.crm.main.services.UserService;
import ru.innopolis.crm.main.sessionbean.UserBean;

import java.util.HashSet;
import java.util.Set;

@Controller
@PreAuthorize(value = "hasRole('M_E')")
@SessionAttributes({"userId", "userLogin"})
@RequestMapping(value = "/UserController")
public class UserController
{
	private static final String STUDENT_ROLE_CODE = "S";
	private static final String INSERT_OR_EDIT = "admin/editUser";
	private static final String LIST_USER = "admin/listUser";
	private static final String CHANGE_PASSWORD = "admin/changePassword";
	private static final String LIST_PARAM = "users";
	private static final String ENTITY_PARAM = "user";
	private static final String BEAN_PARAM = "userBean";
	private static final String LIST_REDIRECT = "redirect:UserController?action=listUser";
	private static final String LIST_PARAM_ROLES = "allRoles";

	private UserService userService;
	private StudentService studentService;
	private RoleService roleService;
	private UserBean userBean;


	@RequestMapping(params = {"!action"}, method = RequestMethod.GET)
	public String defaultForm(Model model)
	{
		model.addAttribute(LIST_PARAM_ROLES, roleService.getAll());
		model.addAttribute(LIST_PARAM, userService.getAll());
		return LIST_USER;
	}

	@RequestMapping(params = {"action=listUser"}, method = RequestMethod.GET)
	public String showForm(Model model)
	{
		model.addAttribute(LIST_PARAM_ROLES, roleService.getAll());
		model.addAttribute(LIST_PARAM, userService.getAll());
		model.addAttribute(BEAN_PARAM, userBean);
		return LIST_USER;
	}

	@RequestMapping(params = {"action=insert"}, method = RequestMethod.GET)
	public String addUser(Model model)
	{
		model.addAttribute(LIST_PARAM_ROLES, roleService.getAll());
		return INSERT_OR_EDIT;
	}

	@RequestMapping(params = {"action=edit"}, method = RequestMethod.GET)
	public String EditUser(Model model,
						   @RequestParam(value = "userId") Long userId)
	{
		model.addAttribute(ENTITY_PARAM, userService.getById(userId));
		model.addAttribute(LIST_PARAM_ROLES, roleService.getAll());
		model.addAttribute(BEAN_PARAM, userBean);
		return INSERT_OR_EDIT;
	}

	@RequestMapping(params = {"action=delete"}, method = RequestMethod.GET)
	public String DeleteUser(Model model,
							 @RequestParam(value = "userId") Long userId)
	{
		userService.deleteById(userId);
		return LIST_REDIRECT;
	}

	@RequestMapping(method = RequestMethod.POST)
	public ModelAndView saveUser(@RequestParam(value = "userId", required = false) Long userId,
								 @RequestParam(value = "login") String login,
								 @RequestParam(value = "password", required = false) String password,
								 @RequestParam(value = "firstName") String firstName,
								 @RequestParam(value = "lastName") String lastName,
								 @RequestParam(value = "email") String email,
								 @RequestParam(value = "isActive", required = false) Boolean isActive,
								 @RequestParam(value = "isDeleted", required = false) Boolean isDeleted,
								 @RequestParam(value = "roles") Long[] roles)
	{
		ModelAndView mav = new ModelAndView();

		if ((login == null) || (login.length() == 0) ||
				(firstName == null) || (firstName.length() == 0) ||
				(lastName == null) || (lastName.length() == 0) ||
				(email == null) || (email.length() == 0))
		{
			mav.setViewName("error");
			return mav;
		}

		if (userId == null)
		{
			userId = -1L;
		}

		if (isDeleted == null)
		{
			isDeleted = false;
		}

		if (isActive == null)
		{
			isActive = (userId < 0) || userService.getById(userId).isActive();
		}

		User user = new User(userId, login, null, firstName, lastName, email, isActive, isDeleted);
		if (password != null)
		{
			user.setPassword(password);
		}

		Set<Role> userRoles = new HashSet<>();
		if (roles != null)
		{
			for (Long roleId :
					roles)
			{
				Role role = roleService.getById(roleId);
				userRoles.add(role);
			}
		}
		user.setRoles(userRoles);

		userService.save(user);

		if (userRoles.contains(roleService.getByCode(STUDENT_ROLE_CODE)))
		{
			if (studentService.getStudentByUserId(user.getId()) == null)
			{
				Student student = new Student();
				student.setUser(user);
				studentService.create(student);
			}
		}

		mav.setViewName(LIST_REDIRECT);

		return mav;
	}

	@RequestMapping(params = {"action=change_password"}, method = RequestMethod.GET)
	public String changePasswordForm(Model model,
									 @RequestParam(value = "userId") Long userId)
	{
		model.addAttribute(ENTITY_PARAM, userService.getById(userId));
		return CHANGE_PASSWORD;
	}

	@RequestMapping(params = {"action=change_password"}, method = RequestMethod.POST)
	public ModelAndView changePassword(@RequestParam(value = "userId") Long userId,
									   @RequestParam(value = "password") String password)
	{
		ModelAndView mav = new ModelAndView();
		User user = userService.getById(userId);
		user.setPassword(password);
		userService.save(user);

		mav.setViewName(LIST_REDIRECT);

		return mav;
	}

	@Autowired
	public void setUserService(UserService userService)
	{
		this.userService = userService;
	}

	@Autowired
	public void setStudentService(StudentService studentService)
	{
		this.studentService = studentService;
	}

	@Autowired
	public void setRoleService(RoleService roleService)
	{
		this.roleService = roleService;
	}

	@Autowired
	public void setUserBean(UserBean userBean)
	{
		this.userBean = userBean;
	}
}