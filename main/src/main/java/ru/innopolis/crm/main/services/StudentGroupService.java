package ru.innopolis.crm.main.services;


import ru.innopolis.crm.main.models.entity.Student;

import java.util.List;
import java.util.Set;


public interface StudentGroupService
{
	int delete(long groupId, long studentId);

	int deleteThatNotActual(long groupId, List<Long> studentIds);

	int insert(long groupId, List<Long> studentIds);
}
