package ru.innopolis.crm.main.models.entity;

import java.sql.Timestamp;

/**
 *
 */
public class Criterion
{

	private long id;
	private String title;
	private int maxPoints;
	private boolean isAdditional;
	private Timestamp created;

	public long getId()
	{
		return id;
	}

	public void setId(long value)
	{
		this.id = value;
	}

	public String getTitle()
	{
		return title;
	}

	public void setTitle(String value)
	{
		this.title = value;
	}

	public int getMaxPoints()
	{
		return maxPoints;
	}

	public void setMaxPoints(int value)
	{
		this.maxPoints = value;
	}

	public boolean isAdditional()
	{
		return isAdditional;
	}

	public void setAdditional(boolean value)
	{
		this.isAdditional = value;
	}

	public Timestamp getCreated()
	{
		return created;
	}

	public void setCreated(Timestamp value)
	{
		this.created = value;
	}

	public Criterion()
	{
	}

	public Criterion(String title, int maxPoints, boolean isAdditional)
	{
		this.title = title;
		this.maxPoints = maxPoints;
		this.isAdditional = isAdditional;
	}

}
