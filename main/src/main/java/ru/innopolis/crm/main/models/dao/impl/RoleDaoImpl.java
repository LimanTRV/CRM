package ru.innopolis.crm.main.models.dao.impl;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import ru.innopolis.crm.main.models.dao.RoleDao;
import ru.innopolis.crm.main.models.entity.Role;

import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Implements <code>RoleDao</code> interface
 *
 * @author Artem Panasyuk, Farit Fattakhov, Roman Taranov
 */
@Repository
public class RoleDaoImpl implements RoleDao
{

	private static final Logger LOG = Logger.getLogger(RoleDaoImpl.class);

	private DataSource dataSource;

	@Autowired
	public void setDataSource(DataSource dataSource)
	{
		this.dataSource = dataSource;
	}

	private static final String SELECT_ALL = "select * from main.role where not is_deleted";

	private static final String SELECT_BY_ID = "select * from main.role where not is_deleted and id = ?";

	private static final String SELECT_BY_CODE = "select * from main.role where not is_deleted and code = ?";

	private static final String SELECT_BY_USER_ID = "select r.* from main.role r" +
			" left join main.user_role ur on r.id = ur.role_id" +
			" where not r.is_deleted and ur.crm_user_id = ?";

	private static final String INSERT = "insert into main.role (code, name, is_deleted) VALUES (?, ?, ?)";

	private static final String UPDATE = "update main.role set code=?, name=?, is_deleted=? where id=?";

	private static final String DELETE = "update main.role set is_deleted=true where id=?";

	private static final String COUNT_USERS_BY_ROLEID = "SELECT count(*) FROM main.user_role ur" +
			" LEFT JOIN main.crm_user cu ON ur.crm_user_id = cu.id" +
			" LEFT JOIN main.role r ON ur.role_id = r.id" +
			" WHERE NOT cu.is_deleted AND NOT r.is_deleted AND role_id = ?";

	@Override
	public Role getById(Long roleId)
	{
		Role role = null;
		try
		{
			Connection connection = dataSource.getConnection();
			PreparedStatement preparedStatement = connection.prepareStatement(SELECT_BY_ID);
			preparedStatement.setLong(1, roleId);
			ResultSet resultSet = preparedStatement.executeQuery();

			if (resultSet.next())
			{
				role = createRole(resultSet);
			}
		}
		catch (SQLException e)
		{
			LOG.error("SQLException:" + e.getMessage());
		}
		return role;
	}

	@Override
	public Role getByCode(String code)
	{
		Role role = null;
		try
		{
			Connection connection = dataSource.getConnection();
			PreparedStatement preparedStatement = connection.prepareStatement(SELECT_BY_CODE);
			preparedStatement.setString(1, code);
			ResultSet resultSet = preparedStatement.executeQuery();

			if (resultSet.next())
			{
				role = createRole(resultSet);
			}
		}
		catch (SQLException e)
		{
			LOG.error("SQLException:" + e.getMessage());
		}
		return role;
	}

	@Override
	public List<Role> getByUserId(Long userId)
	{
		List<Role> list = new ArrayList<>();
		try
		{
			Connection connection = dataSource.getConnection();
			PreparedStatement preparedStatement = connection.prepareStatement(SELECT_BY_USER_ID);
			preparedStatement.setLong(1, userId);
			ResultSet resultSet = preparedStatement.executeQuery();

			while (resultSet.next())
			{
				list.add(createRole(resultSet));
			}
		}
		catch (SQLException e)
		{
			LOG.error("SQLException:" + e.getMessage());
		}
		return list;
	}

	@Override
	public List<Role> getAll()
	{
		List<Role> list = new ArrayList<>();
		try
		{
			Connection connection = dataSource.getConnection();
			Statement statement = connection.createStatement();
			ResultSet resultSet = statement.executeQuery(SELECT_ALL);

			while (resultSet.next())
			{
				list.add(createRole(resultSet));
			}
		}
		catch (SQLException e)
		{
			LOG.error("SQLException:" + e.getMessage());
		}
		return list;
	}

	@Override
	public long create(Role role)
	{
		long lastId = 0;
		try
		{
			Connection connection = dataSource.getConnection();
			PreparedStatement preparedStatement = connection.prepareStatement(INSERT, Statement.RETURN_GENERATED_KEYS);
			preparedStatement.setString(1, role.getCode());
			preparedStatement.setString(2, role.getName());
			preparedStatement.setBoolean(3, role.isDeleted());

			preparedStatement.executeUpdate();
			ResultSet generatedKeys = preparedStatement.getGeneratedKeys();
			if (generatedKeys.next())
			{
				role.setId(generatedKeys.getLong(1));
				lastId = role.getId();
				LOG.info("New Role inserted with id: " + role.getId());
			}
			else
			{
				LOG.info("Role with id: " + role.getId() + "is existed");
			}
		}
		catch (SQLException e)
		{
			LOG.error("SQLException:" + e.getMessage());
		}
		return lastId;
	}

	@Override
	public long update(Role role)
	{
		long count = 0;
		try
		{
			Connection connection = dataSource.getConnection();
			PreparedStatement preparedStatement = connection.prepareStatement(UPDATE);

			preparedStatement.setString(1, role.getCode());
			preparedStatement.setString(2, role.getName());
			preparedStatement.setBoolean(3, role.isDeleted());
			preparedStatement.setLong(4, role.getId());

			count = preparedStatement.executeUpdate();

		}
		catch (SQLException e)
		{
			LOG.error("SQLException:" + e.getMessage());
		}
		return count;
	}

	@Override
	public void delete(Role role)
	{
		try
		{
			Connection connection = dataSource.getConnection();
			PreparedStatement preparedStatement = connection.prepareStatement(DELETE, Statement.RETURN_GENERATED_KEYS);
			preparedStatement.setLong(1, role.getId());

			preparedStatement.executeUpdate();
		}
		catch (SQLException e)
		{
			LOG.error("SQLException:" + e.getMessage());
		}
	}

	@Override
	public int remove(long roleId)
	{
		int count = 0;
		try
		{
			Connection connection = dataSource.getConnection();
			PreparedStatement preparedStatement = connection.prepareStatement(DELETE);
			preparedStatement.setLong(1, roleId);

			count = preparedStatement.executeUpdate();
		}
		catch (SQLException e)
		{
			LOG.error("SQLException:" + e.getMessage());
		}
		return count;
	}

	@Override
	public int countUsersByRoleId(long roleId)
	{
		int count = 0;
		try
		{
			Connection connection = dataSource.getConnection();
			PreparedStatement preparedStatement = connection.prepareStatement(COUNT_USERS_BY_ROLEID);
			preparedStatement.setLong(1, roleId);

			ResultSet resultSet = preparedStatement.executeQuery();
			if (resultSet.next())
			{
				count = resultSet.getInt(1);
			}
		}
		catch (SQLException e)
		{
			LOG.error("SQLException:" + e.getMessage());
		}
		return count;
	}

	/**
	 * Creates new Role from given result set
	 *
	 * @param resultSet - result of a select from table in DB
	 * @return - entity corresponding to the selected row
	 * @throws SQLException
	 */
	private Role createRole(ResultSet resultSet) throws SQLException
	{
		return new Role(
				resultSet.getLong("id"),
				resultSet.getString("code"),
				resultSet.getString("name"),
				resultSet.getBoolean("is_deleted")
		);
	}
}
