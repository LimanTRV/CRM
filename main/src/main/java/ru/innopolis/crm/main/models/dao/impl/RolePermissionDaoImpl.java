package ru.innopolis.crm.main.models.dao.impl;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import ru.innopolis.crm.main.models.dao.RolePermissionDao;
import ru.innopolis.crm.main.models.entity.Operation;
import ru.innopolis.crm.main.models.entity.Resource;

import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Implements <code>RolePermissionDao</code> interface
 *
 * @author Farit Fattakhov, Roman Taranov
 */
@Repository
public class RolePermissionDaoImpl implements RolePermissionDao
{

	private static final Logger LOG = Logger.getLogger(Operation.class);

	private static final String SELECT_BY_USER_ID = "SELECT r.id resId, r.code resCode, r.name resName, r.url, r.is_deleted resDeleted, " +
			"o.id opId, o.code opCode, o.operation_name, o.is_deleted opDeleted " +
			"  FROM main.role_permission rp " +
			"    INNER JOIN main.resource r ON r.id = rp.resource_id and not r.is_deleted " +
			"    INNER JOIN main.operation o ON o.id = rp.operation_id and not o.is_deleted " +
			"WHERE rp.role_id = ?";

	private DataSource dataSource;

	public Map<Resource, List<Operation>> getByRoleId(Long roleId)
	{
		Map<Resource, List<Operation>> permissionMap = new HashMap<>();

		try
		{
			Connection connection = dataSource.getConnection();
			PreparedStatement preparedStatement = connection.prepareStatement(SELECT_BY_USER_ID);
			preparedStatement.setLong(1, roleId);

			ResultSet rs = preparedStatement.executeQuery();

			Resource resource;
			while (rs.next())
			{
				resource = new Resource(rs.getLong("resId"), rs.getString("resCode"),
						rs.getString("resName"), rs.getString("url"), rs.getBoolean("resDeleted"));

				List<Operation> operations;
				if (permissionMap.containsKey(resource))
				{
					operations = permissionMap.get(resource);
				}
				else
				{
					operations = new ArrayList<Operation>();
					permissionMap.put(resource, operations);
				}

				Operation operation = new Operation(rs.getLong("opId"), rs.getString("opCode"),
						rs.getString("operation_name"), rs.getBoolean("opDeleted"));

				operations.add(operation);
			}
		}
		catch (SQLException e)
		{
			LOG.error("SQLException:" + e.getMessage());
		}
		return permissionMap;
	}

	@Override
	public boolean savePermissoin(long roleId, Map<Long, List<Long>> resources)
	{
		Connection connection = null;
		boolean result = true;
		try
		{
			connection = dataSource.getConnection();
			connection.setAutoCommit(false);

			deleteByRoleId(roleId);
			for (Map.Entry<Long, List<Long>> entry : resources.entrySet())
			{
				Long resourceId = entry.getKey();
				for (Long operationId : entry.getValue())
				{
					insert(roleId, resourceId, operationId);
				}
			}

			connection.commit();
		}
		catch (SQLException e)
		{
			result = false;
			LOG.error(e);
			try
			{
				if (connection != null)
				{
					connection.rollback();
				}
			}
			catch (SQLException e1)
			{
				LOG.error(e1);
			}
		}
		finally
		{
			try
			{
				if (connection != null)
				{
					connection.setAutoCommit(true);
				}
			}
			catch (SQLException e)
			{
				LOG.error(e);
			}
		}
		return result;
	}

	@Override
	public int insert(long roleId, long resourceId, long operationId)
	{
		int count = 0;
		try
		{
			Connection connection = dataSource.getConnection();
			PreparedStatement preparedStatement = connection.prepareStatement("INSERT INTO main.role_permission(role_id, resource_id, operation_id) VALUES (?, ?, ?)");
			preparedStatement.setLong(1, roleId);
			preparedStatement.setLong(2, resourceId);
			preparedStatement.setLong(3, operationId);
			count = preparedStatement.executeUpdate();
		}
		catch (SQLException e)
		{
			LOG.error(e);
		}
		return count;
	}

	@Override
	public int deleteByRoleId(long roleId)
	{
		int count = 0;
		try
		{
			Connection connection = dataSource.getConnection();
			PreparedStatement preparedStatement = connection.prepareStatement("DELETE FROM main.role_permission WHERE role_id = ?");
			preparedStatement.setLong(1, roleId);
			count = preparedStatement.executeUpdate();
		}
		catch (SQLException e)
		{
			LOG.error(e);
		}
		return count;
	}

	@Autowired
	public void setDataSource(DataSource dataSource)
	{
		this.dataSource = dataSource;
	}
}
