package ru.innopolis.crm.main.services;

import ru.innopolis.crm.main.models.entity.Resource;

import java.util.List;

/**
 * Service for working with entity <code>Resource</code>
 */
public interface ResourceService
{

	/**
	 * Get an <code>Resource</code> object from DB by id
	 *
	 * @param resourceId
	 * @return <code>Resource</code> entity
	 */
	Resource getById(Long resourceId);

	/**
	 * Delete an <code>Resource</code> object from DB by id
	 *
	 * @param resourceId
	 */
	void deleteById(Long resourceId);

	/**
	 * Creates a list of <code>Resource</code> objects from DB
	 *
	 * @return a new list of <code>Resource</code> objects
	 */
	List<Resource> getAll();

	/**
	 * Create or update an <code>Resource</code> object to DB
	 *
	 * @param resource <code>Resource</code> entity
	 */
	void save(Resource resource);
}
