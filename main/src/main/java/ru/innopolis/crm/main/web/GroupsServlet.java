package ru.innopolis.crm.main.web;

import org.apache.log4j.Logger;
import ru.innopolis.crm.main.models.dao.exceptions.GroupDataAccessException;
import ru.innopolis.crm.main.models.dao.exceptions.StudentDataAccessException;
import ru.innopolis.crm.main.models.entity.StudyGroup;
import ru.innopolis.crm.main.services.impl.GroupServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * Controller for view groups of students page
 */
public class GroupsServlet extends HttpServlet {
    private static Logger LOGGER = Logger.getLogger(GroupsServlet.class);
    private static GroupServiceImpl groupServiceImpl = new GroupServiceImpl();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            List<StudyGroup> groupList = groupServiceImpl.findAll();
            req.setAttribute("groupList", groupList);
            getServletContext().getRequestDispatcher("/students/groups.jsp").forward(req, resp);
        } catch (GroupDataAccessException e){
            getServletContext().getRequestDispatcher("/error.jsp").forward(req, resp);
        }
    }

    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            long id = Long.valueOf(req.getParameter("id"));
            if (0 < groupServiceImpl.delete(id)){
                resp.getWriter().print("Группа успешно удалена");
            } else {
                resp.setStatus(400);
            }
        } catch (NullPointerException | NumberFormatException | StudentDataAccessException | GroupDataAccessException e){
            resp.setStatus(400);
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String groupId = req.getParameter("id");
        String name = req.getParameter("name");
        String description = req.getParameter("description");
        String courseType = req.getParameter("type");
        if (name == null || description == null || name.isEmpty() || description.isEmpty() || courseType == null || courseType.isEmpty()) {
            LOGGER.error("Got an empty parameter");
            resp.setStatus(400);
            return;
        }
        try {
            int result = 0;
            if (groupId == null || groupId.isEmpty()) {
                StudyGroup groupItem = new StudyGroup(0, name, description, courseType);
                result = groupServiceImpl.insert(groupItem);
            } else {
                StudyGroup groupItem = groupServiceImpl.findById(Long.valueOf(groupId));
                groupItem.setName(name);
                groupItem.setCourseType(courseType);
                groupItem.setDescription(description);
                result = groupServiceImpl.update(groupItem);
            }
            if (result > 0){
                resp.getWriter().write("Группа успешно сохранена");
            } else {
                resp.setStatus(400);
            }
        } catch (NumberFormatException | GroupDataAccessException e){
            resp.setStatus(400);
        }
    }
}