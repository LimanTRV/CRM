package ru.innopolis.crm.main.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.innopolis.crm.main.connection.DataSourceFactory;
import ru.innopolis.crm.main.models.dao.OperationDao;
import ru.innopolis.crm.main.models.dao.ResourceDao;
import ru.innopolis.crm.main.models.dao.RoleDao;
import ru.innopolis.crm.main.models.dao.RolePermissionDao;
import ru.innopolis.crm.main.models.dao.impl.OperationDaoImpl;
import ru.innopolis.crm.main.models.dao.impl.ResourceDaoImpl;
import ru.innopolis.crm.main.models.dao.impl.RoleDaoImpl;
import ru.innopolis.crm.main.models.dao.impl.RolePermissionDaoImpl;
import ru.innopolis.crm.main.models.entity.Operation;
import ru.innopolis.crm.main.models.entity.Resource;
import ru.innopolis.crm.main.models.entity.Role;
import ru.innopolis.crm.main.services.PermissionService;

import javax.sql.DataSource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * User permission service
 */
@Service
public class PermissionServiceImpl implements PermissionService
{
	private RolePermissionDao rolePermissionDao;
	private RoleDao roleDao;
	private ResourceDao resourceDao;
	private OperationDao operationDao;

	@Override
	public boolean hasUserPermission(Long userId, String resourceCode, String operationCode)
	{
		Map<Resource, List<Operation>> userPermissions = new HashMap<>();

		List<Role> roleList = roleDao.getByUserId(userId);
		if (roleList == null)
		{
			return false;
		}

		Resource resource = resourceDao.getByCode(resourceCode);

		if (resource == null)
		{
			return false;
		}

		Operation operation = operationDao.getByCode(operationCode);

		if (operation == null)
		{
			return false;
		}

		for (Role role :
				roleList)
		{
			Map<Resource, List<Operation>> rolePermission = rolePermissionDao.getByRoleId(role.getId());

			for (Map.Entry<Resource, List<Operation>> permission :
					rolePermission.entrySet())
			{
				Resource newResource = permission.getKey();
				List<Operation> userOperations;
				if (userPermissions.containsKey(newResource))
				{
					userOperations = userPermissions.get(newResource);
				}
				else
				{
					userOperations = new ArrayList<Operation>();
					userPermissions.put(newResource, userOperations);
				}

				for (Operation newOperation :
						permission.getValue())
				{
					if (!userOperations.contains(newOperation))
					{
						userOperations.add(newOperation);
					}
				}
			}
		}

		if (userPermissions.containsKey(resource))
		{
			List<Operation> userOperations = userPermissions.get(resource);
			return userOperations.contains(operation);
		}
		else
		{
			return false;
		}
	}

	@Override
	public Map<Resource, List<Operation>> getByRoleId(Long roleId)
	{
		return rolePermissionDao.getByRoleId(roleId);
	}

	@Override
	public boolean savePermissoin(long roleId, Map<Long, List<Long>> resources)
	{
		return rolePermissionDao.savePermissoin(roleId, resources);
	}

	@Override
	public int insert(long roleId, long resourceId, long operationId)
	{
		return rolePermissionDao.insert(roleId, resourceId, operationId);
	}

	@Override
	public int deleteByRoleId(long roleId)
	{
		return rolePermissionDao.deleteByRoleId(roleId);
	}

	@Autowired
	public void setRolePermissionDao(RolePermissionDao rolePermissionDao)
	{
		this.rolePermissionDao = rolePermissionDao;
	}

	@Autowired
	public void setRoleDao(RoleDao roleDao)
	{
		this.roleDao = roleDao;
	}

	@Autowired
	public void setResourceDao(ResourceDao resourceDao)
	{
		this.resourceDao = resourceDao;
	}

	@Autowired
	public void setOperationDao(OperationDao operationDao)
	{
		this.operationDao = operationDao;
	}
}
