package ru.innopolis.crm.main.services;

import org.apache.commons.lang3.StringUtils;
import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpHead;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import javax.servlet.http.HttpServletRequest;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

/**
 * Created by Roman Taranov on 02.05.2017.
 */

@Service
public class FileUtilsBean
{

	private static final int UPLOAD_FILE_MAX_THRESHOLD = 31457280;

	private static final Logger LOGGER = Logger.getLogger(FileUtilsBean.class);

	private HttpClient httpClient;

	/**
	 * Проверим mime-тип
	 *
	 * @param request
	 * @param filedName
	 * @return
	 */
	public List<MultipartFile> getFilesFromRequest(HttpServletRequest request, String filedName)
	{
		MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
		for (MultipartFile f : multipartRequest.getFiles(filedName))
		{

			if (!("application/x-rar-compressed".equals(f.getContentType())
					|| "application/octet-stream".equals(f.getContentType())
					|| "application/zip".equals(f.getContentType())))
			{ // Запретим загрузку, если Mime-тип неподходящий
				return null;
			}
			return multipartRequest.getFiles(filedName);
		}

		return null;
	}

	/**
	 * Создание файла и директории при необходимости. Загрузка данных в файл
	 *
	 * @param filePath
	 * @param fileName
	 * @param data
	 * @throws FileNotFoundException
	 * @throws IOException
	 */
	public void saveDataToFile(String filePath, String fileName, byte[] data) throws FileNotFoundException, IOException
	{
		File dir = new File(filePath);
		if (!dir.exists())
		{
			dir.mkdirs();
		}
		File file = new File(dir, fileName);
		try (FileOutputStream fs = new FileOutputStream(file))
		{
			fs.write(data);

			fs.flush();
		}
	}

	/**
	 * Метод загружает файл по заданному url
	 *
	 * @param gitUrl   url архива
	 * @param filePath путь к файлу
	 * @return {@code true} в случае успеха, {@code false} в случае ошибки при загрузке
	 */
	public boolean loadDataToFile(String gitUrl, String filePath)
	{

		try
		{
			Path path = Paths.get(filePath);
			if (Files.notExists(path.getParent()))
			{
				Files.createDirectories(path.getParent());
			}
			if (Files.exists(path))
			{
				Files.delete(path);
			}
			Files.createFile(path);

			BufferedOutputStream os = new BufferedOutputStream(new FileOutputStream(filePath));
			HttpGet httpGet = new HttpGet(gitUrl);
			HttpResponse httpResponse = httpClient.execute(httpGet);

			int statusCode = httpResponse.getStatusLine().getStatusCode();

			if (statusCode == HttpStatus.SC_OK)
			{
				FileCopyUtils.copy(httpResponse.getEntity().getContent(), os);
			}

			return true;

		}
		catch (IOException e)
		{
			LOGGER.error(e);
		}

		return false;
	}


	/**
	 * Метод проверяет, что по данному URL находится zip-архив размером не более
	 * UPLOAD_FILE_MAX_THRESHOLD
	 *
	 * @param gitUrl url
	 * @return {@code true} если по url находится zip-архив размером UPLOAD_FILE_MAX_THRESHOLD
	 * {@code false}
	 */
	public boolean isUrlValid(String gitUrl)
	{
		if (StringUtils.isEmpty(gitUrl))
		{
			return false;
		}

		try
		{
			HttpHead headMethod = new HttpHead(gitUrl);
			HttpResponse httpResponse = httpClient.execute(headMethod);
			int statusCode = httpResponse.getStatusLine().getStatusCode();

			if (statusCode != HttpStatus.SC_OK)
			{
				return false;
			}

			Header contentTypeHeader = httpResponse.getLastHeader("Content-Type");
			Header contentLengthHeader = httpResponse.getLastHeader("Content-Length");
			if (contentTypeHeader != null && contentLengthHeader != null &&
					"application/zip".equals(contentTypeHeader.getValue()) &&
					Integer.valueOf(contentLengthHeader.getValue()) < UPLOAD_FILE_MAX_THRESHOLD)
			{

				return true;
			}

		}
		catch (IOException e)
		{
			LOGGER.error(e);
		}

		return false;
	}

	@Autowired
	public void setHttpClient(HttpClient httpClient)
	{
		this.httpClient = httpClient;
	}
}
