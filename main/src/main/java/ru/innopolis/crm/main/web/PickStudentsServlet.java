package ru.innopolis.crm.main.web;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import org.apache.log4j.Logger;
import ru.innopolis.crm.main.models.dao.exceptions.GroupDataAccessException;
import ru.innopolis.crm.main.models.dao.exceptions.StudentDataAccessException;
import ru.innopolis.crm.main.models.entity.Student;
import ru.innopolis.crm.main.models.entity.StudyGroup;
import ru.innopolis.crm.main.services.impl.GroupServiceImpl;
import ru.innopolis.crm.main.services.impl.StudentGroupServiceImpl;
import ru.innopolis.crm.main.services.impl.StudentServiceImpl;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * Controller of page of choosing students
 */

public class PickStudentsServlet extends HttpServlet {
    private static Logger LOGGER = Logger.getLogger(PickStudentsServlet.class);
    private static StudentServiceImpl studentServiceImpl = new StudentServiceImpl();
    private static GroupServiceImpl groupServiceImpl = new GroupServiceImpl();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String groupId = req.getParameter("group");
        StudyGroup groupItem;
        try {
            groupItem = groupServiceImpl.findById(Long.valueOf(groupId));
            List<Student> studentList = studentServiceImpl.findAll();
            List<Student> studentListByGroup = studentServiceImpl.getStudentsByGroupId(Long.valueOf(groupId));
            if (studentList != null && !studentList.isEmpty()) {
                for (Student student : studentList) {
                    if (studentListByGroup.contains(student)) {
                        student.setIsInGroup(1);
                    }
                }
            }
            req.setAttribute("studentList", studentList);
            req.setAttribute("groupItem", groupItem);
            getServletContext().getRequestDispatcher("/students/choose_student.jsp").forward(req, resp);
        } catch (NullPointerException | NumberFormatException | GroupDataAccessException e) {
            LOGGER.error(e.getMessage());
            getServletContext().getRequestDispatcher("/error.jsp").forward(req, resp);
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String groupId = req.getParameter("groupId");
        String studentId = req.getParameter("studentId");
        String boundStatus = req.getParameter("status");

        try {
            if (Boolean.valueOf(boundStatus)){
                if(!studentServiceImpl.attachToGroup(Long.valueOf(studentId), Long.valueOf(groupId))){
                    resp.setStatus(400);
                }
            } else {
                if(!studentServiceImpl.detachFromGroup(Long.valueOf(studentId), Long.valueOf(groupId))){
                    resp.setStatus(400);
                }
            }
        } catch (NullPointerException | NumberFormatException | StudentDataAccessException e) {
            LOGGER.error(e.getMessage());
            resp.setStatus(400);
        }
    }
}