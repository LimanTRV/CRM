package ru.innopolis.crm.main.models.dao;

import java.util.List;

import ru.innopolis.crm.main.models.dao.exceptions.DaoException;
import ru.innopolis.crm.main.models.entity.SettingsOption;

/**
 *
 */
public interface SettingsOptionDao
{

	List<SettingsOption> getAll() throws DaoException;

	SettingsOption getById(long id) throws DaoException;

	SettingsOption getByCode(String code) throws DaoException;

	int insert(SettingsOption option) throws DaoException;

	int update(SettingsOption option) throws DaoException;

	int delete(SettingsOption option) throws DaoException;
}
