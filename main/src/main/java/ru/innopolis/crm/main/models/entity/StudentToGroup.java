package ru.innopolis.crm.main.models.entity;

/**
 * Created by Roman Taranov on 20.04.2017.
 */
@Deprecated
public class StudentToGroup
{
	private long id;
	private Student student;
	private StudyGroup group;

	public StudentToGroup()
	{
	}

	public StudentToGroup(long id, Student student, StudyGroup group)
	{
		this.id = id;
		this.student = student;
		this.group = group;
	}

	public long getId()
	{
		return id;
	}

	public void setId(long id)
	{
		this.id = id;
	}

	public Student getStudent()
	{
		return student;
	}

	public void setStudent(Student student)
	{
		this.student = student;
	}

	public StudyGroup getGroup()
	{
		return group;
	}

	public void setGroup(StudyGroup group)
	{
		this.group = group;
	}
}
