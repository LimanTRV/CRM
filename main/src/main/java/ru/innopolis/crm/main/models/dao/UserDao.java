package ru.innopolis.crm.main.models.dao;


import ru.innopolis.crm.main.models.entity.User;
import ru.innopolis.crm.main.models.entity.UserAuthority;

import java.util.List;

/**
 * DAO interface to insert <code>User</code> objects into DB or get <code>User</code> objects from DB
 *
 * @author Artem Panasyuk, Farit Fattakhov, Roman Taranov
 */
public interface UserDao
{
	/**
	 * Creates an <code>User</code> object from DB by id
	 *
	 * @param userId - identificator of object in DB table
	 * @return a new <code>User</code> object
	 */
	User getById(Long userId);

	/**
	 * Creates an <code>User</code> object from DB by id
	 *
	 * @param userLogin - identificator of object in DB table
	 * @return a new <code>User</code> object
	 */
	User getByLogin(String userLogin);

	/**
	 * Creates a list of <code>User</code> objects from DB
	 *
	 * @return a new list of <code>User</code> objects
	 */
	List<User> getAll();

	/**
	 * Insert object <code>User</code> to DB
	 *
	 * @param user <code>User</code> entity
	 */
	void create(User user);

	/**
	 * Update object <code>User</code> in DB
	 *
	 * @param user <code>User</code> entity
	 */
	void update(User user);

	/**
	 * Update object <code>User</code> in DB
	 * sets is_deleted column value to true
	 *
	 * @param user <code>User</code> entity
	 */
	void delete(User user);

	long ifStudent(long userId);

	List<UserAuthority> getAuthorities(User user);
}
