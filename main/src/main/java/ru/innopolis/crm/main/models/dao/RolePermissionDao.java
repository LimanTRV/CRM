package ru.innopolis.crm.main.models.dao;

import ru.innopolis.crm.main.models.entity.Operation;
import ru.innopolis.crm.main.models.entity.Resource;

import java.util.List;
import java.util.Map;

/**
 * DAO interface to access role_permission table
 *
 * @author Farit Fattakhov, Roman Taranov
 */
public interface RolePermissionDao
{
	Map<Resource, List<Operation>> getByRoleId(Long roleId);

	/**
	 * Метод для записи пермишинов в БД
	 *
	 * @param roleId    роль
	 * @param resources мап ресурс - список операций
	 */
	boolean savePermissoin(long roleId, Map<Long, List<Long>> resources);

	/**
	 * Метод для вставки данных в БД
	 *
	 * @param roleId      роль
	 * @param resourceId  ресурс
	 * @param operationId опреация
	 * @return кол-во вставленных записей
	 */
	int insert(long roleId, long resourceId, long operationId);

	/**
	 * Метод для удаления пермишинов по роль ид
	 *
	 * @param roleId ид
	 * @return кол-во обновленных записей
	 */
	int deleteByRoleId(long roleId);
}
