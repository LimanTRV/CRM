package ru.innopolis.crm.main.web;


import org.apache.log4j.Logger;
import ru.innopolis.crm.main.models.dao.exceptions.GroupDataAccessException;
import ru.innopolis.crm.main.models.dao.exceptions.StudentDataAccessException;
import ru.innopolis.crm.main.models.entity.Student;
import ru.innopolis.crm.main.models.entity.StudyGroup;
import ru.innopolis.crm.main.services.impl.GroupServiceImpl;
import ru.innopolis.crm.main.services.impl.StudentServiceImpl;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * Controller of students list pages
 */
public class StudentsServlet extends HttpServlet {
    private static Logger LOGGER = Logger.getLogger(StudentsServlet.class);

    private static StudentServiceImpl studentServiceImpl = new StudentServiceImpl();
    private static GroupServiceImpl groupServiceImpl = new GroupServiceImpl();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            String groupId = req.getParameter("group");
            StudyGroup groupItem = null;
            List<Student> studentList = null;
            if (groupId != null){
                try {
                    groupItem = groupServiceImpl.findById(Long.valueOf(groupId));
                    if (groupItem == null){
                        LOGGER.warn("Requested group doesn't exist");
                        resp.sendRedirect("../students/");
                        return;
                    }
                    req.setAttribute("groupItem", groupItem);
                    studentList = studentServiceImpl.getStudentsByGroupId(Long.valueOf(groupId));
                } catch (NullPointerException | NumberFormatException | GroupDataAccessException e) {
                    LOGGER.error(e.getMessage());
                    resp.sendRedirect("../students/");
                    return;
                }
            } else {
                studentList = studentServiceImpl.findAll();
            }
            req.setAttribute("studentList", studentList);
            getServletContext().getRequestDispatcher("/students/students.jsp").forward(req, resp);
        } catch (NullPointerException | StudentDataAccessException e) {
            LOGGER.error(e.getMessage());
            getServletContext().getRequestDispatcher("/error.jsp").forward(req, resp);
        }
    }
}