package ru.innopolis.crm.main.models.entity;

/**
 * Entity for database table main.resource
 */
public class Resource
{
	private Long id;
	private String code;
	private String name;
	private String url;
	private boolean isDeleted;

	public Resource(Long id, String code, String name, String url, boolean isDeleted)
	{
		this.id = id;
		this.code = code;
		this.name = name;
		this.url = url;
		this.isDeleted = isDeleted;
	}

	public Long getId()
	{
		return id;
	}

	public void setId(Long id)
	{
		this.id = id;
	}

	public String getCode()
	{
		return code;
	}

	public void setCode(String code)
	{
		this.code = code;
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public String getUrl()
	{
		return url;
	}

	public void setUrl(String url)
	{
		this.url = url;
	}

	public boolean isDeleted()
	{
		return isDeleted;
	}

	public void setDeleted(boolean deleted)
	{
		isDeleted = deleted;
	}

	@Override
	public boolean equals(Object o)
	{
		if (this == o)
		{
			return true;
		}
		if (o == null || getClass() != o.getClass())
		{
			return false;
		}

		Resource resource = (Resource) o;

		if (id != null ? !id.equals(resource.id) : resource.id != null)
		{
			return false;
		}
		if (code != null ? !code.equals(resource.code) : resource.code != null)
		{
			return false;
		}
		if (name != null ? !name.equals(resource.name) : resource.name != null)
		{
			return false;
		}
		if (url != null ? !url.equals(resource.url) : resource.url != null)
		{
			return false;
		}

		return true;
	}

	@Override
	public int hashCode()
	{
		int result = id != null ? id.hashCode() : 0;
		result = 31 * result + (code != null ? code.hashCode() : 0);
		result = 31 * result + (name != null ? name.hashCode() : 0);
		result = 31 * result + (url != null ? url.hashCode() : 0);
		return result;
	}
}
