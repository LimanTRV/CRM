package ru.innopolis.crm.main.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.innopolis.crm.main.connection.DataSourceFactory;
import ru.innopolis.crm.main.models.dao.GroupDao;
import ru.innopolis.crm.main.models.dao.impl.GroupDaoImpl;
import ru.innopolis.crm.main.models.entity.StudyGroup;
import ru.innopolis.crm.main.services.GroupService;

import javax.sql.DataSource;
import java.util.List;

@Service
public class GroupServiceImpl implements GroupService
{

	private GroupDao studyGroupDao;

	@Autowired
	public void setStudyGroupDao(GroupDao studyGroupDao)
	{
		this.studyGroupDao = studyGroupDao;
	}

	@Override
	public List<StudyGroup> findAll()
	{
		return studyGroupDao.findAll();
	}

	@Override
	public StudyGroup findById(long id)
	{
		return studyGroupDao.findById(id);
	}

	@Override
	public int update(StudyGroup studyGroup)
	{
		return studyGroupDao.update(studyGroup);
	}

	@Override
	public int insert(StudyGroup studyGroup)
	{
		return studyGroupDao.insert(studyGroup);
	}

	@Override
	public int delete(long id)
	{
		return studyGroupDao.delete(id);
	}

	@Override
	public StudyGroup getFirst()
	{
		return studyGroupDao.getFirst();
	}
}
