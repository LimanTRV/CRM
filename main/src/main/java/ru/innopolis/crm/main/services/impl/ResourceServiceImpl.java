package ru.innopolis.crm.main.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Service;
import ru.innopolis.crm.main.connection.DataSourceFactory;
import ru.innopolis.crm.main.models.dao.ResourceDao;
import ru.innopolis.crm.main.models.dao.impl.ResourceDaoImpl;
import ru.innopolis.crm.main.models.entity.Resource;
import ru.innopolis.crm.main.services.ResourceService;

import javax.sql.DataSource;
import java.util.List;

/**
 * Created by Roman Taranov on 29.04.2017.
 */
@Service
public class ResourceServiceImpl implements ResourceService
{

	private ResourceDao resourceDao;

	@Autowired
	public void setResourceDao(ResourceDao resourceDao)
	{
		this.resourceDao = resourceDao;
	}

	@Override
	public Resource getById(Long resourceId)
	{
		return resourceDao.getById(resourceId);
	}

	@Override
	public List<Resource> getAll()
	{
		return resourceDao.getAll();
	}

	@Override
	public void deleteById(Long resourceId)
	{
		resourceDao.delete(resourceDao.getById(resourceId));
	}

	@Override
	public void save(Resource resource)
	{
		if (resource == null)
		{
			return;
		}

		Long resourceId = resource.getId();

		if ((resourceId != null) && (resourceId > 0))
		{
			resourceDao.update(resource);
		}
		else
		{
			resourceDao.create(resource);
		}
	}
}
