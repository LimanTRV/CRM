package ru.innopolis.crm.main.services;

import ru.innopolis.crm.main.models.entity.Role;

import java.util.List;

/**
 * Service for working with entity <code>Role</code>
 */
public interface RoleService
{
	/**
	 * Get a <code>Role</code> object from DB by id
	 *
	 * @param roleId
	 * @return <code>Role</code> entity
	 */
	Role getById(Long roleId);

	/**
	 * Get a <code>Role</code> object from DB by code
	 *
	 * @param code
	 * @return <code>Role</code> entity
	 */
	Role getByCode(String code);

	/**
	 * Delete a <code>Role</code> object from DB by id
	 *
	 * @param roleId
	 */
	void deleteById(Long roleId);

	/**
	 * Creates a list of <code>Role</code> objects from DB
	 *
	 * @return a new list of <code>Role</code> objects
	 */
	List<Role> getAll();

	/**
	 * Create or update an <code>Role</code> object to DB
	 *
	 * @param role <code>Role</code> entity
	 */
	long save(Role role);

	/**
	 * Delete a <code>Role</code> object from DB by id
	 *
	 * @param roleId id
	 */
	int remove(long roleId);

	/**
	 * Метод возвращает количество пользоватлей по ид роли
	 *
	 * @param roleId ид роли
	 * @return количество пользователей
	 */
	int countUsersByRoleId(long roleId);
}
