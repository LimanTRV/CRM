package ru.innopolis.crm.main.models.entity;

/**
 * Entity for database table main.role
 */
public class Role
{
	private Long id;
	private String name;
	private String code;
	private boolean isDeleted;

	public Role(Long id, String code, String name, boolean isDeleted)
	{
		this.id = id;
		this.name = name;
		this.code = code;
		this.isDeleted = isDeleted;
	}

	public Long getId()
	{
		return id;
	}

	public void setId(Long id)
	{
		this.id = id;
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public String getCode()
	{
		return code;
	}

	public void setCode(String code)
	{
		this.code = code;
	}

	public boolean isDeleted()
	{
		return isDeleted;
	}

	public void setDeleted(boolean deleted)
	{
		isDeleted = deleted;
	}

	@Override
	public boolean equals(Object o)
	{
		if (this == o)
		{
			return true;
		}
		if (o == null || getClass() != o.getClass())
		{
			return false;
		}

		Role role = (Role) o;

		if (id != null ? !id.equals(role.id) : role.id != null)
		{
			return false;
		}
		if (name != null ? !name.equals(role.name) : role.name != null)
		{
			return false;
		}
		if (code != null ? !code.equals(role.code) : role.code != null)
		{
			return false;
		}

		return true;
	}

	@Override
	public int hashCode()
	{
		int result = id != null ? id.hashCode() : 0;
		result = 31 * result + (name != null ? name.hashCode() : 0);
		result = 31 * result + (code != null ? code.hashCode() : 0);
		return result;
	}

	@Override
	public String toString()
	{
		return "Role{" +
				"id=" + id +
				", name='" + name + '\'' +
				", code='" + code + '\'' +
				", isDeleted=" + isDeleted +
				'}';
	}

}