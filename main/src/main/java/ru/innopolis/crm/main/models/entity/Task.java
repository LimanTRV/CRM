package ru.innopolis.crm.main.models.entity;


import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

public class Task
{

	private long id;
	private String title;
	private String description;
	private String taskStandardUrl;
	private boolean isLab;
	private Timestamp created;
	private StudyGroup group;
	private Timestamp endDate;
	private List<Criterion> criteria;

	public String getTaskStandardUrl()
	{
		return taskStandardUrl;
	}

	public void setTaskStandardUrl(String taskStandardUrl)
	{
		this.taskStandardUrl = taskStandardUrl;
	}

	public long getId()
	{
		return id;
	}

	public void setId(long value)
	{
		this.id = value;
	}

	public String getTitle()
	{
		return title;
	}

	public void setTitle(String value)
	{
		this.title = value;
	}

	public String getDescription()
	{
		return description;
	}

	public void setDescription(String value)
	{
		this.description = value;
	}

	public boolean isLab()
	{
		return isLab;
	}

	public void setLab(boolean value)
	{
		this.isLab = value;
	}

	public Timestamp getCreated()
	{
		return created;
	}

	public void setCreated(Timestamp value)
	{
		this.created = value;
	}

	public List<Criterion> getCriteria()
	{
		if (criteria == null)
		{
			criteria = new ArrayList<Criterion>();
		}
		return criteria;
	}

	public void setCriteria(List<Criterion> criterions)
	{
		this.criteria = criterions;
	}

	public Timestamp getEndDate()
	{
		return endDate;
	}

	public void setEndDate(Timestamp endDate)
	{
		this.endDate = endDate;
	}

	public StudyGroup getGroup()
	{
		return group;
	}

	public void setGroup(StudyGroup group)
	{
		this.group = group;
	}

	public Task()
	{
	}

	public Task(String title, String description, boolean isLab)
	{
		this.title = title;
		this.description = description;
		this.isLab = isLab;
	}
}
