package ru.innopolis.crm.main.services.impl;

import ru.innopolis.crm.main.connection.DataSourceFactory;
import ru.innopolis.crm.main.models.dao.StudentGroupDao;
import ru.innopolis.crm.main.models.dao.impl.StudentGroupDaoImpl;
import ru.innopolis.crm.main.services.StudentGroupService;

import javax.sql.DataSource;
import java.util.List;

@Deprecated
public class StudentGroupServiceImpl implements StudentGroupService
{
	private static DataSource dataSource = DataSourceFactory.getDataSource();

	private StudentGroupDao studyGroupDao = new StudentGroupDaoImpl(dataSource);

	@Override
	public int delete(long groupId, long studentId)
	{
		return studyGroupDao.delete(groupId, studentId);
	}

	@Override
	public int deleteThatNotActual(long groupId, List<Long> studentIds)
	{
		int result = 0;
		if (studentIds != null && !studentIds.isEmpty())
		{
			for (Long studentId : studentIds)
			{
				result += studyGroupDao.deleteThatNotActual(groupId, studentId);
			}
		}
		return result;
	}

	@Override
	public int insert(long groupId, List<Long> studentIds)
	{
		int result = 0;
		if (studentIds != null && !studentIds.isEmpty())
		{
			for (Long studentId : studentIds)
			{
				result += studyGroupDao.insert(groupId, studentId);
			}
		}
		return result;
	}

}
