package ru.innopolis.crm.main.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import ru.innopolis.crm.main.models.entity.User;
import ru.innopolis.crm.main.services.UserService;
import ru.innopolis.crm.main.sessionbean.UserBean;

import javax.servlet.http.HttpSession;

@Controller
@PreAuthorize(value = "isAuthenticated()")
@RequestMapping(value = "/pwd")
public class PasswordController
{
	private static final String EDIT = "password";
	private static final String ENTITY_PARAM = "user";

	private UserService userService;
	private UserBean userBean;

	@RequestMapping(method = RequestMethod.GET)
	public ModelAndView EditForm(HttpSession session)
	{

		ModelAndView mav = new ModelAndView();
		mav.addObject(ENTITY_PARAM, userService.getById(userBean.getUserId()));
		mav.setViewName(EDIT);

		return mav;
	}

	@RequestMapping(method = RequestMethod.POST)
	public ModelAndView SaveUser(@RequestParam(value = "old_password") String old_password,
								 @RequestParam(value = "new_password") String new_password)
	{
		ModelAndView mav = new ModelAndView();

		User user;
		if ((user = userService.auth(userBean.getUserName(), old_password)) != null)
		{
			user.setPassword(new_password);
			userService.save(user);
			mav.setViewName("redirect:/");
		}
		else
		{
			mav.addObject("errorText",
					"<h1>Authentification failed. <a href=\"/crm/pwd\"> Click here to try again</a></h1>");
			mav.setViewName("error");
		}

		return mav;
	}

	@Autowired
	public void setUserService(UserService userService)
	{
		this.userService = userService;
	}

	@Autowired
	public void setUserBean(UserBean userBean)
	{
		this.userBean = userBean;
	}
}
