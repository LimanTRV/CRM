package ru.innopolis.crm.main.models.dao.impl;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import ru.innopolis.crm.main.models.dao.SettingsOptionDao;
import ru.innopolis.crm.main.models.dao.exceptions.DaoException;
import ru.innopolis.crm.main.models.entity.SettingsOption;

/**
 *
 */
@SuppressWarnings("Duplicates")
@Repository
public class SettingsOptionDaoImpl implements SettingsOptionDao
{

	private static final Logger LOGGER = Logger.getLogger(SettingsOptionDaoImpl.class);
	private static final String GET_BY_ID_QUERY = "SELECT * FROM main.settings WHERE id=?";
	private static final String GET_BY_CODE_QUERY = "SELECT * FROM main.settings WHERE code=?";
	private static final String SELECT_ALL_QUERY = "SELECT * FROM main.settings";
	private static final String UPDATE_BY_ID = "UPDATE main.settings SET code = ?, set_value = ?, is_actual = ? WHERE id = ?";
	private static final String DELETE_BY_ID = "DELETE FROM main.settings WHERE id = ?";

	private DataSource dataSource;

	@Override
	public List<SettingsOption> getAll() throws DaoException
	{
		List<SettingsOption> settings = new ArrayList<>();
		try
		{
			Connection connection = dataSource.getConnection();
			Statement statement = connection.createStatement();
			ResultSet resultSet = statement.executeQuery(SELECT_ALL_QUERY);
			while (resultSet.next())
			{
				settings.add(newInstance(resultSet));
			}
		}
		catch (SQLException e)
		{
			LOGGER.error("An SQL error occurred :" + e.getMessage() + "error code:" + e.getErrorCode());
			throw new DaoException();
		}
		return settings;
	}

	@Override
	public SettingsOption getById(long id) throws DaoException
	{
		try
		{
			Connection connection = dataSource.getConnection();
			PreparedStatement statement = connection.prepareStatement(GET_BY_ID_QUERY);
			statement.setLong(1, id);
			ResultSet resultSet = statement.executeQuery();
			if (resultSet.next())
			{
				return newInstance(resultSet);
			}
		}
		catch (SQLException e)
		{
			LOGGER.error("An SQL error occurred :" + e.getMessage() + "error code:" + e.getErrorCode());
			throw new DaoException();
		}
		return null;
	}

	@Override
	public SettingsOption getByCode(String code) throws DaoException
	{
		try
		{
			Connection connection = dataSource.getConnection();
			PreparedStatement statement = connection.prepareStatement(GET_BY_CODE_QUERY);
			statement.setString(1, code);
			ResultSet resultSet = statement.executeQuery();
			if (resultSet.next())
			{
				return newInstance(resultSet);
			}
		}
		catch (SQLException e)
		{
			LOGGER.error("An SQL error occurred :" + e.getMessage() + "error code:" + e.getErrorCode());
			throw new DaoException();
		}
		return null;
	}

	@Override
	public int insert(SettingsOption option) throws DaoException
	{
		int result;
		try
		{
			Connection connection = dataSource.getConnection();
			PreparedStatement preparedStatement = null;

			preparedStatement = connection.prepareStatement("INSERT INTO main.settings (code, set_value, is_actual) VALUES (?, ?, ?)");

			preparedStatement.setString(1, option.getCode());
			preparedStatement.setString(2, option.getValue());
			preparedStatement.setBoolean(3, option.isActual());

			result = preparedStatement.executeUpdate();

		}
		catch (SQLException e)
		{
			LOGGER.error("Add <download> in DB failed", e);
			throw new DaoException();
		}

		return result;
	}

	@Override
	public int update(SettingsOption option) throws DaoException
	{
		int updated;
		try
		{
			Connection connection = dataSource.getConnection();

			PreparedStatement preparedStatement =
					connection.prepareStatement(UPDATE_BY_ID);

			preparedStatement.setString(1, option.getCode());
			preparedStatement.setString(2, option.getValue());
			preparedStatement.setBoolean(3, option.isActual());
			preparedStatement.setLong(4, option.getId());

			updated = preparedStatement.executeUpdate();

		}
		catch (SQLException e)
		{
			LOGGER.error("Update <download> failed", e);
			throw new DaoException();
		}

		return updated;
	}

	@Override
	public int delete(SettingsOption option) throws DaoException
	{
		int deleted;

		try
		{
			Connection connection = dataSource.getConnection();
			PreparedStatement statement = connection.prepareStatement(DELETE_BY_ID);
			statement.setLong(1, option.getId());

			deleted = statement.executeUpdate();

		}
		catch (SQLException e)
		{
			LOGGER.error("Delete <download> failed", e);
			throw new DaoException();
		}

		return deleted;
	}

	@Autowired
	public void setDataSource(DataSource dataSource)
	{
		this.dataSource = dataSource;
	}

	private SettingsOption newInstance(ResultSet resultSet) throws SQLException
	{
		SettingsOption option = new SettingsOption();
		option.setId(resultSet.getLong("id"));
		option.setCode(resultSet.getString("code"));
		option.setValue(resultSet.getString("set_value"));
		option.setActual(resultSet.getBoolean("is_actual"));
		return option;
	}
}
