package ru.innopolis.crm.main.models.entity;

import java.util.Set;

/**
 * Entity for database table main.crm_user
 */
public class User
{
	private Long id;
	private String login;
	private String password;
	private String firstName;
	private String lastName;
	private String email;
	private boolean isActive;
	private boolean isDeleted;
	private Set<Role> roles;

	public User(Long id, String login, String password, String firstName, String lastName, String email, boolean isActive, boolean isDeleted)
	{
		this.id = id;
		this.login = login;
		this.password = password;
		this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
		this.isActive = isActive;
		this.isDeleted = isDeleted;
	}

	@Deprecated
	public User()
	{
	}

	public Long getId()
	{
		return id;
	}

	public void setId(Long id)
	{
		this.id = id;
	}

	public String getLogin()
	{
		return login;
	}

	public void setLogin(String login)
	{
		this.login = login;
	}

	public String getPassword()
	{
		return password;
	}

	public void setPassword(String password)
	{
		this.password = password;
	}

	public String getFirstName()
	{
		return firstName;
	}

	public void setFirstName(String firstName)
	{
		this.firstName = firstName;
	}

	public String getLastName()
	{
		return lastName;
	}

	public void setLastName(String lastName)
	{
		this.lastName = lastName;
	}

	public String getEmail()
	{
		return email;
	}

	public void setEmail(String email)
	{
		this.email = email;
	}

	public boolean isActive()
	{
		return isActive;
	}

	public void setActive(boolean active)
	{
		isActive = active;
	}

	public boolean isDeleted()
	{
		return isDeleted;
	}

	public void setDeleted(boolean deleted)
	{
		isDeleted = deleted;
	}

	@Override
	public boolean equals(Object o)
	{
		if (this == o)
		{
			return true;
		}
		if (o == null || getClass() != o.getClass())
		{
			return false;
		}

		User user = (User) o;

		if (id != null ? !id.equals(user.id) : user.id != null)
		{
			return false;
		}
		if (login != null ? !login.equals(user.login) : user.login != null)
		{
			return false;
		}
		if (password != null ? !password.equals(user.password) : user.password != null)
		{
			return false;
		}
		if (firstName != null ? !firstName.equals(user.firstName) : user.firstName != null)
		{
			return false;
		}
		if (lastName != null ? !lastName.equals(user.lastName) : user.lastName != null)
		{
			return false;
		}
		if (email != null ? !email.equals(user.email) : user.email != null)
		{
			return false;
		}

		return true;
	}

	@Override
	public int hashCode()
	{
		int result = id != null ? id.hashCode() : 0;
		result = 31 * result + (login != null ? login.hashCode() : 0);
		result = 31 * result + (password != null ? password.hashCode() : 0);
		result = 31 * result + (firstName != null ? firstName.hashCode() : 0);
		result = 31 * result + (lastName != null ? lastName.hashCode() : 0);
		result = 31 * result + (email != null ? email.hashCode() : 0);
		return result;
	}

	public Set<Role> getRoles()
	{
		return roles;
	}

	public void setRoles(Set<Role> roles)
	{
		this.roles = roles;
	}

	@Override
	public String toString()
	{
		return "User{" +
				"id=" + id +
				", login='" + login + '\'' +
				", firstName='" + firstName + '\'' +
				", lastName='" + lastName + '\'' +
				", email='" + email + '\'' +
				'}';
	}
}
