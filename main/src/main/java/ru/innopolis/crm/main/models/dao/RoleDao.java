package ru.innopolis.crm.main.models.dao;


import ru.innopolis.crm.main.models.entity.Role;

import java.util.List;


/**
 * DAO interface to insert <code>Role</code> objects into DB or get <code>Role</code> objects from DB
 *
 * @author Artem Panasyuk, Farit Fattakhov, Roman Taranov
 */
public interface RoleDao
{

	/**
	 * Creates a <code>Role</code> object from DB by id
	 *
	 * @param roleId - identificator of object in DB table
	 * @return a new default <code>Role</code> object
	 */
	Role getById(Long roleId);

	/**
	 * Creates a <code>Role</code> object from DB by code
	 *
	 * @param code - code of object in DB table
	 * @return a new default <code>Role</code> object
	 */
	Role getByCode(String code);

	/**
	 * Creates a list of <code>Role</code> objects from DB by User Id
	 *
	 * @param userId - code of object in DB table
	 * @return a new list of <code>Role</code> objects
	 */
	List<Role> getByUserId(Long userId);

	/**
	 * Creates a list of <code>Role</code> objects from DB
	 *
	 * @return a new list of <code>Role</code> objects
	 */
	List<Role> getAll();

	/**
	 * Insert object <code>Role</code> to DB
	 *
	 * @param role <code>Role</code> entity
	 */
	long create(Role role);

	/**
	 * Update object <code>Role</code> in DB
	 *
	 * @param role <code>Role</code> entity
	 */
	long update(Role role);

	/**
	 * Update object <code>Role</code> in DB
	 * sets is_deleted column value to true
	 *
	 * @param role <code>Role</code> entity
	 */
	void delete(Role role);

	/**
	 * Update object <code>Role</code> in DB
	 * sets is_deleted column value to true
	 *
	 * @param roleId <code>roleId</code> entity
	 */
	int remove(long roleId);

	/**
	 * Метод возвращает количество пользоватлей по ид роли
	 *
	 * @param roleId ид роли
	 * @return количество пользователей
	 */
	int countUsersByRoleId(long roleId);
}
