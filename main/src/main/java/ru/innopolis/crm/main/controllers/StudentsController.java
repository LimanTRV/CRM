package ru.innopolis.crm.main.controllers;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import ru.innopolis.crm.main.models.dao.exceptions.GroupDataAccessException;
import ru.innopolis.crm.main.models.dao.exceptions.StudentDataAccessException;
import ru.innopolis.crm.main.models.entity.Student;
import ru.innopolis.crm.main.models.entity.StudyGroup;
import ru.innopolis.crm.main.services.GroupService;
import ru.innopolis.crm.main.services.StudentService;

import java.util.List;

/**
 * Controller of students list pages
 */
@Controller
public class StudentsController
{
	private static Logger LOGGER = Logger.getLogger(StudentsController.class);
	private GroupService groupService;
	private StudentService studentService;

	@Autowired
	public void setGroupService(GroupService groupService)
	{
		this.groupService = groupService;
	}

	@Autowired
	public void setStudentService(StudentService studentService)
	{
		this.studentService = studentService;
	}

	@GetMapping("/students/*")
	@PreAuthorize(value = "hasAnyRole('S_V', 'S_E')")
	public String showStudents(@RequestParam("group") Long groupId, Model model)
	{
		try
		{
			StudyGroup groupItem = null;
			List<Student> studentList = null;
			if (groupId != null)
			{
				try
				{
					groupItem = groupService.findById(Long.valueOf(groupId));
					if (groupItem == null)
					{
						LOGGER.warn("Requested group doesn't exist");
						return "redirect:/students/";
					}
					model.addAttribute("groupItem", groupItem);
					studentList = studentService.getStudentsByGroupId(Long.valueOf(groupId));
				}
				catch (NullPointerException | NumberFormatException | GroupDataAccessException e)
				{
					LOGGER.error(e.getMessage());
					return "redirect:/students/";
				}
			}
			else
			{
				studentList = studentService.findAll();
			}
			model.addAttribute("studentList", studentList);
			return "/WEB-INF/pages/groups/students";
		}
		catch (NullPointerException | StudentDataAccessException e)
		{
			LOGGER.error(e.getMessage());
			return "redirect:/error.jsp";
		}
	}

	@GetMapping("/pick_students/")
	@PreAuthorize(value = "hasRole('S_E')")
	public String pickStudents(@RequestParam("group") Long groupId, Model model)
	{
		StudyGroup groupItem;
		try
		{
			groupItem = groupService.findById(groupId);
			List<Student> studentList = studentService.findAll();
			List<Student> studentListByGroup = studentService.getStudentsByGroupId(groupId);
			if (studentList != null && !studentList.isEmpty())
			{
				for (Student student : studentList)
				{
					if (studentListByGroup.contains(student))
					{
						student.setIsInGroup(1);
					}
				}
			}
			model.addAttribute("studentList", studentList);
			model.addAttribute("groupItem", groupItem);
			return "/WEB-INF/pages/groups/pick_students";
		}
		catch (NullPointerException | GroupDataAccessException e)
		{
			LOGGER.error(e.getMessage());
			return "redirect:/error.jsp";
		}
	}

	@PostMapping("/pick_students/")
	@PreAuthorize(value = "hasRole('S_E')")
	public ResponseEntity<Void> boundStudents(@RequestParam("groupId") String groupId, @RequestParam("studentId") String studentId, @RequestParam("status") String boundStatus)
	{
		try
		{
			if (Boolean.valueOf(boundStatus))
			{
				if (studentService.attachToGroup(Long.valueOf(studentId), Long.valueOf(groupId)))
				{
					return new ResponseEntity<Void>(HttpStatus.OK);
				}
			}
			else
			{
				if (studentService.detachFromGroup(Long.valueOf(studentId), Long.valueOf(groupId)))
				{
					return new ResponseEntity<Void>(HttpStatus.OK);
				}
			}
		}
		catch (NullPointerException | StudentDataAccessException | NumberFormatException e)
		{
			LOGGER.error(e.getMessage());
		}
		return new ResponseEntity<Void>(HttpStatus.INTERNAL_SERVER_ERROR);
	}


}
