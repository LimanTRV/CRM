package ru.innopolis.crm.main.models.entity;

import java.util.Objects;
import java.util.Set;

/**
 * Represents a group of students entity.
 */
public class StudyGroup
{

	private long id;
	private String name;
	private String description;
	private String courseType;
	private Set<Task> tasks;
	private boolean isDeleted = false;

	public StudyGroup()
	{
	}

	public StudyGroup(long id, String name, String description, String courseType)
	{
		this.id = id;
		this.name = name;
		this.description = description;
		this.courseType = courseType;
	}

	public long getId()
	{
		return id;
	}

	public void setId(long id)
	{
		this.id = id;
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public String getDescription()
	{
		return description;
	}

	public void setDescription(String description)
	{
		this.description = description;
	}

	public String getCourseType()
	{
		return courseType;
	}

	public void setCourseType(String courseType)
	{
		this.courseType = courseType;
	}

	public boolean isDeleted()
	{
		return isDeleted;
	}

	public void setDeleted(boolean deleted)
	{
		isDeleted = deleted;
	}

	@Override
	public boolean equals(Object o)
	{
		if (this == o)
		{
			return true;
		}
		if (o == null || getClass() != o.getClass())
		{
			return false;
		}
		StudyGroup that = (StudyGroup) o;
		return id == that.id &&
				isDeleted == that.isDeleted &&
				Objects.equals(name, that.name) &&
				Objects.equals(description, that.description) &&
				Objects.equals(courseType, that.courseType);
	}

	@Override
	public int hashCode()
	{
		return Objects.hash(id, name, description, courseType, isDeleted);
	}

	@Override
	public String toString()
	{
		return "StudyGroup{" +
				"id=" + id +
				", name='" + name + '\'' +
				", description='" + description + '\'' +
				", courseType='" + courseType + '\'' +
				", isDeleted=" + isDeleted +
				'}';
	}

	public Set<Task> getTasks()
	{
		return tasks;
	}

	public void setTasks(Set<Task> tasks)
	{
		this.tasks = tasks;
	}
}
