package ru.innopolis.crm.main.sessionbean;

import org.springframework.stereotype.Component;
import ru.innopolis.crm.main.models.entity.UserAuthority;

import java.util.List;

/**
 * Created by Roman Taranov on 06.05.2017.
 */
@Component
public class UserBean
{
	private String userName;
	private long userId;
	private long studentId;
	private List<UserAuthority> authorities;

	public long getStudentId()
	{
		return studentId;
	}

	public void setStudentId(long studentId)
	{
		this.studentId = studentId;
	}

	public List<UserAuthority> getAuthorities()
	{
		return authorities;
	}

	public void setAuthorities(List<UserAuthority> authorities)
	{
		this.authorities = authorities;
	}

	public long getUserId()
	{
		return userId;
	}

	public void setUserId(long userId)
	{
		this.userId = userId;
	}

	public String getUserName()
	{
		return userName;
	}

	public void setUserName(String userName)
	{
		this.userName = userName;
	}
}
