package ru.innopolis.crm.main.models.dao;


import ru.innopolis.crm.main.models.entity.Resource;

import java.util.List;


/**
 * DAO interface to insert <code>Resource</code> objects into DB or get <code>Resource</code> objects from DB
 *
 * @author Artem Panasyuk, Farit Fattakhov, Roman Taranov
 */
public interface ResourceDao
{

	/**
	 * Creates a <code>Resource</code> object from DB by id
	 *
	 * @param resourceId - identificator of object in DB table
	 * @return a new default <code>Resource</code> object
	 */
	Resource getById(Long resourceId);

	/**
	 * Creates an <code>Resource</code> object from DB by code
	 *
	 * @param resourceCode - code of object in DB table
	 * @return a new default <code>Resource</code> object
	 */
	Resource getByCode(String resourceCode);

	/**
	 * Creates a list of <code>Resource</code> objects from DB
	 *
	 * @return a new list of <code>Resource</code> objects
	 */
	List<Resource> getAll();

	/**
	 * Insert object <code>Resource</code> to DB
	 *
	 * @param resource <code>Resource</code> entity
	 */
	void create(Resource resource);

	/**
	 * Update object <code>Resource</code> in DB
	 *
	 * @param resource <code>Resource</code> entity
	 */
	void update(Resource resource);

	/**
	 * Update object <code>Resource</code> in DB
	 * sets is_deleted column value to true
	 *
	 * @param resource <code>Resource</code> entity
	 */
	void delete(Resource resource);
}
