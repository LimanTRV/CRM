package ru.innopolis.crm.main.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import ru.innopolis.crm.main.models.entity.Role;
import ru.innopolis.crm.main.services.OperationService;
import ru.innopolis.crm.main.services.PermissionService;
import ru.innopolis.crm.main.services.ResourceService;
import ru.innopolis.crm.main.services.RoleService;

import javax.servlet.http.HttpServletRequest;
import java.util.*;

@Controller
@PreAuthorize(value = "hasRole('M_E')")
@RequestMapping(value = "/RoleController")
public class RoleController
{
	private static final String INSERT_OR_EDIT = "admin/editRole";
	private static final String LIST_ROLE = "admin/listRole";
	private static final String LIST_PARAM = "roles";
	private static final String ENTITY_PARAM = "role";
	private static final String LIST_REDIRECT = "redirect:RoleController?action=listRole";
	private static final String LIST_PARAM_RES = "resources";
	private static final String LIST_PARAM_OP = "operations";
	private static final String LIST_PARAM_PERM = "permissions";

	private RoleService roleService;
	private OperationService operationService;
	private ResourceService resourceService;
	private PermissionService permissionService;

	@Autowired
	public void setRoleService(RoleService roleService)
	{
		this.roleService = roleService;
	}

	@Autowired
	public void setOperationService(OperationService operationService)
	{
		this.operationService = operationService;
	}

	@Autowired
	public void setResourceService(ResourceService resourceService)
	{
		this.resourceService = resourceService;
	}

	@Autowired
	public void setPermissionService(PermissionService permissionService)
	{
		this.permissionService = permissionService;
	}

	@RequestMapping(params = {"!action"}, method = RequestMethod.GET)
	public String DefaultForm(Model model)
	{
		model.addAttribute(LIST_PARAM, roleService.getAll());
		return LIST_ROLE;
	}

	@RequestMapping(params = {"action=listRole"}, method = RequestMethod.GET)
	public String ShowForm(Model model)
	{
		model.addAttribute(LIST_PARAM, roleService.getAll());
		return LIST_ROLE;
	}

	@RequestMapping(params = {"action=insert"}, method = RequestMethod.GET)
	public String AddRole(Model model)
	{
		model.addAttribute(LIST_PARAM_RES, resourceService.getAll());
		model.addAttribute(LIST_PARAM_OP, operationService.getAll());
		return INSERT_OR_EDIT;
	}

	@RequestMapping(params = {"action=edit"}, method = RequestMethod.GET)
	public String EditRole(Model model,
						   @RequestParam(value = "roleId") Long roleId)
	{
		model.addAttribute(ENTITY_PARAM, roleService.getById(roleId));
		model.addAttribute(LIST_PARAM_RES, resourceService.getAll());
		model.addAttribute(LIST_PARAM_OP, operationService.getAll());
		model.addAttribute(LIST_PARAM_PERM, permissionService.getByRoleId(roleId));
		return INSERT_OR_EDIT;
	}

	@RequestMapping(params = {"action=delete"}, method = RequestMethod.GET)
	public String DeleteRole(Model model,
							 @RequestParam(value = "roleId") Long roleId,
							 RedirectAttributes redirectAttributes)
	{
		int result = roleService.remove(roleId);

		if (result > 0)
		{
			redirectAttributes.addFlashAttribute("message_role", "Роль успешно удалена");
		}
		else
		{
			redirectAttributes.addFlashAttribute("message_role", "Роль используется. Удаление не возможно");
		}

		return LIST_REDIRECT;
	}

	@RequestMapping(method = RequestMethod.POST)
	public ModelAndView SaveRole(@RequestParam(value = "roleId", required = false) Long roleId,
								 @RequestParam(value = "code") String code,
								 @RequestParam(value = "name") String name,
								 @RequestParam(value = "isDeleted", required = false) Boolean isDeleted,
								 RedirectAttributes redirectAttributes,
								 HttpServletRequest req)
	{
		ModelAndView mav = new ModelAndView();

		if ((code == null) || (code.length() == 0) || ("A".equals(code)) || ("S".equals(code)) ||
				(name == null) || (name.length() == 0))
		{
			mav.setViewName("error");
			return mav;
		}

		if (roleId == null)
		{
			roleId = -1L;
		}

		if (isDeleted == null)
		{
			isDeleted = false;
		}

		Role role = null;
		if (roleId > 0)
		{
			role = roleService.getById(roleId);
			role.setCode(code);
			role.setName(name);
			role.setDeleted(isDeleted);
		}
		else
		{
			role = new Role(roleId, code, name, isDeleted);
		}

		long roleIdFromService = roleService.save(role);

		List<String> paramsList = Collections.list(req.getParameterNames());
		Map<Long, List<Long>> permissionIds = new HashMap<Long, List<Long>>();

		for (String parameter : paramsList)
		{
			if (parameter.matches("rl_.*"))
			{
				String[] ids = parameter.substring(3).split("_");

				Long resourceId = Long.parseLong(ids[0]);
				Long operationId = Long.parseLong(ids[1]);

				List<Long> operationIds;
				if (permissionIds.containsKey(resourceId))
				{
					operationIds = permissionIds.get(resourceId);
				}
				else
				{
					operationIds = new ArrayList<>();
					permissionIds.put(resourceId, operationIds);
				}
				operationIds.add(operationId);
			}
		}

		boolean result = permissionService.savePermissoin(roleIdFromService, permissionIds);

		if (result)
		{
			redirectAttributes.addFlashAttribute("message_role", "Роль успешно сохранена");
		}
		else
		{
			redirectAttributes.addFlashAttribute("message_role", "Ошибка сохранения");
		}

		mav.setViewName(LIST_REDIRECT);
		return mav;
	}
}