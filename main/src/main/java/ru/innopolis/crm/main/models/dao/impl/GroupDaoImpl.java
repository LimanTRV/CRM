package ru.innopolis.crm.main.models.dao.impl;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import ru.innopolis.crm.main.models.dao.GroupDao;
import ru.innopolis.crm.main.models.dao.exceptions.GroupDataAccessException;
import ru.innopolis.crm.main.models.entity.StudyGroup;

import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

@Repository
public class GroupDaoImpl implements GroupDao
{
	private static final Logger LOGGER = Logger.getLogger(GroupDaoImpl.class);
	private DataSource dataSource;

	@Autowired
	public void setDataSource(DataSource dataSource)
	{
		this.dataSource = dataSource;
	}

	@Override
	public List<StudyGroup> findAll()
	{
		List<StudyGroup> list = new ArrayList<>();
		Connection connection = null;
		Statement statement = null;
		try
		{
			connection = dataSource.getConnection();
			statement = connection.createStatement();
			ResultSet resultSet = statement.executeQuery("SELECT * FROM main.study_group WHERE not is_deleted");
			while (resultSet.next())
			{
				StudyGroup studyGroup = new StudyGroup(
						resultSet.getLong("id"),
						resultSet.getString("name"),
						resultSet.getString("description"),
						resultSet.getString("course_type")
				);
				list.add(studyGroup);
			}
		}
		catch (SQLException e)
		{
			LOGGER.error("An SQL error occurred :" + e.getMessage() + "error code:" + e.getErrorCode());
			throw new GroupDataAccessException();
		}
		finally
		{
			try
			{
				statement.close();
				connection.close();
			}
			catch (SQLException | NullPointerException e)
			{
				LOGGER.error("An error occurred during closing connection :" + e.getMessage());
			}
		}
		return list;
	}

	@Override
	public StudyGroup findById(long id)
	{
		Connection connection = null;
		Statement statement = null;
		try
		{
			connection = dataSource.getConnection();
			statement = connection.createStatement();
			ResultSet resultSet = statement.executeQuery("SELECT * FROM main.study_group WHERE id = " + id + " and not is_deleted");
			if (resultSet.next())
			{
				return new StudyGroup(
						resultSet.getLong("id"),
						resultSet.getString("name"),
						resultSet.getString("description"),
						resultSet.getString("course_type")
				);
			}
			resultSet.close();
			statement.close();
			connection.close();
		}
		catch (SQLException e)
		{
			LOGGER.error("An SQL error occurred :" + e.getMessage() + "error code:" + e.getErrorCode());
			throw new GroupDataAccessException();
		}
		finally
		{
			try
			{
				statement.close();
				connection.close();
			}
			catch (SQLException | NullPointerException e)
			{
				LOGGER.error("An error occurred during closing connection :" + e.getMessage());
			}
		}
		return null;
	}

	@Override
	public int delete(long id)
	{
		Connection connection = null;
		Statement statement = null;
		try
		{
			connection = dataSource.getConnection();
			statement = connection.createStatement();
			return statement.executeUpdate("UPDATE main.study_group SET is_deleted = true WHERE id = " + id);
		}
		catch (SQLException e)
		{
			LOGGER.error("An SQL error occurred :" + e.getMessage() + "error code:" + e.getErrorCode());
			throw new GroupDataAccessException();
		}
		finally
		{
			try
			{
				statement.close();
				connection.close();
			}
			catch (SQLException | NullPointerException e)
			{
				LOGGER.error("An error occurred during closing connection :" + e.getMessage());
			}
		}
	}

	@Override
	public int insert(StudyGroup studyGroup)
	{
		if (studyGroup == null)
		{
			return 0;
		}
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		try
		{
			connection = dataSource.getConnection();
			preparedStatement = connection.prepareStatement(
					"INSERT INTO main.study_group(name, description, course_type, is_deleted) VALUES (?, ?, ?, FALSE)");
			preparedStatement.setString(1, studyGroup.getName());
			preparedStatement.setString(2, studyGroup.getDescription());
			preparedStatement.setString(3, studyGroup.getCourseType());
			return preparedStatement.executeUpdate();
		}
		catch (SQLException e)
		{
			LOGGER.error("An SQL error occurred :" + e.getMessage() + "error code:" + e.getErrorCode());
			throw new GroupDataAccessException();
		}
		finally
		{
			try
			{
				preparedStatement.close();
				connection.close();
			}
			catch (SQLException | NullPointerException e)
			{
				LOGGER.error("An error occurred during closing connection :" + e.getMessage());
			}
		}
	}

	@Override
	public StudyGroup getFirst()
	{
		try
		{
			ResultSet set = dataSource.getConnection().createStatement().executeQuery("select * from main.study_group where is_deleted = false order by id desc limit 1");
			if (set.next())
			{
				return new StudyGroup(
						set.getLong("id"),
						set.getString("name"),
						set.getString("description"),
						set.getString("course_type")
				);
			}
		}
		catch (SQLException e)
		{
			LOGGER.error(e.getMessage());
		}
		return null;
	}

	@Override
	public int update(StudyGroup studyGroup)
	{
		if (studyGroup == null)
		{
			return 0;
		}
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		try
		{
			connection = dataSource.getConnection();
			preparedStatement = connection.prepareStatement(
					"UPDATE main.study_group SET name = ?, description = ?, course_type = ? WHERE id = ?");
			preparedStatement.setString(1, studyGroup.getName());
			preparedStatement.setString(2, studyGroup.getDescription());
			preparedStatement.setString(3, studyGroup.getCourseType());
			preparedStatement.setLong(4, studyGroup.getId());
			return preparedStatement.executeUpdate();
		}
		catch (SQLException e)
		{
			LOGGER.error("An SQL error occurred :" + e.getMessage() + "error code:" + e.getErrorCode());
			throw new GroupDataAccessException();
		}
		finally
		{
			try
			{
				preparedStatement.close();
				connection.close();
			}
			catch (SQLException | NullPointerException e)
			{
				LOGGER.error("An error occurred during closing connection :" + e.getMessage());
			}
		}
	}
}
