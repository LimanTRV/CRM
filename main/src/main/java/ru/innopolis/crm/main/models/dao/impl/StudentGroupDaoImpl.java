package ru.innopolis.crm.main.models.dao.impl;

import ru.innopolis.crm.main.models.dao.StudentGroupDao;
import ru.innopolis.crm.main.models.dao.StudentDao;
import ru.innopolis.crm.main.models.entity.Student;

import javax.sql.DataSource;
import java.sql.*;
import java.util.*;

/**
 * Created by Roman Taranov on 24.04.2017.
 */
@Deprecated
public class StudentGroupDaoImpl implements StudentGroupDao
{

	private DataSource dataSource;

	public StudentGroupDaoImpl(DataSource dataSource)
	{
		this.dataSource = dataSource;
	}

	public int update(long groupId, long studentId)
	{
		return 0;
	}

	@Override
	public int insert(long groupId, long studentId)
	{
		int lastId = 0;
		try
		{
			Connection connection = dataSource.getConnection();
			PreparedStatement preparedStatement = connection.prepareStatement(
					" INSERT INTO main.student_to_group " +
							" (student_id, study_group_id) " +
							" SELECT ?, ?" +
							" WHERE NOT EXISTS (SELECT 1 FROM main.student_to_group" +
							" WHERE student_id = ? and study_group_id = ?)");
			preparedStatement.setLong(1, studentId);
			preparedStatement.setLong(2, groupId);
			preparedStatement.setLong(3, studentId);
			preparedStatement.setLong(4, groupId);
			preparedStatement.executeUpdate();
			ResultSet rs = preparedStatement.getGeneratedKeys();
			if (rs.next())
			{
				lastId = rs.getInt(1);
			}
			rs.close();
			preparedStatement.close();
			connection.close();
		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}
		return lastId;
	}

	@Override
	public int delete(long groupId, long studentId)
	{
		int result = 0;
		try
		{
			Connection connection = dataSource.getConnection();
			PreparedStatement preparedStatement = connection.prepareStatement(
					"DELETE FROM main.student_to_group " +
							" WHERE  study_group_id = ?  and student_id = ?");
			preparedStatement.setLong(1, groupId);
			preparedStatement.setLong(2, studentId);
			result = preparedStatement.executeUpdate();
			preparedStatement.close();
			connection.close();
		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}
		return result;
	}

	@Override
	public int deleteThatNotActual(long groupId, long studentId)
	{
//        int result = 0;
//        try {
//            Connection connection = dataSource.getConnection();
//            PreparedStatement preparedStatement = connection.prepareStatement(
//                    "DELETE FROM main.student_to_group " +
//                            " WHERE  study_group_id = ?  and student_id = ?");
//            preparedStatement.setLong(1, groupId);
//            preparedStatement.setArray(2, connection.createArrayOf("integer", studentIds.toArray()));
//            result = preparedStatement.executeUpdate();
//            preparedStatement.close();
//            connection.close();
//        } catch (SQLException e) {
//            e.printStackTrace();
//        }
//        return result;
		int result = 0;
		try
		{
			Connection connection = dataSource.getConnection();
			PreparedStatement preparedStatement = connection.prepareStatement(
					"DELETE FROM main.student_to_group " +
							" WHERE  study_group_id = ?  and student_id != ?");
			preparedStatement.setLong(1, groupId);
			preparedStatement.setLong(2, studentId);
			result = preparedStatement.executeUpdate();
			preparedStatement.close();
			connection.close();
		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}
		return result;
	}
}
