package ru.innopolis.crm.main.models.dao.impl;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import ru.innopolis.crm.main.models.dao.OperationDao;
import ru.innopolis.crm.main.models.entity.Operation;

import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Implements <code>OperationDao</code> interface
 *
 * @author Artem Panasyuk, Farit Fattakhov, Roman Taranov
 */
@Repository
public class OperationDaoImpl implements OperationDao
{

	private static final Logger LOG = Logger.getLogger(OperationDaoImpl.class);

	private DataSource dataSource;

	@Autowired
	public void setDataSource(DataSource dataSource)
	{
		this.dataSource = dataSource;
	}

	private static final String SELECT_ALL = "select * from main.operation where not is_deleted";

	private static final String SELECT_BY_ID = "select * from main.operation where not is_deleted and id = ?";

	private static final String SELECT_BY_CODE = "select * from main.operation where not is_deleted and code = ?";

	private static final String INSERT = "insert into main.operation (code, operation_name, is_deleted) VALUES (?, ?, ?)";

	private static final String UPDATE = "update main.operation set code=?, operation_name=?, is_deleted=? where id=?";

	private static final String DELETE = "update main.operation set is_deleted=true where id=?";

	@Override
	public Operation getById(Long operationId)
	{
		Operation operation = null;
		try
		{
			Connection connection = dataSource.getConnection();
			PreparedStatement preparedStatement = connection.prepareStatement(SELECT_BY_ID);
			preparedStatement.setLong(1, operationId);
			ResultSet resultSet = preparedStatement.executeQuery();

			if (resultSet.next())
			{
				operation = createOperation(resultSet);
			}
		}
		catch (SQLException e)
		{
			LOG.error("SQLException:" + e.getMessage());
		}
		return operation;
	}

	@Override
	public Operation getByCode(String operationCode)
	{
		Operation operation = null;
		try
		{
			Connection connection = dataSource.getConnection();
			PreparedStatement preparedStatement = connection.prepareStatement(SELECT_BY_CODE);
			preparedStatement.setString(1, operationCode);
			ResultSet resultSet = preparedStatement.executeQuery();

			if (resultSet.next())
			{
				operation = createOperation(resultSet);
			}
		}
		catch (SQLException e)
		{
			LOG.error("SQLException:" + e.getMessage());
		}
		return operation;
	}

	@Override
	public List<Operation> getAll()
	{
		List<Operation> list = new ArrayList<>();
		try
		{
			Connection connection = dataSource.getConnection();
			Statement statement = connection.createStatement();
			ResultSet resultSet = statement.executeQuery(SELECT_ALL);

			while (resultSet.next())
			{
				list.add(createOperation(resultSet));
			}
		}
		catch (SQLException e)
		{
			LOG.error("SQLException:" + e.getMessage());
		}
		return list;
	}

	@Override
	public void create(Operation operation)
	{
		try
		{
			Connection connection = dataSource.getConnection();
			PreparedStatement preparedStatement = connection.prepareStatement(INSERT, Statement.RETURN_GENERATED_KEYS);
			preparedStatement.setString(1, operation.getCode());
			preparedStatement.setString(2, operation.getName());
			preparedStatement.setBoolean(3, operation.isDeleted());

			preparedStatement.executeUpdate();
			ResultSet generatedKeys = preparedStatement.getGeneratedKeys();
			if (generatedKeys.next())
			{
				operation.setId(generatedKeys.getLong(1));
				LOG.info("New Operation inserted with id: " + operation.getId());
			}
			else
			{
				LOG.info("Operation with id: " + operation.getId() + "is existed");
			}
		}
		catch (SQLException e)
		{
			LOG.error("SQLException:" + e.getMessage());
		}
	}

	@Override
	public void update(Operation operation)
	{
		try
		{
			Connection connection = dataSource.getConnection();
			PreparedStatement preparedStatement = connection.prepareStatement(UPDATE, Statement.RETURN_GENERATED_KEYS);

			preparedStatement.setString(1, operation.getCode());
			preparedStatement.setString(2, operation.getName());
			preparedStatement.setBoolean(3, operation.isDeleted());
			preparedStatement.setLong(4, operation.getId());

			preparedStatement.executeUpdate();

		}
		catch (SQLException e)
		{
			LOG.error("SQLException:" + e.getMessage());
		}
	}

	@Override
	public void delete(Operation operation)
	{
		try
		{
			Connection connection = dataSource.getConnection();
			PreparedStatement preparedStatement = connection.prepareStatement(DELETE, Statement.RETURN_GENERATED_KEYS);
			preparedStatement.setLong(1, operation.getId());

			preparedStatement.executeUpdate();
		}
		catch (SQLException e)
		{
			LOG.error("SQLException:" + e.getMessage());
		}
	}

	/**
	 * Creates new Operation from given result set
	 *
	 * @param resultSet - result of a select from table in DB
	 * @return - entity corresponding to the selected row
	 * @throws SQLException
	 */
	private Operation createOperation(ResultSet resultSet) throws SQLException
	{
		return new Operation(
				resultSet.getLong("id"),
				resultSet.getString("code"),
				resultSet.getString("operation_name"),
				resultSet.getBoolean("is_deleted")
		);
	}
}
