package ru.innopolis.crm.main.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import ru.innopolis.crm.main.models.dao.UserDao;
import ru.innopolis.crm.main.models.entity.User;
import ru.innopolis.crm.main.models.entity.UserAuthority;
import ru.innopolis.crm.main.services.UserService;

import java.util.List;

/**
 * Service implements <code>UserService</code> interface
 */
@Service
public class UserServiceImpl implements UserService
{
	private PasswordEncoder encoder;

	private UserDao userDao;

	@Override
	public User getById(Long userId)
	{
		return userDao.getById(userId);
	}

	@Override
	public User getByLogin(String userLogin)
	{
		return userDao.getByLogin(userLogin);
	}

	@Override
	public void deleteById(Long userId)
	{
		userDao.delete(userDao.getById(userId));
	}

	@Override
	public List<User> getAll()
	{
		return userDao.getAll();
	}

	@Override
	public void save(User user)
	{
		if (user == null)
		{
			return;
		}

		Long userId = user.getId();

		if ((userId != null) && (userId > 0))
		{
			userDao.update(user);
		}
		else
		{
			userDao.create(user);
		}
	}

	@Override
	public User auth(String login, String password)
	{
		User user = userDao.getByLogin(login);

		if (encoder.matches(password, user.getPassword()))
		{
			return user;
		}

		return null;
	}

	@Override
	public long ifStudent(long userId)
	{
		return userDao.ifStudent(userId);
	}

	@Override
	public List<UserAuthority> getAuthorities(User user)
	{
		return userDao.getAuthorities(user);
	}

	@Autowired
	public void setUserDao(UserDao userDao)
	{
		this.userDao = userDao;
	}

	@Autowired
	public void setEncoder(PasswordEncoder encoder)
	{
		this.encoder = encoder;
	}
}
