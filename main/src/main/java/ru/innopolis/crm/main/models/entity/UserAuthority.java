package ru.innopolis.crm.main.models.entity;

/**
 * Created by Roman Taranov on 07.05.2017.
 */
public class UserAuthority
{
	String name;

	public UserAuthority(String name)
	{
		this.name = name;
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	@Override
	public String toString()
	{
		return "UserAuthority{" +
				"name='" + name + '\'' +
				'}';
	}
}

