package ru.innopolis.crm.main.models.entity;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.Objects;

/**
 * Represents student entity
 */
@XmlRootElement
public class Student
{
	private long id;
	private User user;
	private boolean isDeleted = false;
	private int isInGroup = 0;

	public Student()
	{
	}

	public long getId()
	{
		return id;
	}

	public void setId(long id)
	{
		this.id = id;
	}

	public User getUser()
	{
		return user;
	}

	public void setUser(User user)
	{
		this.user = user;
	}

	public boolean isDeleted()
	{
		return isDeleted;
	}

	public void setDeleted(boolean deleted)
	{
		isDeleted = deleted;
	}

	public int getIsInGroup()
	{
		return isInGroup;
	}

	public void setIsInGroup(int isInGroup)
	{
		this.isInGroup = isInGroup;
	}

	@Override
	public boolean equals(Object o)
	{
		if (this == o)
		{
			return true;
		}
		if (o == null || getClass() != o.getClass())
		{
			return false;
		}
		Student student = (Student) o;
		return id == student.id &&
				isDeleted == student.isDeleted &&
				isInGroup == student.isInGroup &&
				Objects.equals(user, student.user);
	}

	@Override
	public int hashCode()
	{
		return Objects.hash(id, user, isDeleted, isInGroup);
	}

	@Override
	public String toString()
	{
		return "Student{" +
				"id=" + id +
				", user=" + user +
				", isDeleted=" + isDeleted +
				", isInGroup=" + isInGroup +
				'}';
	}
}