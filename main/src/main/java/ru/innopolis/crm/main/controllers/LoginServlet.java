package ru.innopolis.crm.main.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import ru.innopolis.crm.main.sessionbean.UserBean;

import java.util.List;

@Controller
@RequestMapping(value = "/")
public class LoginServlet
{

	private static final String ROLE_ADMIN = "ROLE_M_E";
	private static final String ROLE_TEACHER = "ROLE_T_E";
	private static final String ROLE_STUDENT = "ROLE_TD_V";
	private static final String ROLE_CENTER_ADMIN = "ROLE_PC_E";

	private UserBean userBean;

	@Autowired
	public void setUserBean(UserBean userBean)
	{
		this.userBean = userBean;
	}

	@RequestMapping(method = RequestMethod.GET)
	public String redirectMainPage()
	{
		if (userBean.getUserId() == 0)
		{
			return "login";
		}

		if (userBean.getAuthorities()
				.stream().anyMatch(u -> ROLE_ADMIN.equals(u.getName())))
		{
			return "redirect:/UserController?action=listUser";
		}
		else if (userBean.getAuthorities()
				.stream().anyMatch(u -> ROLE_TEACHER.equals(u.getName())))
		{
			return "redirect:/task/list";
		}
		else if (userBean.getAuthorities()
				.stream().anyMatch(u -> ROLE_CENTER_ADMIN.equals(u.getName())))
		{
			return "redirect:/groups/";
		}
		else if (userBean.getAuthorities()
				.stream().anyMatch(u -> ROLE_STUDENT.equals(u.getName())))
		{
			return "redirect:/lessons/";
		}
		return "login";
	}

	@RequestMapping("/error/403")
	public String forbidden()
	{
		return "403";
	}
}
