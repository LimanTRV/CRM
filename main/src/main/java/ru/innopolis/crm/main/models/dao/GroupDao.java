package ru.innopolis.crm.main.models.dao;


import ru.innopolis.crm.main.models.entity.StudyGroup;

import java.util.List;

public interface GroupDao
{

	List<StudyGroup> findAll();

	StudyGroup findById(long id);

	int insert(StudyGroup studyGroup);

	int delete(long id);

	int update(StudyGroup studyGroup);

	StudyGroup getFirst();
	//int delete(StudyGroup group);
	//int save(StudyGroup group);
}
