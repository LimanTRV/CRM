package ru.innopolis.crm.main.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.innopolis.crm.main.models.dao.RoleDao;
import ru.innopolis.crm.main.models.entity.Role;
import ru.innopolis.crm.main.services.RoleService;

import java.util.List;

/**
 * Service implements <code>RoleService</code> interface
 */
@Service
public class RoleServiceImpl implements RoleService
{

	private RoleDao roleDao;
	private static final long ROLE_ADMIN_ID = 1;
	private static final long ROLE_STUDENT_ID = 2;

	@Autowired
	public void setRoleDao(RoleDao roleDao)
	{
		this.roleDao = roleDao;
	}

	@Override
	public Role getById(Long roleId)
	{
		return roleDao.getById(roleId);
	}

	@Override
	public Role getByCode(String code)
	{
		return roleDao.getByCode(code);
	}

	@Override
	public List<Role> getAll()
	{
		return roleDao.getAll();
	}

	@Override
	public void deleteById(Long roleId)
	{
		if ((roleId == ROLE_ADMIN_ID) || (roleId == ROLE_STUDENT_ID))
		{
			return;
		}

		roleDao.delete(roleDao.getById(roleId));
	}

	@Override
	public long save(Role role)
	{
		if (role == null)
		{
			return 0;
		}

		Long roleId = role.getId();

		if ((roleId != null) && (roleId > 0))
		{
			roleDao.update(role);
			return role.getId();
		}
		else
		{
			return roleDao.create(role);
		}
	}

	@Override
	public int remove(long roleId)
	{
		if ((roleId == ROLE_ADMIN_ID) || (roleId == ROLE_STUDENT_ID))
		{
			return -1;
		}

		if (countUsersByRoleId(roleId) > 0)
		{
			return -1;
		}

		return roleDao.remove(roleId);
	}

	@Override
	public int countUsersByRoleId(long roleId)
	{
		return roleDao.countUsersByRoleId(roleId);
	}
}
