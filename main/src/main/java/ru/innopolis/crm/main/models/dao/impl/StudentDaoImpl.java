package ru.innopolis.crm.main.models.dao.impl;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import ru.innopolis.crm.main.models.dao.StudentDao;

import ru.innopolis.crm.main.models.dao.exceptions.StudentDataAccessException;
import ru.innopolis.crm.main.models.entity.Student;
import ru.innopolis.crm.main.models.entity.StudyGroup;
import ru.innopolis.crm.main.models.entity.User;

import javax.mail.MethodNotSupportedException;
import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

@Repository
public class StudentDaoImpl implements StudentDao
{
	private static final Logger LOGGER = Logger.getLogger(StudentDaoImpl.class);
	private DataSource dataSource;

	@Autowired
	public void setDataSource(DataSource dataSource)
	{
		this.dataSource = dataSource;
	}

	@Override
	public List<Student> getStudentsByTask(Long taskId)
	{
		List<Student> students = new ArrayList<>(16);

		try
		{
			Connection connection = dataSource.getConnection();
			Statement statement = connection.createStatement();
			ResultSet resultSet = null;

			resultSet = statement.executeQuery("SELECT std.*, usr.* FROM task.download tsk " +
					" left join main.student std on tsk.student_id = std.id and not std.is_deleted " +
					" left join main.crm_user usr on std.crm_user_id = usr.id and not usr.is_deleted " +
					" where not tsk.is_deleted   " +
					" AND tsk.task_id = " + taskId);

			while (resultSet.next())
			{
				Student student = new Student();
				student.setId(resultSet.getLong("id"));
				student.setDeleted(resultSet.getBoolean("is_deleted"));
				User user = new User();
				user.setId(resultSet.getLong("crm_user_id"));
				user.setFirstName(resultSet.getString("first_name"));
				user.setLastName(resultSet.getString("last_name"));
				user.setEmail(resultSet.getString("email"));
				user.setLogin(resultSet.getString("login"));
				user.setPassword(resultSet.getString("user_password"));
				user.setActive(resultSet.getBoolean("is_active"));
				student.setUser(user);
				students.add(student);
			}
			statement.close();
			connection.close();
		}
		catch (SQLException e)
		{
			LOGGER.error("Getting list students failed!", e);
			students = null;
		}

		return students;
	}

	@Override
	public List<Long> getStudentGroups(long studentId)
	{
		List<Long> groups = new ArrayList<>();
		try
		{
			ResultSet set = dataSource.getConnection().createStatement().executeQuery("select study_group_id from main.student_to_group where student_id = " + studentId);
			while (set.next())
			{
				groups.add(set.getLong(1));
			}

		}
		catch (SQLException e)
		{
			LOGGER.error(e.getMessage());
		}
		return groups;
	}

	@Override
	public List<Student> findAll()
	{
		List<Student> list = new ArrayList<>();
		Connection connection = null;
		Statement statement = null;
		try
		{
			connection = dataSource.getConnection();
			statement = connection.createStatement();
			ResultSet resultSet = statement.executeQuery("SELECT std.id as student_id, first_name, email, last_name, is_active " +
					"FROM main.student std " +
					"left join main.crm_user usr on std.crm_user_id = usr.id and not std.is_deleted");
			while (resultSet.next())
			{
				User user = new User();
				user.setFirstName(resultSet.getString("first_name"));
				user.setEmail(resultSet.getString("email"));
				user.setLastName(resultSet.getString("last_name"));
				user.setActive(resultSet.getBoolean("is_active"));

				Student student = new Student();
				student.setId(resultSet.getLong(1));
				student.setUser(user);

				list.add(student);
			}

		}
		catch (SQLException e)
		{
			LOGGER.error("An SQL error occurred :" + e.getMessage() + "error code:" + e.getErrorCode());
			throw new StudentDataAccessException();
		}
		finally
		{
			try
			{
				statement.close();
				connection.close();
			}
			catch (SQLException | NullPointerException e)
			{
				LOGGER.error("An error occurred during closing connection :" + e.getMessage());
			}
		}
		return list;
	}

	@Override
	public Student getStudent(long id)
	{
		List<Student> list = new ArrayList<>();
		Connection connection = null;
		Statement statement = null;
		try
		{
			connection = dataSource.getConnection();
			statement = connection.createStatement();
			ResultSet resultSet = statement.executeQuery("SELECT * FROM main.student std " +
					" left join main.crm_user usr on std.crm_user_id = usr.id" +
					" WHERE std.id = " + id + " and not std.is_deleted limit 1");
			if (resultSet.next())
			{
				User user = new User();
				user.setFirstName(resultSet.getString("first_name"));
				user.setEmail(resultSet.getString("email"));
				user.setLastName(resultSet.getString("last_name"));
				user.setActive(resultSet.getBoolean("is_active"));

				Student student = new Student();
				student.setId(resultSet.getLong(1));
				student.setDeleted(resultSet.getBoolean(2));
				student.setUser(user);
				return student;
			}
		}
		catch (SQLException e)
		{
			LOGGER.error("An SQL error occurred :" + e.getMessage() + "error code:" + e.getErrorCode());
			throw new StudentDataAccessException();
		}
		finally
		{
			try
			{
				statement.close();
				connection.close();
			}
			catch (SQLException | NullPointerException e)
			{
				LOGGER.error("An error occurred during closing connection :" + e.getMessage());
			}
		}
		return null;
	}

	@Deprecated
	public Student findById(long id) throws MethodNotSupportedException
	{
		throw new MethodNotSupportedException("Метод заменён на getStudent");
	}

	@Override
	public Student getStudentByUserId(long id)
	{
		Connection connection = null;
		Statement statement = null;
		try
		{
			connection = dataSource.getConnection();
			statement = connection.createStatement();
			ResultSet resultSet = statement.executeQuery("SELECT std.id as student_id, first_name, email, last_name, is_active " +
					"FROM main.student std " +
					" left join main.crm_user usr on std.crm_user_id = usr.id" +
					" WHERE usr.id = " + id + " and not std.is_deleted limit 1");
			if (resultSet.next())
			{
				Student result = new Student();
				result.setId(resultSet.getLong("student_id"));
				result.setDeleted(false);
				User user = new User();
				user.setFirstName(resultSet.getString("first_name"));
				user.setEmail(resultSet.getString("email"));
				user.setLastName(resultSet.getString("last_name"));
				user.setActive(resultSet.getBoolean("is_active"));
				result.setUser(user);
				return result;
			}
		}
		catch (SQLException e)
		{
			LOGGER.error("An SQL error occurred :" + e.getMessage() + "error code:" + e.getErrorCode());
			throw new StudentDataAccessException();
		}
		finally
		{
			try
			{
				statement.close();
				connection.close();
			}
			catch (SQLException | NullPointerException e)
			{
				LOGGER.error("An error occurred during closing connection :" + e.getMessage());
			}
		}
		return null;
	}

	@Override
	public int delete(long id)
	{
		Connection connection = null;
		Statement statement = null;
		try
		{
			connection = dataSource.getConnection();
			statement = connection.createStatement();
			return statement.executeUpdate("UPDATE main.student SET is_deleted=true WHERE id = " + id);
		}
		catch (SQLException e)
		{
			LOGGER.error("An SQL error occurred :" + e.getMessage() + "error code:" + e.getErrorCode());
			throw new StudentDataAccessException();
		}
		finally
		{
			try
			{
				statement.close();
				connection.close();
			}
			catch (SQLException | NullPointerException e)
			{
				LOGGER.error("An error occurred during closing connection :" + e.getMessage());
			}
		}
	}

	public boolean attachToGroup(long studentID, long groupID)
	{
		Connection connection = null;
		Statement statement = null;
		try
		{
			connection = dataSource.getConnection();
			statement = connection.createStatement();
			return 0 < statement.executeUpdate("INSERT INTO main.student_to_group (student_id, study_group_id) VALUES (" + studentID + ", " + groupID + ")");
		}
		catch (SQLException e)
		{
			LOGGER.error("An SQL error occurred :" + e.getMessage() + "error code:" + e.getErrorCode());
			throw new StudentDataAccessException();
		}
		finally
		{
			try
			{
				statement.close();
				connection.close();
			}
			catch (SQLException | NullPointerException e)
			{
				LOGGER.error("An error occurred during closing connection :" + e.getMessage());
			}
		}
	}

	@Override
	public boolean detachFromGroup(long studentID, long groupID)
	{
		Connection connection = null;
		Statement statement = null;
		try
		{
			connection = dataSource.getConnection();
			statement = connection.createStatement();
			return 0 < statement.executeUpdate("DELETE FROM main.student_to_group WHERE student_id = " + studentID + " AND study_group_id = " + groupID);
		}
		catch (SQLException e)
		{
			LOGGER.error("An SQL error occurred :" + e.getMessage() + "error code:" + e.getErrorCode());
			throw new StudentDataAccessException();
		}
		finally
		{
			try
			{
				statement.close();
				connection.close();
			}
			catch (SQLException | NullPointerException e)
			{
				LOGGER.error("An error occurred during closing connection :" + e.getMessage());
			}
		}
	}

	@Override
	public List<Student> getStudentsByGroupId(long groupId)
	{
		List<Student> list = new ArrayList<>();
		Connection connection = null;
		Statement statement = null;
		try
		{
			connection = dataSource.getConnection();
			statement = connection.createStatement();
			PreparedStatement preparedStatement = connection.prepareStatement(
					"SELECT std.*, usr.* FROM main.student std" +
							" left join main.crm_user usr on std.crm_user_id = usr.id and not std.is_deleted" +
							" left join main.student_to_group stg on std.id = stg.student_id" +
							" where stg.study_group_id = ? group by std.id, usr.id");
			preparedStatement.setLong(1, groupId);
			ResultSet resultSet = preparedStatement.executeQuery();
			while (resultSet.next())
			{
				Student student = new Student();
				student.setId(resultSet.getLong(1));
				student.setDeleted(resultSet.getBoolean(2));
				User user = new User();
				user.setFirstName(resultSet.getString("first_name"));
				user.setEmail(resultSet.getString("email"));
				user.setLastName(resultSet.getString("last_name"));
				user.setActive(resultSet.getBoolean("is_active"));
				student.setUser(user);
				list.add(student);
			}
		}
		catch (SQLException e)
		{
			LOGGER.error("An SQL error occurred :" + e.getMessage() + "error code:" + e.getErrorCode());
			throw new StudentDataAccessException();
		}
		finally
		{
			try
			{
				statement.close();
				connection.close();
			}
			catch (SQLException | NullPointerException e)
			{
				LOGGER.error("An error occurred during closing connection :" + e.getMessage());
			}
		}
		return list;
	}

	@Override
	public int insert(Student student)
	{
		if (student == null)
		{
			return 0;
		}
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		try
		{
			connection = dataSource.getConnection();
			preparedStatement = connection.prepareStatement("INSERT INTO main.student( " +
					" crm_user_id, is_deleted) VALUES (?, FALSE )", Statement.RETURN_GENERATED_KEYS);
			preparedStatement.setLong(1, student.getUser().getId());
			preparedStatement.executeUpdate();

			ResultSet rs = preparedStatement.getGeneratedKeys();
			if (rs.next())
			{
				return rs.getInt(1);
			}
		}
		catch (SQLException e)
		{
			LOGGER.error("An SQL error occurred :" + e.getMessage() + "error code:" + e.getErrorCode());
			throw new StudentDataAccessException();
		}
		finally
		{
			try
			{
				preparedStatement.close();
				connection.close();
			}
			catch (SQLException | NullPointerException e)
			{
				LOGGER.error("An error occurred during closing connection :" + e.getMessage());
			}
		}
		return 0;
	}

	@Override
	public int update(Student student)
	{
		if (student == null)
		{
			return 0;
		}
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		try
		{
			connection = dataSource.getConnection();
			preparedStatement = connection.prepareStatement(
					"UPDATE main.student SET is_deleted=?, crm_user_id=? WHERE id = ?");
			preparedStatement.setBoolean(1, student.isDeleted());
			preparedStatement.setLong(2, student.getUser().getId());
			preparedStatement.setLong(3, student.getId());
			return preparedStatement.executeUpdate();
		}
		catch (SQLException e)
		{
			LOGGER.error("An SQL error occurred :" + e.getMessage() + "error code:" + e.getErrorCode());
			throw new StudentDataAccessException();
		}
		finally
		{
			try
			{
				preparedStatement.close();
				connection.close();
			}
			catch (SQLException | NullPointerException e)
			{
				LOGGER.error("An error occurred during closing connection :" + e.getMessage());
			}
		}
	}
}
