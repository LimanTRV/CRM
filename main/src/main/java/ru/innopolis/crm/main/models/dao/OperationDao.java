package ru.innopolis.crm.main.models.dao;

import ru.innopolis.crm.main.models.entity.Operation;

import java.util.List;

/**
 * DAO interface to insert <code>Operation</code> objects into DB or get <code>Operation</code> objects from DB
 *
 * @author Artem Panasyuk, Farit Fattakhov, Roman Taranov
 */
public interface OperationDao
{
	/**
	 * Creates an <code>Operation</code> object from DB by id
	 *
	 * @param operationId - identificator of object in DB table
	 * @return a new default <code>Operation</code> object
	 */
	Operation getById(Long operationId);

	/**
	 * Creates an <code>Operation</code> object from DB by code
	 *
	 * @param operationCode - code of object in DB table
	 * @return a new default <code>Operation</code> object
	 */
	Operation getByCode(String operationCode);

	/**
	 * Creates a list of <code>Operation</code> objects from DB
	 *
	 * @return a new list of <code>Operation</code> objects
	 */
	List<Operation> getAll();

	/**
	 * Insert object <code>Operation</code> to DB
	 *
	 * @param operation <code>Operation</code> entity
	 */
	void create(Operation operation);

	/**
	 * Update object <code>Operation</code> in DB
	 *
	 * @param operation <code>Operation</code> entity
	 */
	void update(Operation operation);

	/**
	 * Update object <code>Operation</code> in DB
	 * sets is_deleted column value to true
	 *
	 * @param operation <code>Operation</code> entity
	 */
	void delete(Operation operation);
}
