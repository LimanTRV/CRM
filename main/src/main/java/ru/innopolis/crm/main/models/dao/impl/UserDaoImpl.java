package ru.innopolis.crm.main.models.dao.impl;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Repository;
import ru.innopolis.crm.main.models.dao.RoleDao;
import ru.innopolis.crm.main.models.dao.UserDao;
import ru.innopolis.crm.main.models.entity.Role;
import ru.innopolis.crm.main.models.entity.User;
import ru.innopolis.crm.main.models.entity.UserAuthority;

import javax.sql.DataSource;
import java.sql.*;
import java.util.*;


/**
 * Implements <code>UserDao</code> interface
 *
 * @author Artem Panasyuk, Farit Fattakhov, Roman Taranov
 */
@Repository
public class UserDaoImpl implements UserDao
{

	private static final Logger LOG = Logger.getLogger(UserDaoImpl.class);

	private DataSource dataSource;

	private RoleDao roleDao;

	public PasswordEncoder encoder;

	private static final String SELECT_ALL = "select cu.id, cu.login, cu.user_password, cu.first_name, cu.last_name," +
			" cu.email, cu.is_active, cu.is_deleted, r.id as role_id, r.code, r.name, r.is_deleted as role_is_deleted" +
			" from main.crm_user cu" +
			"  left join main.user_role ur on cu.id = ur.crm_user_id" +
			"  left join main.role r on r.id = ur.role_id " +
			" where not cu.is_deleted";

	private static final String SELECT_BY_ID = "select cu.id, cu.login, cu.user_password, cu.first_name, cu.last_name," +
			" cu.email, cu.is_active, cu.is_deleted, r.id as role_id, r.code, r.name, r.is_deleted as role_is_deleted" +
			" from main.crm_user cu" +
			"  left join main.user_role ur on cu.id = ur.crm_user_id" +
			"  left join main.role r on r.id = ur.role_id " +
			" where not cu.is_deleted and cu.id = ?";

	private static final String SELECT_BY_LOGIN = "select cu.id, cu.login, cu.user_password, cu.first_name, cu.last_name," +
			" cu.email, cu.is_active, cu.is_deleted, r.id as role_id, r.code, r.name, r.is_deleted as role_is_deleted" +
			" from main.crm_user cu" +
			"  left join main.user_role ur on cu.id = ur.crm_user_id" +
			"  left join main.role r on r.id = ur.role_id " +
			" where not cu.is_deleted and cu.login = ?";

	private static final String INSERT = "insert into main.crm_user (" +
			"login, user_password, first_name, last_name, email, is_active, is_deleted)" +
			" values (?, ?, ?, ?, ?, ?, ?)";

	private static final String UPDATE = "update main.crm_user set login=?, user_password = coalesce(?, user_password), " +
			"first_name=?, last_name=?, email=?, is_active=?, is_deleted=? where id=?";

	private static final String DELETE = "update main.crm_user set is_deleted=true where id=?";

	private static final String INSERT_ROLE = "insert into main.role (code, name, is_deleted) values (?, ?, ?)";

	private static final String INSERT_USER_ROLE = "insert into main.user_role(crm_user_id, role_id)\n" +
			"select ? , ? \n" +
			" where not exists (select id from main.user_role where crm_user_id = ? and role_id = ?);";

	private static final String DELETE_USER_ROLE = "delete from main.user_role where crm_user_id = ? and role_id = ?";

	private static final String DELETE_USER_ROLES = "delete from main.user_role where crm_user_id = ?";

	private static final String SELECT_AUTHORITIES_BY_LOGIN = "SELECT cu.login, 'ROLE_' || r.code || '_' || o.code AS role FROM main.crm_user cu\n" +
			"  INNER JOIN main.user_role ur ON ur.crm_user_id = cu.id\n" +
			"  INNER JOIN main.role rl ON rl.id = ur.role_id AND NOT rl.is_deleted\n" +
			"  INNER JOIN main.role_permission rp ON rp.role_id = ur.role_id\n" +
			"  INNER JOIN main.resource r ON r.id = rp.resource_id AND NOT r.is_deleted\n" +
			"  INNER JOIN main.operation o ON o.id = rp.operation_id AND NOT o.is_deleted\n" +
			"WHERE NOT cu.is_deleted AND cu.login = ?";

	@Autowired
	public void setDataSource(DataSource dataSource)
	{
		this.dataSource = dataSource;
	}

	@Autowired
	public void setRoleDao(RoleDao roleDao)
	{
		this.roleDao = roleDao;
	}

	@Autowired
	public void setEncoder(PasswordEncoder encoder)
	{
		this.encoder = encoder;
	}

	@Override
	public User getById(Long userId)
	{
		User user = null;
		try
		{
			Connection connection = dataSource.getConnection();
			PreparedStatement preparedStatement = connection.prepareStatement(SELECT_BY_ID);
			preparedStatement.setLong(1, userId);

			ResultSet rs = preparedStatement.executeQuery();
			while (rs.next())
			{
				if (user == null)
				{
					user = createUser(rs);
					user.setRoles(new HashSet<>());
				}

				Long roleId = rs.getLong("role_id");
				if (roleId > 0)
				{
					Role role = createRole(rs);
					user.getRoles().add(role);
				}
			}
		}
		catch (SQLException e)
		{
			LOG.error("SQLException:" + e.getMessage());
		}
		return user;
	}

	@Override
	public User getByLogin(String userLogin)
	{
		User user = null;
		try
		{
			Connection connection = dataSource.getConnection();
			PreparedStatement preparedStatement = connection.prepareStatement(SELECT_BY_LOGIN);
			preparedStatement.setString(1, userLogin);

			ResultSet rs = preparedStatement.executeQuery();
			while (rs.next())
			{
				if (user == null)
				{
					user = createUser(rs);
					user.setRoles(new HashSet<>());
				}

				Long roleId = rs.getLong("role_id");
				if (roleId > 0)
				{
					Role role = createRole(rs);
					user.getRoles().add(role);
				}
			}
		}
		catch (SQLException e)
		{
			LOG.error("SQLException:" + e.getMessage());
		}
		return user;
	}

	@Override
	public List<User> getAll()
	{
		Map<Long, User> map = new HashMap<>();
		try
		{
			Connection connection = dataSource.getConnection();
			Statement statement = connection.createStatement();
			ResultSet rs = statement.executeQuery(SELECT_ALL);

			User user;
			while (rs.next())
			{
				Long id = rs.getLong("id");
				user = map.get(id);
				if (user == null)
				{
					user = createUser(rs);
					user.setRoles(new HashSet<>());
					map.put(id, user);
				}

				Long roleId = rs.getLong("role_id");
				if (roleId > 0)
				{
					Role role = createRole(rs);
					user.getRoles().add(role);
				}
			}
		}
		catch (SQLException e)
		{
			LOG.error("SQLException:" + e.getMessage());
		}
		return new ArrayList<>(map.values());
	}

	@Override
	public void create(User user)
	{
		try
		{
			Connection connection = dataSource.getConnection();
			PreparedStatement preparedStatement =
					connection.prepareStatement(INSERT, Statement.RETURN_GENERATED_KEYS);
			preparedStatement.setString(1, user.getLogin());
			preparedStatement.setString(2, encoder.encode(user.getPassword()));
			preparedStatement.setString(3, user.getFirstName());
			preparedStatement.setString(4, user.getLastName());
			preparedStatement.setString(5, user.getEmail());
			preparedStatement.setBoolean(6, user.isActive());
			preparedStatement.setBoolean(7, user.isDeleted());

			preparedStatement.executeUpdate();
			ResultSet generatedKeys = preparedStatement.getGeneratedKeys();
			if (generatedKeys.next())
			{
				user.setId(generatedKeys.getLong(1));
				LOG.info("New User inserted with id: " + user.getId());
			}
			if (user.getRoles() != null && !user.getRoles().isEmpty())
			{
				for (Role role : user.getRoles())
				{
					if (roleDao.getById(role.getId()) == null)
					{
						preparedStatement = connection.prepareStatement(INSERT_ROLE, Statement.RETURN_GENERATED_KEYS);
						preparedStatement.setString(1, role.getCode());
						preparedStatement.setString(2, role.getName());
						preparedStatement.setBoolean(3, role.isDeleted());

						preparedStatement.executeUpdate();
						generatedKeys = preparedStatement.getGeneratedKeys();
						if (generatedKeys.next())
						{
							role.setId(generatedKeys.getLong(1));
							LOG.info("New Role inserted with id: " + role.getId());
						}
					}

					preparedStatement = connection.prepareStatement(INSERT_USER_ROLE);
					preparedStatement.setLong(1, user.getId());
					preparedStatement.setLong(2, role.getId());
					preparedStatement.setLong(3, user.getId());
					preparedStatement.setLong(4, role.getId());
					preparedStatement.executeUpdate();
				}
			}
		}
		catch (SQLException e)
		{
			LOG.error("SQLException:" + e.getMessage());
		}
	}

	@Override
	public void update(User user)
	{
		try
		{
			Connection connection = dataSource.getConnection();

			User userFromDB = getById(user.getId());
			if (userFromDB == null)
			{
				return;
			}

			PreparedStatement preparedStatement = connection.prepareStatement(UPDATE, Statement.RETURN_GENERATED_KEYS);

			preparedStatement.setString(1, user.getLogin());
			if (user.getPassword() == null)
			{
				preparedStatement.setNull(2, Types.VARCHAR);
			}
			else
			{
				if (!userFromDB.getPassword().equals(user.getPassword()))
				{
					preparedStatement.setString(2, encoder.encode(user.getPassword()));
				}
				else
				{
					preparedStatement.setString(2, userFromDB.getPassword());
				}
			}
			preparedStatement.setString(3, user.getFirstName());
			preparedStatement.setString(4, user.getLastName());
			preparedStatement.setString(5, user.getEmail());
			preparedStatement.setBoolean(6, user.isActive());
			preparedStatement.setBoolean(7, user.isDeleted());
			preparedStatement.setLong(8, user.getId());

			preparedStatement.executeUpdate();

			if (userFromDB.getRoles() != null)
			{
				for (Role roleFromDB :
						userFromDB.getRoles())
				{
					long countNewRoles = user.getRoles().stream().filter((r) -> r.getId().equals(roleFromDB.getId())).count();
					if (countNewRoles == 0)
					{
						preparedStatement = connection.prepareStatement(DELETE_USER_ROLE);

						preparedStatement.setLong(1, userFromDB.getId());
						preparedStatement.setLong(2, roleFromDB.getId());

						preparedStatement.executeUpdate();
					}
				}
			}

			if (user.getRoles() != null && !user.getRoles().isEmpty())
			{
				for (Role role : user.getRoles())
				{
					if (roleDao.getById(role.getId()) == null)
					{
						preparedStatement = connection.prepareStatement(INSERT_ROLE, Statement.RETURN_GENERATED_KEYS);
						preparedStatement.setString(1, role.getCode());
						preparedStatement.setString(2, role.getName());
						preparedStatement.setBoolean(3, role.isDeleted());
						preparedStatement.executeUpdate();

						ResultSet generatedKeys = preparedStatement.getGeneratedKeys();
						if (generatedKeys.next())
						{
							role.setId(generatedKeys.getLong(1));
							LOG.info("New Role inserted with id: " + role.getId());
						}
					}

					preparedStatement = connection.prepareStatement(INSERT_USER_ROLE);
					preparedStatement.setLong(1, user.getId());
					preparedStatement.setLong(2, role.getId());
					preparedStatement.setLong(3, user.getId());
					preparedStatement.setLong(4, role.getId());
					preparedStatement.executeUpdate();
				}
			}
		}
		catch (SQLException e)
		{
			LOG.error("SQLException:" + e.getMessage());
		}
	}

	@Override
	public void delete(User user)
	{
		try
		{
			Connection connection = dataSource.getConnection();
			PreparedStatement preparedStatement =
					connection.prepareStatement(DELETE, Statement.RETURN_GENERATED_KEYS);
			preparedStatement.setLong(1, user.getId());
			preparedStatement.executeUpdate();

			preparedStatement =
					connection.prepareStatement(DELETE_USER_ROLES, Statement.RETURN_GENERATED_KEYS);
			preparedStatement.setLong(1, user.getId());
			preparedStatement.executeUpdate();
		}
		catch (SQLException e)
		{
			LOG.error("SQLException:" + e.getMessage());
		}
	}

	@Override
	public long ifStudent(long userId)
	{
		try
		{
			ResultSet set = dataSource.getConnection().createStatement().executeQuery("select s.id from main.student s join main.crm_user u on s.crm_user_id = u.id where u.is_deleted = false and u.id = " + userId);
			if (set.next())
			{
				return set.getInt(1);
			}

			return 0;
		}
		catch (SQLException e)
		{
			LOG.error(e.getMessage());
			return 0;
		}
	}

	@Override
	public List<UserAuthority> getAuthorities(User user)
	{
		List<UserAuthority> authorities = new ArrayList<>();
		try
		{
			Connection connection = dataSource.getConnection();
			PreparedStatement preparedStatement = connection.prepareStatement(SELECT_AUTHORITIES_BY_LOGIN);
			preparedStatement.setString(1, user.getLogin());

			ResultSet rs = preparedStatement.executeQuery();
			while (rs.next())
			{
				if (user != null)
				{
					String role = rs.getString("role");
					if (role != null)
					{
						UserAuthority authority = new UserAuthority(rs.getString("role"));
						authorities.add(authority);
					}
				}
			}
			return authorities;
		}
		catch (SQLException e)
		{
			LOG.error("SQLException:" + e.getMessage());
		}
		return null;
	}

	/**
	 * Creates new User from given result set
	 *
	 * @param resultSet - result of a select from table in DB
	 * @return - entity corresponding to the selected row
	 * @throws SQLException
	 */
	private User createUser(ResultSet resultSet) throws SQLException
	{
		return new User(
				resultSet.getLong("id"),
				resultSet.getString("login"),
				resultSet.getString("user_password"),
				resultSet.getString("first_name"),
				resultSet.getString("last_name"),
				resultSet.getString("email"),
				resultSet.getBoolean("is_active"),
				resultSet.getBoolean("is_deleted")
		);
	}

	/**
	 * Creates new Role from given result set
	 *
	 * @param resultSet - result of a select from table in DB
	 * @return - entity corresponding to the selected row
	 * @throws SQLException
	 */
	private Role createRole(ResultSet resultSet) throws SQLException
	{
		return new Role(
				resultSet.getLong("role_id"),
				resultSet.getString("code"),
				resultSet.getString("name"),
				resultSet.getBoolean("role_is_deleted")
		);
	}
}
