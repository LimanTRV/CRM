package ru.innopolis.crm.main.models.dao;

import ru.innopolis.crm.main.models.entity.Student;

import java.util.List;
import java.util.Set;

@Deprecated
public interface StudentGroupDao
{
	int delete(long groupId, long studentId);

	int deleteThatNotActual(long groupId, long studentId);

	int insert(long groupId, long studentId);
}
