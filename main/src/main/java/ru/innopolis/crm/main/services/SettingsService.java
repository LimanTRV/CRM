package ru.innopolis.crm.main.services;

import java.util.List;

import ru.innopolis.crm.main.models.entity.SettingsOption;
import ru.innopolis.crm.main.services.exceptions.ServiceException;

/**
 *
 */
public interface SettingsService
{

	List<SettingsOption> getAll() throws ServiceException;

	SettingsOption getById(long id) throws ServiceException;

	SettingsOption getByCode(String code) throws ServiceException;

	int insert(SettingsOption option) throws ServiceException;

	int update(SettingsOption option) throws ServiceException;

	int delete(SettingsOption option) throws ServiceException;
}
