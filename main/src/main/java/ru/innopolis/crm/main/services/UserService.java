package ru.innopolis.crm.main.services;


import ru.innopolis.crm.main.models.entity.User;
import ru.innopolis.crm.main.models.entity.UserAuthority;

import java.util.List;

/**
 * Service for working with entity <code>User</code>
 */
public interface UserService
{
	/**
	 * Get a <code>User</code> object from DB by id
	 *
	 * @param userId
	 * @return <code>User</code> entity
	 */
	User getById(Long userId);

	/**
	 * Get a <code>User</code> object from DB by id
	 *
	 * @param userLogin
	 * @return <code>User</code> entity
	 */
	User getByLogin(String userLogin);

	/**
	 * Delete a <code>User</code> object from DB by id
	 *
	 * @param userId
	 */
	void deleteById(Long userId);

	/**
	 * Creates a list of <code>User</code> objects from DB
	 *
	 * @return a new list of <code>User</code> objects
	 */
	List<User> getAll();

	/**
	 * Create or update an <code>User</code> object to DB
	 *
	 * @param user <code>User</code> entity
	 */
	void save(User user);

	/**
	 * Get a <code>User</code> object from DB by login and password
	 *
	 * @param login
	 * @param password
	 * @return <code>User</code> entity
	 */
	User auth(String login, String password);

	long ifStudent(long groupId);

	List<UserAuthority> getAuthorities(User user);
}
