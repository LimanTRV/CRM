package ru.innopolis.crm.main.controllers;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import ru.innopolis.crm.main.models.dao.exceptions.GroupDataAccessException;
import ru.innopolis.crm.main.models.entity.StudyGroup;
import ru.innopolis.crm.main.services.GroupService;

import java.util.List;

/**
 * Controller for view groups of students page
 */
@Controller
@RequestMapping("/groups/")
public class GroupController
{
	private static Logger LOGGER = Logger.getLogger(GroupController.class);
	private GroupService groupService;

	@Autowired
	public void setGroupService(GroupService groupService)
	{
		this.groupService = groupService;
	}

	@GetMapping
	@PreAuthorize(value = "hasAnyRole('G_V', 'G_E')")
	public String getGroups(Model model)
	{
		try
		{
			List<StudyGroup> groupList = groupService.findAll();
			model.addAttribute("groupList", groupList);
			return "/WEB-INF/pages/groups/groups";
		}
		catch (NullPointerException | GroupDataAccessException e)
		{
			LOGGER.error(e.getMessage());
			return "error";
		}
	}

	@DeleteMapping
	@PreAuthorize(value = "hasRole('G_E')")
	public ResponseEntity<Void> deleteGroup(@ModelAttribute("id") Long id)
	{
		try
		{
			if (0 < groupService.delete(id))
			{
				return new ResponseEntity<Void>(HttpStatus.OK);
			}
			else
			{
				return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
			}
		}
		catch (NullPointerException | GroupDataAccessException e)
		{
			LOGGER.error(e.getMessage());
			return new ResponseEntity<Void>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@PostMapping
	@PreAuthorize(value = "hasRole('G_E')")
	public ResponseEntity<Void> updateGroups(@RequestParam("id") Long id, @RequestParam("name") String name,
											 @RequestParam("description") String description, @RequestParam("type") String type)
	{

		try
		{
			int result;
			if (id == 0)
			{
				StudyGroup groupItem = new StudyGroup(0, name, description, type);
				result = groupService.insert(groupItem);
			}
			else
			{
				StudyGroup groupItem = groupService.findById(id);
				groupItem.setName(name);
				groupItem.setCourseType(type);
				groupItem.setDescription(description);
				result = groupService.update(groupItem);
			}
			if (result > 0)
			{
				return new ResponseEntity<Void>(HttpStatus.OK);
			}
			else
			{
				return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
			}
		}
		catch (NullPointerException | GroupDataAccessException e)
		{
			LOGGER.error(e.getMessage());
			return new ResponseEntity<Void>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}


}
