package ru.innopolis.crm.main.services;

import ru.innopolis.crm.main.models.entity.Operation;

import java.util.List;

/**
 * Service for working with entity <code>Operation</code>
 */
public interface OperationService
{

	/**
	 * Get an <code>Operation</code> object from DB by id
	 *
	 * @param operationId
	 * @return <code>Operation</code> entity
	 */
	Operation getById(Long operationId);

	/**
	 * Delete an <code>Operation</code> object from DB by id
	 *
	 * @param operationId
	 */
	void deleteById(Long operationId);

	/**
	 * Creates a list of <code>Operation</code> objects from DB
	 *
	 * @return a new list of <code>Operation</code> objects
	 */
	List<Operation> getAll();

	/**
	 * Create or update an <code>Operation</code> object to DB
	 *
	 * @param operation <code>Operation</code> entity
	 */
	void save(Operation operation);
}
