package ru.innopolis.crm.main.models.dao;

import ru.innopolis.crm.main.models.entity.Student;
import ru.innopolis.crm.main.models.entity.StudyGroup;

import javax.mail.MethodNotSupportedException;
import java.util.List;

public interface StudentDao
{
	List<Long> getStudentGroups(long studentId);

	List<Student> findAll();

	Student getStudent(long id);

	Student getStudentByUserId(long id);

	int insert(Student student);

	int delete(long id);

	int update(Student student);

	List<Student> getStudentsByGroupId(long groupId);

	boolean attachToGroup(long studentID, long groupID);

	boolean detachFromGroup(long studentID, long groupID);

	List<Student> getStudentsByTask(Long taskId);

	@Deprecated
	Student findById(long id) throws MethodNotSupportedException;
	//int save(Student student);
}
