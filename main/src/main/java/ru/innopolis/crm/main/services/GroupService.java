package ru.innopolis.crm.main.services;


import org.springframework.stereotype.Service;
import ru.innopolis.crm.main.models.entity.StudyGroup;

import java.util.List;

@Service
public interface GroupService
{
	List<StudyGroup> findAll();

	StudyGroup findById(long id);

	int update(StudyGroup studyGroup);

	int insert(StudyGroup studyGroup);

	int delete(long id);

	StudyGroup getFirst();
}
