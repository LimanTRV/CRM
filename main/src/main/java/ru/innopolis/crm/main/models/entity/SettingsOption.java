package ru.innopolis.crm.main.models.entity;

/**
 * Сущность из таблицы настроек
 */
public class SettingsOption
{

	private long id;
	private String code;
	private String value;
	private boolean actual;

	public long getId()
	{
		return id;
	}

	public void setId(long id)
	{
		this.id = id;
	}

	public String getCode()
	{
		return code;
	}

	public void setCode(String code)
	{
		this.code = code;
	}

	public String getValue()
	{
		return value;
	}

	public void setValue(String value)
	{
		this.value = value;
	}

	public boolean isActual()
	{
		return actual;
	}

	public void setActual(boolean actual)
	{
		this.actual = actual;
	}
}
