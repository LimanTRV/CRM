package ru.innopolis.crm.main.services;

import ru.innopolis.crm.main.models.entity.Operation;
import ru.innopolis.crm.main.models.entity.Resource;

import java.util.List;
import java.util.Map;

/**
 * User Permissions Service
 */
public interface PermissionService
{
	public boolean hasUserPermission(Long userId, String resourceCode, String operationCode);

	Map<Resource, List<Operation>> getByRoleId(Long roleId);

	/**
	 * Метод для записи пермишинов в БД
	 *
	 * @param roleId    роль
	 * @param resources мап ресурс - список операций
	 */
	boolean savePermissoin(long roleId, Map<Long, List<Long>> resources);

	/**
	 * Метод для вставки данных в БД
	 *
	 * @param roleId      роль
	 * @param resourceId  ресурс
	 * @param operationId опреация
	 * @return кол-во вставленных записей
	 */
	int insert(long roleId, long resourceId, long operationId);

	/**
	 * Метод для удаления пермишинов по роль ид
	 *
	 * @param roleId ид
	 * @return кол-во обновленных записей
	 */
	int deleteByRoleId(long roleId);
}
