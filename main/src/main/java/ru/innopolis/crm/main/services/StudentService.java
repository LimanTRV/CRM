package ru.innopolis.crm.main.services;


import ru.innopolis.crm.main.models.entity.Student;

import java.util.List;


public interface StudentService
{
	List<Long> getStudentGroups(long studentId);

	List<Student> findAll();

	List<Student> getStudentsByGroupId(long groupId);

	Student findById(long id);

	Student getStudentByUserId(long id);

	List<Student> getStudentsByTask(Long taskId);

	int create(Student student);

	boolean attachToGroup(long studentID, long groupID);

	boolean detachFromGroup(long studentID, long groupID);

}
