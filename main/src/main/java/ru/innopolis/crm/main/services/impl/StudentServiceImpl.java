package ru.innopolis.crm.main.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.innopolis.crm.main.models.dao.StudentDao;
import ru.innopolis.crm.main.models.entity.Student;
import ru.innopolis.crm.main.services.StudentService;

import java.util.List;

/**
 * Student data access service implementation
 */
@Service
public class StudentServiceImpl implements StudentService
{
	private StudentDao studentDao;

	@Autowired
	public void setStudentDao(StudentDao studentDao)
	{
		this.studentDao = studentDao;
	}

	@Override
	public List<Long> getStudentGroups(long studentId)
	{
		return studentDao.getStudentGroups(studentId);
	}

	@Override
	public List<Student> findAll()
	{
		return studentDao.findAll();
	}

	@Override
	public List<Student> getStudentsByGroupId(long groupId)
	{
		return studentDao.getStudentsByGroupId(groupId);
	}

	@Override
	public Student findById(long id)
	{
		return studentDao.getStudent(id);
	}

	@Override
	public Student getStudentByUserId(long id)
	{
		return studentDao.getStudentByUserId(id);
	}

	@Override
	public List<Student> getStudentsByTask(Long taskId)
	{
		return null;
	}

	@Override
	public int create(Student student)
	{
		return studentDao.insert(student);
	}

	@Override
	public boolean attachToGroup(long studentID, long groupID)
	{
		return studentDao.attachToGroup(studentID, groupID);
	}

	@Override
	public boolean detachFromGroup(long studentID, long groupID)
	{
		return studentDao.detachFromGroup(studentID, groupID);
	}

}