package ru.innopolis.crm.main.models.entity;

/**
 * Entity for database table main.operation
 */
public class Operation
{
	private Long id;
	private String code;
	private String name;
	private boolean isDeleted;

	public Operation(Long id, String code, String name, boolean isDeleted)
	{
		this.id = id;
		this.code = code;
		this.name = name;
		this.isDeleted = isDeleted;
	}

	public Long getId()
	{
		return id;
	}

	public void setId(Long id)
	{
		this.id = id;
	}

	public String getCode()
	{
		return code;
	}

	public void setCode(String code)
	{
		this.code = code;
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public boolean isDeleted()
	{
		return isDeleted;
	}

	public void setDeleted(boolean deleted)
	{
		isDeleted = deleted;
	}

	@Override
	public boolean equals(Object o)
	{
		if (this == o)
		{
			return true;
		}
		if (o == null || getClass() != o.getClass())
		{
			return false;
		}

		Operation operation = (Operation) o;

		if (id != null ? !id.equals(operation.id) : operation.id != null)
		{
			return false;
		}
		if (code != null ? !code.equals(operation.code) : operation.code != null)
		{
			return false;
		}
		if (name != null ? !name.equals(operation.name) : operation.name != null)
		{
			return false;
		}

		return true;
	}

	@Override
	public int hashCode()
	{
		int result = id != null ? id.hashCode() : 0;
		result = 31 * result + (code != null ? code.hashCode() : 0);
		result = 31 * result + (name != null ? name.hashCode() : 0);
		return result;
	}
}
