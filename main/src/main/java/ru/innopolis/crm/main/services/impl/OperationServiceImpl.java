package ru.innopolis.crm.main.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.innopolis.crm.main.models.dao.OperationDao;
import ru.innopolis.crm.main.models.entity.Operation;
import ru.innopolis.crm.main.services.OperationService;

import java.util.List;

/**
 * Service implements <code>OperationService</code> interface
 */
@Service
public class OperationServiceImpl implements OperationService
{

	private OperationDao operationDao;

	@Autowired
	public void setOperationDao(OperationDao operationDao)
	{
		this.operationDao = operationDao;
	}

	@Override
	public Operation getById(Long operationId)
	{
		return operationDao.getById(operationId);
	}

	@Override
	public List<Operation> getAll()
	{
		return operationDao.getAll();
	}

	@Override
	public void deleteById(Long operationId)
	{
		operationDao.delete(operationDao.getById(operationId));
	}

	@Override
	public void save(Operation operation)
	{
		if (operation == null)
		{
			return;
		}

		Long operationId = operation.getId();

		if ((operationId != null) && (operationId > 0))
		{
			operationDao.update(operation);
		}
		else
		{
			operationDao.create(operation);
		}
	}
}
