﻿SET client_encoding = 'UTF8';

DROP SEQUENCE IF EXISTS task.download_id_seq CASCADE;
DROP TABLE IF EXISTS task.download CASCADE;
DROP SEQUENCE IF EXISTS main.settings_id_seq CASCADE;
DROP TABLE IF EXISTS main.settings CASCADE;
DROP SEQUENCE IF EXISTS task.criterion_to_task_id_seq CASCADE;
DROP TABLE IF EXISTS task.criterion_to_task CASCADE;
DROP SEQUENCE IF EXISTS task.task_to_group_id_seq CASCADE;
DROP TABLE IF EXISTS task.task_to_group CASCADE;
DROP SEQUENCE IF EXISTS lesson.journal_id_seq CASCADE;
DROP TABLE IF EXISTS lesson.journal CASCADE;
DROP SEQUENCE IF EXISTS lesson.student_activity_id_seq CASCADE;
DROP TABLE IF EXISTS lesson.student_activity  CASCADE;
DROP SEQUENCE IF EXISTS lesson.lesson_id_seq CASCADE;
DROP TABLE IF EXISTS lesson.lesson CASCADE;
DROP SEQUENCE IF EXISTS task.mark_id_seq CASCADE;
DROP TABLE IF EXISTS task.mark CASCADE;
DROP SEQUENCE IF EXISTS main.student_to_group_id_seq CASCADE;
DROP TABLE IF EXISTS main.student_to_group CASCADE;
DROP SEQUENCE IF EXISTS main.study_group_id_seq CASCADE;
DROP TABLE IF EXISTS main.study_group CASCADE;
DROP TABLE IF EXISTS main.role_permission CASCADE;
DROP SEQUENCE IF EXISTS main.role_resource_id_seq CASCADE;
DROP TABLE IF EXISTS main.role_resource CASCADE;
DROP SEQUENCE IF EXISTS task.task_id_seq CASCADE;
DROP TABLE IF EXISTS task.task CASCADE;
DROP SEQUENCE IF EXISTS main.operation_resource_id_seq CASCADE;
DROP TABLE IF EXISTS main.operation_resource CASCADE;
DROP SEQUENCE IF EXISTS main.resource_id_seq CASCADE;
DROP TABLE IF EXISTS main.resource CASCADE;
DROP SEQUENCE IF EXISTS main.operation_id_seq CASCADE;
DROP TABLE IF EXISTS main.operation CASCADE;
DROP SEQUENCE IF EXISTS profile.questionnaire_criterion_instance_id_seq CASCADE;
DROP TABLE IF EXISTS profile.questionnaire_criterion_instance CASCADE;

DROP SEQUENCE IF EXISTS profile.questionnaire_instance_id_seq CASCADE;
DROP TABLE IF EXISTS profile.questionnaire_instance CASCADE;
DROP SEQUENCE IF EXISTS profile.questionnaire_criterion_to_questionnaire_id_seq CASCADE;
DROP TABLE IF EXISTS profile.questionnaire_criterion_to_questionnaire CASCADE;
DROP SEQUENCE IF EXISTS profile.questionnaire_id_seq CASCADE;
DROP TABLE IF EXISTS profile.questionnaire CASCADE;
DROP SEQUENCE IF EXISTS profile.transcript_id_seq CASCADE;
DROP TABLE IF EXISTS profile.transcript CASCADE;

DROP SEQUENCE IF EXISTS profile.questionnaire_criterion_id_seq CASCADE;
DROP TABLE IF EXISTS profile.questionnaire_criterion CASCADE;

DROP SEQUENCE IF EXISTS profile.criterion_group_id_seq CASCADE;
DROP TABLE IF EXISTS profile.criterion_group CASCADE;
DROP SEQUENCE IF EXISTS profile.criterion_type_id_seq CASCADE;
DROP TABLE IF EXISTS profile.criterion_type CASCADE;
DROP SEQUENCE IF EXISTS task.criterion_id_seq CASCADE;
DROP TABLE IF EXISTS task.criterion CASCADE;

DROP TABLE IF EXISTS main.student CASCADE;
DROP TABLE IF EXISTS main.user_role CASCADE;
DROP TABLE IF EXISTS main.role CASCADE;
DROP TABLE IF EXISTS main.crm_user CASCADE;

DROP SCHEMA IF EXISTS public;

DROP SCHEMA IF EXISTS main;
DROP SCHEMA IF EXISTS lesson;
DROP SCHEMA IF EXISTS task;
DROP SCHEMA IF EXISTS profile;

CREATE SCHEMA main;
CREATE SCHEMA lesson;
CREATE SCHEMA task;
CREATE SCHEMA profile;


CREATE TABLE main.crm_user
(
  id serial NOT NULL,
  login character varying(255) NOT NULL,
  user_password character varying(255) NOT NULL,
  first_name character varying(255) NOT NULL,
  last_name character varying(255) NOT NULL,
  email character varying(255) NOT NULL,
  is_active boolean NOT NULL,
  is_deleted boolean NOT NULL default FALSE,
  CONSTRAINT pk_user PRIMARY KEY (id)
);

CREATE UNIQUE INDEX crm_user_login_uindex ON main.crm_user (login);

CREATE TABLE main.role
(
  id serial NOT NULL,
  name character varying(255) NOT NULL,
  code character varying(255) NOT NULL,
  is_deleted boolean NOT NULL default FALSE,
  CONSTRAINT pk_role PRIMARY KEY (id)
);

CREATE TABLE main.user_role
(
  id serial NOT NULL,
  crm_user_id integer NOT NULL,
  role_id integer NOT NULL,
  CONSTRAINT pk_user_role PRIMARY KEY (id),
  CONSTRAINT fk_crm_user_role FOREIGN KEY (crm_user_id)
  REFERENCES main.crm_user (id) MATCH SIMPLE
  ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT fk_user_role_role FOREIGN KEY (role_id)
  REFERENCES main.role (id) MATCH SIMPLE
  ON UPDATE NO ACTION ON DELETE NO ACTION
);

CREATE TABLE main.student
(
  id serial NOT NULL,
  is_deleted boolean NOT NULL default FALSE,
  crm_user_id integer,
  CONSTRAINT pk_student PRIMARY KEY (id),
  CONSTRAINT fk_student_crm_user FOREIGN KEY (crm_user_id)
  REFERENCES main.crm_user (id) MATCH SIMPLE
  ON UPDATE NO ACTION ON DELETE NO ACTION
);

CREATE TABLE task.criterion
(
  id serial NOT NULL,
  title character varying(255) NOT NULL,
  max_points integer NOT NULL,
  is_additional boolean DEFAULT false,
  is_deleted boolean NOT NULL default FALSE,
  created timestamp without time zone DEFAULT CURRENT_TIMESTAMP,
  CONSTRAINT pk_criterion PRIMARY KEY (id)
);

CREATE TABLE profile.criterion_type
(
  id serial NOT NULL,
  name character varying(255),
  is_deleted boolean NOT NULL default FALSE,
  max_grade integer NOT NULL,
  CONSTRAINT pk_criterion_type PRIMARY KEY (id)
);

CREATE TABLE profile.criterion_group
(
  id serial NOT NULL,
  name character varying(255),
  is_deleted boolean NOT NULL default FALSE,
  CONSTRAINT pk_criterion_group PRIMARY KEY (id)
);


CREATE TABLE profile.questionnaire_criterion
(
  id serial NOT NULL,
  name character varying(255),
  description character varying(2000) COLLATE pg_catalog."default",
  criterion_type_id integer NOT NULL,
  crm_user_id_created integer NOT NULL,
  crm_user_id_edited integer,
  edit_date timestamp without time zone,
  is_deleted boolean NOT NULL,
  criterion_group_id integer NOT NULL,
  CONSTRAINT pk_questionnaire_criterion PRIMARY KEY (id),
  CONSTRAINT fk_criterion_group_to_questionnaire_instance FOREIGN KEY (criterion_group_id)
  REFERENCES profile.criterion_group (id) MATCH SIMPLE
  ON UPDATE NO ACTION
  ON DELETE NO ACTION,
  CONSTRAINT fk_criterion_type_to_questionnaire_criterion FOREIGN KEY (criterion_type_id)
  REFERENCES profile.criterion_type (id) MATCH SIMPLE
  ON UPDATE NO ACTION
  ON DELETE NO ACTION,
  CONSTRAINT fk_crm_user_to_questionnaire_criterion FOREIGN KEY (crm_user_id_created)
  REFERENCES main.crm_user (id) MATCH SIMPLE
  ON UPDATE NO ACTION
  ON DELETE NO ACTION,
  CONSTRAINT fk_crm_user_to_questionnaire_criterion2 FOREIGN KEY (crm_user_id_edited)
  REFERENCES main.crm_user (id) MATCH SIMPLE
  ON UPDATE NO ACTION
  ON DELETE NO ACTION
);

CREATE TABLE profile.transcript
(
  id serial NOT NULL,
  questionnaire_criterion_id integer NOT NULL,
  min_value integer NOT NULL,
  max_value integer NOT NULL,
  text_value character varying(2000) COLLATE pg_catalog."default",
  is_deleted boolean NOT NULL default FALSE,
  description character varying(2000) COLLATE pg_catalog."default",
  CONSTRAINT pk_transcript PRIMARY KEY (id),
  CONSTRAINT fk_question_criterion_to_transcript FOREIGN KEY (questionnaire_criterion_id)
  REFERENCES profile.questionnaire_criterion (id) MATCH SIMPLE
  ON UPDATE NO ACTION
  ON DELETE NO ACTION
);


CREATE TABLE profile.questionnaire
(
  id serial NOT NULL,
  name character varying(255),
  is_default boolean NOT NULL,
  is_deleted boolean NOT NULL,
  CONSTRAINT pk_questionnaire PRIMARY KEY (id)
);

CREATE TABLE profile.questionnaire_criterion_to_questionnaire
(
  id serial NOT NULL,
  questionnaire_criterion_id integer NOT NULL,
  questionnaire_id integer NOT NULL,
  is_deleted boolean NOT NULL default FALSE,
  CONSTRAINT pk_quest_criterion_to_questionnaire PRIMARY KEY (id),
  CONSTRAINT fk_quest_criterion_to_quest_quest_criterion FOREIGN KEY (questionnaire_criterion_id)
  REFERENCES profile.questionnaire_criterion(id) MATCH SIMPLE
  ON UPDATE NO ACTION
  ON DELETE NO ACTION,
  CONSTRAINT fk_quest_criterion_to_quest_questionnaire FOREIGN KEY (questionnaire_id)
  REFERENCES profile.questionnaire (id) MATCH SIMPLE
  ON UPDATE NO ACTION
  ON DELETE NO ACTION
);

CREATE TABLE profile.questionnaire_instance
(
  id serial NOT NULL,
  name character varying(255),
  student_id integer NOT NULL,
  is_deleted boolean NOT NULL default FALSE,
  edit_user_id integer NOT NULL,
  is_finished boolean NOT NULL,
  CONSTRAINT pk_quest_instance PRIMARY KEY (id),
  CONSTRAINT fk_crm_user_to_quest_instance FOREIGN KEY (edit_user_id)
  REFERENCES main.crm_user (id) MATCH SIMPLE
  ON UPDATE NO ACTION
  ON DELETE NO ACTION,
  CONSTRAINT fk_student_to_quest_instance FOREIGN KEY (student_id)
  REFERENCES main.student (id) MATCH SIMPLE
  ON UPDATE NO ACTION
  ON DELETE NO ACTION
);

CREATE TABLE profile.questionnaire_criterion_instance
(
  id serial NOT NULL,
  questionnaire_instance_id integer NOT NULL,
  name character varying(255),
  max_grade integer NOT NULL,
  grade integer,
  criterion_type_id integer NOT NULL,
  crm_user_id_edited integer,
  edit_date timestamp without time zone,
  is_deleted boolean NOT NULL default FALSE,
  criterion_group_id integer NOT NULL,
  questionaire_criterion_id INTEGER NOT NULL,
  CONSTRAINT pk_quest_criterion_instance PRIMARY KEY (id),
  CONSTRAINT fk_criterion_group_to_quest_criterion_instance FOREIGN KEY (criterion_group_id)
  REFERENCES profile.criterion_group (id) MATCH SIMPLE
  ON UPDATE NO ACTION
  ON DELETE NO ACTION,
  CONSTRAINT fk_criterion_type_to_quest_criterion_instance FOREIGN KEY (criterion_type_id)
  REFERENCES profile.criterion_type (id) MATCH SIMPLE
  ON UPDATE NO ACTION
  ON DELETE NO ACTION,
  CONSTRAINT fk_crm_user_to_quest_criterion_instance FOREIGN KEY (crm_user_id_edited)
  REFERENCES main.crm_user (id) MATCH SIMPLE
  ON UPDATE NO ACTION
  ON DELETE NO ACTION,
  CONSTRAINT fk_quest_instance_to_quest_criterion_instance FOREIGN KEY (questionnaire_instance_id)
  REFERENCES profile.questionnaire_instance (id) MATCH SIMPLE
  ON UPDATE NO ACTION
  ON DELETE NO ACTION,
  CONSTRAINT fk_quest_criterion_to_quest_criterion_instance FOREIGN KEY (questionaire_criterion_id)
  REFERENCES profile.questionnaire_criterion (id) MATCH SIMPLE
  ON UPDATE NO ACTION
  ON DELETE NO ACTION
);

CREATE TABLE main.operation
(
  id serial NOT NULL,
  code character varying(255),
  operation_name character varying(255),
  is_deleted boolean NOT NULL default FALSE,
  CONSTRAINT pk_operation PRIMARY KEY (id)
);

CREATE TABLE main.resource
(
  id serial NOT NULL,
  code character varying(255),
  name character varying(255),
  url character varying(255),
  is_deleted boolean NOT NULL default FALSE,
  CONSTRAINT pk_resource PRIMARY KEY (id)
);

CREATE TABLE main.operation_resource
(
  id serial NOT NULL,
  resource_id integer NOT NULL,
  operation_id integer NOT NULL,
  CONSTRAINT pk_operation_resource PRIMARY KEY (id),
  CONSTRAINT fk_operation_resource_resource FOREIGN KEY (resource_id)
  REFERENCES main.resource (id) MATCH SIMPLE
  ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT fk_operation_resource_operation FOREIGN KEY (operation_id)
  REFERENCES main.operation (id) MATCH SIMPLE
  ON UPDATE NO ACTION ON DELETE NO ACTION
);

CREATE TABLE main.role_permission
(
  role_id integer NOT NULL,
  resource_id integer NOT NULL,
  operation_id integer NOT NULL,
  CONSTRAINT fk_role_permission_role FOREIGN KEY (role_id)
  REFERENCES main.role (id) MATCH SIMPLE
  ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT fk_role_permission_resource FOREIGN KEY (resource_id)
  REFERENCES main.resource (id) MATCH SIMPLE
  ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT fk_role_permission_operation FOREIGN KEY (operation_id)
  REFERENCES main.operation (id) MATCH SIMPLE
  ON UPDATE NO ACTION ON DELETE NO ACTION
);

CREATE TABLE task.task
(
  id serial NOT NULL,
  title character varying(255) not null,
  description character varying(2000) not null,
  task_standard_url character varying(255),
  is_lab boolean default false,
  created timestamp without time zone DEFAULT CURRENT_TIMESTAMP,
  end_date timestamp without time zone,
  CONSTRAINT pk_task PRIMARY KEY (id)
);

CREATE TABLE main.role_resource
(
  id serial NOT NULL,
  resource_id integer NOT NULL,
  role_id integer NOT NULL,
  CONSTRAINT pk_role_resource PRIMARY KEY (id),
  CONSTRAINT fk_role_resource_resource FOREIGN KEY (resource_id)
  REFERENCES main.resource (id) MATCH SIMPLE
  ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT fk_role_resource_role FOREIGN KEY (role_id)
  REFERENCES main.role (id) MATCH SIMPLE
  ON UPDATE NO ACTION ON DELETE NO ACTION
);


CREATE TABLE main.study_group
(
  id serial NOT NULL,
  name character varying(255),
  description character varying(2000),
  course_type character varying(1000),
  is_deleted boolean NOT NULL default FALSE,
  CONSTRAINT pk_study_group PRIMARY KEY (id)
)
WITH (
OIDS=FALSE
);

CREATE TABLE main.student_to_group
(
  id serial NOT NULL,
  student_id integer NOT NULL,
  study_group_id integer NOT NULL,
  CONSTRAINT pk_student_to_group PRIMARY KEY (id),
  CONSTRAINT fk_student_to_group_study_group FOREIGN KEY (study_group_id)
  REFERENCES main.study_group (id) MATCH SIMPLE
  ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT fk_student_to_group_student FOREIGN KEY (student_id)
  REFERENCES main.student (id) MATCH SIMPLE
  ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
OIDS=FALSE
);

CREATE TABLE task.mark(
  id serial NOT NULL,
  is_deleted boolean NOT NULL default FALSE,
  student_id integer NOT NULL,
  task_id integer NOT NULL,
  criterion_id integer NOT NULL,
  points integer NOT NULL,
  CONSTRAINT pk_mark PRIMARY KEY (id),
  CONSTRAINT fk_mark_student FOREIGN KEY (student_id)
  REFERENCES main.student (id) MATCH SIMPLE
  ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT fk_mark_task FOREIGN KEY (task_id)
  REFERENCES task.task(id) MATCH SIMPLE
  ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT fk_mark_criterion FOREIGN KEY (criterion_id)
  REFERENCES task.criterion(id) MATCH SIMPLE
  ON UPDATE NO ACTION ON DELETE NO ACTION
);


CREATE TABLE lesson.lesson  (
  id serial NOT NULL,
  study_group_id  integer NOT NULL,
  lesson_date  timestamp without time zone NOT NULL,
  room  character varying(255) NOT NULL,
  topic  character varying(255) NOT NULL,
  description  character varying(2000),
  lesson_comment  character varying(2000),
  token character varying(16),
  token_expiration timestamp without time zone,
  is_deleted  boolean NOT NULL default FALSE,
  CONSTRAINT pk_lesson PRIMARY KEY (id),
  CONSTRAINT fk_lesson_study_group FOREIGN KEY (study_group_id)
  REFERENCES main.study_group(id) MATCH SIMPLE
  ON UPDATE NO ACTION ON DELETE NO ACTION
);

CREATE TABLE lesson.student_activity  (
  id serial NOT NULL,
  student_id  integer NOT NULL,
  lesson_id  integer NOT NULL,
  activity_grade  integer,
  activity_comment  character varying(2000),
  is_deleted  boolean NOT NULL default FALSE,
  CONSTRAINT pk_student_activity PRIMARY KEY (id),
  CONSTRAINT fk_student_activity_lesson FOREIGN KEY (lesson_id)
  REFERENCES lesson.lesson (id) MATCH SIMPLE
  ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT fk_student_activity_student FOREIGN KEY (student_id)
  REFERENCES main.student (id) MATCH SIMPLE
  ON UPDATE NO ACTION ON DELETE NO ACTION
);




CREATE TABLE lesson.journal
(
  id serial NOT NULL,
  lesson_id integer NOT NULL,
  student_id integer NOT NULL,
  time_check timestamp without time zone NOT NULL,
  is_deleted  boolean NOT NULL default FALSE,
  CONSTRAINT pk_journal PRIMARY KEY (id),
  CONSTRAINT fk_journal_student FOREIGN KEY (student_id)
  REFERENCES main.student (id) MATCH SIMPLE
  ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT fk_journal_lesson FOREIGN KEY (lesson_id)
  REFERENCES lesson.lesson(id) MATCH SIMPLE
  ON UPDATE NO ACTION ON DELETE NO ACTION
);


CREATE TABLE task.task_to_group
(
  id serial NOT NULL,
  task_id integer not null,
  study_group_id integer not null,
  CONSTRAINT pk_task_to_group PRIMARY KEY (id),
  CONSTRAINT fk_task_group_task FOREIGN KEY (task_id)
  REFERENCES task.task (id) MATCH SIMPLE
  ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT fk_task_group_group FOREIGN KEY (study_group_id)
  REFERENCES main.study_group (id) MATCH SIMPLE
  ON UPDATE NO ACTION ON DELETE NO ACTION
);

CREATE TABLE task.criterion_to_task
(
  id serial NOT NULL,
  task_id integer not null,
  criterion_id integer not null,
  CONSTRAINT pk_criterion_to_task PRIMARY KEY (id),
  CONSTRAINT fk_criterion_task_task FOREIGN KEY (task_id)
  REFERENCES task.task(id) MATCH SIMPLE
  ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT fk_criterion_task_criterion FOREIGN KEY (criterion_id)
  REFERENCES task.criterion (id) MATCH SIMPLE
  ON UPDATE NO ACTION ON DELETE NO ACTION
);



CREATE TABLE main.settings
(
  id serial NOT NULL,
  code character varying(255),
  set_value character varying(255),
  is_actual boolean NOT NULL,
  CONSTRAINT pk_settings PRIMARY KEY (id),
  CONSTRAINT setting_code_key UNIQUE (code)
)
WITH (
OIDS=FALSE
);

INSERT INTO main.settings(code, set_value, is_actual)
    VALUES ('upload_path', 'c:/uploads', TRUE);

CREATE TABLE task.download
(
  id serial NOT NULL,
  student_id integer NOT NULL,
  task_id integer NOT NULL,
  git_url character varying(255),
  download_date timestamp without time zone NOT NULL,
  download_file_name character varying(255),
  download_url character varying(255),
  is_deleted boolean NOT NULL DEFAULT false,
  CONSTRAINT pk_download PRIMARY KEY (id),
  CONSTRAINT fk_download_student FOREIGN KEY (student_id)
  REFERENCES main.student (id) MATCH SIMPLE
  ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT fk_download_task FOREIGN KEY (task_id)
  REFERENCES task.task (id) MATCH SIMPLE
  ON UPDATE NO ACTION ON DELETE NO ACTION
);





INSERT INTO main.crm_user(login, user_password, first_name, last_name, email, is_active, is_deleted) VALUES ('bloh','$2a$06$L5qQStwN3wxifKElM5ZgwOejC.ri8/sQtEwqvcTpMcSNdk3URHnL2', 'Joshua', 'Bloh', 'bloh@java.com', TRUE, FALSE);
INSERT INTO main.crm_user(login, user_password, first_name, last_name, email, is_active, is_deleted) VALUES ('admin','$2a$06$L5qQStwN3wxifKElM5ZgwOejC.ri8/sQtEwqvcTpMcSNdk3URHnL2', 'Иосиф', 'Джугашвили', 'iosif@baku.com', TRUE, FALSE);
INSERT INTO main.crm_user(login, user_password, first_name, last_name, email, is_active, is_deleted) VALUES ('user','$2a$06$L5qQStwN3wxifKElM5ZgwOejC.ri8/sQtEwqvcTpMcSNdk3URHnL2', 'Карлос', 'Кастанеда', 'huan@kaktus.com', TRUE, FALSE);
INSERT INTO main.crm_user(login, user_password, first_name, last_name, email, is_active, is_deleted) VALUES ('guest','$2a$06$L5qQStwN3wxifKElM5ZgwOejC.ri8/sQtEwqvcTpMcSNdk3URHnL2', 'Kasper', 'Ghost', 'ghost@busters.com', TRUE, FALSE);

INSERT INTO main.crm_user (id, login, user_password, first_name, last_name, email, is_active, is_deleted) VALUES (5, 'Login2', 'Password2', 'Firstname2', 'Lastname2', 'Email2', true, false);
INSERT INTO main.crm_user (id, login, user_password, first_name, last_name, email, is_active, is_deleted) VALUES (6, 'Login3', 'Password3', 'Firstname3', 'Lastname3', 'Email3', true, false);
INSERT INTO main.crm_user (id, login, user_password, first_name, last_name, email, is_active, is_deleted) VALUES (7, 'Login4', 'Password4', 'Firstname4', 'Lastname4', 'Email4', true, false);
INSERT INTO main.crm_user (id, login, user_password, first_name, last_name, email, is_active, is_deleted) VALUES (8, 'Login5', 'Password5', 'Firstname5', 'Lastname5', 'Email5', true, false);
INSERT INTO main.crm_user (id, login, user_password, first_name, last_name, email, is_active, is_deleted) VALUES (9, 'Login6', 'Password6', 'Firstname6', 'Lastname6', 'Email6', true, false);
INSERT INTO main.crm_user (id, login, user_password, first_name, last_name, email, is_active, is_deleted) VALUES (10, 'Login7', 'Password7', 'Firstname7', 'Lastname7', 'Email7', true, false);
INSERT INTO main.crm_user (id, login, user_password, first_name, last_name, email, is_active, is_deleted) VALUES (11, 'Login8', 'Password8', 'Firstname8', 'Lastname8', 'Email8', true, false);
INSERT INTO main.crm_user (id, login, user_password, first_name, last_name, email, is_active, is_deleted) VALUES (12, 'Login9', 'Password9', 'Firstname9', 'Lastname9', 'Email9', true, false);
INSERT INTO main.crm_user (id, login, user_password, first_name, last_name, email, is_active, is_deleted) VALUES (13, 'Login10', 'Password10', 'Firstname10', 'Lastname10', 'Email10', true, false);
INSERT INTO main.crm_user (id, login, user_password, first_name, last_name, email, is_active, is_deleted) VALUES (14, 'Login11', 'Password11', 'Firstname11', 'Lastname11', 'Email11', true, false);
INSERT INTO main.crm_user (id, login, user_password, first_name, last_name, email, is_active, is_deleted) VALUES (15, 'Login12', 'Password12', 'Firstname12', 'Lastname12', 'Email12', true, false);
INSERT INTO main.crm_user (id, login, user_password, first_name, last_name, email, is_active, is_deleted) VALUES (16, 'Login13', 'Password13', 'Firstname13', 'Lastname13', 'Email13', true, false);
INSERT INTO main.crm_user (id, login, user_password, first_name, last_name, email, is_active, is_deleted) VALUES (17, 'Login14', 'Password14', 'Firstname14', 'Lastname14', 'Email14', true, false);
INSERT INTO main.crm_user (id, login, user_password, first_name, last_name, email, is_active, is_deleted) VALUES (18, 'Login15', 'Password15', 'Firstname15', 'Lastname15', 'Email15', true, false);
INSERT INTO main.crm_user (id, login, user_password, first_name, last_name, email, is_active, is_deleted) VALUES (19, 'Login16', 'Password16', 'Firstname16', 'Lastname16', 'Email16', true, false);
INSERT INTO main.crm_user (id, login, user_password, first_name, last_name, email, is_active, is_deleted) VALUES (20, 'Login17', 'Password17', 'Firstname17', 'Lastname17', 'Email17', true, false);
INSERT INTO main.crm_user (id, login, user_password, first_name, last_name, email, is_active, is_deleted) VALUES (21, 'Login18', 'Password18', 'Firstname18', 'Lastname18', 'Email18', true, false);
INSERT INTO main.crm_user (id, login, user_password, first_name, last_name, email, is_active, is_deleted) VALUES (22, 'Login19', 'Password19', 'Firstname19', 'Lastname19', 'Email19', true, false);

INSERT INTO main.crm_user (id, login, user_password, first_name, last_name, email, is_active, is_deleted) VALUES (23, 'viewer', '$2a$06$L5qQStwN3wxifKElM5ZgwOejC.ri8/sQtEwqvcTpMcSNdk3URHnL2', 'viewer', 'viewer', 'r@rr.ru', true, false);
INSERT INTO main.crm_user (id, login, user_password, first_name, last_name, email, is_active, is_deleted) VALUES (24, 'teacher', '$2a$06$L5qQStwN3wxifKElM5ZgwOejC.ri8/sQtEwqvcTpMcSNdk3URHnL2', 'teacher', 'teacher', 'r@rr.ru', true, false);
INSERT INTO main.crm_user (id, login, user_password, first_name, last_name, email, is_active, is_deleted) VALUES (25, 'ca', '$2a$06$L5qQStwN3wxifKElM5ZgwOejC.ri8/sQtEwqvcTpMcSNdk3URHnL2', 'ca', 'ca', 'r@rr.ru', true, false);


INSERT INTO main.role (id, name, code, is_deleted) VALUES (1, 'Admin', 'A', false);
INSERT INTO main.role (id, name, code, is_deleted) VALUES (2, 'Student', 'S', false);
INSERT INTO main.role (id, name, code, is_deleted) VALUES (3, 'Center admin', 'CA', false);
INSERT INTO main.role (id, name, code, is_deleted) VALUES (4, 'Viewer', 'V', false);
INSERT INTO main.role (id, name, code, is_deleted) VALUES (5, 'Teacher', 'T', false);

INSERT INTO main.user_role (id, crm_user_id, role_id) VALUES (1, 2, 1);
INSERT INTO main.user_role (id, crm_user_id, role_id) VALUES (2, 1, 2);
INSERT INTO main.user_role (id, crm_user_id, role_id) VALUES (3, 3, 2);
INSERT INTO main.user_role (id, crm_user_id, role_id) VALUES (4, 4, 2);

INSERT INTO main.user_role (id, crm_user_id, role_id) VALUES (5, 23, 4);
INSERT INTO main.user_role (id, crm_user_id, role_id) VALUES (6, 24, 5);
INSERT INTO main.user_role (id, crm_user_id, role_id) VALUES (7, 25, 3);

INSERT INTO main.resource (id, code, name, url, is_deleted) VALUES (1, 'M', 'Menu', '/', false);
INSERT INTO main.resource (id, code, name, url, is_deleted) VALUES (2, 'L', 'Lesson', '/lessons', false);
INSERT INTO main.resource (id, code, name, url, is_deleted) VALUES (3, 'LV', 'Lesson view', '/lessons/view', false);
INSERT INTO main.resource (id, code, name, url, is_deleted) VALUES (4, 'LVS', 'Lesson view student', '/lessons/view/student', false);
INSERT INTO main.resource (id, code, name, url, is_deleted) VALUES (5, 'T', 'Task', '/task', false);
INSERT INTO main.resource (id, code, name, url, is_deleted) VALUES (6, 'TL', 'Task list', '/task/list', false);
INSERT INTO main.resource (id, code, name, url, is_deleted) VALUES (7, 'TM', 'Task mark', '/task/mark', false);
INSERT INTO main.resource (id, code, name, url, is_deleted) VALUES (8, 'TD', 'Task download', '/task/download', false);
INSERT INTO main.resource (id, code, name, url, is_deleted) VALUES (9, 'R', 'Rating', '/rating', false);
INSERT INTO main.resource (id, code, name, url, is_deleted) VALUES (10, 'G', 'Groups', '/groups', false);
INSERT INTO main.resource (id, code, name, url, is_deleted) VALUES (11, 'S', 'Students', '/students', false);
INSERT INTO main.resource (id, code, name, url, is_deleted) VALUES (12, 'P', 'Profile', '/profile', false);
INSERT INTO main.resource (id, code, name, url, is_deleted) VALUES (13, 'PC', 'Profile constructor', '/profile', false);

INSERT INTO main.operation (id, code, operation_name, is_deleted) VALUES (1, 'V', 'View', false);
INSERT INTO main.operation (id, code, operation_name, is_deleted) VALUES (2, 'E', 'Edit', false);

INSERT INTO main.student(crm_user_id) VALUES (1);
INSERT INTO main.student(crm_user_id) VALUES (5);
INSERT INTO main.student(crm_user_id) VALUES (3);
INSERT INTO main.study_group(name, description, course_type) VALUES ('группа 1', 'java-разработчики', 'java');
INSERT INTO main.study_group(name, description, course_type) VALUES ('группа 2', 'php-разработчики', 'php');
INSERT INTO main.study_group(name, description, course_type) VALUES ('группа 3', 'c++-разработчики', 'c++');
INSERT INTO main.study_group(name, description, course_type) VALUES ('группа 4', 'android-разработчики', 'android');

INSERT INTO main.student_to_group(student_id, study_group_id) VALUES (1, 1);
INSERT INTO main.student_to_group(student_id, study_group_id) VALUES (1, 2);
INSERT INTO main.student_to_group(student_id, study_group_id) VALUES (1, 3);
INSERT INTO main.student_to_group(student_id, study_group_id) VALUES (1, 4);
INSERT INTO main.student_to_group(student_id, study_group_id) VALUES (2, 1);
INSERT INTO main.student_to_group(student_id, study_group_id) VALUES (2, 2);
INSERT INTO main.student_to_group(student_id, study_group_id) VALUES (2, 3);
INSERT INTO main.student_to_group(student_id, study_group_id) VALUES (2, 4);
INSERT INTO main.student_to_group(student_id, study_group_id) VALUES (3, 1);
INSERT INTO main.student_to_group(student_id, study_group_id) VALUES (3, 2);
INSERT INTO main.student_to_group(student_id, study_group_id) VALUES (3, 3);
INSERT INTO main.student_to_group(student_id, study_group_id) VALUES (3, 4);


INSERT INTO profile.criterion_type (id, name, is_deleted, max_grade) VALUES (1, 'Технические навыки', false, 5);
INSERT INTO profile.criterion_type (id, name, is_deleted, max_grade) VALUES (2, 'Личностные характеристики', false, 3);

INSERT INTO profile.criterion_group (id, name, is_deleted) VALUES (1, 'Группа1', false);
INSERT INTO profile.criterion_group (id, name, is_deleted) VALUES (2, 'Группа2', false);
INSERT INTO profile.criterion_group (id, name, is_deleted) VALUES (3, 'Группа3', false);
INSERT INTO profile.criterion_group (id, name, is_deleted) VALUES (4, 'Группа4', false);

INSERT INTO profile.questionnaire_instance (id, name, student_id, is_deleted, edit_user_id, is_finished) VALUES (1, 'Анкета студента: Иван Иванов', 1, false, 2, false);

INSERT INTO profile.questionnaire_criterion (id, name, description, criterion_type_id, criterion_group_id, crm_user_id_created, crm_user_id_edited, edit_date, is_deleted)
VALUES (1, 'Навык1', 'Навык1', 1, 1, 2, null, null, false);
INSERT INTO profile.questionnaire_criterion (id, name, description, criterion_type_id, criterion_group_id, crm_user_id_created, crm_user_id_edited, edit_date, is_deleted)
VALUES (2, 'Навык2', 'Навык2', 1, 1, 2, null, null, false);
INSERT INTO profile.questionnaire_criterion (id, name, description, criterion_type_id, criterion_group_id, crm_user_id_created, crm_user_id_edited, edit_date, is_deleted)
VALUES (3, 'Навык3', 'Навык3', 1, 1, 2, null, null, false);
INSERT INTO profile.questionnaire_criterion (id, name, description, criterion_type_id, criterion_group_id, crm_user_id_created, crm_user_id_edited, edit_date, is_deleted)
VALUES (4, 'Навык4', 'Навык4', 1, 1, 2, null, null, false);
INSERT INTO profile.questionnaire_criterion (id, name, description, criterion_type_id, criterion_group_id, crm_user_id_created, crm_user_id_edited, edit_date, is_deleted)
VALUES (5, 'Навык5', 'Навык5', 1, 2, 2, null, null, false);
INSERT INTO profile.questionnaire_criterion (id, name, description, criterion_type_id, criterion_group_id, crm_user_id_created, crm_user_id_edited, edit_date, is_deleted)
VALUES (6, 'Навык6', 'Навык6', 1, 2, 2, null, null, false);
INSERT INTO profile.questionnaire_criterion (id, name, description, criterion_type_id, criterion_group_id, crm_user_id_created, crm_user_id_edited, edit_date, is_deleted)
VALUES (7, 'Характеристика1', 'Характеристика1', 2, 3, 2, null, null, false);
INSERT INTO profile.questionnaire_criterion (id, name, description, criterion_type_id, criterion_group_id, crm_user_id_created, crm_user_id_edited, edit_date, is_deleted)
VALUES (8, 'Характеристика2', 'Характеристика2', 2, 3, 2, null, null, false);
INSERT INTO profile.questionnaire_criterion (id, name, description, criterion_type_id, criterion_group_id, crm_user_id_created, crm_user_id_edited, edit_date, is_deleted)
VALUES (9, 'Характеристика3', 'Характеристика3', 2, 3, 2, null, null, false);
INSERT INTO profile.questionnaire_criterion (id, name, description, criterion_type_id, criterion_group_id, crm_user_id_created, crm_user_id_edited, edit_date, is_deleted)
VALUES (10, 'Характеристика4', 'Характеристика4', 2, 4, 2, null, null, false);
INSERT INTO profile.questionnaire_criterion (id, name, description, criterion_type_id, criterion_group_id, crm_user_id_created, crm_user_id_edited, edit_date, is_deleted)
VALUES (11, 'Характеристика5', 'Характеристика5', 2, 4, 2, null, null, false);
INSERT INTO profile.questionnaire_criterion (id, name, description, criterion_type_id, criterion_group_id, crm_user_id_created, crm_user_id_edited, edit_date, is_deleted)
VALUES (12, 'Характеристика6', 'Характеристика6', 2, 4, 2, null, null, false);

INSERT INTO profile.transcript (questionnaire_criterion_id, min_value, max_value, description, text_value, is_deleted)
VALUES (1, 1, 1, 'Описание балла 1 в Навыке1', 'Детализация балла 1 в Навыке1', false);
INSERT INTO profile.transcript (questionnaire_criterion_id, min_value, max_value, description, text_value, is_deleted)
VALUES (1, 2, 2, 'Описание балла 2 в Навыке1', 'Детализация балла 2 в Навыке1', false);
INSERT INTO profile.transcript (questionnaire_criterion_id, min_value, max_value, description, text_value, is_deleted)
VALUES (1, 3, 3, 'Описание балла 3 в Навыке1', 'Детализация балла 3 в Навыке1', false);
INSERT INTO profile.transcript (questionnaire_criterion_id, min_value, max_value, description, text_value, is_deleted)
VALUES (1, 4, 4, 'Описание балла 4 в Навыке1', 'Детализация балла 4 в Навыке1', false);
INSERT INTO profile.transcript (questionnaire_criterion_id, min_value, max_value, description, text_value, is_deleted)
VALUES (1, 5, 5, 'Описание балла 5 в Навыке1', 'Детализация балла 5 в Навыке1', false);

INSERT INTO profile.transcript (questionnaire_criterion_id, min_value, max_value, description, text_value, is_deleted)
VALUES (2, 1, 3, 'Описание баллов 1-3 в Навыке2', 'Детализация баллов 1-3 в Навыке2', false);
INSERT INTO profile.transcript (questionnaire_criterion_id, min_value, max_value, description, text_value, is_deleted)
VALUES (2, 4, 5, 'Описание баллов 4-5 в Навыке2', 'Детализация баллов 4-5 в Навыке2', false);

INSERT INTO profile.transcript (questionnaire_criterion_id, min_value, max_value, description, text_value, is_deleted)
VALUES (3, 1, 5, 'Описание баллов 1-5 в Навыке3', 'Детализация баллов 1-5 в Навыке3', false);

INSERT INTO profile.transcript (questionnaire_criterion_id, min_value, max_value, description, text_value, is_deleted)
VALUES (4, 1, 5, 'Описание баллов 1-5 в Навыке4', 'Детализация баллов 1-5 в Навыке4', false);

INSERT INTO profile.transcript (questionnaire_criterion_id, min_value, max_value, description, text_value, is_deleted)
VALUES (5, 1, 5, 'Описание баллов 1-5 в Навыке5', 'Детализация баллов 1-5 в Навыке5', false);

INSERT INTO profile.transcript (questionnaire_criterion_id, min_value, max_value, description, text_value, is_deleted)
VALUES (6, 1, 5, 'Описание баллов 1-5 в Навыке6', 'Детализация баллов 1-5 в Навыке6', false);

INSERT INTO profile.transcript (questionnaire_criterion_id, min_value, max_value, description, text_value, is_deleted)
VALUES (7, 1, 1, 'Описание балла 1 в Характеристике1', 'Детализация балла 1 в Характеристике1', false);
INSERT INTO profile.transcript (questionnaire_criterion_id, min_value, max_value, description, text_value, is_deleted)
VALUES (7, 2, 2, 'Описание балла 2 в Характеристике1', 'Детализация балла 2 в Характеристике1', false);
INSERT INTO profile.transcript (questionnaire_criterion_id, min_value, max_value, description, text_value, is_deleted)
VALUES (7, 3, 3, 'Описание балла 3 в Характеристике1', 'Детализация балла 3 в Характеристике1', false);

INSERT INTO profile.transcript (questionnaire_criterion_id, min_value, max_value, description, text_value, is_deleted)
VALUES (8, 1, 1, 'Описание балла 1 в Характеристике2', 'Детализация балла 1 в Характеристике2', false);
INSERT INTO profile.transcript (questionnaire_criterion_id, min_value, max_value, description, text_value, is_deleted)
VALUES (8, 2, 2, 'Описание балла 2 в Характеристике2', 'Детализация балла 2 в Характеристике2', false);
INSERT INTO profile.transcript (questionnaire_criterion_id, min_value, max_value, description, text_value, is_deleted)
VALUES (8, 3, 3, 'Описание балла 3 в Характеристике2', 'Детализация балла 3 в Характеристике2', false);

INSERT INTO profile.transcript (questionnaire_criterion_id, min_value, max_value, description, text_value, is_deleted)
VALUES (9, 1, 5, 'Описание баллов 1-3 в Характеристике3', 'Детализация баллов 1-3 в Характеристике3', false);

INSERT INTO profile.transcript (questionnaire_criterion_id, min_value, max_value, description, text_value, is_deleted)
VALUES (10, 1, 5, 'Описание баллов 1-3 в Характеристике4', 'Детализация баллов 1-3 в Характеристике4', false);

INSERT INTO profile.transcript (questionnaire_criterion_id, min_value, max_value, description, text_value, is_deleted)
VALUES (11, 1, 5, 'Описание баллов 1-3 в Характеристике5', 'Детализация баллов 1-3 в Характеристике5', false);

INSERT INTO profile.transcript (questionnaire_criterion_id, min_value, max_value, description, text_value, is_deleted)
VALUES (12, 1, 5, 'Описание баллов 1-3 в Характеристике6', 'Детализация баллов 1-3 в Характеристике6', false);


INSERT INTO profile.questionnaire_criterion_instance (id, questionnaire_instance_id, name, max_grade, grade, criterion_type_id, criterion_group_id, crm_user_id_edited, edit_date, questionaire_criterion_id, is_deleted)
VALUES (1, 1, 'Навык1', 5, 0, 1, 1, 2, null, 1, false);
INSERT INTO profile.questionnaire_criterion_instance (id, questionnaire_instance_id, name, max_grade, grade, criterion_type_id, criterion_group_id, crm_user_id_edited, edit_date, questionaire_criterion_id, is_deleted)
VALUES (2, 1, 'Навык2', 5, 0, 1, 1, 2, null, 2, false);
INSERT INTO profile.questionnaire_criterion_instance (id, questionnaire_instance_id, name, max_grade, grade, criterion_type_id, criterion_group_id, crm_user_id_edited, edit_date, questionaire_criterion_id, is_deleted)
VALUES (3, 1, 'Навык3', 5, 0, 1, 1, 2, null, 3, false);
INSERT INTO profile.questionnaire_criterion_instance (id, questionnaire_instance_id, name, max_grade, grade, criterion_type_id, criterion_group_id, crm_user_id_edited, edit_date, questionaire_criterion_id, is_deleted)
VALUES (4, 1, 'Навык4', 5, 0, 1, 1, 2, null, 4, false);
INSERT INTO profile.questionnaire_criterion_instance (id, questionnaire_instance_id, name, max_grade, grade, criterion_type_id, criterion_group_id, crm_user_id_edited, edit_date, questionaire_criterion_id, is_deleted)
VALUES (5, 1, 'Навык5', 5, 0, 1, 2, 2, null, 5, false);
INSERT INTO profile.questionnaire_criterion_instance (id, questionnaire_instance_id, name, max_grade, grade, criterion_type_id, criterion_group_id, crm_user_id_edited, edit_date, questionaire_criterion_id, is_deleted)
VALUES (6, 1, 'Навык6', 5, 0, 1, 2, 2, null, 6, false);
INSERT INTO profile.questionnaire_criterion_instance (id, questionnaire_instance_id, name, max_grade, grade, criterion_type_id, criterion_group_id, crm_user_id_edited, edit_date, questionaire_criterion_id, is_deleted)
VALUES (7, 1, 'Характеристика1', 3, 0, 2, 3, 2, null, 7, false);
INSERT INTO profile.questionnaire_criterion_instance (id, questionnaire_instance_id, name, max_grade, grade, criterion_type_id, criterion_group_id, crm_user_id_edited, edit_date, questionaire_criterion_id, is_deleted)
VALUES (8, 1, 'Характеристика2', 3, 0, 2, 3, 2, null, 8, false);
INSERT INTO profile.questionnaire_criterion_instance (id, questionnaire_instance_id, name, max_grade, grade, criterion_type_id, criterion_group_id, crm_user_id_edited, edit_date, questionaire_criterion_id, is_deleted)
VALUES (9, 1, 'Характеристика3', 3, 0, 2, 3, 2, null, 9, false);
INSERT INTO profile.questionnaire_criterion_instance (id, questionnaire_instance_id, name, max_grade, grade, criterion_type_id, criterion_group_id, crm_user_id_edited, edit_date, questionaire_criterion_id, is_deleted)
VALUES (10, 1, 'Характеристика4', 3, 0, 2, 4, 2, null, 10, false);
INSERT INTO profile.questionnaire_criterion_instance (id, questionnaire_instance_id, name, max_grade, grade, criterion_type_id, criterion_group_id, crm_user_id_edited, edit_date, questionaire_criterion_id, is_deleted)
VALUES (11, 1, 'Характеристика5', 3, 0, 2, 4, 2, null, 11, false);
INSERT INTO profile.questionnaire_criterion_instance (id, questionnaire_instance_id, name, max_grade, grade, criterion_type_id, criterion_group_id, crm_user_id_edited, edit_date, questionaire_criterion_id, is_deleted)
VALUES (12, 1, 'Характеристика6', 3, 0, 2, 4, 2, null, 12, false);


INSERT INTO profile.questionnaire (id, name, is_default, is_deleted) VALUES (1, 'Шаблон анкеты', true, false);

INSERT INTO profile.questionnaire_criterion_to_questionnaire (id, questionnaire_criterion_id, questionnaire_id, is_deleted) VALUES (1, 1, 1, false);
INSERT INTO profile.questionnaire_criterion_to_questionnaire (id, questionnaire_criterion_id, questionnaire_id, is_deleted) VALUES (2, 2, 1, false);
INSERT INTO profile.questionnaire_criterion_to_questionnaire (id, questionnaire_criterion_id, questionnaire_id, is_deleted) VALUES (3, 3, 1, false);
INSERT INTO profile.questionnaire_criterion_to_questionnaire (id, questionnaire_criterion_id, questionnaire_id, is_deleted) VALUES (4, 4, 1, false);
INSERT INTO profile.questionnaire_criterion_to_questionnaire (id, questionnaire_criterion_id, questionnaire_id, is_deleted) VALUES (5, 5, 1, false);
INSERT INTO profile.questionnaire_criterion_to_questionnaire (id, questionnaire_criterion_id, questionnaire_id, is_deleted) VALUES (6, 6, 1, false);
INSERT INTO profile.questionnaire_criterion_to_questionnaire (id, questionnaire_criterion_id, questionnaire_id, is_deleted) VALUES (7, 7, 1, false);
INSERT INTO profile.questionnaire_criterion_to_questionnaire (id, questionnaire_criterion_id, questionnaire_id, is_deleted) VALUES (8, 8, 1, false);
INSERT INTO profile.questionnaire_criterion_to_questionnaire (id, questionnaire_criterion_id, questionnaire_id, is_deleted) VALUES (9, 9, 1, false);
INSERT INTO profile.questionnaire_criterion_to_questionnaire (id, questionnaire_criterion_id, questionnaire_id, is_deleted) VALUES (10, 10, 1, false);
INSERT INTO profile.questionnaire_criterion_to_questionnaire (id, questionnaire_criterion_id, questionnaire_id, is_deleted) VALUES (11, 11, 1, false);
INSERT INTO profile.questionnaire_criterion_to_questionnaire (id, questionnaire_criterion_id, questionnaire_id, is_deleted) VALUES (12, 12, 1, false);


INSERT INTO main.study_group (id, name, description, course_type, is_deleted) VALUES (5, 'Name4', 'Description4', null, false);
INSERT INTO main.study_group (id, name, description, course_type, is_deleted) VALUES (6, 'Name5', 'Description5', null, false);
INSERT INTO main.study_group (id, name, description, course_type, is_deleted) VALUES (7, 'Name6', 'Description6', null, false);
INSERT INTO main.study_group (id, name, description, course_type, is_deleted) VALUES (8, 'Name7', 'Description7', null, false);
INSERT INTO main.study_group (id, name, description, course_type, is_deleted) VALUES (9, 'Name8', 'Description8', null, false);
INSERT INTO main.study_group (id, name, description, course_type, is_deleted) VALUES (10, 'Name9', 'Description9', null, false);
INSERT INTO main.study_group (id, name, description, course_type, is_deleted) VALUES (11, 'Name10', 'Description10', null, false);
INSERT INTO main.study_group (id, name, description, course_type, is_deleted) VALUES (12, 'Name11', 'Description11', null, false);
INSERT INTO main.study_group (id, name, description, course_type, is_deleted) VALUES (13, 'Name12', 'Description12', null, false);
INSERT INTO main.study_group (id, name, description, course_type, is_deleted) VALUES (14, 'Name13', 'Description13', null, false);
INSERT INTO main.study_group (id, name, description, course_type, is_deleted) VALUES (15, 'Name14', 'Description14', null, false);
INSERT INTO main.study_group (id, name, description, course_type, is_deleted) VALUES (16, 'Name15', 'Description15', null, false);
INSERT INTO main.study_group (id, name, description, course_type, is_deleted) VALUES (17, 'Name16', 'Description16', null, false);
INSERT INTO main.study_group (id, name, description, course_type, is_deleted) VALUES (18, 'Name17', 'Description17', null, false);
INSERT INTO main.study_group (id, name, description, course_type, is_deleted) VALUES (19, 'Name18', 'Description18', null, false);
INSERT INTO main.study_group (id, name, description, course_type, is_deleted) VALUES (20, 'Name19', 'Description19', null, false);

INSERT INTO main.student (id, is_deleted, crm_user_id) VALUES (4, false, 6);
INSERT INTO main.student (id, is_deleted, crm_user_id) VALUES (5, false, 7);
INSERT INTO main.student (id, is_deleted, crm_user_id) VALUES (6, false, 8);
INSERT INTO main.student (id, is_deleted, crm_user_id) VALUES (7, false, 9);
INSERT INTO main.student (id, is_deleted, crm_user_id) VALUES (8, false, 10);
INSERT INTO main.student (id, is_deleted, crm_user_id) VALUES (9, false, 11);
INSERT INTO main.student (id, is_deleted, crm_user_id) VALUES (10, false, 12);
INSERT INTO main.student (id, is_deleted, crm_user_id) VALUES (11, false, 13);
INSERT INTO main.student (id, is_deleted, crm_user_id) VALUES (12, false, 14);
INSERT INTO main.student (id, is_deleted, crm_user_id) VALUES (13, false, 15);
INSERT INTO main.student (id, is_deleted, crm_user_id) VALUES (14, false, 16);
INSERT INTO main.student (id, is_deleted, crm_user_id) VALUES (15, false, 17);
INSERT INTO main.student (id, is_deleted, crm_user_id) VALUES (16, false, 18);
INSERT INTO main.student (id, is_deleted, crm_user_id) VALUES (17, false, 19);
INSERT INTO main.student (id, is_deleted, crm_user_id) VALUES (18, false, 20);
INSERT INTO main.student (id, is_deleted, crm_user_id) VALUES (19, false, 21);
INSERT INTO main.student (id, is_deleted, crm_user_id) VALUES (20, false, 22);

INSERT INTO main.student_to_group (student_id, study_group_id) VALUES (4, 2);
INSERT INTO main.student_to_group (student_id, study_group_id) VALUES (5, 2);
INSERT INTO main.student_to_group (student_id, study_group_id) VALUES (6, 2);
INSERT INTO main.student_to_group (student_id, study_group_id) VALUES (7, 2);
INSERT INTO main.student_to_group (student_id, study_group_id) VALUES (8, 2);
INSERT INTO main.student_to_group (student_id, study_group_id) VALUES (9, 2);
INSERT INTO main.student_to_group (student_id, study_group_id) VALUES (10, 2);
INSERT INTO main.student_to_group (student_id, study_group_id) VALUES (11, 2);
INSERT INTO main.student_to_group (student_id, study_group_id) VALUES (12, 2);
INSERT INTO main.student_to_group (student_id, study_group_id) VALUES (13, 2);
INSERT INTO main.student_to_group (student_id, study_group_id) VALUES (14, 2);
INSERT INTO main.student_to_group (student_id, study_group_id) VALUES (15, 2);
INSERT INTO main.student_to_group (student_id, study_group_id) VALUES (16, 2);
INSERT INTO main.student_to_group (student_id, study_group_id) VALUES (17, 2);
INSERT INTO main.student_to_group (student_id, study_group_id) VALUES (18, 2);
INSERT INTO main.student_to_group (student_id, study_group_id) VALUES (19, 2);
INSERT INTO main.student_to_group (student_id, study_group_id) VALUES (20, 2);

INSERT INTO lesson.lesson (id, study_group_id, lesson_date, room, topic, description, lesson_comment, token, token_expiration, is_deleted) VALUES (1, 1, '2017-04-22 01:54:43.919000', '0', 'Topic0', 'Description0', 'LessonComment0', 'Token0', '2017-04-22 01:54:43.919000', false);
INSERT INTO lesson.lesson (id, study_group_id, lesson_date, room, topic, description, lesson_comment, token, token_expiration, is_deleted) VALUES (5, 5, '2017-04-22 01:54:44.356000', '4', 'Topic4', 'Description4', 'LessonComment4', 'Token4', '2017-04-22 01:54:44.356000', false);
INSERT INTO lesson.lesson (id, study_group_id, lesson_date, room, topic, description, lesson_comment, token, token_expiration, is_deleted) VALUES (6, 6, '2017-04-22 01:54:44.433000', '5', 'Topic5', 'Description5', 'LessonComment5', 'Token5', '2017-04-22 01:54:44.433000', false);
INSERT INTO lesson.lesson (id, study_group_id, lesson_date, room, topic, description, lesson_comment, token, token_expiration, is_deleted) VALUES (7, 7, '2017-04-22 01:54:44.483000', '6', 'Topic6', 'Description6', 'LessonComment6', 'Token6', '2017-04-22 01:54:44.483000', false);
INSERT INTO lesson.lesson (id, study_group_id, lesson_date, room, topic, description, lesson_comment, token, token_expiration, is_deleted) VALUES (8, 8, '2017-04-22 01:54:44.528000', '7', 'Topic7', 'Description7', 'LessonComment7', 'Token7', '2017-04-22 01:54:44.528000', false);
INSERT INTO lesson.lesson (id, study_group_id, lesson_date, room, topic, description, lesson_comment, token, token_expiration, is_deleted) VALUES (9, 9, '2017-04-22 01:54:44.543000', '8', 'Topic8', 'Description8', 'LessonComment8', 'Token8', '2017-04-22 01:54:44.543000', false);
INSERT INTO lesson.lesson (id, study_group_id, lesson_date, room, topic, description, lesson_comment, token, token_expiration, is_deleted) VALUES (10, 10, '2017-04-22 01:54:44.563000', '9', 'Topic9', 'Description9', 'LessonComment9', 'Token9', '2017-04-22 01:54:44.563000', false);
INSERT INTO lesson.lesson (id, study_group_id, lesson_date, room, topic, description, lesson_comment, token, token_expiration, is_deleted) VALUES (11, 11, '2017-04-22 01:54:44.608000', '10', 'Topic10', 'Description10', 'LessonComment10', 'Token10', '2017-04-22 01:54:44.608000', false);
INSERT INTO lesson.lesson (id, study_group_id, lesson_date, room, topic, description, lesson_comment, token, token_expiration, is_deleted) VALUES (12, 12, '2017-04-22 01:54:44.645000', '11', 'Topic11', 'Description11', 'LessonComment11', 'Token11', '2017-04-22 01:54:44.645000', false);
INSERT INTO lesson.lesson (id, study_group_id, lesson_date, room, topic, description, lesson_comment, token, token_expiration, is_deleted) VALUES (13, 13, '2017-04-22 01:54:44.677000', '12', 'Topic12', 'Description12', 'LessonComment12', 'Token12', '2017-04-22 01:54:44.677000', false);
INSERT INTO lesson.lesson (id, study_group_id, lesson_date, room, topic, description, lesson_comment, token, token_expiration, is_deleted) VALUES (14, 14, '2017-04-22 01:54:44.717000', '13', 'Topic13', 'Description13', 'LessonComment13', 'Token13', '2017-04-22 01:54:44.717000', false);
INSERT INTO lesson.lesson (id, study_group_id, lesson_date, room, topic, description, lesson_comment, token, token_expiration, is_deleted) VALUES (15, 15, '2017-04-22 01:54:44.745000', '14', 'Topic14', 'Description14', 'LessonComment14', 'Token14', '2017-04-22 01:54:44.745000', false);
INSERT INTO lesson.lesson (id, study_group_id, lesson_date, room, topic, description, lesson_comment, token, token_expiration, is_deleted) VALUES (16, 16, '2017-04-22 01:54:44.778000', '15', 'Topic15', 'Description15', 'LessonComment15', 'Token15', '2017-04-22 01:54:44.778000', false);
INSERT INTO lesson.lesson (id, study_group_id, lesson_date, room, topic, description, lesson_comment, token, token_expiration, is_deleted) VALUES (17, 17, '2017-04-22 01:54:44.802000', '16', 'Topic16', 'Description16', 'LessonComment16', 'Token16', '2017-04-22 01:54:44.802000', false);
INSERT INTO lesson.lesson (id, study_group_id, lesson_date, room, topic, description, lesson_comment, token, token_expiration, is_deleted) VALUES (18, 18, '2017-04-22 01:54:44.833000', '17', 'Topic17', 'Description17', 'LessonComment17', 'Token17', '2017-04-22 01:54:44.833000', false);
INSERT INTO lesson.lesson (id, study_group_id, lesson_date, room, topic, description, lesson_comment, token, token_expiration, is_deleted) VALUES (19, 19, '2017-04-22 01:54:44.858000', '18', 'Topic18', 'Description18', 'LessonComment18', 'Token18', '2017-04-22 01:54:44.858000', false);
INSERT INTO lesson.lesson (id, study_group_id, lesson_date, room, topic, description, lesson_comment, token, token_expiration, is_deleted) VALUES (20, 20, '2017-04-22 01:54:44.891000', '19', 'Topic19', 'Description19', 'LessonComment19', 'Token19', '2017-04-22 01:54:44.891000', false);
INSERT INTO lesson.lesson (id, study_group_id, lesson_date, room, topic, description, lesson_comment, token, token_expiration, is_deleted) VALUES (2, 2, '2017-04-22 01:54:44.191000', '1', 'Topic1', 'Это свойство платформы (например, операционной системы, виртуальной машины и т. д.) или приложения, состоящее в том, что процесс, порождённый в операционной системе, может состоять из нескольких потоков, выполняющихся «параллельно», то есть без предписанного порядка во времени. При выполнении некоторых задач такое разделение может достичь более эффективного использования ресурсов вычислительной машины.', 'Тест комментария', 'LI41U', '2017-04-24 15:09:50.754000', false);
INSERT INTO lesson.lesson (id, study_group_id, lesson_date, room, topic, description, lesson_comment, token, token_expiration, is_deleted) VALUES (3, 3, '2017-04-22 01:54:44.225000', '2', 'Topic2', 'Description2', 'LessonComment2', 'Ktvn5', '2017-04-24 02:50:21.188000', false);
INSERT INTO lesson.lesson (id, study_group_id, lesson_date, room, topic, description, lesson_comment, token, token_expiration, is_deleted) VALUES (4, 4, '2017-04-22 01:54:44.286000', '3', 'Topic3', 'Description3', 'LessonComment3', 'mJ83J', '2017-04-24 03:09:58.117000', false);

INSERT INTO lesson.journal (id, lesson_id, student_id, time_check, is_deleted) VALUES (1, 1, 1, '2017-04-22 01:54:44.058000', false);
INSERT INTO lesson.journal (id, lesson_id, student_id, time_check, is_deleted) VALUES (3, 3, 3, '2017-04-22 01:54:44.238000', false);
INSERT INTO lesson.journal (id, lesson_id, student_id, time_check, is_deleted) VALUES (5, 5, 5, '2017-04-22 01:54:44.377000', false);
INSERT INTO lesson.journal (id, lesson_id, student_id, time_check, is_deleted) VALUES (6, 6, 6, '2017-04-22 01:54:44.439000', false);
INSERT INTO lesson.journal (id, lesson_id, student_id, time_check, is_deleted) VALUES (8, 8, 8, '2017-04-22 01:54:44.531000', false);
INSERT INTO lesson.journal (id, lesson_id, student_id, time_check, is_deleted) VALUES (9, 9, 9, '2017-04-22 01:54:44.547000', false);
INSERT INTO lesson.journal (id, lesson_id, student_id, time_check, is_deleted) VALUES (10, 10, 10, '2017-04-22 01:54:44.568000', false);
INSERT INTO lesson.journal (id, lesson_id, student_id, time_check, is_deleted) VALUES (11, 11, 11, '2017-04-22 01:54:44.612000', false);
INSERT INTO lesson.journal (id, lesson_id, student_id, time_check, is_deleted) VALUES (12, 12, 12, '2017-04-22 01:54:44.648000', false);
INSERT INTO lesson.journal (id, lesson_id, student_id, time_check, is_deleted) VALUES (13, 13, 13, '2017-04-22 01:54:44.681000', false);
INSERT INTO lesson.journal (id, lesson_id, student_id, time_check, is_deleted) VALUES (14, 14, 14, '2017-04-22 01:54:44.720000', false);
INSERT INTO lesson.journal (id, lesson_id, student_id, time_check, is_deleted) VALUES (15, 15, 15, '2017-04-22 01:54:44.748000', false);
INSERT INTO lesson.journal (id, lesson_id, student_id, time_check, is_deleted) VALUES (16, 16, 16, '2017-04-22 01:54:44.782000', false);
INSERT INTO lesson.journal (id, lesson_id, student_id, time_check, is_deleted) VALUES (17, 17, 17, '2017-04-22 01:54:44.811000', false);
INSERT INTO lesson.journal (id, lesson_id, student_id, time_check, is_deleted) VALUES (18, 18, 18, '2017-04-22 01:54:44.836000', false);
INSERT INTO lesson.journal (id, lesson_id, student_id, time_check, is_deleted) VALUES (19, 19, 19, '2017-04-22 01:54:44.868000', false);
INSERT INTO lesson.journal (id, lesson_id, student_id, time_check, is_deleted) VALUES (20, 20, 20, '2017-04-22 01:54:44.900000', false);
INSERT INTO lesson.journal (id, lesson_id, student_id, time_check, is_deleted) VALUES (4, 2, 4, '2017-04-22 01:54:44.339000', false);
INSERT INTO lesson.journal (id, lesson_id, student_id, time_check, is_deleted) VALUES (7, 2, 7, '2017-04-22 01:54:44.487000', false);
INSERT INTO lesson.journal (id, lesson_id, student_id, time_check, is_deleted) VALUES (2, 2, 2, '2017-04-24 14:33:05.753000', false);
INSERT INTO lesson.journal (id, lesson_id, student_id, time_check, is_deleted) VALUES (21, 2, 10, '2017-04-24 14:34:19.395000', false);
INSERT INTO lesson.journal (id, lesson_id, student_id, time_check, is_deleted) VALUES (22, 2, 18, '2017-04-24 14:41:58.035000', false);
INSERT INTO lesson.journal (id, lesson_id, student_id, time_check, is_deleted) VALUES (23, 2, 16, '2017-04-24 14:44:59.499000', false);
INSERT INTO lesson.journal (id, lesson_id, student_id, time_check, is_deleted) VALUES (24, 2, 5, '2017-04-24 15:07:28.737000', false);

INSERT INTO task.criterion (id, title, max_points, is_additional, created, is_deleted) VALUES (1, 'Работоспособность кода', 10, false, '2017-04-22 15:41:41.281084', false);
INSERT INTO task.criterion (id, title, max_points, is_additional, created, is_deleted) VALUES (2, 'Соблюдение сроков сдачи', 10, false, '2017-04-22 15:41:41.302525', false);
INSERT INTO task.criterion (id, title, max_points, is_additional, created, is_deleted) VALUES (3, 'Наличие javadoc', 10, false, '2017-04-22 15:41:41.325539', false);
INSERT INTO task.criterion (id, title, max_points, is_additional, created, is_deleted) VALUES (4, 'Использование ООП', 10, false, '2017-04-22 15:41:41.355104', false);
INSERT INTO task.criterion (id, title, max_points, is_additional, created, is_deleted) VALUES (5, 'Отслеживание остановки потока', 10, false, '2017-04-22 15:41:41.374902', false);
INSERT INTO task.criterion (id, title, max_points, is_additional, created, is_deleted) VALUES (6, 'Отсутствие data racing', 10, false, '2017-04-22 15:41:41.393374', false);
INSERT INTO task.criterion (id, title, max_points, is_additional, created, is_deleted) VALUES (7, 'Корректность по модели памяти', 10, false, '2017-04-22 15:41:41.412849', false);

INSERT INTO task.task (id, title, description, is_lab, created, end_date) VALUES (1, 'Лабораторная 1', 'Необходимо разработать программу, которая получает на вход список ресурсов,
 содержащих текст, и считает общее количество вхождений (для всех ресурсов)
 каждого слова. Каждый ресурс должен быть обработан в отдельном потоке,
 текст не должен содержать инностранных символов, только кириллица,
 знаки препинания и цифры. Отчет должен строиться в режиме реального времени, знаки препинания и цифры в отчет не входят.
 Все ошибки должны быть корректно обработаны, все API покрыто модульными тестами', true, '2017-04-22 15:35:48.156862','2017-04-28 16:08:38.087000');
INSERT INTO task.task (id, title, description, is_lab, created, end_date) VALUES (2, 'Практика Multithreading', 'Написать программу для вычисления многочлена с использованием собственного семафора.', false, '2017-04-23 16:08:38.087000', '2017-04-28 16:08:38.087000');
INSERT INTO task.task (id, title, description, is_lab, created, end_date) VALUES (3, 'Лабораторная 2', 'Задание на лабораторную работу', true, '2017-04-23 19:04:22.375000', '2017-04-28 16:08:38.087000');
INSERT INTO task.task (id, title, description, is_lab, created, end_date) VALUES (4, 'tgtgt', 'rgfrgtg', true, '2017-04-24 20:05:18.109932', '2017-04-28 16:08:38.087000');
INSERT INTO task.task (id, title, description, is_lab, created, end_date) VALUES (5, 'и еще', 'лпка', true, '2017-04-26 13:42:56.404816','2017-04-28 16:08:38.087000');
INSERT INTO task.task (id, title, description, is_lab, created, end_date) VALUES (6, 'раолвоэпд', 'лтвдптклдтпдлтк', true, '2017-04-26 14:02:28.365371','2017-04-28 16:08:38.087000');

INSERT INTO task.criterion_to_task (id, task_id, criterion_id) VALUES (1, 1, 1);
INSERT INTO task.criterion_to_task (id, task_id, criterion_id) VALUES (2, 1, 2);
INSERT INTO task.criterion_to_task (id, task_id, criterion_id) VALUES (3, 1, 3);
INSERT INTO task.criterion_to_task (id, task_id, criterion_id) VALUES (4, 1, 4);
INSERT INTO task.criterion_to_task (id, task_id, criterion_id) VALUES (5, 1, 5);
INSERT INTO task.criterion_to_task (id, task_id, criterion_id) VALUES (6, 1, 6);
INSERT INTO task.criterion_to_task (id, task_id, criterion_id) VALUES (7, 1, 7);
INSERT INTO task.criterion_to_task (id, task_id, criterion_id) VALUES (8, 2, 1);
INSERT INTO task.criterion_to_task (id, task_id, criterion_id) VALUES (9, 2, 2);
INSERT INTO task.criterion_to_task (id, task_id, criterion_id) VALUES (10, 2, 3);
INSERT INTO task.criterion_to_task (id, task_id, criterion_id) VALUES (11, 2, 4);

INSERT INTO task.task_to_group (id, task_id, study_group_id) VALUES (3, 1, 1);
INSERT INTO task.task_to_group (id, task_id, study_group_id) VALUES (4, 2, 1);
INSERT INTO task.task_to_group (id, task_id, study_group_id) VALUES (5, 3, 1);

INSERT INTO task.mark (id, is_deleted, student_id, task_id, criterion_id, points) VALUES (1, false, 1, 1, 1, 12);
INSERT INTO task.mark (id, is_deleted, student_id, task_id, criterion_id, points) VALUES (2, false, 1, 1, 2, 10);
INSERT INTO task.mark (id, is_deleted, student_id, task_id, criterion_id, points) VALUES (7, false, 2, 1, 2, 4);
INSERT INTO task.mark (id, is_deleted, student_id, task_id, criterion_id, points) VALUES (44, false, 2, 1, 7, 1);
INSERT INTO task.mark (id, is_deleted, student_id, task_id, criterion_id, points) VALUES (43, false, 2, 1, 6, 10);
INSERT INTO task.mark (id, is_deleted, student_id, task_id, criterion_id, points) VALUES (45, false, 1, 1, 4, 4);
INSERT INTO task.mark (id, is_deleted, student_id, task_id, criterion_id, points) VALUES (42, false, 1, 1, 6, 10);
INSERT INTO task.mark (id, is_deleted, student_id, task_id, criterion_id, points) VALUES (41, false, 1, 1, 7, 4);
INSERT INTO task.mark (id, is_deleted, student_id, task_id, criterion_id, points) VALUES (11, false, 1, 1, 5, 9);
INSERT INTO task.mark (id, is_deleted, student_id, task_id, criterion_id, points) VALUES (8, false, 2, 1, 3, 6);
INSERT INTO task.mark (id, is_deleted, student_id, task_id, criterion_id, points) VALUES (9, false, 2, 1, 4, 10);
INSERT INTO task.mark (id, is_deleted, student_id, task_id, criterion_id, points) VALUES (3, false, 1, 1, 3, 10);
INSERT INTO task.mark (id, is_deleted, student_id, task_id, criterion_id, points) VALUES (10, false, 1, 1, 5, 3);


INSERT INTO task.download (student_id, task_id, git_url, download_date, download_file_name, download_url)
VALUES (1, 1, 'git1', '23.01.17', 'file1', 'downloadurl1');
INSERT INTO task.download (student_id, task_id, git_url, download_date, download_file_name, download_url)
VALUES (2, 2, 'git2', '23.01.17', 'file2', 'downloadurl2');
INSERT INTO task.download (student_id, task_id, git_url, download_date, download_file_name, download_url)
VALUES (2, 1, 'git1', '23.01.17', 'file3', 'downloadurl3');


INSERT INTO main.role_permission (role_id, resource_id, operation_id) VALUES (1, 1, 1);
INSERT INTO main.role_permission (role_id, resource_id, operation_id) VALUES (1, 1, 2);
INSERT INTO main.role_permission (role_id, resource_id, operation_id) VALUES (1, 2, 1);
INSERT INTO main.role_permission (role_id, resource_id, operation_id) VALUES (1, 2, 2);
INSERT INTO main.role_permission (role_id, resource_id, operation_id) VALUES (1, 3, 1);
INSERT INTO main.role_permission (role_id, resource_id, operation_id) VALUES (1, 3, 2);
INSERT INTO main.role_permission (role_id, resource_id, operation_id) VALUES (1, 4, 1);
INSERT INTO main.role_permission (role_id, resource_id, operation_id) VALUES (1, 4, 2);
INSERT INTO main.role_permission (role_id, resource_id, operation_id) VALUES (1, 5, 1);
INSERT INTO main.role_permission (role_id, resource_id, operation_id) VALUES (1, 5, 2);
INSERT INTO main.role_permission (role_id, resource_id, operation_id) VALUES (1, 6, 1);
INSERT INTO main.role_permission (role_id, resource_id, operation_id) VALUES (1, 6, 2);
INSERT INTO main.role_permission (role_id, resource_id, operation_id) VALUES (1, 7, 1);
INSERT INTO main.role_permission (role_id, resource_id, operation_id) VALUES (1, 7, 2);
INSERT INTO main.role_permission (role_id, resource_id, operation_id) VALUES (1, 9, 1);
INSERT INTO main.role_permission (role_id, resource_id, operation_id) VALUES (1, 9, 2);
INSERT INTO main.role_permission (role_id, resource_id, operation_id) VALUES (1, 10, 1);
INSERT INTO main.role_permission (role_id, resource_id, operation_id) VALUES (1, 10, 2);
INSERT INTO main.role_permission (role_id, resource_id, operation_id) VALUES (1, 11, 1);
INSERT INTO main.role_permission (role_id, resource_id, operation_id) VALUES (1, 11, 2);
INSERT INTO main.role_permission (role_id, resource_id, operation_id) VALUES (1, 12, 1);
INSERT INTO main.role_permission (role_id, resource_id, operation_id) VALUES (1, 12, 2);
INSERT INTO main.role_permission (role_id, resource_id, operation_id) VALUES (1, 13, 1);
INSERT INTO main.role_permission (role_id, resource_id, operation_id) VALUES (1, 13, 2);
INSERT INTO main.role_permission (role_id, resource_id, operation_id) VALUES (2, 2, 1);
INSERT INTO main.role_permission (role_id, resource_id, operation_id) VALUES (2, 4, 1);
INSERT INTO main.role_permission (role_id, resource_id, operation_id) VALUES (2, 8, 1);
INSERT INTO main.role_permission (role_id, resource_id, operation_id) VALUES (3, 10, 1);
INSERT INTO main.role_permission (role_id, resource_id, operation_id) VALUES (3, 11, 1);
INSERT INTO main.role_permission (role_id, resource_id, operation_id) VALUES (3, 12, 1);
INSERT INTO main.role_permission (role_id, resource_id, operation_id) VALUES (3, 13, 1);
INSERT INTO main.role_permission (role_id, resource_id, operation_id) VALUES (3, 13, 2);
INSERT INTO main.role_permission (role_id, resource_id, operation_id) VALUES (4, 1, 1);
INSERT INTO main.role_permission (role_id, resource_id, operation_id) VALUES (4, 2, 1);
INSERT INTO main.role_permission (role_id, resource_id, operation_id) VALUES (4, 3, 1);
INSERT INTO main.role_permission (role_id, resource_id, operation_id) VALUES (4, 4, 1);
INSERT INTO main.role_permission (role_id, resource_id, operation_id) VALUES (4, 5, 1);
INSERT INTO main.role_permission (role_id, resource_id, operation_id) VALUES (4, 6, 1);
INSERT INTO main.role_permission (role_id, resource_id, operation_id) VALUES (4, 7, 1);
INSERT INTO main.role_permission (role_id, resource_id, operation_id) VALUES (4, 9, 1);
INSERT INTO main.role_permission (role_id, resource_id, operation_id) VALUES (4, 10, 1);
INSERT INTO main.role_permission (role_id, resource_id, operation_id) VALUES (4, 11, 1);
INSERT INTO main.role_permission (role_id, resource_id, operation_id) VALUES (4, 12, 1);
INSERT INTO main.role_permission (role_id, resource_id, operation_id) VALUES (4, 13, 1);
INSERT INTO main.role_permission (role_id, resource_id, operation_id) VALUES (5, 2, 1);
INSERT INTO main.role_permission (role_id, resource_id, operation_id) VALUES (5, 2, 2);
INSERT INTO main.role_permission (role_id, resource_id, operation_id) VALUES (5, 3, 1);
INSERT INTO main.role_permission (role_id, resource_id, operation_id) VALUES (5, 3, 2);
INSERT INTO main.role_permission (role_id, resource_id, operation_id) VALUES (5, 4, 1);
INSERT INTO main.role_permission (role_id, resource_id, operation_id) VALUES (5, 4, 2);
INSERT INTO main.role_permission (role_id, resource_id, operation_id) VALUES (5, 5, 1);
INSERT INTO main.role_permission (role_id, resource_id, operation_id) VALUES (5, 5, 2);
INSERT INTO main.role_permission (role_id, resource_id, operation_id) VALUES (5, 6, 1);
INSERT INTO main.role_permission (role_id, resource_id, operation_id) VALUES (5, 6, 2);
INSERT INTO main.role_permission (role_id, resource_id, operation_id) VALUES (5, 7, 1);
INSERT INTO main.role_permission (role_id, resource_id, operation_id) VALUES (5, 7, 2);
INSERT INTO main.role_permission (role_id, resource_id, operation_id) VALUES (5, 9, 1);
INSERT INTO main.role_permission (role_id, resource_id, operation_id) VALUES (5, 9, 2);
INSERT INTO main.role_permission (role_id, resource_id, operation_id) VALUES (5, 10, 1);
INSERT INTO main.role_permission (role_id, resource_id, operation_id) VALUES (5, 10, 2);
INSERT INTO main.role_permission (role_id, resource_id, operation_id) VALUES (5, 11, 1);
INSERT INTO main.role_permission (role_id, resource_id, operation_id) VALUES (5, 11, 2);
INSERT INTO main.role_permission (role_id, resource_id, operation_id) VALUES (5, 12, 1);
INSERT INTO main.role_permission (role_id, resource_id, operation_id) VALUES (5, 12, 2);

SELECT SETVAL('profile.criterion_group_id_seq', COALESCE(MAX(id), 1) ) FROM profile.criterion_group;
SELECT SETVAL('task.criterion_id_seq', COALESCE(MAX(id), 1) ) FROM task.criterion;
SELECT SETVAL('task.criterion_to_task_id_seq', COALESCE(MAX(id), 1) ) FROM task.criterion_to_task;
SELECT SETVAL('profile.criterion_type_id_seq', COALESCE(MAX(id), 1) ) FROM profile.criterion_type;
SELECT SETVAL('main.crm_user_id_seq', COALESCE(MAX(id), 1) ) FROM main.crm_user;
SELECT SETVAL('task.download_id_seq', COALESCE(MAX(id), 1) ) FROM task.download;
SELECT SETVAL('lesson.journal_id_seq', COALESCE(MAX(id), 1) ) FROM lesson.journal;
SELECT SETVAL('lesson.lesson_id_seq', COALESCE(MAX(id), 1) ) FROM lesson.lesson;
SELECT SETVAL('task.mark_id_seq', COALESCE(MAX(id), 1) ) FROM task.mark;
SELECT SETVAL('main.operation_id_seq', COALESCE(MAX(id), 1) ) FROM main.operation;
SELECT SETVAL('main.operation_resource_id_seq', COALESCE(MAX(id), 1) ) FROM main.operation_resource;
SELECT SETVAL('profile.questionnaire_criterion_id_seq', COALESCE(MAX(id), 1) ) FROM profile.questionnaire_criterion;
SELECT SETVAL('profile.questionnaire_criterion_instance_id_seq', COALESCE(MAX(id), 1) ) FROM profile.questionnaire_criterion_instance;
SELECT SETVAL('profile.questionnaire_criterion_to_questionnaire_id_seq', COALESCE(MAX(id), 1) ) FROM profile.questionnaire_criterion_to_questionnaire;
SELECT SETVAL('profile.questionnaire_id_seq', COALESCE(MAX(id), 1) ) FROM profile.questionnaire;
SELECT SETVAL('profile.questionnaire_instance_id_seq', COALESCE(MAX(id), 1) ) FROM profile.questionnaire_instance;
SELECT SETVAL('main.resource_id_seq', COALESCE(MAX(id), 1) ) FROM main.resource;
SELECT SETVAL('main.role_id_seq', COALESCE(MAX(id), 1) ) FROM main.role;
SELECT SETVAL('main.role_resource_id_seq', COALESCE(MAX(id), 1) ) FROM main.role_resource;
SELECT SETVAL('main.settings_id_seq', COALESCE(MAX(id), 1) ) FROM main.settings;
SELECT SETVAL('lesson.student_activity_id_seq', COALESCE(MAX(id), 1) ) FROM lesson.student_activity;
SELECT SETVAL('main.student_id_seq', COALESCE(MAX(id), 1) ) FROM main.student;
SELECT SETVAL('main.student_to_group_id_seq', COALESCE(MAX(id), 1) ) FROM main.student_to_group;
SELECT SETVAL('main.study_group_id_seq', COALESCE(MAX(id), 1) ) FROM main.study_group;
SELECT SETVAL('task.task_id_seq', COALESCE(MAX(id), 1) ) FROM task.task;
SELECT SETVAL('task.task_to_group_id_seq', COALESCE(MAX(id), 1) ) FROM task.task_to_group;
SELECT SETVAL('profile.transcript_id_seq', COALESCE(MAX(id), 1) ) FROM profile.transcript;
SELECT SETVAL('main.user_role_id_seq', COALESCE(MAX(id), 1) ) FROM main.user_role;