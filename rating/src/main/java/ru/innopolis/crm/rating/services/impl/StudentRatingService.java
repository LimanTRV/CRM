package ru.innopolis.crm.rating.services.impl;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.innopolis.crm.main.utils.InjectLogger;
import ru.innopolis.crm.rating.exceptions.StudentRatingDaoException;
import ru.innopolis.crm.rating.exceptions.StudentRatingServiceException;
import ru.innopolis.crm.rating.models.dao.StudentRatingDaoImpl;
import ru.innopolis.crm.rating.models.dao.interfaces.StudentRatingDao;
import ru.innopolis.crm.rating.services.RatingServiceInterface;
import java.sql.Date;

/**
 * Service compute attendance rating of students.
 */
@Service
public class StudentRatingService implements RatingServiceInterface  {

    @InjectLogger(StudentRatingService.class)
    private Logger logger;

    private StudentRatingDao ratingDao;

    @Autowired
    public void setRatingDao(StudentRatingDao ratingDao) {
        this.ratingDao = ratingDao;
    }

    @Override
    public int[][] getAttendanceRatingInPercentsForAllStudentsInGroupLimitedByPeriod(long groupId, Date from, Date to) throws StudentRatingServiceException {
        try {
            return ratingDao.getAttendanceRatingInPercentsForAllStudentsInGroupLimitedByPeriod(groupId, from, to);
        } catch (StudentRatingDaoException e) {
            logger.error("problem with getting attendance rating");
            throw new StudentRatingServiceException();
        }
    }

    @Override
    public int[][] getStudentActivityRating(long groupId, Date from, Date to) throws StudentRatingServiceException {
        try {
            return ratingDao.getStudentActivityRating(groupId, from, to);
        } catch (StudentRatingDaoException e) {
            logger.error("problem with getting activity rating");
            throw new StudentRatingServiceException();
        }
    }

    @Override
    public int[][] getStudentTaskRating(long groupId, Date from, Date to) throws StudentRatingServiceException {
        try {
            return ratingDao.getStudentTaskRating(groupId, from, to);
        } catch (StudentRatingDaoException e) {
            logger.error("problem with getting task rating");
            throw new StudentRatingServiceException();
        }
    }
}
