package ru.innopolis.crm.rating.services;


import ru.innopolis.crm.rating.exceptions.StudentRatingServiceException;
import java.sql.Date;

/**
 * Created by Di on 22.04.2017.
 */
public interface RatingServiceInterface {
    /**
     * Creates a two dimensional array of <code>int</code>
     * First element is id of student
     * And the second is his rating in percents
     * @param groupId a <code>long</code> id of group
     * @param from a starting <code>Date</code>
     * @param to an ending <code>Date</code>
     * @return 2d array with id and attendance rating
     */
    int[][] getAttendanceRatingInPercentsForAllStudentsInGroupLimitedByPeriod(long groupId, Date from, Date to) throws StudentRatingServiceException;

    /**
     * Creates a two dimensional array of <code>int</code>
     * First element is id of student
     * And the second is his rating in percents
     * @param groupId a <code>long</code> id of group
     * @param from a starting <code>Date</code>
     * @param to an ending <code>Date</code>
     * @return 2d array with id and activity rating
     */
    int[][] getStudentActivityRating(long groupId, Date from, Date to) throws StudentRatingServiceException;

    /**
     * Creates a two dimensional array of <code>int</code>
     * First element is id of student
     * And the second is his rating in percents
     * @param groupId a <code>long</code> id of group
     * @param from a starting <code>Date</code>
     * @param to an ending <code>Date</code>
     * @return 2d array with id and rating by task marks
     */
    int[][] getStudentTaskRating(long groupId, Date from, Date to) throws StudentRatingServiceException;
}
