package ru.innopolis.crm.rating.models.dao.interfaces;

import ru.innopolis.crm.rating.exceptions.StudentRatingDaoException;
import java.sql.Date;

/**
 * Created by Nemesis on 28.04.17.
 */
public interface StudentRatingDao {
    int[][] getAttendanceRatingInPercentsForAllStudentsInGroupLimitedByPeriod(long groupId, Date from, Date to) throws StudentRatingDaoException;
    int[][] getStudentActivityRating(long groupId, Date from, Date to) throws StudentRatingDaoException;
    int[][] getStudentTaskRating(long groupId, Date from, Date to) throws StudentRatingDaoException;
}
