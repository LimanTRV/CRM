package ru.innopolis.crm.rating.controllers;


import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.apache.log4j.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import ru.innopolis.crm.main.models.entity.StudyGroup;
import ru.innopolis.crm.main.services.StudentService;
import ru.innopolis.crm.main.utils.InjectLogger;
import ru.innopolis.crm.rating.exceptions.StudentRatingServiceException;
import ru.innopolis.crm.rating.services.RatingServiceInterface;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.sql.Date;

/**
 * Created by Di on 22.04.2017.
 */
@Controller
@PreAuthorize(value = "hasRole('R_E')")
@RequestMapping(value = "/rating/")
public class RatingController {

    @InjectLogger(RatingController.class)
    private Logger logger;

    private RatingServiceInterface service;
    private StudentService studentService;


    @Autowired
    public void setService(RatingServiceInterface service) {
        this.service = service;
    }

    @Autowired
    public void setStudentService(StudentService studentService) {
        this.studentService = studentService;
    }

    @RequestMapping(method = RequestMethod.GET)
    public String ratingPage(HttpServletRequest req, Model model) {
        StudyGroup group = (StudyGroup)req.getAttribute("currentGroup");
        if(group != null) {
            model.addAttribute("students", studentService.getStudentsByGroupId(group.getId()));
            model.addAttribute("group", group.getId());
        }
        return "rating/rating";
    }

    @RequestMapping(method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<String> getCertainRating(@RequestParam long groupId, @RequestParam String type,
                                 @RequestParam String from, @RequestParam String to, Model model) {
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        Date fromDate = null, toDate = null;
        try {
            fromDate = new Date(df.parse(from).getTime());
            toDate = new Date(df.parse(to).getTime());
        } catch (NullPointerException | ParseException e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Неправильно переданы параметры");
        }
        int[][] rating = null;
        try {
            switch (type) {
                case "attendance":
                    rating = service.getAttendanceRatingInPercentsForAllStudentsInGroupLimitedByPeriod(groupId, fromDate, toDate);
                    break;
                case "active":
                    rating = service.getStudentActivityRating(groupId, fromDate, toDate);
                    break;
                case "tasks":
                    rating = service.getStudentTaskRating(groupId, fromDate, toDate);
                    break;
                default:
                    return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Неправильно переданы параметры");
            }
        } catch (StudentRatingServiceException e) {
            logger.error("problem with rating service");
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Что-то пошло не так");
        }

        final Gson gson = new GsonBuilder().serializeNulls().create();
        return ResponseEntity.ok(gson.toJson(rating));
    }

    @ExceptionHandler(MissingServletRequestParameterException.class)
    public ResponseEntity<String> handleMissingParams(MissingServletRequestParameterException ex) {
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Неправильно переданы параметры");
    }

    private long getSelectedGroup(HttpServletRequest req) {
        String groupId = req.getParameter("group");
        if(groupId != null && !groupId.isEmpty()) {
            try {
                return Long.valueOf(groupId);
            } catch(NumberFormatException e) {
                logger.error("empty or wrong group id");
                return -1;
            }
        }
        Cookie[] cookies = req.getCookies();
        for(Cookie cookie : cookies) {
            if(cookie.getName().equals("group")) {
                try {
                    return Long.parseLong(cookie.getValue());
                } catch(NumberFormatException e) {
                    logger.error("group cookie wrong formatted");
                }
                break;
            }
        }
        return -1;
    }
}