package ru.innopolis.crm.rating.models.dao;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.stereotype.Repository;
import ru.innopolis.crm.main.utils.InjectLogger;
import ru.innopolis.crm.rating.exceptions.StudentRatingDaoException;
import ru.innopolis.crm.rating.models.dao.interfaces.StudentRatingDao;

import javax.sql.DataSource;
import java.sql.Date;

import java.sql.*;

/**
 * Created by Nemesis on 28.04.17.
 */
@Repository
public class StudentRatingDaoImpl implements StudentRatingDao {
    @InjectLogger(StudentRatingDaoImpl.class)
    private Logger logger;

    private DataSource dataSource;

    @Autowired
    public void setDataSource(DriverManagerDataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    public int[][] getAttendanceRatingInPercentsForAllStudentsInGroupLimitedByPeriod(long groupId, Date from, Date to) throws StudentRatingDaoException {
        try {
            return getRating("with lessons as (select id from lesson.lesson where study_group_id = ? " +
                    "and lesson_date BETWEEN ? and ? and is_deleted = false), " +
                    "cnt as (select count(lessons.id) c from lessons) " +
                    "select s.id sid, count(j.id)*100/cnt.c from main.student s " +
                    "left join lesson.journal j on j.student_id = s.id and j.is_deleted=false " +
                    "and j.lesson_id in (select id from lessons) " +
                    "left join lessons on lessons.id = j.lesson_id " +
                    "join cnt on cnt.c > 0 where s.is_deleted = false " +
                    "group by s.id, cnt.c", groupId, from, to);
        } catch (SQLException e) {
            logger.error(e.getMessage());
            throw new StudentRatingDaoException();
        }
    }

    @Override
    public int[][] getStudentActivityRating(long groupId, Date from, Date to) throws StudentRatingDaoException {
        try {
            return getRating("with lessons as (select id from lesson.lesson where study_group_id = ? " +
                    "and lesson_date BETWEEN ? and ? and is_deleted = false)," +
                    "cnt as (select count(lessons.id) c from lessons) " +
                    "select s.id sid, avg(a.activity_grade)::integer*10 from main.student s " +
                    "left join lesson.student_activity a on a.student_id = s.id and a.is_deleted=false " +
                    "and a.lesson_id in (select id from lessons) " +
                    "left join lessons on lessons.id = a.lesson_id " +
                    "join cnt on cnt.c > 0 where s.is_deleted = false group by s.id", groupId, from, to);
        } catch (SQLException e) {
            logger.error(e.getMessage());
            throw new StudentRatingDaoException();
        }
    }

    @Override
    public int[][] getStudentTaskRating(long groupId, Date from, Date to) throws StudentRatingDaoException {
        try {
            return getRating("with tasks as (select t.id, c.max_points from task.criterion c join " +
                    "task.criterion_to_task ct on ct.criterion_id = c.id join " +
                    "task.task t on ct.task_id = t.id join task.task_to_group ttg on ttg.task_id = t.id " +
                    "where ttg.study_group_id = ? and t.end_date BETWEEN ? and ?)," +
                    "cnt as (select sum(max_points) from tasks) " +
                    "select s.id, sum(m.points)*100/cnt.sum from main.student s left join task.mark m on " +
                    "m.student_id = s.id and m.task_id in (select id from tasks) " +
                    "join cnt on cnt.sum > 0 group by s.id, cnt.sum", groupId, from, to);
        } catch (SQLException e) {
            logger.error(e.getMessage());
            throw new StudentRatingDaoException();
        }
    }

    private int[][] getRating(String query, long groupId, Date from, Date to) throws SQLException {
        PreparedStatement statement = dataSource.getConnection().prepareStatement(query, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
        statement.setLong(1, groupId);
        statement.setDate(2, from);
        statement.setDate(3, to);
        ResultSet set = statement.executeQuery();
        return ratingFromSet(set);
    }

    private int[][] ratingFromSet(ResultSet set) throws SQLException {
        int i = 0;
        if(set.last()) {
            i = set.getRow();
            set.beforeFirst();
        } else return null;
        int[][] rating = new int[i][2];
        i = 0;
        while(set.next()) {
            rating[i][0] = set.getInt(1);
            rating[i++][1] = set.getInt(2);
        }
        return rating;
    }
}