<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>


<html>
<head>
    <title>Login</title>
</head>
<body>
<div id="wrapper" style="text-align: center; padding: 100px 100px 30px 100px">
    <h1>CRM INNOPOLIS IT TRAINING CENTER</h1>
    <form method="post" action="${pageContext.request.contextPath}/">
        <p><input type="text" name="login"/></p>
        <p><input type="password" name="password"/></p>
        <p><input type="submit" value="login"/></p>
        <input type="hidden" name="${_csrf.parameterName}"
               value="${_csrf.token}" />
    </form>
</div>
</body>

</html>
