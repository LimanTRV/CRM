<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<jsp:include page="header.jsp">
    <jsp:param name="title" value=""/>
</jsp:include>

<main class="container">
    <div class="z-depth" style="padding: 20px">
        <div style="margin-left: .75rem; margin-right: .75rem;">
            <h4>ПРОИЗОШЛА НЕПРЕДВИДЕННАЯ ОШИБКА. </h4>
            <h5>Мы уже работаем над ее устранением.</h5>
            <h5><a href="${prev}">Назад</a></h5>
            <h5><a href="${pageContext.request.contextPath}">На главную</a></h5>
        </div>
    </div>
</main>


<%@include file="footer.jsp"%>