﻿<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<sec:authentication var="user" property="principal"/>

<jsp:include page="../header.jsp">
    <jsp:param name="title" value="Список занятий"/>
</jsp:include>

<main class="container">
    <div class="z-depth">
        <div class="row">
            <div style="margin-left: .75rem; margin-right: .75rem">
                <c:if test="${user.studentId == 0 && requestScope.currentGroup == null}">
                    <span style="font-size: 2rem;">Выберите группу</span>
                </c:if>

                <c:if test="${user.studentId != 0 || requestScope.currentGroup != null}">
                    <table class="bordered">
                        <tr>
                            <th>Дата</th>
                            <th>Тема</th>
                            <sec:authorize access="hasRole('L_E')">
                                <th>Комментарий</th>
                            </sec:authorize>
                            <th></th>
                        </tr>
                        <c:forEach items="${requestScope.lessons}" var="lesson">
                            <tr>
                                <td><fmt:formatDate value="${lesson.lessonDate}" type="both"
                                                    dateStyle="long" timeStyle="short"/></td>
                                <td>
                                    <c:choose>
                                        <c:when test="${user.studentId == 0}">
                                            <a href="./view/?group=${requestScope.currentGroup.id}&id=<c:out value="${lesson.id}"/>"><c:out
                                                    value="${lesson.topic}"/></a>
                                        </c:when>
                                        <c:otherwise>
                                            <a href="./view/student/?id=<c:out value="${lesson.id}"/>"><c:out
                                                    value="${lesson.topic}"/></a>
                                        </c:otherwise>
                                    </c:choose>
                                </td>
                                <sec:authorize access="hasRole('L_E')">
                                    <td><c:out value="${lesson.lessonComment}"/></td>
                                    <td>
                                        <a class="dropdown-button right w30" href="#"
                                           data-activates="coll${lesson.id}"><i
                                                class="fa fa-ellipsis-v"></i></a>
                                        <ul id="coll${lesson.id}" class="dropdown-content">
                                            <li>
                                                <a href="./edit/?group=${requestScope.currentGroup.id}&id=<c:out value="${lesson.id}"/>">edit</a>
                                            </li>
                                            <li>
                                                <a href="./delete/?group=${requestScope.currentGroup.id}&id=<c:out value="${lesson.id}"/>">delete</a>
                                            </li>
                                        </ul>
                                    </td>
                                </sec:authorize>
                            </tr>
                        </c:forEach>
                    </table>
                </c:if>
            </div>
        </div>
    </div>
    <sec:authorize access="hasRole('L_E')">
        <a href="./new/?group=${requestScope.currentGroup.id}"
           class="btn-floating btn-large waves-effect waves-light indigo darken-3"><i
                class="fa fa-plus"></i></a>
    </sec:authorize>
</main>
<sec:authorize access="hasRole('L_E')">
    <script>
        $(function () {
            $(".w30").click(e => e.stopPropagation());
            $(".button-collapse").sideNav();
        });
    </script>
</sec:authorize>

<%@include file="../footer.jsp" %>