<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<jsp:include page="../header.jsp">
    <jsp:param name="title" value="Список занятий"/>
</jsp:include>

<script>
    /* Get today's year. */
    function getCurrentYear() {
        var today = new Date();
        return today.getFullYear();
    }

    /* Get today's month. 1 - is January. */
    function getCurrentMonth() {
        var today = new Date();
        return today.getMonth();
    }

    /* Get today's day of month. */
    function getCurrentDayOfMonth() {
        var today = new Date();
        return today.getDate();
    }
</script>


<main class="container">
    <h4 style="padding-top: 25px; padding-left: 25px">Конструктор занятия</h4>
    <form style="padding: 25px;" id="lesson_form" accept-charset="utf-8">
        <div class="row">
            <div class="input-field col s2">
                <input type="date" value="${requestScope.date}" id="lesson_date"
                       class="datepicker">
                <script>$('.datepicker').pickadate({
                    selectMonths: true,
                    selectYears: 15,
                    format: 'yyyy-mm-dd',
                    min: [getCurrentYear(), getCurrentMonth(), getCurrentDayOfMonth()]
                });</script>
                <label for="lesson_date">Дата занятия</label>
            </div>

            <div class="input-field col s2">
                <input id="lesson_room" value="${requestScope.room}" type="text" required="true"
                       class="validate" maxlength="25">
                <label for="lesson_room"
                       data-error="Поле обязательно для заполнения">Аудитория</label>
            </div>

            <div class="input-field col s8">
                <input id="lesson_topic" value="${requestScope.topic}" type="text" required="true"
                       class="validate" maxlength="90">
                <label for="lesson_topic" data-error="Поле обязательно для заполнения">Тема
                    занятия</label>
            </div>
        </div>

        <div class="row">
            <div class="input-field col s12">
                <textarea id="description" required="true" maxlength="2000"
                          class="materialize-textarea">${requestScope.description}</textarea>
                <label for="description">Описание занятия</label>
            </div>
        </div>

        <button class="btn waves-effect waves-light align-right" type="submit" name="sendData"
                id="sendData">
            Сохранить<i class="material-icons right">send</i>
        </button>

    </form>
</main>

<script type="text/javascript" language="JavaScript">

    $(document).ready(function () {
        $("#lesson_form").submit(function () {
            let form = $(this);

            if ($('#lesson_date').val() === '') {
                alert('Выберите дату занятия');
                return false;
            }

            let new_url;
            if ('${requestScope.id}' === '') {
                new_url = '/crm/lessons/new/?group=${requestScope.currentGroup.id}';
            }
            else {
                new_url = '/crm/lessons/edit/?group=${requestScope.currentGroup.id}&id=${requestScope.id}';
            }

            $.ajax({
                type: 'POST',
                url: new_url,
                dataType: 'json',
                data: {
                    less_date: $('input[id=lesson_date]').val(),
                    topic: $('input[id=lesson_topic]').val(),
                    room: $('input[id=lesson_room]').val(),
                    description: $('textarea[id=description]').val(),
                },
                beforeSend: function (data) {
                    form.find('input[type="submit"]').attr('disabled', 'disabled');
                },
                success: function (data) {

                    if (data['status'] === 'OK') {
                        location.href = "../?group=${requestScope.currentGroup.id}";
                    } else {
                        alert('Ошибка: ' + data['error']);
                    }
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    alert(thrownError);
                },
                complete: function (data) {
                    form.find('input[type="submit"]').prop('disabled', false);
                }
            });
            return false;
        });
    });




</script>

<%@include file="../footer.jsp" %>