<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<jsp:include page="../header.jsp">
    <jsp:param name="title" value="${requestScope.lesson.topic}"/>
</jsp:include>

<main class="container">
    <div class="z-depth" style="padding: 20px">
        <div style="margin-left: .75rem; margin-right: .75rem;">

            <div class="row">
                <div class="col s3"><h5>Дата: <fmt:formatDate
                        value="${requestScope.lesson.lessonDate}" pattern="dd.MM.yyyy"/>
                </h5></div>
                <div class="col s3"><h5>Аудитория: ${requestScope.lesson.room}
                </h5></div>
                <div class="col s6"><h5>Группа: ${requestScope.studyGroup.name}
                </h5></div>

            </div>
            <div class="row">
                <div class="col s12">
                    <blockquote style="word-break: break-all">
                            ${requestScope.lesson.description}
                    </blockquote>
                </div>
            </div>

            <div class="col s12">
                <div class="row">
                    <div class="row">
                        <div class="input-field col s12">
                        <textarea id="lesson_comment_id" name="lesson_comment" maxlength="300"
                                  class="materialize-textarea">${requestScope.lesson.lessonComment}</textarea>
                            <label for="lesson_comment_id">Комментарий</label>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col s2"><a class="waves-effect waves-light btn"
                                               href="?id=${requestScope.id}&group=${requestScope.studyGroup.id}&token">Токен</a></div>
                        <div class="col s2"><h5 class="valign-wrapper">${requestScope.token}
                        </h5></div>
                        <div class="col s4" id="tokentimer"></div>
                        <div class="col s4"><h5 class="valign-wrapper">
                            Посещение: ${requestScope.journal}
                        </h5></div>
                    </div>

                    <table class="striped">
                        <thead>
                        <tr>
                            <td class="center-align">Студент</td>
                            <td class="center-align">Активность на занятии</td>
                            <td class="center-align">Комментарий</td>
                            <td class="center-align">Посещение</td>
                        </tr>
                        </thead>
                        <tbody>
                        <c:forEach var="student" items="${requestScope.students}">
                            <tr>
                                <td style="width: 300px;"><c:out value="${student[1]}"/></td>
                                <td style="width: 100px;" class="center-align">
                                    <div class="input-field">
                                        <input id="number_id_${student[0]}"
                                               name="number_${student[0]}" type="number" min="0" max="10"
                                               class="validate"
                                               data-student="${student[0]}"
                                               value="${student[2]}" style="width: 100px">
                                    </div>
                                </td>
                                <td>
                                    <div class="input-field">
                                        <input type="text" id="student_comment_id_${student[0]}"
                                               name="student_comment_${student[0]}"
                                               data-student="${student[0]}"
                                               value="${student[3]}"
                                        maxlength="150"/>
                                    </div>
                                </td>
                                <td style="width: 50px;" class="center-align">
                                    <c:if test="${student[4] != '0'}">
                                        <i style="color: green" class="tiny material-icons">done</i>
                                    </c:if>
                                </td>
                                <input type="hidden" name="ids[]" value="${student[0]}"/>
                            </tr>
                        </c:forEach>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</main>

<!--Import jQuery before materialize.js-->

<script type="text/javascript">
    $(document).ready(function () {
        $('#lesson_comment_id').on('change', function () {
            let comment = $(this).val();

            $.ajax({
                method: 'post',
                dataType: 'json',
                url: '/crm/lessons/view/comment/?group=${requestScope.studyGroup.id}&id=${requestScope.lesson.id}',
                data : {
                    value : comment
                },
                success: function (data) {
                    if (data['status'] === 'OK') {
                        alert('Комментарий сохранен');
                    } else {
                        alert('Ошибка: ' + data['error']);
                    }
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    alert(thrownError);
                }
            });
        });

        $('[id^="student_comment_id_"]').each(function () {
            let commentInput = $(this);
            commentInput.on('change', function () {

                $.ajax({
                    method: 'post',
                    url: '/crm/lessons/view/activity/?group=${requestScope.studyGroup.id}&id=${requestScope.lesson.id}',
                    data : {
                        student : commentInput.data('student'),
                        activityComment : commentInput.val()
                    },
                    success: function (data) {
                        alert('Комментарий сохранен')
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        alert(thrownError);
                    }
                });
            });
        });

        $('[id^="number_id_"]').each(function () {
            let activityInput = $(this);
            activityInput.on('change', function () {

                if (activityInput.val() < 0 || activityInput.val() > 100) {
                    activityInput.val(0);
                    return;
                }

                $.ajax({
                    method: 'post',
                    url: '/crm/lessons/view/points/?group=${requestScope.studyGroup.id}&id=${requestScope.lesson.id}',
                    data : {
                        student : activityInput.data('student'),
                        activityPoints : activityInput.val()
                    },
                    success: function (data) {
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        alert(thrownError);
                    }
                });
            });
        });
        });
    function countDown(second, endMinute, endHour, endDay, endMonth, endYear) {
        var now = new Date();
        second = (arguments.length == 1) ? second + now.getSeconds() : second;
        endYear = typeof(endYear) != 'undefined' ? endYear : now.getFullYear();
        endMonth = endMonth ? endMonth - 1 : now.getMonth();  //номер месяца начинается с 0
        endDay = typeof(endDay) != 'undefined' ? endDay : now.getDate();
        endHour = typeof(endHour) != 'undefined' ? endHour : now.getHours();
        endMinute = typeof(endMinute) != 'undefined' ? endMinute : now.getMinutes();
        //добавляем секунду к конечной дате (таймер показывает время уже спустя 1с.)
        var endDate = new Date(endYear, endMonth, endDay, endHour, endMinute, second + 1);
        var interval = setInterval(function () { //запускаем таймер с интервалом 1 секунду
            var time = endDate.getTime() - now.getTime();
            if (time < 0) {                      //если конечная дата меньше текущей
                clearInterval(interval);
            } else {
                var days = Math.floor(time / 864e5);
                var hours = Math.floor(time / 36e5) % 24;
                var minutes = Math.floor(time / 6e4) % 60;
                if (minutes < 10) minutes = '0' + minutes;
                var seconds = Math.floor(time / 1e3) % 60;
                if (seconds < 10) seconds = '0' + seconds
                var digit = '<div style="width:70px;float:left;text-align:center">' +
                    '<div style="font-family:Stencil;font-size:25px;">';
                var text = '</div><div>'
                var end = '</div></div><div style="float:left;font-size:20px;">:</div>'
                document.getElementById('tokentimer').innerHTML = digit + minutes + text + 'Минут' + end + digit + seconds + text + 'Секунд';
                //digit+days+text+'Дней'+end+digit+hours+text+'Часов'+end+

                if (!seconds && !minutes && !days && !hours) {
                    clearInterval(interval);
                }
            }
            now.setSeconds(now.getSeconds() + 1); //увеличиваем текущее время на 1 секунду
        }, 1000);
    }

    <c:if test="${requestScope.token_expiration >= 0}">
    countDown(<%= request.getAttribute("token_expiration")%>);
    </c:if>

    setTimeout(function () {
        $('#success').fadeOut('slow')
    }, 4000);
</script>

<%@include file="../footer.jsp" %>