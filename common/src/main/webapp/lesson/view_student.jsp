<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<jsp:include page="../header.jsp">
    <jsp:param name="title" value="${requestScope.lesson.topic}"/>
</jsp:include>

<main class="container">
    <div class="z-depth" style="padding: 20px;">
        <div style="margin-left: .75rem; margin-right: .75rem;">

            <c:if test="${requestScope.tokenSuccess != null}" >
                <div class="row" id="success">
                    <div class="col s12" style="border: 1px solid darkblue; background-color: lightblue; ">
                        <p style="color: darkblue; font-weight: bold">${requestScope.tokenSuccess}</p>
                    </div>
                </div>
            </c:if>

            <c:if test="${requestScope.error != null}" >
                <div class="row" id="error">
                    <div class="col s12" style="border: 1px solid darkred; background-color: lightpink; ">
                        <p style="color: darkred; font-weight: bold">${requestScope.tokenError}</p>
                    </div>
                </div>
            </c:if>

            <div class="row">
                <div class="col s3"><h5>Дата: <fmt:formatDate value="${requestScope.lesson.lessonDate}" pattern="dd.MM.yyyy"/>
                </h5></div>
                <div class="col s3"><h5>Аудитория: ${requestScope.lesson.room}
                </h5></div>
                <div class="col s6"><h5>Группа: ${requestScope.lesson.studyGroup.name}
                </h5></div>

            </div>
            <div class="row">
                <div class="col s12">
                    <blockquote>
                        ${requestScope.lesson.description}
                    </blockquote>
                </div>
            </div>

            <form class="col s12" method="post">
                <div class="row">
                    <div class="col s4">
                        <button class="btn waves-effect waves-light" type="submit" name="save">Подтвердить
                            посещение
                        </button>
                    </div>
                    <div class="input-field col s4">
                        <input id="token_id" name="token" type="text" class="validate">
                        <label for="token_id">Введите токен</label>
                    </div>
                    <div class="row">
                        <div class="col s4"><h5> Посещение:  <%= request.getAttribute("journal")%>
                        </h5></div>
                    </div>
                </div>
                <input type="hidden" name="${_csrf.parameterName}"
                       value="${_csrf.token}" />
            </form>
            <div class="row">
                <table class="striped">
                    <thead>
                    <tr>
                        <td>Студент</td>
                        <td class="center-align">Посещение</td>
                    </tr>
                    </thead>
                    <tbody>
                    <c:forEach var="student" items="${requestScope.students}">
                        <tr>
                            <td><c:out value="${student[1]}"/></td>
                            <td style="width: 50px;" class="center-align">
                                <c:if test="${student[2] != '0'}">
                                    <i style="color: green" class="tiny material-icons">done</i>
                                </c:if>
                            </td>
                        </tr>
                    </c:forEach>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</main>
<script type="text/javascript">
    setTimeout(function () {
        $('#success').fadeOut('slow')
    }, 4000);
    setTimeout(function () {
        $('#error').fadeOut('slow')
    }, 4000);
</script>

<%@include file="../footer.jsp"%>