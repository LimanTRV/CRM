﻿<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<sec:authentication var="user" property="principal" />

<html lang="ru">
<head>
    <meta charset="UTF-8"/>
    <meta name="_csrf" content="${_csrf.token}"/>
    <meta name="_csrf_header" content="${_csrf.headerName}"/>
    <title><%=request.getParameter("title")%></title>
    <script src="https://code.jquery.com/jquery-3.2.1.min.js"
            integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4="
            crossorigin="anonymous"></script>
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.98.2/css/materialize.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.98.2/js/materialize.min.js"></script>
    <script src="https://use.fontawesome.com/8916abdc27.js"></script>
    <link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/mustache.js/2.3.0/mustache.min.js"></script>
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/sweetalert2/6.6.0/sweetalert2.min.css">
    <script src="https://cdn.jsdelivr.net/sweetalert2/6.6.0/sweetalert2.min.js"></script>
    <style type="text/css">
        html, body {
            height: 100%;
            position: relative;
        }
        *{word-break: break-all;}
        nav {
            /*background-color: #03a9f4;*/
            box-shadow: 0 0 0 0;
        }
        header {
            height: 200px;
            position: fixed;
            width: 100%;
            z-index: -1;
        }
        #typedtext {
            font-size: 2em;
            position: fixed;
            top: 60px;
            left: 0;
            right: 0;
            color: #fff;
            text-align: center;
        }
        .nav-wrapper {
            width: 100%;
            max-width: 1100px;
            margin: auto;
            padding: 0 20px;
        }
        .navbar-fixed {
            z-index: 1000;
        }
        .container {
            background: #fff;
            margin-top: 120px;
        }
        .w30 {
            color: #555;
            width: 30px;
        }
        .btn-floating {
            position: fixed;
            right: 50px;
            bottom: 50px;
        }
        .tabs .indicator {
            background: #3f51b5 !important;
        }
        .tabs .tab a:hover, .tabs .tab a.active, .tabs .tab a {
            color: #3f51b5;
        }
        .mb-0 {margin-bottom:0;}
        .mb-1 {margin-bottom:10px;}
        .pt-1 {padding-top: 10px;}
        .bt{border-top:1px solid #eee;}
    </style>
    <script>
        function alert(msg, time) {
            Materialize.toast(msg, time||3000);
        }
        $(document).ajaxSend((e, xhr) => {
            xhr.setRequestHeader($("meta[name='_csrf_header']").attr("content"), $("meta[name='_csrf']").attr("content"));
        });
    </script>
</head>
<body>
<div class="navbar-fixed">
    <nav class="indigo">
        <ul id="groups" class="dropdown-content">
            <c:forEach items="${requestScope.groups}" var="group">
                <li><a href="${group[1]}" class="waves-effect waves-light"><c:out value="${group[0]}"></c:out></a></li>
            </c:forEach>
        </ul>
        <sec:authorize access="hasAnyRole('T_E', 'T_V')">
        <ul id="dropdown-task" class="dropdown-content" style="min-width:160px;">
            <li><a href="${pageContext.request.contextPath}/task/list/?group=${requestScope.currentGroup.id}">Просмотр</a></li>
            <li><a href="${pageContext.request.contextPath}/task?group=${requestScope.currentGroup.id}">Редактирование</a></li>
        </ul>
        </sec:authorize>
        <ul id="dropdown-profile" class="dropdown-content">
            <li><a href="${pageContext.request.contextPath}/profile/type?group=${requestScope.currentGroup.id}">Типы критериев</a></li>
            <li><a href="${pageContext.request.contextPath}/profile/group?group=${requestScope.currentGroup.id}">Группы критериев</a></li>
            <li><a href="${pageContext.request.contextPath}/profile/criterion?group=${requestScope.currentGroup.id}">Критерии</a></li>
            <li><a href="${pageContext.request.contextPath}/profile/template?group=${requestScope.currentGroup.id}">Шаблоны анкет</a></li>
        </ul>
        <ul id="dropdown-user" class="dropdown-content">
            <li><a href="${pageContext.request.contextPath}/pwd">Сменить пароль</a></li>
            <li><a href="${pageContext.request.contextPath}/logout.jsp">Выйти</a></li>
        </ul>


        <div class="nav-wrapper">
            <%--<a href="${pageContext.request.contextPath}/" class="brand-logo">Logo</a>--%>
            <ul class="right hide-on-med-and-down">
                <sec:authorize access="hasRole('M_V')">
                    <li><a class="waves-effect waves-light dropdown-button" href="#!" data-activates="groups"><c:if test="${empty requestScope.currentGroup.name}">Выберите группу</c:if>${requestScope.currentGroup.name} <i class="fa fa-angle-down"></i></a></li>
                </sec:authorize>

                <sec:authorize access="hasRole('L_E')">
                    <li><a href="${pageContext.request.contextPath}/lessons/?group=${requestScope.currentGroup.id}" class="waves-effect waves-light">Занятия</a></li>
                </sec:authorize>

                <sec:authorize access="!hasRole('L_E')">
                    <sec:authorize access="hasRole('L_V')">
                        <li><a href="${pageContext.request.contextPath}/lessons/" class="waves-effect waves-light">Занятия</a></li>
                    </sec:authorize>
                </sec:authorize>

                <sec:authorize access="hasAnyRole('T_E', 'T_V')">
                    <li><a class="dropdown-button" href="#!" data-activates="dropdown-task">Задания<i class="fa fa-angle-down"></i></a></li>
                </sec:authorize>

                <sec:authorize access="hasRole('R_E')">
                    <li><a href="${pageContext.request.contextPath}/rating/?group=${requestScope.currentGroup.id}" class="waves-effect waves-light">Рейтинг</a></li>
                </sec:authorize>

                <sec:authorize access="hasRole('TD_V')">
                    <li><a href="${pageContext.request.contextPath}/task/downloads" class="waves-effect waves-light">Загрузка заданий</a></li>
                </sec:authorize>

                <sec:authorize access="hasAnyRole('G_V', 'G_E')">
                    <li><a href="${pageContext.request.contextPath}/groups/" class="waves-effect waves-light">Группы</a></li>
                </sec:authorize>

                <sec:authorize access="hasRole('M_E')">
                    <li><a class="waves-effect waves-light" href="${pageContext.request.contextPath}/UserController?action=listUser">Пользователи</a></li>
                </sec:authorize>

                <sec:authorize access="hasRole('PC_V')">
                    <li><a class="dropdown-button" href="#!" data-activates="dropdown-profile">Настройка анкет <i class="fa fa-angle-down"></i></a></li>
                </sec:authorize>

                <li>
                    <a href="#" class="dropdown-button" href="#!" data-activates="dropdown-user">
                        <sec:authorize access="isAuthenticated()">
                            ${user.userName}
                        </sec:authorize>
                        <i class="fa fa-angle-down"></i>
                    </a>
                </li>
            </ul>
        </div>
    </nav>
</div>
<header class="z-depth-1 indigo">
    <div class="holder">
        <div id="typedtext"><h1><%=request.getParameter("title")%></h1></div>
    </div>
</header>