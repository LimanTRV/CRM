<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html lang="ru">
<head>

    <meta charset="UTF-8"/>
</head>
<body>
<h1>HTTP Status 403 - Access is denied</h1>
<h2>You do not have permission to access this page!</h2>
<h5><a href="${pageContext.request.contextPath}">На главную</a></h5>
</body>
</html>