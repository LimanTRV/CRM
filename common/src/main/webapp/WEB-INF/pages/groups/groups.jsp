﻿<%--
  Created by IntelliJ IDEA.
  User: Olesya
  Date: 20.04.2017
  Time: 20:25
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<jsp:include page="/header.jsp">
    <jsp:param name="title" value="Groups"/>
</jsp:include>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.15.1/jquery.validate.min.js"></script>

<main class="container z-depth-1">
    <nav class="blue z-depth-1" style="padding-bottom: 10px">
        <div class="nav-wrapper">
            <div class="col s12">
                <a href="../groups/" class="breadcrumb">Группы</a>
            </div>
        </div>
    </nav>

    <div style="padding: 25px">
        <table class="bordered">
            <thead>
            <tr>
                <th>Наименование</th>
                <th>Описание</th>
                <th>Тип курса</th>
                <th></th>
            </tr>
            </thead>
            <tbody>
            <c:if test="${not empty requestScope.groupList}">
                <c:forEach items="${requestScope.groupList}" var="groupItem" varStatus="count">
                    <tr id="group_<c:out value="${groupItem.id}" escapeXml="true" />">
                        <td><c:out value="${groupItem.name}" escapeXml="true"/></td>
                        <td><c:out value="${groupItem.description}" escapeXml="true"/></td>
                        <td><c:out value="${groupItem.courseType}" escapeXml="true"/></td>
                        <td style="text-align: right">
                            <a href="../students/?group=<c:out value="${groupItem.id}" escapeXml="true" />">студенты</a>
                        </td>
                        <td style="text-align: right"><sec:authorize access="hasRole('G_E')">
                            <a data-target="editing-window" class="modal-trigger" onclick="makeModal(
                                '<c:out value="${groupItem.id}" escapeXml="true" />',
                                '<c:out value="${groupItem.name}" escapeXml="true" />',
                                '<c:out value="${groupItem.courseType}" escapeXml="true" />',
                                '<c:out value="${groupItem.description}" escapeXml="true" />'
                            )">
                                <i style="font-size: 20px" class="fa fa-pencil"></i>
                            </a>
                            <a data-target="submit-deletion" class="modal-trigger" onclick="$('#group_for_deletion').val('<c:out value="${groupItem.id}" escapeXml="true" />')">
                                <i style="padding-left: 10px; font-size: 20px" class="fa fa-minus-square-o"></i>
                            </a></sec:authorize>
                        </td>
                    </tr>
                </c:forEach>
            </c:if>
            </tbody>
        </table><sec:authorize access="hasRole('G_E')">
        <a class="btn-floating modal-trigger btn-large waves-effect waves-light blue" data-target="editing-window" onclick="makeModal()">
            <i class="material-icons" style="font-size: 20px">add</i>
        </a></sec:authorize>
    </div>
</main>

<sec:authorize access="hasRole('G_E')">
<div id="editing-window" class="modal">
    <form id="group_editor">
        <div id="" class="modal-content">
            <h4>Конструктор группы</h4>
            <div style="padding-top: 25px; padding-left: 25px; padding-right: 25px">
                <input type="hidden" id="group_id" value="">
                <div class="row">
                    <div class="input-field col s9">
                        <label for="group_name">Наименование</label>
                        <input type="text" autocomplete="off" required id="group_name" maxlength="50" value="">
                    </div>
                    <div class="input-field col s3">
                        <label for="group_type">Тип курса</label>
                        <input type="text" autocomplete="off" required id="group_type" maxlength="20" value="">
                    </div>
                </div>
                <div class="row" style="padding: 0.75rem;">
                <div class="input-field">
                    <label for="group_description">Описание</label>
                    <input type="text" autocomplete="off" required id="group_description" maxlength="75" value="">
                </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <a class="modal-action modal-close waves-effect waves-green btn-flat">Отмена</a>
            <a class="submit waves-effect waves-green btn-flat" onclick="saveGroup()">Сохранить</a>
        </div>
    </form>
</div>
</sec:authorize>

<div id="submit-deletion" class="modal">
    <input type="hidden" id="group_for_deletion" value="">
    <div class="modal-content">
            <h4>Вы уверены?</h4>
            <div style="padding-top: 25px; padding-left: 25px; padding-right: 25px">
                <h5>Удаление группы очень отвественный шаг.<br/>Вы точно хотите на этой пойти?</h5>
            </div>
        </div>
        <div class="modal-footer">
            <a class="modal-action modal-close waves-effect waves-green btn-flat">Отмена</a>
            <a class="modal-action modal-close waves-effect waves-green btn-flat" onclick="deleteGroup($('#group_for_deletion').val());">Удалить группу</a>
        </div>
</div>

<script type="text/javascript" language="JavaScript">
    $(document).ready(function(){
        // the "href" attribute of .modal-trigger must specify the modal ID that wants to be triggered
        $('.modal').modal();
    });

    function makeModal(id, name, type, description) {
        $("#group_id").val(id);
        $("#group_name").val(name);
        $("#group_type").val(type);
        $("#group_description").val(description);
        Materialize.updateTextFields();
    }

    <sec:authorize access="hasRole('G_E')">
    function saveGroup() {
        var id = $("#group_id").val();
        var name = $("#group_name").val();
        var type = $("#group_type").val();
        var description = $("#group_description").val();
        if (!name){
            alert("Необходимо заполнить имя группы");
            return;
        }
        if (!description){
            alert("Необходимо заполнить описание");
            return;
        }
        if (!type){
            alert("Необходимо заполнить тип группы");
            return;
        }
        $('#editing-window').modal('close');
        $.ajax({
            url: './',
            type: "POST",
            data: {
                id: $("#group_id").val(),
                name: $("#group_name").val(),
                type: $("#group_type").val(),
                description: $("#group_description").val(),
            },
            success: function () {
                alert("Группа успешно сохранена");
                if ($("#group_id").val() == ''){
                    window.location.reload(true);
                } else {
                    var row = $("#group_" + id).find("td");
                    row[0].innerText = name;
                    row[1].innerText = description;
                    row[2].innerText = type;
                }
            },
            error: function () {
                alert("Не удалось сохранить группу");
            }
        });
    }

    function deleteGroup(id) {
        $.ajax({
            url: '../groups/?id=' + id,
            type: "DELETE",
            data: {
                id: id,
            },
            success: function () {
                alert("Группа успешно удалена");
                $("#group_" + id).remove();
            },
            error : function () {
                alert("Не удалось удалить группу");
            }
        });
    }
    </sec:authorize>
</script>

<%@include file="/footer.jsp"%>
