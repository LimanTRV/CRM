﻿<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<jsp:include page="/header.jsp">
    <jsp:param name="title" value="Students"/>
</jsp:include>

<main class="container">
    <nav class="blue z-depth-1" style="padding-bottom: 10px">
        <div class="nav-wrapper">
            <div class="col s12">
                <c:choose><c:when test="${not empty requestScope.groupItem}">
                    <a href="../groups/" class="breadcrumb">Группы</a>
                    <a href="../students/?group=<c:out value="${requestScope.groupItem.id}" escapeXml="true" />" class="breadcrumb">Студенты в группе</a>
                </c:when>
                <c:otherwise>
                    <a href="../students/" class="breadcrumb">Список студентов</a>
                </c:otherwise></c:choose>
            </div>
        </div>
    </nav>

    <input type="hidden" class="dev_group_id" value="<c:out value="${requestScope.groupItem.id}" escapeXml="true" />">
    <div style="padding: 25px">
    <table class="bordered">
        <thead>
        <tr>
            <th>Фамилия</th>
            <th>Имя</th>
            <th>Логин</th>
            <th>Email</th>
            <th></th>
        </tr>
        </thead>
        <tbody>
        <c:if test="${not empty requestScope.studentList}">
            <c:forEach items="${requestScope.studentList}" var="studentItem" varStatus="count">
                <tr id="student_<c:out value="${studentItem.id}" escapeXml="true" />">
                    <td><c:out value="${studentItem.user.lastName}" escapeXml="true"/></td>
                    <td><c:out value="${studentItem.user.firstName}" escapeXml="true"/></td>
                    <td><c:out value="${studentItem.user.login}" escapeXml="true"/></td>
                    <td><c:out value="${studentItem.user.email}" escapeXml="true"/></td>
                    <td>
                        <a href="../profile?group=<c:out value="${requestScope.groupItem.id}" escapeXml="true" />&studentId=${studentItem.id}">Анкета</a>
                    </td>
                </tr>
            </c:forEach>
        </c:if>
        </tbody>
    </table>
    </div><sec:authorize access="hasRole('S_E')">
    <a href="../pick_students/?group=<c:out value="${requestScope.groupItem.id}" escapeXml="true" />" class="btn-floating modal-trigger btn-large waves-effect waves-light blue">
        <i class="material-icons" style="font-size: 20px">mode_edit</i>
    </a></sec:authorize>
</main>

<%@include file="/footer.jsp"%>