﻿<%--
  Created by IntelliJ IDEA.
  User: Olesya
  Date: 20.04.2017
  Time: 20:25
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/header.jsp">
    <jsp:param name="title" value="Выбор студентов"/>
</jsp:include>

<main class="container z-depth-1">
    <nav class="blue z-depth-1" style="padding-bottom: 10px">
        <div class="nav-wrapper">
            <div class="col s12">
                <a href="../groups/" class="breadcrumb">Группы</a>
                <a href="../students/?group=<c:out value="${requestScope.groupItem.id}" escapeXml="true" />" class="breadcrumb">Студенты в группе</a>
                <a href="../pick_students/?group=<c:out value="${requestScope.groupItem.id}" escapeXml="true" />" class="breadcrumb">Добавление студентов</a>
            </div>
        </div>
    </nav>

    <input type="hidden" class="dev_group_id" value="<c:out value="${requestScope.groupItem.id}" escapeXml="true" />">
    <div style="padding:25px">
        <div class="input-field">
            <input id="searchField" type="search" onkeyup="applyFilter()">
            <label class="label-icon" for="searchField"><i class="material-icons">search</i></label>
            <i onclick="$('#searchField').val(''); applyFilter();" class="material-icons">close</i>
        </div>
        <table class="bordered highlight" id="students_table">
            <thead>
            <tr>
                <th>Студент</th>
                <th>Логин</th>
                <th>Email</th>
                <th></th>
            </tr>
            </thead>
            <tbody>
            <c:if test="${not empty requestScope.studentList}">
                <c:forEach items="${requestScope.studentList}" var="studentItem" varStatus="count">
                    <tr student-id="<c:out value="${studentItem.id}" escapeXml="true" />">
                        <td><c:out value="${studentItem.user.lastName}" escapeXml="true"/>
                        <c:out value="${studentItem.user.firstName}" escapeXml="true"/></td>
                        <td><c:out value="${studentItem.user.login}" escapeXml="true"/></td>
                        <td><c:out value="${studentItem.user.email}" escapeXml="true"/></td>
                        <td style="text-align: right">
                            <input type="checkbox" id="checkbox_${studentItem.id}" onchange="sendData(${studentItem.id})"
                                                      @nbsp<c:if test="${studentItem.isInGroup == '1'}"> checked </c:if> />
                            <label for="checkbox_${studentItem.id}"></label>


                        </td>
                    </tr>
                </c:forEach>
            </c:if>
            </tbody>
        </table>
    </div>
</main>

<script>
    function sendData(id) {
        $.ajax({
            url: './',
            type: "POST",
            data: {
                groupId: ${requestScope.groupItem.id},
                studentId: id,
                status: $("#checkbox_" + id).is(":checked"),
            },
            success: function (answer) {
                alert("Студент успешно добавлен в группу");
            },
            error: function () {
                alert("Не удалось обновить привязку к группе");
            }
        });
    }

    function applyFilter() {
        var filter, tr, name, i, email, filtered;
        filter = $('#searchField').val().toUpperCase();
        tr = $('#students_table').find('tr');

        for (i = 0; i < tr.length; i++) {
            name = tr[i].getElementsByTagName("td")[0];
            email = tr[i].getElementsByTagName("td")[2];
            if (name + email) {
                if ((name.innerHTML.toUpperCase().indexOf(filter) > -1) || (email.innerHTML.toUpperCase().indexOf(filter) > -1)){
                    tr[i].style.display = "";
                } else {
                    tr[i].style.display = "none";
                }
            }
        }
    }

</script>


<%@include file="/footer.jsp" %>