<!--
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
-->
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Error Page</title>
</head>
<body>
<!--Текст можно задавать двумя способами
1) req.setAttribute("errorText", "") - этот вариант html теги переваривает точно
2) jsp:param name="errorText" value=""
-->
<c:choose>
    <c:when test="${empty requestScope.errorText}">
        <%=request.getParameter("errorText")%>
    </c:when>
    <c:otherwise>
        <c:out value="${requestScope.errorText}" escapeXml="false"/>
    </c:otherwise>
</c:choose>
</body>
</html>
