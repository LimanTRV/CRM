<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<jsp:include page="../header.jsp">
    <jsp:param name="title" value="Admin panel"/>
</jsp:include>


<main class="container" style="padding: 40px">

    <div style="display: inline">
        <ul class="nextLevel" >
            <li style="display: inline"><a class=""
                                           href="UserController?action=listUser">Пользователи</a> |
            </li>
            <li style="display: inline">
                <a class=""
                   href="RoleController?action=listRole">Роли</a>
            </li>
        </ul>
    </div>
    <div>
    <p align="right"><a href="RoleController?action=insert">Add Role</a></p>
    <table border=1 width="100%">
        <thead>
        <tr align="center">
            <th>Role Id</th>
            <th>Code</th>
            <th>Name</th>
            <!--th>Deleted</th-->
            <th colspan=2>Action</th>
        </tr>
        </thead>
        <tbody>
        <c:forEach items="${requestScope.roles}" var="role">
            <tr>
                <td><c:out value="${role.id}"/></td>
                <td><c:out value="${role.code}"/></td>
                <td><c:out value="${role.name}"/></td>
                <%--<td>--%>
                    <%--<table>--%>
                        <%--<tr>--%>
                            <%--<c:forEach items="${role.roles}" var="role">--%>
                                <%--<td><c:out value="${role.name}"></c:out>--%>
                                <%--</td>--%>
                            <%--</c:forEach>--%>
                        <%--</tr>--%>
                    <%--</table>--%>
                <%--</td>--%>
                <%--td><c:out value="${role.deleted}"/></td--%>
                <td>
                    <c:if test="${role.code != 'A' && role.code != 'S'}">
                        <a href="RoleController?action=edit&roleId=<c:out value="${role.id}"/>">Update</a>
                    </c:if>
                </td>
                <td>
                    <c:if test="${role.code != 'A' && role.code != 'S'}">
                        <a href="RoleController?action=delete&roleId=<c:out value="${role.id}"/>">Delete</a>
                    </c:if>
                </td>
            </tr>
        </c:forEach>
        </tbody>
    </table>
    </div>
</main>

<script type="text/javascript">
    Materialize.toast('${message_role}', 3000);
</script>

<%@include file="../footer.jsp"%>