<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<jsp:include page="../header.jsp">
    <jsp:param name="title" value="Admin panel"/>
</jsp:include>


<main class="container" style="padding: 40px">
    <form method="POST" action='UserController' name="frmAddUser">
        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
        <input type="hidden" readonly="readonly" name="userId" value="<c:out value="${requestScope.user.id}" />"/>
        <table>
            <tr>
                <td><div class="input-field"><input id="login" type="text" data-type="eng" name="login" autocomplete="off" required class="validate" maxlength="255" value="<c:out value="${requestScope.user.login}" />"/>
                    <label style="display: inline-block; width: 100%;" for="login" data-error="Поле должно быть заполнено латинницей.">Login</label></div></td>
            </tr>
            <c:if test="${empty requestScope.user.id}">
            <tr>
                <td><div class="input-field"><input id="password" type="password" name="password" autocomplete="off" <c:out value="${empty requestScope.user ? 'required' : ''}"/> value=""/>
                    <label style="display: inline-block; width: 100%;" for="password"  data-error="At least one number, one lowercase and one uppercase letter, at least 8 characters that are letters, numbers or the underscore">Password</label></div></td>
            </tr>
            </c:if>
            <tr>
                <td><div class="input-field"><input id="firstname" type="text" data-type="rus" name="firstName" class="validate" maxlength="255" value="<c:out value="${requestScope.user.firstName}" />"/>
                    <label style="display: inline-block; width: 100%;" for="firstname" data-error="Поле должно быть заполнено.">First Name</label></div></td>
            </tr>
            <tr>
                <td><div class="input-field"><input id="lastname" type="text" data-type="rus" name="lastName" class="validate" maxlength="255" value="<c:out value="${requestScope.user.lastName}" />"/>
                    <label style="display: inline-block; width: 100%;" for="lastname" data-error="Поле должно быть заполнено.">Last Name</label></div></td>
            </tr>
            <tr>
                <td><div class="input-field"><input id="email" type="text" data-type="mail" name="email" class="validate" maxlength="255" value="<c:out value="${requestScope.user.email}" />"/>
                    <label style="display: inline-block; width: 100%;" for="email" data-error="Введите e-mail.">Email</label></div></td>
            </tr>
            <tr>
                <td>
                    <div class="input-field">
                        <select id="roles" multiple name="roles" title="User roles">
                            <option value="" disabled selected>Choose your option</option>
                            <c:forEach items="${requestScope.allRoles}" var="role">
                                <option value="${role.id}" <c:forEach items="${requestScope.user.roles}" var="userRole"><c:if test="${userRole.id eq role.id}">selected</c:if></c:forEach> <c:if test="${not empty requestScope.user && requestScope.user.id == requestScope.userBean.userId && role.code == 'A'}">disabled</c:if>><c:out value="${role.name}"/></option>
                            </c:forEach>
                        </select>
                        <label style="display: inline-block; width: 100%;" for="roles" data-error="Выберите роли.">Roles</label>
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    <input type="checkbox" id="active" name="isActive" value="true" <c:if test="${empty requestScope.user or requestScope.user.active}">checked="checked"</c:if> <c:if test="${not empty requestScope.user && requestScope.user.id == requestScope.userBean.userId}">disabled</c:if>/>
                    <label for="active">Active</label>
                </td>
            </tr>
        </table>
        <button class="btn waves-effect waves-light " type="submit" name="action">
            <c:out value="${empty requestScope.user ? 'Add' : 'Save'}"/>
        </button>
    </form>
</main>
<script>
    $("form").submit(function(e) {
        var that = $(this), error = false, rxs = {eng:/^\w+$/,rus:/^[a-zA-Zа-яА-ЯёЁ]+$/,mail:/^[-\w.]+@([A-z0-9][-A-z0-9]+\.)+[A-z]{2,4}$/};
        that.find("input[type=text]:not([readonly]), input[type=password]").each(function(ndx,el){
            var t = $(el);
            var type = t.data("type");
            if(!type || !rxs.hasOwnProperty(type))
                return;

            if(!rxs[type].test(t.val())) {
                t.removeClass("valid").addClass("invalid");
                error = true;
            } else {
                t.addClass("valid").removeClass("invalid");
            }
        });

        that.find("input[name='password']").each(function(ndx,el){
            var re = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}$/;
            var t = $(el);
            if(!re.test(t.val())){
                t.removeClass("valid").addClass("invalid");
                error = true;
            } else {
                t.addClass("valid").removeClass("invalid");
            }
        });

        var select = that.find("select");
        if (!select.val().length) {
            select.removeClass("valid").addClass("invalid");
            error = true;
        }
        else {
            select.addClass("valid").removeClass("invalid");
        }

        if(error)
            e.preventDefault();
    });

    $(document).ready(function() {
        $('select').material_select();
    });
</script>
<%@include file="../footer.jsp"%>