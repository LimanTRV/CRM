<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<jsp:include page="../header.jsp">
    <jsp:param name="title" value="Admin panel"/>
</jsp:include>


<main class="container" style="padding: 40px">


    <div style="display: inline">
        <ul class="nextLevel">
            <li style="display: inline"><a class=""
                                           href="UserController?action=listUser">Пользователи</a> |
            </li>
            <li style="display: inline">
                <a class=""
                   href="RoleController?action=listRole">Роли</a>
            </li>
        </ul>
    </div>
    <div>
        <p align="right"><a href="UserController?action=insert">Add User</a></p>
        <table border=1 width="100%">
            <thead>
            <tr align="center">
                <th>User Id</th>
                <th>Login</th>
                <th>First Name</th>
                <th>Last Name</th>
                <th>Email</th>
                <th>Role</th>
                <th>Active</th>
                <!--th>Deleted</th-->
                <th></th>
            </tr>
            </thead>
            <tbody>
            <c:forEach items="${requestScope.users}" var="user">
                <tr>
                    <td><c:out value="${user.id}"/></td>
                    <td><c:out value="${user.login}"/></td>
                    <td><c:out value="${user.firstName}"/></td>
                    <td><c:out value="${user.lastName}"/></td>
                    <td><c:out value="${user.email}"/></td>
                    <td><c:forEach items="${user.roles}" var="role" varStatus="loop">
                        ${role.name}${!loop.last ? ', ' : ''}
                    </c:forEach></td>
                    <td><c:out value="${user.active}"/></td>
                    <td>
                        <a class="dropdown-button right w30" href="#" data-activates="coll${user.id}"><i
                                class="fa fa-ellipsis-v"></i></a>
                        <ul id="coll${user.id}" class="dropdown-content">
                            <li><a href="UserController?action=edit&userId=<c:out value="${user.id}"/>">Edit</a></li>
                            <c:if test="${requestScope.userBean.userId != user.id}">
                                <li><a href="UserController?action=change_password&userId=<c:out value="${user.id}"/>">Change
                                    password</a></li>
                                <li><a href="UserController?action=delete&userId=<c:out value="${user.id}"/>">Delete</a>
                                </li>
                            </c:if>
                        </ul>
                    </td>
                </tr>
            </c:forEach>
            </tbody>
        </table>
    </div>
</main>
<%@include file="../footer.jsp" %>