<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<jsp:include page="../header.jsp">
    <jsp:param name="title" value="Admin panel"/>
</jsp:include>

<main class="container" style="padding: 40px">
    <form method="POST" action='UserController?action=change_password' name="frmAddUser">
        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
        <input type="hidden" readonly="readonly" name="userId" value="<c:out value="${requestScope.user.id}" />"/>
        <table>
            <tr>
                <td><div class="input-field"><input id="password" type="password" name="password" value=""/>
                    <label style="display: inline-block; width: 100%;" for="password"  data-error="At least one number, one lowercase and one uppercase letter, at least 8 characters that are letters, numbers or the underscore">Password</label></div></td>
            </tr>
            <tr>
                <td><div class="input-field"><input id="confirm-password" type="password" name="confirm-password" value=""/>
                    <label style="display: inline-block; width: 100%;" for="confirm-password"  data-error="At least one number, one lowercase and one uppercase letter, at least 8 characters that are letters, numbers or the underscore">Confirm password</label></div></td>
            </tr>
        </table>

        <button class="btn waves-effect waves-light " type="submit" name="action">Save</button>
    </form>
</main>

<script>
    $('form').submit(function(e) {
        var that = $(this), error = false;

        if (that.find("input[name='password']").val() != that.find("input[name='confirm-password']").val()) {
            alert("Пароли не совпадают");
            error = true;
        }

        that.find("input[name='password'], input[name='confirm-password']").each(function(ndx,el){
            var re = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}$/;
            var t = $(el);

            if(!re.test(t.val())){
                t.removeClass("valid").addClass("invalid");
                error = true;
            } else {
                t.addClass("valid").removeClass("invalid");
            }
        });

        if(error)
            e.preventDefault();
    });
</script>

<%@include file="../footer.jsp"%>