<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<jsp:include page="../header.jsp">
    <jsp:param name="title" value="Admin panel"/>
</jsp:include>


<main class="container" style="padding: 40px">
<form method="POST" action='RoleController' name="frmAddRole">
    <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
    <input type="hidden" readonly="readonly" name="roleId" value="<c:out value="${requestScope.role.id}" />"/>
    <p><div class="input-field"><input id="code" type="text" data-type="eng" name="code" required class="validate" maxlength="255" value="<c:out value="${requestScope.role.code}" />"/>
    <label style="display: inline-block; width: 100%;" for="code" data-error="Поле должно быть заполнено латинницей.">Code</label></div></p>
    <p><div class="input-field"><input id="name" type="text" name="name" required value="<c:out value="${requestScope.role.name}" />"/>
    <label style="display: inline-block; width: 100%;" for="name" >Name</label></div></p>
    <table border=1 width="100%">
        <thead>
        <tr align="center">
            <th>Resource</th><c:forEach items="${requestScope.operations}" var="operation">
            <th>${operation.name}</th>
        </c:forEach>
        </tr>
        </thead>
        <tbody>
        <c:forEach items="${requestScope.resources}" var="resource">
            <tr>
                <td><c:out value="${resource.name}"/></td>
                <c:forEach items="${requestScope.operations}" var="operation">
                    <c:set var="permitted" scope="session" value="false"/>
                    <c:forEach var="permission" items="${requestScope.permissions}">
                        <c:if test="${permission.key.id eq resource.id}">
                            <c:forEach var="operationInDB" items="${permission.value}">
                                <c:if test="${operationInDB.id eq operation.id}"><c:set var="permitted" scope="session" value="true"/></c:if>
                            </c:forEach>
                        </c:if>
                </c:forEach>
                <td>
                    <input name="rl_${resource.id}_${operation.id}" value="0" type="checkbox" id="rl_${resource.id}_${operation.id}" <c:if test="${permitted}">checked="checked"</c:if> />
                    <label for="rl_${resource.id}_${operation.id}" class="tooltipped" data-position="bottom" data-delay="50" data-tooltip="${resource.name} / ${operation.name}"></label>
                </td>
            </c:forEach>
            </tr>
        </c:forEach>
        </tbody>
    </table>
    <button class="btn waves-effect waves-light " type="submit" name="action">
        <c:out value="${empty requestScope.role ? 'Add' : 'Save'}"/>
    </button>
</form>

</main>
<script>
    $("form").submit(function(e) {

        var that = $(this), error = false, rxs = {eng:/^\w+$/,rus:/^[a-zA-Zа-яА-ЯёЁ]+$/,mail:/^[-\w.]+@([A-z0-9][-A-z0-9]+\.)+[A-z]{2,4}$/};
        that.find("input[type=text]:not([readonly]), input[type=password]").each(function(ndx,el){
            var t = $(el);
            var type = t.data("type");
            if(!type || !rxs.hasOwnProperty(type))
                return;

            if(!rxs[type].test(t.val())) {
                t.removeClass("valid").addClass("invalid");
                error = true;
            } else {
                t.addClass("valid").removeClass("invalid");
            }
        });
        if(error)
            e.preventDefault();
    });
</script>
<%@include file="../footer.jsp"%>