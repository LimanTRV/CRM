<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<jsp:include page="../header.jsp">
    <jsp:param name="title" value="Рейтинг студентов"/>
</jsp:include>
<div class="progress indigo lighten-3 hide" style="position:fixed;top:0;z-index:1001;margin:0;">
    <div class="indeterminate indigo darken-1"></div>
</div>
<div class="container">
    <div class="z-depth-1">
        <div class="row" style="padding:10px 0;">
            <div class="col s12">
                <div class="input-field col s6">
                    <select id="type">
                        <option value="attendance">По посещаемости</option>
                        <option value="active">По активности</option>
                        <option value="tasks">По баллам</option>
                    </select>
                    <label>Тип рейтинга</label>
                </div>
                <div class="input-field col s3">
                    <label for="from">Начало</label>
                    <input id="from" type="date" class="datepicker">
                </div>
                <div class="input-field col s3">
                    <label for="to">Начало</label>
                    <input id="to" type="date" class="datepicker">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col s12 center-align">
                <a id="gogo" class="waves-effect waves-light btn indigo">Выполнить</a>
            </div>
        </div>
        <div class="row">
            <div class="col s12">
                <table  class="bordered">
                    <thead>
                        <tr>
                            <th>Студент</th>
                            <th>Рейтинг</th>
                        </tr>
                    </thead>
                    <tbody>
                        <c:forEach items="${students}" var="student">
                            <tr>
                                <td>
                                    ${student.user.lastName} ${student.user.firstName}
                                </td>
                                <td id="rt_${student.id}" class="rating_st">-</td>
                            </tr>
                        </c:forEach>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<script>
    $(() => {
        let groupId = +"${group}";
        let loading = false;
        const type = $('#type');
        type.material_select();

        const pickers = $('.datepicker').pickadate({selectMonths: true,selectYears: 15});
        pickers.eq(0).pickadate("picker").set('select', new Date() - 30 * 24 * 60 * 60 * 1000);
        pickers.eq(1).pickadate("picker").set('select', new Date());
        $("#gogo").click(() => {
            if(!groupId)
                return alert("Группа не выбрана");
            if(loading)
                return alert("Дождитесь окончания загрузки");
            const from = pickers.eq(0).pickadate('picker').get('select', 'yyyy-mm-dd');
            const to   = pickers.eq(1).pickadate('picker').get('select', 'yyyy-mm-dd');
            const pattern = /^201\d\-\d{2}-\d{2}$/;
            if(!pattern.test(from) || !pattern.test(to))
                return alert("Необходимо выбрать дату");
            if(+new Date(from) > +new Date(to))
                return alert("Выбран некорректный диапазон дат");
            $(".progress").removeClass("hide");
            loading = true;
            $.post('${pageContext.request.contextPath}/rating/', {from, to, type:type.val(), groupId}, d => {
                if(!d || !d.length)
                    return $('.rating_st').html('0%');
                for(var arr of d)
                    $("#rt_" + arr[0]).text(arr[1] + "%");
            }, 'json').fail((d) => alert(d.responseJSON.error || "Неизвестная ошибка")).always(() => {
                loading = false;
                $(".progress").addClass("hide");
            });
        });
    });
</script>
<jsp:include page="../footer.jsp"/>