<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<jsp:include page="header.jsp">
    <jsp:param name="title" value="Change password"/>
</jsp:include>


<main class="container" style="padding: 40px">
    <form method="POST" action='pwd' name="changePWD">
        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
        <input type="hidden" readonly="readonly" name="userId" value="<c:out value="${user.id}" />"/>
        <table>
            <%--<tr>--%>
                <%--<td><div class="input-field"><input id="login" type="text" readonly="readonly" name="login" required value="<c:out value="${user.login}" />"/>--%>
                    <%--<label style="display: inline-block; width: 100%;" for="login">Login</label></div></td>--%>
            <%--</tr>--%>
            <tr>
                <td><div class="input-field"><input id="old_password" type="password" name="old_password" required value=""/>
                    <label style="display: inline-block; width: 100%;" for="old_password">Old Password</label></div></td>
            </tr>
            <tr>
                <td><div class="input-field"><input id="new_password" type="password" name="new_password" required value=""/>
                    <label style="display: inline-block; width: 100%;" for="new_password" data-error="At least one number, one lowercase and one uppercase letter, at least 8 characters that are letters, numbers or the underscore">New Password</label></div></td>
            </tr>
            <tr>
                <td><div class="input-field"><input id="new_password2" type="password" name="new_password2" required value=""/>
                    <label style="display: inline-block; width: 100%;" for="new_password2" data-error="Must be equal to previous field.">Confirm password</label></div></td>
            </tr>
        </table>
        <button class="btn waves-effect waves-light " type="submit" name="action">
            <c:out value="Save"/>
        </button>
    </form>
</main>
<script>
    $("form").submit(function(e) {

        var that = $(this), error = false, rxs = {eng:/^\w+$/,rus:/^[a-zA-Zа-яА-ЯёЁ]+$/,mail:/^[-\w.]+@([A-z0-9][-A-z0-9]+\.)+[A-z]{2,4}$/};
        that.find("input[type=text]:not([readonly]), input[name=new_password]").each(function(ndx,el){
            var t = $(el);
            var type = t.data("type");
            if(!type || !rxs.hasOwnProperty(type))
                return;

            if(!rxs[type].test(t.val())) {
                t.removeClass("valid").addClass("invalid");
                error = true;
            } else {
                t.addClass("valid").removeClass("invalid");
            }
        });

        that.find("input[name='new_password']").each(function(ndx,el){
            var re = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}$/;
            var t = $(el);
            if(!re.test(t.val())){
                t.removeClass("valid").addClass("invalid");
                error = true;
            } else {
                t.addClass("valid").removeClass("invalid");
            }
        });

        var new_password = $("input[name=new_password]");
        var new_password2 = $("input[name=new_password2]");

        if(new_password2.val() != new_password.val()){
            new_password2.removeClass("valid").addClass("invalid");
            error = true;
        } else {
            new_password2.addClass("valid").removeClass("invalid");
        }

        if(error)
            e.preventDefault();
    });

    $(document).ready(function() {
        $('select').material_select();
    });
</script>
<%@include file="footer.jsp"%>
