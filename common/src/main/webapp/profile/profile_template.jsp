﻿<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<jsp:include page="../header.jsp">
    <jsp:param name="title" value="Шаблоны анкет"/>
</jsp:include>

<c:set var="editAllowed" scope="session" value="${false}"/>

<sec:authorize access="hasRole('PC_E')">
    <c:set var="editAllowed" scope="session" value="${true}"/>
</sec:authorize>

<main class="container">
    <div class="z-depth" style="padding: 20px">
        <div style="margin-left: .75rem; margin-right: .75rem;">

            <c:if test="${requestScope.success != null}" >
                <div class="row" id="success">
                    <div class="col s12" style="border: 1px solid darkblue; background-color: lightblue; ">
                        <p style="color: darkblue; font-weight: bold"><%= request.getAttribute("success")%></p>
                    </div>
                </div>
            </c:if>

            <c:if test="${requestScope.error != null}" >
                <div class="row" id="error">
                    <div class="col s12" style="border: 1px solid darkred; background-color: lightpink; ">
                        <p style="color: darkred; font-weight: bold"><%= request.getAttribute("error")%></p>
                    </div>
                </div>
            </c:if>


            <div class="row">
                <form method="post" class="col s12">
                    <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
                    <input type="hidden" name="templateId" value="${requestScope.editTemplate.id}" />
                    <c:if test="${editAllowed}">
                    <div class="row">
                        <div class="input-field col s12">
                            <input id="nameId" name="name" type="text" class="validate" value="${requestScope.editTemplate.name}" required="required" maxlength="250">
                            <label for="nameId">Наименование шаблона</label>
                        </div>
                    </div>
                    </c:if>

                    <div class="row">
                        <c:if test="${editAllowed}">
                        <div class="col s6"><h4>Критерии</h4></div>
                        </c:if>
                        <div class="col s6"><h4>Шаблон</h4></div>
                    </div>

                    <div class="row">
                        <c:if test="${editAllowed}">
                        <div class="col s6">

                            <ul class="collapsible criteria_from_ul" data-collapsible="accordion" style="overflow-y: auto; height: 300px;" >
                                <c:forEach items="${requestScope.criterionList}" var="criterion" >
                                    <li data-id="${criterion.id}">
                                        <div class="collapsible-header">${criterion.name}<a class="secondary-content criteria_from" style="cursor: pointer;"><i class="material-icons">send</i></a></div>
                                        <div class="collapsible-body">
                                            <p>Описание: ${criterion.description}</p>
                                            <p>Тип: ${criterion.profileCriterionType.name}</p>
                                            <p>Группа: ${criterion.profileCriterionGroup.name}</p>
                                        </div>
                                    </li>
                                </c:forEach>
                            </ul>
                        </div>
                        </c:if>
                        <c:if test="${editAllowed}">
                            <div class="col s6">
                                <ul class="collapsible criteria_to_ul" data-collapsible="accordion" style="overflow-y: auto; height: 300px;" >
                                    <c:forEach items="${requestScope.criterionProfileList}" var="criterion" >
                                        <li data-id="${criterion.id}">
                                            <div class="collapsible-header">${criterion.name}<c:if test="${editAllowed}"><a class="secondary-content criteria_to" style="cursor: pointer;"><i class="material-icons" style="transform: rotate(180deg);">send</i></a></div></c:if>
                                            <div class="collapsible-body">
                                                <p>Описание: ${criterion.description}</p>
                                                <p>Тип: ${criterion.profileCriterionType.name}</p>
                                                <p>Группа: ${criterion.profileCriterionGroup.name}</p>
                                            </div>
                                            <input type="hidden" name="ids[]" value="${criterion.id}" />
                                        </li>
                                    </c:forEach>
                                </ul>
                            </div>
                        </c:if>
                    </div>
                    <c:if test="${editAllowed}">
                    <div class="row">
                        <div class="input-field col s12">
                            <input type="checkbox" id="defaultId" name="default" ${requestScope.defaultChecked}  />
                            <label for="defaultId">Использовать по умолчанию</label>
                        </div>
                    </div>

                    <div class="row">
                        <div class="input-field col s4">
                            <button class="btn waves-effect waves-light" type="submit" name="save">Сохранить</button>
                        </div>
                    </div>
                    </c:if>
                </form>
            </div>


            <div class="row">
                <table class="striped">
                    <thead>
                    <tr>
                        <th>Список шаблонов</th>
                        <th></th>
                    </tr>
                    </thead>

                    <tbody>
                    <c:forEach items="${requestScope.templates}" var="template">
                        <tr>
                            <td>
                                ${template.name}
                                <c:if test="${template.isDefault()}">
                                     [ по умолчанию ]
                                </c:if>
                            </td>
                            <c:if test="${editAllowed}">
                                <td style="width: 100px;">
                                    <a href="${pageContext.request.contextPath}/profile/template?edit=${template.id}&group=${requestScope.currentGroup.id}"><i class="material-icons">mode_edit</i></a>
                                    <c:if test="${editAllowed}"><a href="${pageContext.request.contextPath}/profile/template?delete=${template.id}&group=${requestScope.currentGroup.id}"><i class="material-icons">delete</i></a></c:if>
                                </td>
                            </c:if>
                        </tr>
                    </c:forEach>
                    </tbody>
                </table>
            </div>

        </div>
    </div>
</main>

<script type="text/javascript">

    $('.collapsible').collapsible();

    setTimeout(function () {
        $('#success').fadeOut('slow')
    }, 4000);
    setTimeout(function () {
        $('#error').fadeOut('slow')
    }, 4000);

    $(document).on('click', '.criteria_from', function() {
        let $ul = $('.criteria_to_ul');
        let $li = $(this).closest("li");
        $(this).removeClass("criteria_from");
        $(this).addClass("criteria_to");
        $(this).find("i").removeAttr("style");
        $(this).find("i").attr("style", "transform: rotate(180deg);");
        $($li).append("<input type='hidden' name='ids[]' value='" + $li.data('id') + "' />");
        $($li).appendTo($ul);
    });

    $(document).on('click', '.criteria_to', function() {
        let $ul = $('.criteria_from_ul');
        let $li = $(this).closest("li");
        $(this).removeClass("criteria_to");
        $(this).addClass("criteria_from");
        $(this).find("i").removeAttr("style");
        $($li).find("input").remove();
        $($li).appendTo($ul);
    });
</script>

<%@include file="../footer.jsp"%>