<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>


<jsp:include page="../header.jsp">
    <jsp:param name="title" value="Критерии"/>
</jsp:include>

<c:set var="editAllowed" scope="session" value="${false}"/>

<sec:authorize access="hasRole('PC_E')">
    <c:set var="editAllowed" scope="session" value="${true}"/>
</sec:authorize>

<main class="container">
    <div class="z-depth" style="padding: 20px">
        <div style="margin-left: .75rem; margin-right: .75rem;">

            <c:if test="${requestScope.success != null}" >
                <div class="row" id="success">
                    <div class="col s12" style="border: 1px solid darkblue; background-color: lightblue; ">
                        <p style="color: darkblue; font-weight: bold"><%= request.getAttribute("success")%></p>
                    </div>
                </div>
            </c:if>

            <c:if test="${requestScope.error != null}" >
                <div class="row" id="error">
                    <div class="col s12" style="border: 1px solid darkred; background-color: lightpink; ">
                        <p style="color: darkred; font-weight: bold"><%= request.getAttribute("error")%></p>
                    </div>
                </div>
            </c:if>

            <div class="row">
                <form class="col s12" method="post" id="qwe">
                    <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
                    <c:if test="${editAllowed}">
                    <div class="row">
                        <div class="input-field col s12">
                            <input id="criterionIdValue" name="criterionIdValueName"
                                   value="${requestScope.profileCriterionTemplate.id}"
                                   type="hidden">
                            <input id="nameId" name="criterionName" type="text"
                                   value="${requestScope.profileCriterionTemplate.name}" class="validate" maxlength="250">
                            <label for="nameId">Наименование критерия</label>
                        </div>
                    </div>
                    </c:if>

                    <div class="row">
                        <c:if test="${editAllowed}">
                        <div class="input-field col s4">

                            Группа анкет:
                            <select name="profileGroup" id="profileGroupId">
                                <option disabled selected>Выберите группу анкет</option>
                                <c:forEach items="${requestScope.profileCriterionGroups}" var="profileCriterionGroup">
                                    <option value="${profileCriterionGroup.id}"
                                            <c:if test="${requestScope.profileCriterionTemplate
                                            .profileCriterionGroupId == profileCriterionGroup.id}">selected</c:if> >
                                        <c:out value="${profileCriterionGroup.name}"></c:out> </option>
                                </c:forEach>
                            </select>


                        </div>

                        <div class="input-field col s2">
                        </div>

                        <div class="input-field col s4">
                            Тип анкет:
                            <select name="profileType" id="profileTypeId">
                                <option disabled selected>Выберите тип анкет</option>
                                <c:forEach items="${requestScope.profileCriterionTypes}" var="profileCriterionType">
                                    <option value="<c:out value="${profileCriterionType.id}"></c:out>"
                                            <c:if test="${requestScope.profileCriterionTemplate
                                            .profileCriterionTypeId == profileCriterionType.id}">selected</c:if>   >
                                        <c:out value="${profileCriterionType.name}"></c:out> </option>
                                </c:forEach>
                            </select>

                            Максимальное количество балов:
                            <input id="maxGradeInputFieldId" type="text"
                                    value="${maxGradeVal}"
                                   name="maxGrade" style="width: 50px" readonly maxlength="250">

                        </div>

                        <div class="input-field col s2">
                        </div>

                        <div class="row">
                            <div class="input-field col s12">
                                <input name="profileDescription" type="text" id="descriptId"
                                       value="${requestScope.profileCriterionTemplate.description}"
                                       class="validate" maxlength="250">
                                <label for="descriptId">Описание характеристики</label>




                            </div>
                        </div>
                        </c:if>

                        <div class="row">
                            <div class="input-field col s12">

                                Расшифровка

                                <table id="transcriptTable">
                                    <thead>
                                    <tr>
                                        <th></th>
                                        <th>Минимальный бал</th>
                                        <th>Максимальный бал</th>
                                        <th>Краткое описание</th>
                                        <th>Расшифровка</th>
                                    </tr>
                                    </thead>

                                    <tbody>
                                    <c:forEach items="${requestScope.transcripts}" var="transcript">
                                        <tr>
                                            <td style="width: 0px" >
                                                <input style="width: 0px"
                                                       type="hidden" value="<c:out value="${transcript.id}"></c:out>"
                                                >
                                            </td>
                                            <td ><input id="minTypeGrade0"
                                                        value="<c:out value="${transcript.minGrade}"></c:out>"
                                                        type="number" min="1"></td>
                                            <td ><input id="maxTypeGrade0"
                                                        value="<c:out value="${transcript.maxGrade}"></c:out>"
                                                        type="number" min="1"></td>
                                            <td ><input id="briefTypeDescription0"
                                                        value="<c:out value="${transcript.description}"></c:out>"
                                                        type="text" maxlength="250"></td>
                                            <td ><input id="descriptionType0"
                                                        value="<c:out value="${transcript.textValue}"></c:out>"
                                                        type="text" maxlength="250"></td>
                                            <c:if test="${editAllowed}"><td>
                                                <a href="javascript:void(0)"
                                                   onclick="javascript:deleteTranscriptRow(this)" >
                                                <i class="material-icons">delete</i></a>
                                            </td></c:if>

                                        </tr>
                                    </c:forEach>

                                    </tbody>
                                </table>


                                <c:if test="${editAllowed}">
                                <a href="javascript:void(0);"
                                   onclick="addRowToTranscriptTable()"
                                   name="addTranscriptRaw"
                                ><i class="waves-effect waves-light btn">Добавить транскрипт</i></a>
                                </c:if>






                            </div>
                        </div>

                        <c:if test="${editAllowed}">
                        <div class="row">
                            <div class="input-field col s12">


                                <button
                                        class="btn waves-effect waves-light"
                                        type="button"
                                        id="saveCriterionButtonId"
                                        name = "changeCriterion"
                                        value="saveCriterionButton"
                                >Сохранить критерий</button>


                            </div>
                        </div>
                        </c:if>




                    </div>
                </form>
            </div>

        </div>
    </div>
</main>






<script>

    // Inserted rows in transcript table.
    var insertedTranscriptRows = 0;

    // Counter for json sent operations. When json send data
    // counter increase to 1 when json get response counter decrease.
    var tmp = 0;

    // Number of raw of transcript table.
    var transcriptRawNumbers = 0;

    // Array of transcript raw.
    var transcriptArr = [];



    // Selector initialization
    $('select').material_select();

    // Insert row in transcript table.
    function addRowToTranscriptTable() {


        cloneRow();

        setZeroIdOfNewRaw();
    }


    // Clone last transcript row with new id for tr and td elements.
    // Every next row increase id on 1.
    // For example first:  trId,
    function cloneRow() {
        $("#transcriptTable tr").eq(1).clone().appendTo("#transcriptTable");


        setZeroIdOfNewRaw();

    }


    // Set value of all new transcript input id to 0.
    function setZeroIdOfNewRaw() {
        $("#transcriptTable tr:last").find("input").eq(0).val(0);
    }













    /* Begin transmitting data from web to server. */
    $('#saveCriterionButtonId').on("click", function() {

        sendCriterionTemplate();
    });


    /* Get field values from view and send as ProfileCriterionTemplate
     * object to server. */
    function sendCriterionTemplate() {

        var isDataOk =  checkEmptyFields();

        if (isDataOk === true) {


            var dataCriterion = {
                id: $("#criterionIdValue").val(),
                name: $("#nameId").val(),
                description: $("#descriptId").val(),
                profileCriterionTypeId: parseInt($("#profileTypeId option:selected").val()),
                profileCriterionGroupId: parseInt($("#profileGroupId option:selected").val()),
                createUserId: 0,
                editUserId: 0,
                editDate: null,
                isDeleted: false,
                profileCriterionType: null,
                profileCriterionGroup: null
            };


            $.ajax({
                url: "<%=request.getContextPath()%>/profile/criterion/edit/editCriterion",
                method: "post",
                data: JSON.stringify(dataCriterion),
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                /* If success adding of criterion than add transcripts
                 * to new criterion id. */
                success: function(criterionId) {

                    var criterionId = criterionId['criterionIdVal'];

                    console.log("criterion transmitted successfully");
                    console.log("Criterion id: " + criterionId);

                    sendTranscripts(criterionId);
                }

            });
        }
    }


    /* Get all values in transcript raw and send them as
     * json array object to server. */
    function sendTranscripts(criterionId) {
        len = $('#transcriptTable> tbody > tr > td > input').length;

        len = $('#transcriptTable> tbody > tr > td > input').length;

        transcriptRawNumbers = len / 5;

        var id = 0;
        var profileCriterionTemplateId = 0
        var minGrade = 0;
        var maxGrade = 0;
        var descr = "";
        var textVal = "";

        for(var i = 0; i < len; i = i + 5)
        {
            id = $('#transcriptTable> tbody > tr > td > input')[i].value;
            profileCriterionTemplateId = $("#criterionIdValue").val();
            minGrade = $('#transcriptTable> tbody > tr > td > input')[i+1].value;
            maxGrade = $('#transcriptTable> tbody > tr > td > input')[i+2].value;
            descr = $('#transcriptTable> tbody > tr > td > input')[i+3].value;
            textVal = $('#transcriptTable> tbody > tr > td > input')[i+4].value;

            var data = {
                id: id,
                profileCriterionTemplateId: profileCriterionTemplateId,
                minGrade:  minGrade,
                maxGrade: maxGrade,
                description: descr,
                textValue: textVal,
                isDeleted: false
            };

            transcriptArr.push(data);
        }

        sendData();
    }





    // Send all raw of transcript to server through json.
    function sendData() {

        console.log("Transcript array:" + transcriptArr);

        $.ajax({
            url: "<%=request.getContextPath()%>/profile/criterion/edit/addEditTranscripts",
            method: "post",
            data: JSON.stringify(transcriptArr),
            dataType: "json",
            contentType: "application/json; charset=utf-8",


            // If success than redirect to add criterionTemplate web page.
            success: function(isSuccess){
                console.log("Response after add transcripts: " +
                    isSuccess['success']);
                window.location.replace('<%=request.getContextPath()%>/profile/criterion');
            }

        });

    }



    /* Function check empty fields in every field on this page. */
   function checkEmptyFields() {
       if ($("#nameId").val() === "")
       {
           alert("Поле \"Наименование критерия\" не заполнено.");
           return false;
       }
       if ($("#profileGroupId").val() === null)
       {
           alert("Группа анкет не выбрана");
           return false;
       }
       if ($("#profileTypeId").val() === null)
       {
           alert("Тип анкет не выбран.")
           return false;
       }
       if ($("#descriptId").val() === "")
       {
           alert("Поле \"Описание характеристики\" не заполнено.");
           return false;
       }

       return checkEmptyFieldsInTranscript();
   }


   /* Function check every empty field in transcript table. */
   function checkEmptyFieldsInTranscript() {
       len = $('#transcriptTable> tbody > tr > td > input').length;
       for(var i = 0; i < len; i = i + 1)
       {
           someVal = $('#transcriptTable> tbody > tr > td > input')[i].value;
           if (someVal == "")
           {
               alert("Одно из полеЙ в таблице \"Расшифровка\" не заполнено.");
               return false;
           }
       }

       return true;
   }


    // Function remove transcript if it already exist than delete transcript
    // from database and remove in view. If it is new transcript than
    // remove transcript from view.
    function deleteTranscriptRow(context) {

       var id = $(context).closest('tr').children().children()[0].value;

       // If it is row that already exist (id != 0) in data base than delete it
        // in data base by request.
       if (id > 0)
       {
           $.ajax({
               url: "<%=request.getContextPath()%>/profile/criterion/edit/deleteTranscript",
               method: 'post',
               dataType: 'json',
               data:  {transcriptIdForDelete : id},

               // If transcript successfully deleted than delete this raw in view.
               success: function (data) {
                   console.log("Transcript successfully deleted: " + data['success']);
                   if (data['success'] === "success")
                   {

                       var inputColumnNum = $('#transcriptTable> tbody > tr > td > input').length;

                       /* If it is last row than make new row by cloning,
                        delete old row in view and than clean data from new row. */
                       if (inputColumnNum === 5)
                       {
                           cloneRow();
                           $(context).closest('tr').remove();
                           cleanFirstTranscriptRow();
                       }
//                     /* If it is not last row than simply delete row from view. */
                       else
                       {
                           $(context).closest('tr').remove();
                       }


                   }
               }
           });
       }
       // If id == 0 than it is new row. It not exist in data base
       // so simply delete this row in view.
       if (id === "0")
       {
           $(context).closest('tr').remove();
       }

    }


    // Function clean first transcript raw.
    function cleanFirstTranscriptRow() {
        $('#transcriptTable> tbody > tr > td > input')[0].value = 0;
        $('#transcriptTable> tbody > tr > td > input')[1].value = "";
        $('#transcriptTable> tbody > tr > td > input')[2].value = "";
        $('#transcriptTable> tbody > tr > td > input')[3].value = "";
        $('#transcriptTable> tbody > tr > td > input')[4].value = "";

    }




</script>



<%@include file="../footer.jsp"%>