<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<jsp:include page="../header.jsp">
    <jsp:param name="title" value="Анкета"/>
</jsp:include>

<div class="container" style="padding: 20px;">
    <form method="post">
        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
        <div class="row">
            <div class="col s6"><h6 class="left-align">Студент: <c:out value="${requestScope.studentName}"></c:out></h6>
                                <h6 class="left-align">Преподаватель: <c:out value="${requestScope.teacherName}"></c:out></h6>
            </div>
            <div class="col s6">
                <div class="switch right-align ">
                    <label>
                        Анкета заполнена<input name="is_finished" type="checkbox" <c:if test="${requestScope.isFinished eq true}">checked="checked"</c:if> <sec:authorize access="!hasRole('P_E')">disabled="disabled"</sec:authorize> />
                        <span class="lever"></span>
                    </label>
                </div>
            </div>
        </div>
        <div class="row">
            <%--<div class="col s6">--%>
                <div class="input-field col s6">
                    <sec:authorize access="hasRole('P_E')">
                    <select id="template" onchange="showModal(this, 'respawn', this.value); return false;">
                        <option value="" disabled selected>Сменить шаблон</option><c:forEach items="${requestScope.templates}" var="template">
                        <option value="${template.id}">${template.name}</option></c:forEach>
                    </select>
                    </sec:authorize>
                </div>
            <%--</div>--%>
            <div class="col s6">
                <div class="right-align">
                    <sec:authorize access="hasRole('P_E')">
                    <button class="btn waves-effect waves-light " type="submit" name="save">Сохранить анкету</button>
                    </sec:authorize>
                </div>
            </div>
        </div>

        <ul class="tabs" >
            <c:forEach var="criterionGroupsToTypesMap" items="${requestScope.criterionGroupsToTypesMap}">
            <li class="tab col s12"><a href="#cr_type_${criterionGroupsToTypesMap.key.id}">${criterionGroupsToTypesMap.key.name}</a></li>
            </c:forEach>
        </ul>
        <c:forEach var="criterionGroupsToTypesMap" items="${requestScope.criterionGroupsToTypesMap}">
            <div id="cr_type_${criterionGroupsToTypesMap.key.id}" class="col s12">
                <table class="bordered highlight responsive-table">
                    <thead>
                        <tr>
                            <th></th>
                            <sec:authorize access="hasRole('P_E')">
                                <c:forEach var="i" begin="1" end="${criterionGroupsToTypesMap.key.maxGrade}">
                                    <th>${i}</th>
                                </c:forEach>
                            </sec:authorize>
                            <sec:authorize access="!hasRole('P_E') and hasRole('P_V')">
                                <th>Балл</th>
                                <th>Расшифровка</th>
                            </sec:authorize>
                        </tr>
                    </thead>
                    <c:forEach var="criterionGroup" items="${criterionGroupsToTypesMap.value}">
                        <thead id="${criterionGroup.id}">
                        <tr>
                            <th>${criterionGroup.name}</th>
                            <sec:authorize access="!hasRole('P_E') and hasRole('P_V')">
                                <th></th>
                                <th id="top"></th>
                            </sec:authorize>
                        </tr>
                        </thead>
                        <tbody id="${criterionGroup.id}">
                        <c:forEach var="profileCriterion" items="${requestScope.profileCriteria}">
                            <c:if test="${profileCriterion.profileCriterionTypeId eq criterionGroupsToTypesMap.key.id and profileCriterion.profileCriterionGroupId eq criterionGroup.id}">
                            <tr id="${profileCriterion.grade}">
                                <td id="name">${profileCriterion.name}</td>
                                <sec:authorize access="hasRole('P_E')">
                                        <c:forEach var="i" begin="1" end="${criterionGroupsToTypesMap.key.maxGrade}">
                                            <td>
                                                <input name="cr_id_${profileCriterion.id}" value="${i}" type="checkbox" id="cr_id_${profileCriterion.id}_v_${i}" <c:if test="${profileCriterion.grade eq i}">checked="checked"</c:if> />
                                                <c:set var="tooltip" scope="session" value="Описание балла не указано в шаблоне критерия."/>
                                                <c:forEach var="transcript" items="${profileCriterion.transcripts}">
                                                    <c:if test="${transcript.minGrade le i and transcript.maxGrade ge i}">
                                                        <c:set var="tooltip" scope="session" value="${transcript.description}"/>
                                                    </c:if>
                                                </c:forEach>
                                                <label for="cr_id_${profileCriterion.id}_v_${i}" class="tooltipped" data-position="bottom" data-delay="50" data-tooltip="${tooltip}"></label>
                                            </td>
                                        </c:forEach>
                                </sec:authorize>
                                <sec:authorize access="!hasRole('P_E') and hasRole('P_V')">
                                        <td>${profileCriterion.grade>0 ? profileCriterion.grade : ""}</td>
                                        <c:set var="gradeDetailes" scope="session" value="<Текст расшифровки не указан в шаблоне критерия.>"/>
                                        <c:if test="${profileCriterion.grade eq 0}">
                                            <c:set var="gradeDetailes" scope="session" value="Оценка не проставлена"/>
                                        </c:if>
                                        <c:forEach var="transcript" items="${profileCriterion.transcripts}">
                                            <c:if test="${transcript.minGrade le profileCriterion.grade and transcript.maxGrade ge profileCriterion.grade}">
                                                <c:set var="gradeDetailes" scope="session" value="${transcript.textValue}"/>
                                            </c:if>
                                        </c:forEach>
                                        <td>${gradeDetailes}</td>
                                </sec:authorize>
                            </tr>
                            </c:if>
                        </c:forEach>
                        </tbody>
                    </c:forEach>
                </table>
            </div>
        </c:forEach>
    </form>
</div>

<div id="modal1" class="modal modal-fixed-footer">
    <div class="modal-content">
        <h5 id="errorHeader"></h5>
        <p id="errorText"></p>
    </div>
    <div class="modal-footer">
        <a href="#" class="modal-action modal-close waves-effect waves-green btn-flat">Ok</a>
    </div>
</div>

<div id="respawn" class="modal">
    <div class="modal-content">
        <h5>Вы уверены, что хотите сменить шаблон?</h5>
        <p>Смена шаблона приведёт к удалению существующей анкеты.</p>
    </div>
    <div class="modal-footer">
        <a class="waves-effect waves-red btn-flat" onclick="$('#respawn').modal('close'); return false;">Cancel</a>
        <a class="waves-effect waves-green btn-flat" id="respawn_YesBtn">Yes</a>
    </div>
</div>

<script>
    $("input:checkbox").on('click', function() {
        var $box = $(this);
        var $box_finished = $("input:checkbox[name='is_finished']");
        var $btn = $("button[name='save']");

        if ($box.attr("name") === "is_finished"){
            if ($box.is(":checked")) {
                var group = "input:checkbox[name!='" + $box.attr("name") + "']";
                var res = [];

                $(group).each(function() {
                    var crname = $.trim($(this).closest("tr").text());
                    var group_name = "input:checkbox[name='" +  $(this).attr("name") + "']:checked"
                    if((!($(group_name).length > 0))&&($.inArray(crname, res) < 0)){
                        res.push(crname);
                    }
                });

                if (res.length > 0) {
                    $box.prop("checked", false);

                    $('h5#errorHeader').text("Не выставлена оценка по следующим критериям:");
                    $('p#errorText').html(res.join("<br>"));
                    $('#modal1').modal('open');
                }
            }else{
                //var group = "input:checkbox[name!='" + $box.attr("name") + "']";
                //$(group).removeAttr("disabled");
            }
        }

        if ($box_finished.is(":checked")) {
            //$btn.attr("disabled", "disabled");
        } else {
            $btn.removeAttr("disabled");
        }

        if ($box_finished.is(":checked") && ($box.attr("name")!="is_finished")) {
            $box.prop("checked", !$box.is(":checked"));
        } else {
            if ($box.is(":checked")) {
                var group = "input:checkbox[name='" + $box.attr("name") + "']";
                $(group).prop("checked", false);
                $box.prop("checked", true);
            } else {
                $box.prop("checked", false);
            }
        }
    });

    $( document ).ready(function() {
        var $box = $("input:checkbox[name='is_finished']");
        var $btn = $("button[name='save']");
        var $slc = $("select[name='template']");
        if ($box.is(":checked")) {
            $btn.attr("disabled", "disabled");
            $slc.attr("disabled", "disabled");
        } else {
            $btn.removeAttr("disabled");
            $slc.removeAttr("disabled");
        }
    });

    $(document).ready(function(){
        $('.modal').modal();
        $('select').material_select();
    });

    function getTopCriteria(groupId, count){
        var rows = $('tbody#'+groupId+'>tr').filter(function() {
            return $(this).attr("id") > "0";
        }).get();
        rows.sort(function(a, b) {
            var A = parseInt($(a).attr('id'));
            var B = parseInt($(b).attr('id'));
            if(A > B) {
                return -1;
            }
            if(A < B) {
                return 1;
            }
            return 0;
        });

        var rows2 = rows.slice(0,count);

        var result = "";

        $.each(rows2, function(index, row) {
            result = result + $(row).children('td[id="name"]').text() + ", ";
        });

        return result.slice(0, -2);
    }

    $(document).ready(function(){
        var heads = $('thead[id]').get();

        $.each(heads, function(index, head){
            var id = parseInt($(head).attr('id'))

            $(head).children('tr').eq(0).children('th[id="top"]').text(getTopCriteria(id, 3));
        })
    });

    function showModal(but, modal, ref){
        console.log('val: ' + ref);
        $('#' + modal).modal('open');
        $('#' + modal + '_YesBtn').click(function(){ $('#' + modal).modal('close'); document.location = '${pageContext.request.contextPath}/profile/add?template=' + ref + '&studentId=${requestScope.studentId}&group=${requestScope.currentGroup.id}'; });
    }
</script>

<%@include file="../footer.jsp"%>