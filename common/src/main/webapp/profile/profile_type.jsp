﻿<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<jsp:include page="../header.jsp">
    <jsp:param name="title" value="Типы критериев"/>
</jsp:include>

<c:set var="editAllowed" scope="session" value="${false}"/>

<sec:authorize access="hasRole('PC_E')">
    <c:set var="editAllowed" scope="session" value="${true}"/>
</sec:authorize>

<main class="container">
    <div class="z-depth" style="padding: 20px">
        <div style="margin-left: .75rem; margin-right: .75rem;">

            <c:if test="${requestScope.success != null}" >
                <div class="row" id="success">
                    <div class="col s12" style="border: 1px solid darkblue; background-color: lightblue; ">
                        <p style="color: darkblue; font-weight: bold"><%= request.getAttribute("success")%></p>
                    </div>
                </div>
            </c:if>

            <c:if test="${requestScope.error != null}" >
                <div class="row" id="error">
                    <div class="col s12" style="border: 1px solid darkred; background-color: lightpink; ">
                        <p style="color: darkred; font-weight: bold"><%= request.getAttribute("error")%></p>
                    </div>
                </div>
            </c:if>

            <c:if test="${editAllowed}">
            <div class="row">
                <form method="post" class="col s12">
                    <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
                    <input type="hidden" name="typeId" value="${requestScope.editType.id}" />
                    <div class="row">
                        <div class="input-field col s12">
                            <input id="nameId" name="name" type="text" class="validate" value="${requestScope.editType.name}" required="required" maxlength="250">
                            <label for="nameId">Наименование типа критерия</label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col s4">
                            <input id="maxId" name="max" type="number" min="0" max="10" class="validate" value="${requestScope.editType.maxGrade}" required="required">
                            <label for="maxId">Максимальный бал</label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col s4">
                            <button class="btn waves-effect waves-light" type="submit" name="save">Сохранить</button>
                        </div>
                    </div>
                </form>
            </div>
            </c:if>

            <div class="row">
                <table class="striped">
                    <thead>
                    <tr>
                        <th>Наименование типа</th>
                        <th>Максимальная оценка</th>
                        <c:if test="${editAllowed}"><th></th></c:if>
                    </tr>
                    </thead>

                    <tbody>
                    <c:forEach items="${requestScope.types}" var="type">
                        <tr>
                            <td>${type.name}</td>
                            <td>${type.maxGrade}</td>
                            <c:if test="${editAllowed}">
                            <td>
                                <a href="${pageContext.request.contextPath}/profile/type?edit=${type.id}&group=${requestScope.currentGroup.id}"><i class="material-icons">mode_edit</i></a>
                                <a href="${pageContext.request.contextPath}/profile/type?delete=${type.id}&group=${requestScope.currentGroup.id}"><i class="material-icons">delete</i></a>
                            </td>
                            </c:if>
                        </tr>
                    </c:forEach>
                    </tbody>
                </table>
            </div>

        </div>
    </div>
</main>

<script type="text/javascript">
    setTimeout(function () {
        $('#success').fadeOut('slow')
    }, 4000);
    setTimeout(function () {
        $('#error').fadeOut('slow')
    }, 4000);
</script>

<%@include file="../footer.jsp"%>