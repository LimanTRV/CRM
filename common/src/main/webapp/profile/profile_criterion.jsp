﻿﻿<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>


<jsp:include page="../header.jsp">
    <jsp:param name="title" value="Критерии"/>
</jsp:include>

<c:set var="editAllowed" scope="session" value="${false}"/>

<sec:authorize access="hasRole('PC_E')">
    <c:set var="editAllowed" scope="session" value="${true}"/>
</sec:authorize>

<main class="container">
    <div class="z-depth" style="padding: 20px">
        <div style="margin-left: .75rem; margin-right: .75rem;">

            <c:if test="${requestScope.success != null}" >
                <div class="row" id="success">
                    <div class="col s12" style="border: 1px solid darkblue; background-color: lightblue; ">
                        <p style="color: darkblue; font-weight: bold"><%= request.getAttribute("success")%></p>
                    </div>
                </div>
            </c:if>

            <c:if test="${requestScope.error != null}" >
                <div class="row" id="error">
                    <div class="col s12" style="border: 1px solid darkred; background-color: lightpink; ">
                        <p style="color: darkred; font-weight: bold"><%= request.getAttribute("error")%></p>
                    </div>
                </div>
            </c:if>

            <div class="row">
                <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />

                <form class="col s12" method="get" id="qwe" >
                    <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
                    <c:if test="${editAllowed}">
                    <div class="row">
                        <div class="input-field col s12">
                            <input id="nameId" name="criterionName" type="text"
                                    class="validate" maxlength="250">
                            <label for="nameId">Наименование критерия</label>
                        </div>
                    </div>
                    </c:if>

                    <div class="row">
                        <c:if test="${editAllowed}">
                        <div class="input-field col s4">

                            Группа анкет:
                            <select name="profileGroup" id="profileGroupId">
                                <option disabled selected>Выберите группу анкет</option>
                                <c:forEach items="${requestScope.profileCriterionGroups}" var="profileCriterionGroup">
                                    <option value="${profileCriterionGroup.id}" >
                                        <c:out value="${profileCriterionGroup.name}"></c:out> </option>
                                </c:forEach>
                            </select>


                        </div>

                        <div class="input-field col s2">
                        </div>

                        <div class="input-field col s4">
                            Тип анкет:
                            <select name="profileType" id="profileTypeId">
                                <option disabled selected>Выберите тип анкет</option>
                                <c:forEach items="${requestScope.profileCriterionTypes}" var="profileCriterionType">
                                    <option value="<c:out value="${profileCriterionType.id}"></c:out>" >
                                        <c:out value="${profileCriterionType.name}"></c:out> </option>
                                </c:forEach>
                            </select>

                            Максимальное количество балов:
                            <input id="maxGradeInputFieldId" type="text"

                                   name="maxGrade" style="width: 50px" readonly maxlength="250">

                        </div>

                        <div class="input-field col s2">
                        </div>

                        <div class="row">
                            <div class="input-field col s12">
                                <input name="profileDescription" type="text" id="descriptId"
                                       value="${requestScope.profileCriterionTemplate.description}"
                                       class="validate" maxlength="250">
                                <label for="descriptId">Описание характеристики</label>




                            </div>
                        </div>


                        <div class="row">
                            <div class="input-field col s12">

                                Расшифровка

                                <table id="transcriptTable">
                                    <thead>
                                        <tr>
                                            <th>Минимальный бал</th>
                                            <th>Максимальный бал</th>
                                            <th>Краткое описание</th>
                                            <th>Расшифровка</th>
                                        </tr>
                                    </thead>

                                    <tbody>

                                        <tr>
                                                <td ><input id="minTypeGrade0"
                                                            type="number" min="1"></td>
                                                <td ><input id="maxTypeGrade0"
                                                            type="number" min="1"></td>
                                                <td ><input id="briefTypeDescription0"
                                                            type="text" maxlength="250"></td>
                                                <td ><input id="descriptionType0"
                                                            type="text" maxlength="250"></td>
                                                <td>
                                                    <a href="javascript:void(0)" id="deleteTranscriptRow"
                                                       onclick="javascript:deleteTranscriptRow(this)" >
                                                        <i class="material-icons">delete</i></a>
                                                </td>
                                        </tr>

                                    </tbody>
                                </table>



                                <a href="javascript:void(0);"
                                   onclick="addRowToTranscriptTable()"
                                   name="addTranscriptRaw"
                                ><i class="waves-effect waves-light btn">Добавить расшифровку</i></a>







                            </div>
                        </div>

                        <div class="row">
                            <div class="input-field col s12">

                                <button
                                        class="btn waves-effect waves-light"
                                        type="button"
                                        id="addCriterionButtonId"
                                        name = "changeCriterion"
                                        value="addCriterionButton"
                                        >Добавить критерий</button>





                            </div>
                        </div>
                        </c:if>

                        <div class="row">
                            <div class="input-field col s12">





                                <table  class="striped">

                                    <thead>
                                    <tr>
                                        <th align="center">Имя критерия</th>
                                        <th align="center">Описание</th>
                                        <c:if test="${editAllowed}">
                                            <th align="center">Редактирование</th>
                                        </c:if>
                                    </tr>
                                    </thead>

                                    <tbody>
                                    <c:forEach items="${requestScope.profileCriterionTemplates}" var="profileCriterionTemplate">
                                        <tr>
                                            <td align="center">
                                                <c:out value="${profileCriterionTemplate.name} "></c:out>
                                            </td>

                                            <td align="center">
                                                <p>
                                                    <c:out value="${profileCriterionTemplate.description} "></c:out>
                                                </p>

                                            </td>
                                            <c:if test="${editAllowed}">
                                                <td align="center">
                                                    <a  onclick="setEditMode()"
                                                        href="${pageContext.request.contextPath}/profile/criterion/edit?edit=${profileCriterionTemplate.id}">
                                                        <i class="material-icons">mode_edit</i></a>
                                                    <a href="${pageContext.request.contextPath}/profile/criterion?delete=${profileCriterionTemplate.id}">
                                                        <i class="material-icons">delete</i></a>
                                                </td>
                                            </c:if>
                                        </tr>

                                    </c:forEach>
                                    </tbody>
                                </table>

                            </div>
                        </div>



                    </div>
                </form>
            </div>

        </div>
    </div>
</main>






<script>

    // Inserted rows in transcript table.
    var insertedTranscriptRows = 0;

    // Number of raw of transcript table.
    var transcriptRawNumbers = 0;

    // Array of transcript raw.
    var transcriptArr = [];




    // Selector initialization
    $('select').material_select();

    // Insert row in transcript table.
    function addRowToTranscriptTable() {

        var addTranscriptTableRowCounter = addTranscriptTableRowCounter + 1;

        insertedTranscriptRows = insertedTranscriptRows + 1;

        cloneRowWithNewId();




    }


    // Clone last transcript row with new id for tr and td elements.
    // Every next row increase id on 1.
    // For example first:  trId,
    function cloneRowWithNewId() {

        $("#transcriptTable tr").eq(1).clone().attr('id', 'someId')
            .appendTo("#transcriptTable");

    }
    
    
    

    
    





    // Send selected profile type and set max Grade in view.
    $(document).ready(function() {
        $("#profileTypeId").change(function(){

            console.log("Selected type id" + $('#profileTypeId option:selected').val());

            $.ajax({
                url: '<%=request.getContextPath()%>/profile/criterion/maxGrade',
                method: 'post',
                dataType: 'json',
                data:  {sendSelectedType : $('#profileTypeId option:selected').val()},
                success: function (data) {
                    console.log("Success Max grade: " + data['maxGrade']);
                    $('input[name=maxGrade]').val(data['maxGrade']);
                },
                fail: function (data) {
                    console.log("Fail Max grade: " + data['maxGrade']);
                }

            });
        });
    });



    /* Get all values in transcript raw and send them as
     * json array object to server. */
    $('#addCriterionButtonId').on("click", function() {

        sendCriterionTemplate();
    });


    /* Get field values from view and send as ProfileCriterionTemplate
     * object. */
    function sendCriterionTemplate() {

        var isDataOk =  checkEmptyFields();

        if (isDataOk === true) {


            var dataCriterion = {
                id: 0,
                name: $("#nameId").val(),
                description: $("#descriptId").val(),
                profileCriterionTypeId: parseInt($("#profileTypeId option:selected").val()),
                profileCriterionGroupId: parseInt($("#profileGroupId option:selected").val()),
                createUserId: 0,
                editUserId: 0,
                editDate: null,
                isDeleted: false,
                profileCriterionType: null,
                profileCriterionGroup: null
            };


            $.ajax({
                url: "<%=request.getContextPath()%>/profile/criterion/addCriterion",
                method: "post",
                data: JSON.stringify(dataCriterion),
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                /* If success adding of criterion than add transcripts
                   * to new criterion id. */
                  success: function(criterionId) {

                      var criterionId = criterionId['criterionIdVal'];

                      console.log("criterion transmitted successfully");
                      console.log("Criterion id: " + criterionId);

                      sendTranscripts(criterionId);
                  }

            });
        }
    }



     /* Get all values in transcript raw and send them as
     * json array object to server. */
    function sendTranscripts(criterionId) {
        len = $('#transcriptTable> tbody > tr > td > input').length;

        transcriptRawNumbers = len / 4;

        var minGrade = 0;
        var maxGrade = 0;
        var descr = "";
        var textVal = "";

        for(var i = 0; i < len; i = i + 4)
        {
            minGrade = $('#transcriptTable> tbody > tr > td > input')[i].value;
            maxGrade = $('#transcriptTable> tbody > tr > td > input')[i+1].value;
            descr = $('#transcriptTable> tbody > tr > td > input')[i+2].value;
            textVal = $('#transcriptTable> tbody > tr > td > input')[i+3].value;

            var data = {
                id: 0,
                profileCriterionTemplateId: criterionId,
                minGrade:  minGrade,
                maxGrade: maxGrade,
                description: descr,
                textValue: textVal,
                isDeleted: false
            };

            transcriptArr.push(data);
        }

        sendData();
    }





    // Send all raw of transcript to server through json.
    function sendData() {



        console.log("Transcripts: " + transcriptArr);




            console.log("Transcript array:" + transcriptArr);

            $.ajax({
                url: "<%=request.getContextPath()%>/profile/criterion/addTranscripts",
                method: "post",
                data: JSON.stringify(transcriptArr),
                dataType: "json",
                contentType: "application/json; charset=utf-8",


                // If success than submit this page.
                success: function(isSuccess){
                      console.log("Response after add transcripts: " +
                                            isSuccess['success']);
                    $("#qwe").submit();
                }

            });

    }





    /* Function check empty fields in every field on this page. */
    function checkEmptyFields() {
        if ($("#nameId").val() === "")
        {
            alert("Поле \"Наименование критерия\" не заполнено.");
            return false;
        }
        if ($("#profileGroupId").val() === null)
        {
            alert("Группа анкет не выбрана");
            return false;
        }
        if ($("#profileTypeId").val() === null)
        {
            alert("Тип анкет не выбран.")
            return false;
        }
        if ($("#descriptId").val() === "")
        {
            alert("Поле \"Описание характеристики\" не заполнено.");
            return false;
        }

        return checkEmptyFieldsInTranscript();
    }


    /* Function check every empty field in transcript table. */
    function checkEmptyFieldsInTranscript() {
        len = $('#transcriptTable> tbody > tr > td > input').length;
        for(var i = 0; i < len; i = i + 1)
        {
            someVal = $('#transcriptTable> tbody > tr > td > input')[i].value;
            if (someVal == "")
            {
                alert("Одно из полеЙ в таблице \"Расшифровка\" не заполнено.");
                return false;
            }
        }

        return true;
    }



    // Function remove transcript raw.
    function deleteTranscriptRow(context) {

       var inputColumnNum = $('#transcriptTable> tbody > tr > td > input').length;

       /* If it is last row than forbid delete it. */
       if (inputColumnNum > 4)
       {
           $(context).closest('tr').remove();
       }
    }


</script>



<%@include file="../footer.jsp"%>