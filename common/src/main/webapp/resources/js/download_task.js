var downloadedFileName = "";
$(function () {

    $(".dev_git_url").prop('disabled', false);
    $(".dev_doc_input").prop('disabled', true);
    $(".dev_download_but").prop('disabled', true);

    $(".dev_download_but").bind('click', function (e) {
        var fileElem = $('.dev_doc_input');
        if (fileElem) {
            fileElem.click();
        }
        e.preventDefault();
        return false;
    });

    $('.dev_doc_input').change(function () {
        handleFiles(this, this.files);
    });

    $('.dev_save_document_button').on('click', function () {
        saveDownload();
    });
});
$(document).on('click', '.dev_download_delete', function () {
    deleteDownload(this);
});
function disableGit() {
    $("#radioBtnFile").prop('checked', true);
    $("#radioBtnGit").prop('checked', false);
    $(".dev_doc_input").prop('disabled', false);
    $(".dev_download_but").prop('disabled', false);
    $(".dev_git_url").prop('disabled', true);
    $(".dev_git_url").val("");
}

function disableFile() {
    $("#radioBtnFile").prop('checked', false);
    $("#radioBtnGit").prop('checked', true);
    $(".dev_git_url").prop('disabled', false);
    //$(".dev_git_url").val("");
    $(".dev_doc_input").prop('disabled', true);
    $(".dev_download_but").prop('disabled', true);
}

function deleteDoc(el) {
    var new_input = $(el).closest('.dev_download_block').find('.dev_doc_input').clone();
    $(el).closest('.dev_download_block').find('.dev_doc_input').replaceWith($(new_input));
    $('.dev_loaded_document').html('');
    $('.dev_doc_input').change(function () {
        handleWordFiles(this, this.files);

    });

}
;
function handleFiles(el, files) {
    for (var i = 0, f; f = files[i]; i++) {

        var reader = new FileReader();

        reader.onload = (function (f) {
            return function (e) {
                //console.log(f);
                downloadedFileName = f.name;
                $(".dev_loaded_document").html('<div class="document_name">' + f.name + '</div><span class="delete delete_word_document" onclick="deleteDoc(this);"></span>');
            };
        })(f);

        reader.readAsDataURL(f);

    }
}

function saveDownload() {
    if ($('.dev_save_document_button').hasClass('loading')) {
        return;
    }
    $('.dev_save_document_button').addClass('loading');
//TODO studentId
    var bean = {};

    if (typeof $(".dev_task").val() == 'undefined' || $(".dev_task").val() == "" || $(".dev_task").val() == null) {
        alert('Пожалуйста выберите задание!');
        return false;
    } else {
        bean['taskId'] = $('.dev_task').val() * 1;
    }
    bean['gitUrl'] = $('.dev_git_url').val();
    bean['downloadUrl'] = downloadedFileName;
    var dataStr = JSON.stringify(bean);
    var form = $('#downloadForm');
    $('[name=dataStr]').val(dataStr);
    form.ajaxSubmit({
        url: "downloads",
        type: "POST",
        //data: dataStr,
        contentType: 'application/json; charset=utf-8',
        dataType: "json",
        success: function (answer) {
            if (answer) {
                if (answer.status === 'SUCCESS') {
                    location.href = "downloads";
                    alert(answer.message);
                } else {
                    $('.dev_save_document_button').removeClass('loading');
                    alert(answer.message);
                }
            }

        }

    });
}


function deleteDownload(el) {
    var downloadId = $(el).closest('tr').attr('download_id') * 1;
    var dataValue = {'delete': '1', 'downloadId': downloadId};

    var dialogDiv = jQuery('<div/>', {
        title: "Подтверждение удаления",
        html: "Вы уверены, что хотите удалить документ?"
    }).appendTo('body');

    var dialog = dialogDiv.dialog({
            modal: true
            , close: function (event, ui) {
                dialog.dialog("destroy");
                dialog.remove();
            }
            , buttons: [
                {text: "Да",
                    click: function () {
                        $.ajax({
                            url: "downloads",
                            data: dataValue,
                            type: "POST",
                            dataType: "json",
                            success: function (answer) {
                                if (answer) {
                                    if (answer.status === "SUCCESS") {
                                        $(el).closest('tr').remove();
                                    } else {
                                        alert(answer.message);
                                    }
                                }
                            }
                        });
                        $(this).dialog("close");
                    }
                }
                , {text: "Отмена", click: function () {
                    $(this).dialog("close");
                }}
            ]
        }
    );
}