﻿<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<jsp:include page="../header.jsp">
    <jsp:param name="title" value="Tasks and Criteria"/>
</jsp:include>


<main class="container">
    <div class="z-depth-1">
        <div class="row">
            <div class="col s12">
                <ul class="tabs">
                    <li class="tab col s3"><a href="#labs" class="waves-effect">Lab works</a></li>
                    <li class="tab col s3"><a href="#practice" class="waves-effect">Practice works</a></li>
                    <li class="tab col s3"><a href="#crits" class="waves-effect">Criteria</a></li>
                </ul>
            </div>
            <div id="labs" class="col s12"></div>
            <div id="practice" class="col s12"></div>
            <div id="crits" class="col s12"></div>
        </div>
    </div>
    <a class="btn-floating btn-large waves-effect waves-light indigo darken-3"><i class="fa fa-plus"></i></a>
</main>

<script id="criteria_tmpl" type="x-tmpl-mustache">
    {{#criteria.length}}
        <table id="criterionTableId" class="bordered pt-1">
            <thead>
            <tr>
                <th style="width:400px;">Title</th>
                <th style="width:280px;">Max points</th>
                <th style="width:100px;">Is additional</th>
                <th></th>
            </tr>
            </thead>
            <tbody>
            {{#criteria}}
                <tr>
                    <td>
                        <span>{{title}}</span>
                    </td>
                    <td>
                        <span>{{maxPoints}}</span>
                    </td>
                    <td>
                    {{#isAdditional}}Да{{/isAdditional}}
                    {{^isAdditional}}Нет{{/isAdditional}}
                    </td>
                    <td>
                        <a class="dropdown-button right w30" href="#" data-activates="criteriacoll{{id}}">
							<i class="fa fa-ellipsis-v"></i>
						</a>
                        <ul id="criteriacoll{{id}}" class="dropdown-content">
                            <li><a href="#" class="crit_edit" data-id="{{id}}">Edit</a></li>
                            <li><a href="#" class="crit_delete" data-id="{{id}}">Delete</a></li>
                        </ul>
                    </td>
                </tr>
            {{/criteria}}
            </tbody>
        </table>
    {{/criteria.length}}
    {{^criteria.length}}
        <h5 class="center-align bt pt-1">No criteria yet</h5>
    {{/criteria.length}}


</script>
<script id="tasks_tmpl" type="x-tmpl-mustache">
    {{#tasks.length}}
		<ul class="collapsible_tasks mb-0" data-collapsible="accordion">
			{{#tasks}}
			    <li>
					<div class="collapsible-header">
						<i class="fa fa-angle-down"></i>
						{{title}}
						<a class="dropdown-button right w30" href="#" data-activates="taskscoll{{id}}">
							<i class="fa fa-ellipsis-v"></i>
						</a>
                        <ul id="taskscoll{{id}}" class="dropdown-content">
                            <li><a href="#" class="task_edit" data-id="{{id}}">Edit</a></li>
                            <li><a href="#" class="task_delete" data-id="{{id}}">Delete</a></li>
                        </ul>
					</div>
					<div class="collapsible-body" style="overflow: visible;">
						<span>{{description}}</span>
						<br><br>
						<div class="input-field crits-to-task">
                            <select multiple data-task="{{id}}">
                                <option value="0" disabled>Выберите критерий</option>
                                {{#options}}{{/options}}
                            </select>
                            <label>Критерии</label>
                        </div>
                        <div class="input-field">
                            <input type="date" data-date="{{endDate}}" class="datepicker task-deadline" data-task="{{id}}"/>
                            <label>Срок сдачи</label>
                        </div>
					</div>
			    </li>
			{{/tasks}}
		</ul>
	{{/tasks.length}}
	{{^tasks.length}}
	    <h5 class="center-align bt pt-1">No works yet</h5>
	{{/tasks.length}}


</script>
<script>
    $(function () {
        function getFrom(from, id) {
            for (var i = 0; i < data[from].length; i++)
                if (data[from][i].id === id)
                    return {ndx: i, item: data[from][i]};
        }

        var data = {
            tasks: ${requestScope.tasks},
            criteria: ${requestScope.criteria}
        };


        var tmpls = {
            tasks: $('#tasks_tmpl').remove().html(),
            criteria: $('#criteria_tmpl').remove().html()
        };

        var tab = 0;

        Mustache.parse(tmpls.tasks);

        function reload_tasks(ndx) {
            ndx = ndx || tab;
            var name = !ndx ? "labs" : "practice";
            var tmpl = $('#' + name).html(Mustache.render(tmpls['tasks'],
                {
                    tasks: data['tasks'].filter(function (a) {
                        return a.isLab == !ndx;
                    }),
                    options: function () {
                        return function (text, render) {
                            var ret = '';

                            data['criteria'].forEach(c => {
                                ret += '<option value="' + c.id + '" data-points="' + c.maxPoints + '" data-is-additional="' + c.isAdditional + '"';
                                if (this.criteria != null) {
                                    for (var dc of this.criteria) {
                                        if (dc.id === c.id) {
                                            ret += " selected";
                                            break;
                                        }
                                    }
                                }
                                ret += ">" + c.title + ' (' + c.maxPoints + ')' + (c.isAdditional ? ' - Дополнительный' : '') + '</option>';
                            });
                            return render(ret);
                        }
                    }
                }))
                .children('.collapsible_tasks').collapsible();
            tmpl.find(".w30").click(function (e) {
                e.stopPropagation();
            }).dropdown();
            tmpl.find('select').material_select();
            tmpl.find('.datepicker').pickadate({
                selectMonths: true,
                selectYears: 100,
                format: 'dd mm yyyy',
                onStart: function () {
                    const date = this.$node.data('date');
                    if (date)
                        this.set('select', date, {format: 'dd mm yyyy'})
                }
            });
            tmpl.find('.task_edit').click(function () {
                var tmp = getFrom('tasks', $(this).data('id'));
                if (!tmp)
                    return alert("Something went really wrong");

                var task = tmp.item, ndx = tmp.ndx;

                swal_html({
                    title: task.title,
                    desc: task.description,
                    endDate: task.endDate,
                    preConfirm: function () {
                        return new Promise(function (res, rej) {
                            var title = $("#tmp_task_title").val(), desc = $("#tmp_task_desc").val();
                            var endDate = $("#tmp_task_date").val();
                            if (!title || !desc)
                                return rej("Both fields must be filled");

                            if (title.length > 255)
                                return rej("Слишком длинное название задания");
                            if (desc.length > 2000)
                                return rej("Слишком длинное описание задания");

                            let data = {
                                title: title,
                                description: desc,
                                id: task.id,
                                date: endDate
                            };
                            $.ajax({
                                url: '<%=request.getContextPath()%>/task/edit',
                                method: 'post',
                                dataType: 'json',
                                data: data
                            }).done(res).fail(
                                rej.bind(null, "Unknown vzhukh happened")
                            );
                        });
                    }
                }).then(function (d) {
                    data['tasks'][ndx] = d;
                    reload_tasks();
                    swal({
                        type: 'success',
                        title: 'Request finished!',
                        html: 'Task was edited'
                    });
                }, function () {
                });
                $('.datepicker').pickadate({
                    selectMonths: true,
                    selectYears: 100,
                    format: 'dd mm yyyy',
                    onStart: function () {
                        const date = this.$node.data('date');
                        if (date)
                            this.set('select', date, {format: 'dd mm yyyy'})
                    }
                });
            });
            tmpl.find('.task_delete').click(function () {
                var tmp = getFrom('tasks', $(this).data('id'));
                if (!tmp)
                    return alert("Something went really wrong");

                var task = tmp.item, ndx = tmp.ndx;
                swal({
                    type: 'question',
                    title: 'R u sure?',
                    showCancelButton: true,
                    confirmButtonText: 'Yes, delete',
                    showLoaderOnConfirm: true,
                    allowOutsideClick: false,
                    preConfirm: function () {
                        return new Promise(function (res, rej) {
                            let data = {
                                id: task.id
                            };
                            $.ajax({url: '<%=request.getContextPath()%>/task/delete', method: 'post', data: data})
                                .done(res).fail(rej.bind(null, "Unknown vzhukh happened"))
                        });
                    }
                }).then(function () {
                    data['tasks'].splice(ndx, 1);
                    reload_tasks();
                    swal({
                        type: 'success',
                        title: 'Hooray',
                        text: 'Task was successfully deleted'
                    });
                }, function () {
                });
            });
            tmpl.find('.crits-to-task select').on('change', function () {
                let sum = 0;
                $(this).find('option:selected').each(function() {
                    if ($(this).data('is-additional'))
                        return;

                    sum += $(this).data('points');
                    if (sum > 100) {
                        alert('Вы не можете выбрать этот критерий, так как сумма всех баллов превышает 100');
                    }
                });

                if (sum > 100) {
                    return false;
                }

                var data = {
                    'criterionId': $(this).val(),
                    'taskId': $(this).data('task')
                };

                $.ajax({
                    url: '<%=request.getContextPath()%>/task/connectcriterion',
                    method: 'post',
                    dataType: 'json',
                    data: data,
                    error: function () {
                        alert('Ошибка при выполнении запроса');
                    }
                });
            });
            tmpl.find('.task-deadline').on('change', function () {
                var data = {
                    'date': $(this).val(),
                    'id': $(this).data('task')
                };

                $.ajax({
                    url: '<%=request.getContextPath()%>/task/changedate',
                    method: 'post',
                    dataType: 'json',
                    data: data,
                    error: function () {
                        alert('Ошибка при выполнении запроса');
                    }
                });
            });
        }

        function reload_criteria() {
            var title, max, is;
            var criteria = $("#crits").html(Mustache.render(tmpls['criteria'], {criteria: data['criteria']}));
            criteria.find(".w30").dropdown();
            criteria.find('.crit_edit').click(function () {
                var tmp = getFrom('criteria', $(this).data('id'));
                if (!tmp)
                    return alert("Something went really wrong");

                var crit = tmp.item, ndx = tmp.ndx;
                swal_criteria({
                    title: crit.title,
                    max: crit.maxPoints,
                    is: crit.isAdditional,
                    preConfirm: function () {
                        return new Promise(function (res, rej) {
                            title = $("#tmp_crit_title").val();
                            max = +$("#tmp_crit_max").val();
                            is = $("#tmp_crit_is").prop('checked');
                            if (!title || !max)
                                return rej("Both fields must be filled");
                            if (max <= 0) {
                                return rej("Максимальный балл должен быть положительным числом");
                            }
                            if (max > 100) {
                                return rej("Максимальный балл не можетбыть больше 100");
                            }

                            

                            var dt = {
                                id: crit.id,
                                max: max
                            };
                            $.ajax({
                                url: '<%=request.getContextPath()%>/criteria/check',
                                data: dt,
                                method: 'get',
                                dataType: 'json'
                            }).done(res).fail(rej.bind(null, "Unknown vzhukh happened"));
                        });
                    }
                }).then(function (r) {
                    return new Promise(function (res, rej) {
                        if (r.conflicts) {
                            swal({
                                type: 'warning',
                                title: 'Conflicts!',
                                html: 'Есть оценки, превышающие ' + max + ' баллов. Продолжить?',
                                showCancelButton: true,
                                cancelButtonText: "Нет",
                                preConfirm: function () {
                                    return new Promise(function (res, rej) {
                                        var dt = {
                                            title: title,
                                            max_points: max,
                                            is_additional: is,
                                            id: crit.id
                                        };
                                        $.ajax({
                                            url: '<%=request.getContextPath()%>/criteria/edit',
                                            data: dt, method: 'post', dataType: 'json'
                                        }).done(res).fail(rej.bind(null, "Unknown vzhukh happened"));
                                    });
                                }
                            }).then(function (res) {
                                data['criteria'][ndx] = res;
                                reload_criteria();
                                swal({
                                    type: 'success',
                                    title: 'Request finished!',
                                    html: 'Criterion was edited'
                                });
                            }, function () {
                            });
                        }
                        else {
                            var dt = {
                                title: title,
                                max_points: max,
                                is_additional: is,
                                id: crit.id
                            };
                            $.ajax({
                                url: '<%=request.getContextPath()%>/criteria/edit',
                                data: dt,
                                method: 'post',
                                dataType: 'json',
                                success: function(res) {
                                    data['criteria'][ndx] = res;
                                    reload_criteria();
                                    swal({
                                        type: 'success',
                                        title: 'Request finished!',
                                        html: 'Criterion was edited'
                                    });
                                }
                            });
                        }
                    });
                }, function () {
                });
            });

            criteria.find('.crit_delete').click(function () {
                var tmp = getFrom('criteria', $(this).data('id'));
                if (!tmp)
                    return alert("Something went really wrong");

                var crit = tmp.item, ndx = tmp.ndx;
                swal({
                    type: 'question',
                    title: 'R u sure?',
                    showCancelButton: true,
                    confirmButtonText: 'Yes, delete',
                    showLoaderOnConfirm: true,
                    allowOutsideClick: false,
                    preConfirm: function () {
                        return new Promise(function (res, rej) {
                            $.ajax({url: '<%=request.getContextPath()%>/criteria/delete?id=' + crit.id, method: 'post'})
                                .done(res).fail(rej.bind(null, "Unknown vzhukh happened"))
                        });
                    }
                }).then(function () {
                    data['criteria'].splice(ndx, 1);
                    reload_criteria();
                    swal({
                        type: 'success',
                        title: 'Hooray',
                        text: 'Task was successfully deleted'
                    });
                }, function () {
                });
            });
        }

        function swal_html(d) {
            return swal({
                title: 'Input title and description',
                html: '<input class="swal2-input" maxlength="255" value="' + (d.title || '') + '" id="tmp_task_title" placeholder="Title" type="text" style="display: block;">' +
                '<textarea class="swal2-textarea" maxlength="2000" id="tmp_task_desc" placeholder="Description" style="display: block;">' + (d.desc || '') + '</textarea>' +
                '<input type="date" data-date="' + (d.endDate || '') + '" class="datepicker" id="tmp_task_date" placeholder="Срок сдачи"/>' +
                '<input type="checkbox" checked="checked" class="filled-in" id="tmp_task_is">',
//                '<label for="tmp_lab_is" class="mb-1">'+d.check+'</label>',
                showCancelButton: true,
                confirmButtonText: 'Submit',
                showLoaderOnConfirm: true,
                allowOutsideClick: false,
                preConfirm: d.preConfirm
            });
        }

        function swal_criteria(d) {
            return swal({
                title: 'Input title and max points',
                html: '<input class="swal2-input" style="margin-bottom:0;" value="' + (d.title || '') + '" id="tmp_crit_title" placeholder="Title" type="text">' +
                '<input class="swal2-input" style="max-width:100%;" value="' + (d.max || '') + '" min="1" max="100" id="tmp_crit_max" placeholder="Max points" type="number">' +
                '<input type="checkbox" ' + (d.is ? 'checked="checked"' : '') + ' class="filled-in" id="tmp_crit_is">' +
                '<label for="tmp_crit_is" class="mb-1">Is additional</label>',
                showCancelButton: true,
                confirmButtonText: 'Submit',
                showLoaderOnConfirm: true,
                allowOutsideClick: false,
                preConfirm: d.preConfirm
            });
    }

        reload_tasks(0);
        reload_tasks(1);
        reload_criteria();


        $('ul.tabs').on('click', 'a', function () {
            tab = $(this).parent().index()
        }).tabs();
        $('.btn-floating').click(function () {

            if (tab < 2) {
                if (!${requestScope.currentGroup.id})
                    return alert("Группа не выбрана");
                swal_html({
                    preConfirm: function () {
                        return new Promise(function (res, rej) {
                            var group = ${requestScope.currentGroup.id};
                            if (group < 1)
                                return rej("Группа не выбрана");

                            var title = $("#tmp_task_title").val(), desc = $("#tmp_task_desc").val();//, isLab = $("#tmp_lab_is").prop('checked');
                            var endDate = $("#tmp_task_date").val();
                            if (!title || !desc)
                                return rej("Both fields must be filled");


                            if (title.length > 255)
                                return rej("Слишком длинное название задания");
                            if (desc.length > 2000)
                                return rej("Слишком длинное описание задания");

                            $.post('<%=request.getContextPath()%>/task/add', {
                                title: title,
                                description: desc,
                                isLab: !tab,
                                groupId: group,
                                date: endDate
                            }, res, 'json')
                                .fail(
                                    rej.bind(null, "Unknown error happened.")
                                );
                        });
                    }
                }).then(function (d) {
                    data['tasks'].push(d);
                    reload_tasks();
                    swal({
                        type: 'success',
                        title: 'Request finished!',
                        text: 'Task was added'
                    });
                }, function () {
                });
                $('.datepicker').pickadate({
                    selectMonths: true,
                    selectYears: 100,
                    format: 'dd mm yyyy',
                    onStart: function () {
                        const date = this.$node.data('date');
                        if (date)
                            this.set('select', date, {format: 'dd mm yyyy'})
                    }
                });
                return;
            }
            swal_criteria({
                preConfirm: function () {
                    return new Promise(function (res, rej) {
                        var title = $("#tmp_crit_title").val(), max = $("#tmp_crit_max").val(),
                            is = $("#tmp_crit_is").prop('checked');
                        if (!title || !max)
                        {
                            return rej("Both fields must be filled");
                        }

                        /* Check: points for criteria must be positive. */
                        if (max <= 0) {
                            return rej("Максимальный балл должен быть положительным числом.");
                        }

                        /* Check: points have not be more than 0. */
                        if (max > 100)
                        {
                            return rej("Максимальный балл не может быть больше 100.");
                        }

                        $.post('<%=request.getContextPath()%>/criteria', {
                            title: title,
                            max_points: max,
                            is_additional: is
                        }, res, 'json')
                            .fail(rej.bind(null, "Unknown vzhukh happened"));
                    });
                }
            }).then(function (d) {
                data['criteria'].unshift(d);
                reload_criteria();
                swal({
                    type: 'success',
                    title: 'Request finished!',
                    text: 'Criterion was added'
                });
            }, function () {
            });
        });
    });
</script>

<%@include file="../footer.jsp" %>
