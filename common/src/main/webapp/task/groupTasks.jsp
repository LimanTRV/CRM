<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<jsp:include page="../header.jsp">
    <jsp:param name="title" value="Задания"/>
</jsp:include>
<main class="container">
    <div class="z-depth">
        <div class="row">
            <div id="tasks" class="col s12">
                <div style="padding: 0 10px;" class="z-depth-1">
                    <table class="bordered">
                        <thead>
                        <tr>
                            <th>Название</th>
                            <th>Дата добавления</th>
                            <th>Ссылка на эталонное решение</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        <c:forEach items="${requestScope.tasks}" var="task">
                            <tr>
                                <td><a href="${pageContext.request.contextPath}/task/mark/?group=${requestScope.currentGroup.id}&task=${task.id}"
                                       class="waves-effect waves-light"><c:out
                                        value="${task.title}"/></a></td>
                                <td><fmt:formatDate value="${task.created}" type="both"
                                                    dateStyle="long" timeStyle="short"/>
                                </td>
                                <form id="uploadingForm" method="POST" action="./?group=${requestScope.currentGroup.id}" >
                                    <td><input type="url" value="${task.taskStandardUrl}" id="task_standard_url" name="task_standard_url" style="padding-left: 10px" /></td>
                                    <td>
                                        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
                                        <input type="hidden" name="task_id" value="${task.id}" />
                                        <button style="width: 50px; border:0; background:#fff;" type="submit" name="urlSave" >
                                            <i class="fa fa-floppy-o" aria-hidden="true"></i>
                                        </button>
                                    </td>
                                </form>
                            </tr>
                        </c:forEach>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

</main>


<script>

</script>
