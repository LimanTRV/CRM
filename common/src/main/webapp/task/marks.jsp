<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<jsp:include page="../header.jsp">
    <jsp:param name="title" value="Оценки за задание ${requestScope.task.title}"/>
</jsp:include>

<main class="container">
    <div class="z-depth">
        <div class="row">
            <div id="tasks" class="col s12">
                <div style="padding: 0 10px;" class="z-depth-1">
                    <ul class="collapsible" data-collapsible="accordion">
                        <li>
                            <div class="collapsible-header">Условия задания</div>
                            <div class="collapsible-body"><p>${requestScope.task.description}</p>
                            </div>
                        </li>
                    </ul>
                    <div style="width: 100%; overflow-x: auto;">
                        <table class="bordered">
                            <thead>
                            <tr>
                                <th>First name</th>
                                <th>Last name</th>
                                <c:forEach var="criterion" items="${requestScope.criteria}">
                                    <th>
                                        <c:out value="${criterion.title}"/>
                                    </th>
                                </c:forEach>
                                <sec:authorize access="hasRole('T_E')">
                                    <th></th>
                                </sec:authorize>
                            </tr>
                            </thead>
                            <tbody>
                            <c:forEach var="marksEntry" items="${requestScope.marks}">
                                <tr>
                                    <td><c:out value="${marksEntry.key.user.firstName}"/></td>
                                    <td><c:out value="${marksEntry.key.user.lastName}"/></td>
                                    <c:forEach var="markValue" items="${marksEntry.value}">
                                        <td>
                                            <div class="mark-show">${markValue.points}</div>
                                            <div class="input-field inline mark-edit" style="display: none;">
                                                <input id="points" type="number" class="validate"
                                                       min="0" max="${markValue.criterion.maxPoints}"
                                                       value="${markValue.points}"
                                                       data-max="${markValue.criterion.maxPoints}"
                                                       data-mark="${markValue.id}"
                                                       data-criterion="${markValue.criterion.id}"
                                                       data-student="${marksEntry.key.id}">
                                            </div>
                                        </td>
                                    </c:forEach>
                                    <sec:authorize access="hasRole('T_E')">
                                        <td><i class="fa fa-pencil mark-switcher" style="cursor : pointer;"></i></td>
                                    </sec:authorize>
                                </tr>
                            </c:forEach>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
<sec:authorize access="hasRole('T_E')">
    <script>
        $(function () {
            let marksIsEditable = false;
            $('.mark-switcher').on("click", function () {
                let tr = $(this).closest("tr");
                if (marksIsEditable) {
                    tr.find('.mark-show').show();
                    tr.find('.mark-edit').hide();
                }
                else {
                    tr.find('.mark-show').hide();
                    tr.find('.mark-edit').show();
                }
                marksIsEditable = !marksIsEditable;
            });

            $(".mark-edit input").on("change", function () {
                $(this).removeClass(".invalid");

                let maxPoints = $(this).data("max");
                let points = $(this).val();

                $(this).closest("tr").find();

                if (points > maxPoints) {
                    $(this).val(maxPoints);
                    points = maxPoints;
                }
                if (points < 0) {
                    $(this).addClass(".invalid");
                    return;
                }

                let data = {
                    id: $(this).data("mark"),
                    taskId: ${requestScope.task.id},
                    studentId: $(this).data("student"),
                    criterionId: $(this).data("criterion"),
                    points: $(this).val()
                };
                console.log(data);

                let edit = $(this);
                edit.closest('td').children(".mark-show").text(data.points);
                $.ajax({
                    url: "<%=request.getContextPath()%>/mark/save",
                    method: "post",
                    data: data,
                    dataType: "json",
                    success: function (resp) {
                        if ($.isNumeric(resp.taskId) && resp > 0) {
                            edit.data("mark", resp);
                        }
                    },
                    error: function () {
                        alert("Ошибка при выполнении запроса!");
                    }
                });
            });
        });
    </script>
</sec:authorize>