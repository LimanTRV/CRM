<jsp:include page="../header.jsp">
    <jsp:param name="title" value="Загрузка заданий"/>
    <jsp:param name="showUser" value="show"/>
</jsp:include>


<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>

<c:if test="${not empty allTasks}">
<main class="container">
    <div class="z-depth">
        <div class="row">
            <div id="crits" class="col s12">
                <div style="padding: 0 10px;" class="z-depth-1">
                    <form id="uploadingForm" method="POST"
                          action="./downloads/file/?${_csrf.parameterName}=${_csrf.token}"
                          enctype="multipart/form-data" novalidate>
                        <table class="bordered">
                            <tr>
                                <td style="width: 220px">
                                    <label>Выберите задание</label>
                                    <div style="padding-top: 5px">
                                        <select id="cht" name="task_id" class="browser-default">
                                            <option value="" disabled selected></option>
                                            <c:if test="${not empty allTasks}">
                                                <c:forEach items="${allTasks}" var="task">
                                                    <option value="${task.id}">${task.title}</option>
                                                </c:forEach>
                                            </c:if>
                                        </select>
                                    </div>
                                </td>
                                <td style="width: 180px">
                                    <label>Выберите источник</label>
                                    <div style="padding-top: 5px">
                                        <input type="radio" id="radioBtnGit" name="rbt" checked
                                               onclick="$('#file-div').hide(); $('#url-div').show();"/>
                                        <label for="radioBtnGit">Cсылка на Git</label>
                                    </div>
                                    <div style="">
                                        <input type="radio" id="radioBtnFile" name="rbt"
                                               onclick="$('#file-div').show(); $('#url-div').hide();"/>
                                        <label for="radioBtnFile">Zip-архив</label>
                                    </div>
                                </td>
                                <td>
                                    <div class="file-field input-field" id="url-div"
                                         style="margin-right: 10px">
                                        <i class="fa fa-git-square prefix"
                                           style="font-size: 50px; color: #454545"
                                           aria-hidden="true"></i>
                                        <input type="url" id="git_url" style="padding-left: 10px"
                                               required/>
                                    </div>
                                    <div class="file-field input-field" id="file-div"
                                         style="display: none;">
                                        <div class="btn">
                                            <span>файл</span>
                                            <input type="file" id="file" name="file" accept=".zip">
                                        </div>
                                        <div class="file-path-wrapper">
                                            <input class="file-path validate" type="text"
                                                   name="git_url">
                                        </div>
                                    </div>
                                </td>
                                <td style="width: 195px">
                                    <button class="btn waves-effect waves-light" type="submit">
                                        Сохранить<i class="material-icons right">send</i>
                                    </button>
                                </td>
                            </tr>
                        </table>
                    </form>
                    <table class="bordered">
                        <thead>
                        <tr>
                            <th>Задание</th>
                            <th>Пример решения</th>
                            <th>Дата загрузки</th>
                            <th>Ссылка в репозиторий</th>
                            <th>Имя файла</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        <c:if test="${not empty downloadList}">
                            <c:forEach items="${downloadList}" var="downloadItem">
                                <tr id="uploading_${downloadItem.id}">
                                    <td>${downloadItem.task.title}</td>
                                    <td>${downloadItem.task.taskStandardUrl}</td>
                                    <td>${downloadItem.downloadDate}</td>
                                    <td>${downloadItem.gitUrl}</td>
                                    <td>${downloadItem.fileName}</td>
                                    <td>
                                        <a data-target="submit-deletion" class="modal-trigger"
                                           onclick="$('#uploading_for_deletion').val('${downloadItem.id}')">
                                            <i style="padding-left: 10px; font-size: 20px"
                                               class="fa fa-trash-o"></i>
                                        </a>
                                    </td>
                                </tr>
                            </c:forEach>
                        </c:if>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</main>
</c:if>
<c:if test="${empty allTasks}">
    <main class="container z-depth-1">
        <div class="col s12 m7" style="margin-left: 20%; margin-right: 20%; padding-top: 40px; padding-bottom: 40px">
            <h3 class="header" style="color: #ee6e73">
                <i class="fa fa-exclamation-circle" aria-hidden="true" style="font-size: 3rem"></i>
                Ошибка доступа
            </h3>
            <div class="card horizontal">
                <div class="card-stacked">
                    <div class="card-content">
                        <p style="word-break: normal">К сожалению, для Вашей учётной записи не доступна загрузка выполненных работ.<br/>
                            Пожалуйста, обратитесь к админитратру для получения соответствующих прав</p>
                    </div>
                    <div class="card-action">
                        <a href="../">На главную</a>
                    </div>
                </div>
            </div>
        </div>
    </main>
</c:if>


<div id="submit-deletion" class="modal">
    <input type="hidden" id="uploading_for_deletion" value="">
    <div class="modal-content">
        <h4>Вы уверены?</h4>
        <div style="padding-top: 25px; padding-left: 25px; padding-right: 25px">
            <h5>Вы действительно хотите удалить информацию о загрузке?</h5>
        </div>
    </div>
    <div class="modal-footer">
        <a class="modal-action modal-close waves-effect waves-green btn-flat">Отмена</a>
        <a class="modal-action modal-close waves-effect waves-green btn-flat"
           onclick="deleteUploading($('#uploading_for_deletion').val());">Удалить</a>
    </div>
</div>

<script>
    $(document).ready(function () {
        $('.modal').modal();
        alert('${uploadingMessage}');

        $("#uploadingForm").submit(function (e) {
            if ($("#radioBtnGit").is(":checked")) {
                e.preventDefault();
                saveURL();
                return false;
            } else {
                if ($('#cht').val() == null) {
                    e.preventDefault();
                    alert("Необходимо выбрать занятие");
                    return false;
                }
                var file = $('#file')[0].files[0];
                if (file == null) {
                    e.preventDefault();
                    alert("Необходимо выбрать файл");
                    return false;
                }
                if (file.name.substr(file.name.length - 3, file.name.length) != 'zip') {
                    e.preventDefault();
                    alert("Недопустимый тип файла");
                    return false;
                }
            }
        });

    });

    function deleteUploading(id) {
        $.ajax({
            url: "./downloads/delete?downloadId=" + id,
            data: {
                downloadId: id
            },
            type: "POST",
            success: function (answer) {
                alert("Решение успешно удалено");
                $("#uploading_" + id).remove();
            },
            error: function () {
                alert("Не удалось удалить решение")
            }
        });
    }

    function saveURL() {
        if ($('#cht').val() == null) {
            alert("Необходимо выбрать задание");
            return;
        }
        var uploadingId = $('#cht').val();
        var gitUrl = $('#git_url').val();
        var re = /(http(s)?:\/\/)?(www\.)?github\.com\/\b([-a-zA-Z0-9‌​@:%_\/.~#?&=]*)(\.zip)/i;
        if (!gitUrl.match(re)) {
            alert("Введите корректную ссылку");
            return;
        }
        if (uploadingId == null) {
            alert("Необходимо выбрать занятие");
            return;
        }
        $.ajax({
            url: "./downloads/git/",
            data: {
                uploadingId: uploadingId,
                url: gitUrl,
            },
            type: "POST",
            dataType: "json",
            success: function (answer) {
                if (answer) {
                    if (answer.status === 'SUCCESS') {
                        location.href = "downloads";
                        alert(answer.message);
                    } else {
                        alert(answer.message);
                    }
                }
            },
            error: function () {
                alert("Не удалось сохранить решение")
            }
        });
    }

    $("#cht").change(function () {
        $(".chosen").removeClass("chosen");
        $("#task_" + $(this).val()).addClass("chosen");
    });


</script>
</body>
</html>

<%@include file="../footer.jsp" %>