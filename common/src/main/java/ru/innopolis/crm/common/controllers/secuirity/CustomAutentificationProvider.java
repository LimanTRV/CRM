package ru.innopolis.crm.common.controllers.secuirity;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import ru.innopolis.crm.main.models.entity.User;
import ru.innopolis.crm.main.models.entity.UserAuthority;
import ru.innopolis.crm.main.services.UserService;
import ru.innopolis.crm.main.sessionbean.UserBean;

import java.util.ArrayList;
import java.util.Collection;

/**
 * Created by arty on 07.05.2017.
 */
public class CustomAutentificationProvider implements AuthenticationProvider {

    private UserService userService;
    private UserBean userBean;
    private PasswordEncoder encoder;

    @Autowired
    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    @Autowired
    public void setUserBean(UserBean userBean) {
        this.userBean = userBean;
    }

    @Autowired
    public void setEncoder(PasswordEncoder encoder) {
        this.encoder = encoder;
    }

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        User user = userService.getByLogin(authentication.getName());
        if (user == null)
            throw new UsernameNotFoundException("user not found");

        String password = authentication.getCredentials().toString();
        if (encoder.matches(password, user.getPassword())) {
            org.springframework.security.core.userdetails.User springUser = buildUser(user);
            return new UsernamePasswordAuthenticationToken(userBean, password, springUser.getAuthorities());
        }
        return null;
    }

    private org.springframework.security.core.userdetails.User buildUser(User user) {
        Collection<GrantedAuthority> authorities = new ArrayList<>();
        for (UserAuthority userAuthority : userService.getAuthorities(user))
            authorities.add(new SimpleGrantedAuthority(userAuthority.getName()));

        userBean.setStudentId(userService.ifStudent(user.getId()));
        userBean.setUserName(user.getFirstName());
        userBean.setUserId(user.getId());
        userBean.setAuthorities(userService.getAuthorities(user));

        return new org.springframework.security.core.userdetails.User(user.getLogin(), user.getPassword(), user.isActive(),
                true, true, user.isDeleted(), authorities);
    }

    @Override
    public boolean supports(Class<?> authentication) {
        return authentication.equals(UsernamePasswordAuthenticationToken.class);
    }
}
