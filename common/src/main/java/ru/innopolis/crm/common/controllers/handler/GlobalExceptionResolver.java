package ru.innopolis.crm.common.controllers.handler;

import org.apache.log4j.Logger;
import org.springframework.core.Ordered;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.web.servlet.HandlerExceptionResolver;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.json.MappingJackson2JsonView;
import ru.innopolis.crm.main.utils.InjectLogger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Created by Nemesis on 06.05.17.
 */
public class GlobalExceptionResolver implements HandlerExceptionResolver, Ordered {
    @InjectLogger(GlobalExceptionResolver.class)
    private Logger logger;

    @Override
    public ModelAndView resolveException(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object o, Exception e) {
        logger.error("global exception resolver (" + e.getClass().getSimpleName() + ") : " +e.getMessage());
        httpServletResponse.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
        if("XMLHttpRequest".equals(httpServletRequest.getHeader("X-Requested-With"))) {
            ModelAndView view = new ModelAndView(new MappingJackson2JsonView());
            view.addObject("error", e.getMessage());
            return view;
        }

        if(e instanceof AccessDeniedException)
            return new ModelAndView("403");

        ModelAndView view = new ModelAndView("exception");
        view.addObject("prev", httpServletRequest.getRequestURI());
        return view;
    }

    @Override
    public int getOrder() {
        return Integer.MIN_VALUE;
    }
}
