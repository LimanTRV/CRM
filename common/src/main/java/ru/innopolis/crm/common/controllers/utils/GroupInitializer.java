package ru.innopolis.crm.common.controllers.utils;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;
import ru.innopolis.crm.main.models.entity.StudyGroup;
import ru.innopolis.crm.main.services.GroupService;
import ru.innopolis.crm.main.sessionbean.UserBean;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 */
public class GroupInitializer extends HandlerInterceptorAdapter {

    private static Logger logger = Logger.getLogger(GroupInitializer.class);

    private GroupService groupService;
    private UserBean userBean;

    @Autowired
    public void setUserBean(UserBean userBean) {
        this.userBean = userBean;
    }

    @Override
    public boolean preHandle(HttpServletRequest sre, HttpServletResponse response, Object handler) throws Exception {
        //if(userBean.getStudentId() != 0)
            //return true;

        StudyGroup currentGroup;
        String str = sre.getParameter("group");

        if(str == null)
            currentGroup = groupService.getFirst();
        else
            try {
                long currentGroupId = Long.parseLong(str);
                currentGroup = groupService.findById(currentGroupId);
            } catch (NumberFormatException e) {
                logger.error(e);
                currentGroup = groupService.getFirst();
            }

        sre.setAttribute("currentGroup", currentGroup);
        sre.setAttribute("groups", buildGroupUrls(sre.getQueryString()));

        Cookie cookie = new Cookie("group", Long.toString(currentGroup.getId()));
        cookie.setMaxAge(60 * 60 * 24 * 365);
        cookie.setPath(sre.getServletContext().getContextPath());
        response.addCookie(cookie);

        return true;
    }

    private String[][] buildGroupUrls(String q) {
        q = q == null ? "" : q.replaceFirst("group=\\d*&?", "");
        final String query = q.isEmpty() ? "" : "&" + q;

        return groupService.findAll().stream()
                .map(g -> new String[]{g.getName(), "?group=" + g.getId() + query})
                .toArray(String[][]::new);
    }

    @Autowired
    public void setGroupService(GroupService groupService) {
        this.groupService = groupService;
    }
}
