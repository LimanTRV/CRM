package ru.innopolis.crm.common.controllers.utils;

import org.apache.log4j.Logger;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;
import ru.innopolis.crm.main.utils.InjectLogger;

import java.lang.reflect.Field;

/**
 * Created by Nemesis on 02.05.17.
 */
public class LoggerInjectionBeanPostProcessor implements BeanPostProcessor {
    @Override
    public Object postProcessBeforeInitialization(Object o, String s) throws BeansException {
        return o;
    }

    @Override
    public Object postProcessAfterInitialization(Object o, String s) throws BeansException {
        Field[] fields = o.getClass().getDeclaredFields();
        for(Field field : fields) {
            InjectLogger annotation = field.getAnnotation(InjectLogger.class);
            if(annotation == null)
                continue;
            Logger logger = Logger.getLogger(annotation.value());
            field.setAccessible(true);
            try {
                field.set(o, logger);
            } catch (IllegalAccessException e) {
                //TODO: залогировать создание логгера :D
                throw new RuntimeException(e);
            }
        }
        return o;
    }
}
