package ru.innopolis.crm.task.services.impl;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.innopolis.crm.main.models.entity.Task;
import ru.innopolis.crm.task.models.dao.TaskDao;
import ru.innopolis.crm.task.models.dao.impl.TaskDaoImpl;
import ru.innopolis.crm.task.models.exceptions.DaoException;
import ru.innopolis.crm.task.services.TaskService;
import ru.innopolis.crm.task.services.exceptions.ServiceException;

import java.util.List;

/**
 * Created by Roman Taranov on 24.04.17.
 */
@Service
public class TaskServiceImpl implements TaskService
{

	private TaskDao taskDao = new TaskDaoImpl();

	private static final Logger LOG = Logger.getLogger(TaskServiceImpl.class);

	@Override
	public List<Task> getByGroupId(long id) throws ServiceException
	{
		try
		{
			return taskDao.getAllByGroup(id);
		}
		catch (DaoException e)
		{
			LOG.error("ServiceException:" + e.getMessage());
			throw new ServiceException();
		}
	}

	@Override
	public Task getById(long id) throws ServiceException
	{
		try
		{
			return taskDao.getById(id);
		}
		catch (DaoException e)
		{
			LOG.error("ServiceException:" + e.getMessage());
			throw new ServiceException();
		}
	}

	@Override
	public List<Task> getAllByStudentId(long studentId) throws ServiceException
	{
		try
		{
			return taskDao.getAllByStudentId(studentId);
		}
		catch (DaoException e)
		{
			LOG.error("ServiceException:" + e.getMessage());
			throw new ServiceException();
		}
	}

	@Override
	public boolean delete(Task task) throws ServiceException
	{
		try
		{
			return taskDao.delete(task);
		}
		catch (DaoException e)
		{
			LOG.error("ServiceException:" + e.getMessage());
			throw new ServiceException();
		}
	}

	@Override
	public boolean update(Task task) throws ServiceException
	{
		try
		{
			return taskDao.update(task);
		}
		catch (DaoException e)
		{
			LOG.error("ServiceException:" + e.getMessage());
			throw new ServiceException();
		}
	}

	@Override
	public boolean updateUrl(String taskStandardUrl, long taskId) throws ServiceException
	{
		try
		{
			return taskDao.updateUrl(taskStandardUrl, taskId);
		}
		catch (DaoException e)
		{
			LOG.error("ServiceException:" + e.getMessage());
			throw new ServiceException();
		}
	}

	@Override
	public boolean create(Task task) throws ServiceException
	{
		try
		{
			return taskDao.create(task);
		}
		catch (DaoException e)
		{
			LOG.error("ServiceException:" + e.getMessage());
			throw new ServiceException();
		}
	}

	@Override
	public List<Task> getAllByGroup(long groupId) throws ServiceException
	{
		try
		{
			return taskDao.getAllByGroup(groupId);
		}
		catch (DaoException e)
		{
			LOG.error("ServiceException:" + e.getMessage());
			throw new ServiceException();
		}
	}

	@Autowired
	public void setTaskDao(TaskDao taskDao)
	{
		this.taskDao = taskDao;
	}
}