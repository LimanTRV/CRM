package ru.innopolis.crm.task.controllers;

import org.apache.commons.io.FilenameUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import ru.innopolis.crm.main.models.entity.Student;
import ru.innopolis.crm.main.models.entity.Task;
import ru.innopolis.crm.main.services.SettingsService;
import ru.innopolis.crm.main.services.StudentService;
import ru.innopolis.crm.main.services.FileUtilsBean;
import ru.innopolis.crm.main.sessionbean.UserBean;
import ru.innopolis.crm.task.models.Answer;
import ru.innopolis.crm.task.models.Status;
import ru.innopolis.crm.task.models.entity.Download;
import ru.innopolis.crm.task.services.DownloadService;
import ru.innopolis.crm.task.services.TaskService;
import ru.innopolis.crm.task.services.exceptions.ServiceException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.File;
import java.io.IOException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

/**
 * Controller for solutions uploading page.
 * Uploads zip-archives from client computer or git repository
 * <p>
 * <p>
 * Created by Roman Taranov on 22.04.2017.
 */
@Controller
@PreAuthorize(value = "hasRole('TD_V')")
@RequestMapping(value = "/task/downloads")
@SessionAttributes("uploadingMessage")
public class DownloadController
{
	private static final String JSP_NAME = "task/downloads";
	private static final String DOCUMENTS_PATH = "/labs";

	private static Logger LOGGER = Logger.getLogger(DownloadController.class);

	private UserBean userBean;
	private DownloadService downloadService;
	private TaskService taskService;
	private StudentService studentService;
	private FileUtilsBean fileUtilsBean;
	private SettingsService settingsService;


	@GetMapping
	public String showList(HttpServletRequest request, HttpServletResponse response, Model model)
	{
		Student student = studentService.getStudentByUserId(userBean.getUserId());
		if (student == null)
		{
			LOGGER.warn("Trying to access to uploads of undefined user");
			return JSP_NAME;
		}
		List<Download> downloadList = downloadService.getTasksByStudentId(student.getId());
		List<Task> allTasksForGroup = null;
		try
		{
			allTasksForGroup = taskService.getAllByStudentId(student.getId());
		}
		catch (ServiceException e)
		{
			LOGGER.error(e.getMessage());
		}
		model.addAttribute("downloadList", downloadList);
		model.addAttribute("allTasks", allTasksForGroup);
		return JSP_NAME;
	}

	/**
	 * Uploading a file
	 */
	@PostMapping("/file/")
	public String uploadFile(@RequestParam("file") MultipartFile originalFile, @RequestParam("task_id") long taskId, Model model)
	{
		if (!originalFile.isEmpty())
		{
			try
			{
				long studentId = studentService.getStudentByUserId(userBean.getUserId()).getId();
				File newFile = new File(createPath(studentId, taskId));
				newFile.mkdirs();
				originalFile.transferTo(newFile);

				Download download = downloadService.getByStudentAndTask(studentId, taskId);
				if (download != null)
				{
					download.setFileName(newFile.getName());
					download.setGitUrl(null);
					downloadService.updateDownload(download);
					download.setDownloadDate(new Timestamp(System.currentTimeMillis()));
				}
				else
				{
					download = new Download();
					download.setStudentId(studentId);
					download.setTaskId(taskId);
					download.setDownloadUrl(newFile.getPath());
					download.setFileName(newFile.getName());
					download.setGitUrl(null);
					download.setDownloadDate(new Timestamp(System.currentTimeMillis()));
					downloadService.addDownload(download);
				}

			}
			catch (IOException | NullPointerException e)
			{
				LOGGER.warn(e.getMessage());
				model.addAttribute("uploadingMessage", "Не удалось сохранить решение");
				return "redirect:/task/downloads";
			}
			model.addAttribute("uploadingMessage", "Файл успешно загружен");
			return "redirect:/task/downloads";
		}
		else
		{
			LOGGER.warn("got an empty file");
			model.addAttribute("uploadingMessage", "Не удалось сохранить решение");
			return "redirect:/task/downloads";
		}
	}

	@RequestMapping(value = "/git/", method = RequestMethod.POST)
	@ResponseBody
	public Answer saveGit(@RequestParam("uploadingId") long taskId,
						  @RequestParam("url") String gitUrl)
	{

		Student student = studentService.getStudentByUserId(userBean.getUserId());

		if (fileUtilsBean.isUrlValid(gitUrl))
		{
			String originalName = FilenameUtils.getName(gitUrl);
			String outputFile = createPath(student.getId(), taskId);

			if (fileUtilsBean.loadDataToFile(gitUrl, outputFile))
			{
				Download download = downloadService.getByStudentAndTask(student.getId(), taskId);
				if (download != null)
				{
					download.setFileName(originalName);
					download.setGitUrl(gitUrl);
					download.setDownloadDate(new Timestamp(System.currentTimeMillis()));
					downloadService.updateDownload(download);
				}
				else
				{
					download = new Download();
					download.setStudentId(student.getId());
					download.setTaskId(taskId);
					download.setGitUrl(gitUrl);
					download.setFileName(originalName);
					download.setDownloadDate(new Timestamp(System.currentTimeMillis()));
					downloadService.addDownload(download);
				}

				return new Answer(Status.SUCCESS, "Загрузка завершена");
			}
			else
			{
				LOGGER.error("error while loading " + gitUrl);
				return new Answer(Status.ERROR, "Ошибка при загрузке");
			}

		}
		else
		{
			return new Answer(Status.ERROR, "Неправильный URL");
		}
	}

	/**
	 * Generates file and path according the naming policy:
	 * 1. Name form setting "upload_path"
	 * 2. Subfolder from constant DOCUMENTS_PATH
	 * 3. Range of students id folder (e.g. "1-1000")
	 * 4. Student id folder
	 * 5. Filename, made of task id and current date (e.g. "3_08.05.2017_9.24.zip")
	 *
	 * @returns a pair, which key is path and value is filename
	 */
	private String createPath(long studentId, long taskId)
	{
		try
		{
			SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy_h.mm");
			long t = (studentId - 1) / 1000;
			return settingsService.getByCode("upload_path").getValue()
					+ DOCUMENTS_PATH + "/"
					+ (t * 1000 + 1) + "-" + (t * 1000 + 1000) + "/"
					+ studentId + "/"
					+ "task" + taskId + "_"
					+ sdf.format(Calendar.getInstance().getTime()) + ".zip";
		}
		catch (ServiceException e)
		{
			LOGGER.error(e.getMessage());
		}
		return null;
	}

	/**
	 * Удаление загрузки
	 *
	 * @param downloadId
	 * @return
	 */
	@RequestMapping(value = "/delete", method = RequestMethod.POST)
	public ResponseEntity<Void> remove(@RequestParam("downloadId") long downloadId)
	{
		if (downloadService.deleteDownload(downloadId) > 0)
		{
			return new ResponseEntity<>(HttpStatus.OK);
		}
		return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
	}

	@Autowired
	public void setDownloadService(DownloadService downloadService)
	{
		this.downloadService = downloadService;
	}

	@Autowired
	public void setTaskService(TaskService taskService)
	{
		this.taskService = taskService;
	}

	@Autowired
	public void setStudentService(StudentService studentService)
	{
		this.studentService = studentService;
	}

	@Autowired
	public void setFileUtilsBean(FileUtilsBean fileUtilsBean)
	{
		this.fileUtilsBean = fileUtilsBean;
	}

	@Autowired
	public void setUserBean(UserBean userBean)
	{
		this.userBean = userBean;
	}

	@Autowired
	public void setSettingsService(SettingsService settingsService)
	{
		this.settingsService = settingsService;
	}
}
