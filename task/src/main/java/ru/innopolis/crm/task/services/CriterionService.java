package ru.innopolis.crm.task.services;

import ru.innopolis.crm.main.models.entity.Criterion;
import ru.innopolis.crm.task.services.exceptions.ServiceException;

import java.util.List;

/**
 * Created by Roman Taranov on 24.04.17.
 */
public interface CriterionService
{
	List<Criterion> getAll() throws ServiceException;

	Criterion getById(long id) throws ServiceException;

	List<Criterion> getAllByTask(long taskId) throws ServiceException;

	boolean delete(Criterion task) throws ServiceException;

	boolean update(Criterion task) throws ServiceException;

	boolean create(Criterion criterion) throws ServiceException;
}