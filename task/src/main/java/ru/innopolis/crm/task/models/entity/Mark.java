package ru.innopolis.crm.task.models.entity;

import ru.innopolis.crm.main.models.entity.Criterion;
import ru.innopolis.crm.main.models.entity.Student;
import ru.innopolis.crm.main.models.entity.Task;

/**
 *
 */
public class Mark
{

	private long id;
	private boolean deleted;
	private int points;
	private long studentId;
	private long taskId;
	private long criterionId;

	private Task task;
	private Criterion criterion;
	private Student student;

	public Mark()
	{
	}

	public Mark(long id, int points, long studentId, long taskId, long criterionId)
	{
		this.id = id;
		this.points = points;
		this.studentId = studentId;
		this.taskId = taskId;
		this.criterionId = criterionId;
	}

	public long getId()
	{
		return id;
	}

	public void setId(long value)
	{
		this.id = value;
	}

	public boolean isDeleted()
	{
		return deleted;
	}

	public void setDeleted(boolean value)
	{
		this.deleted = value;
	}

	public int getPoints()
	{
		return points;
	}

	public void setPoints(int value)
	{
		this.points = value;
	}

	public Task getTask()
	{
		return task;
	}

	public void setTask(Task value)
	{
		this.task = value;
	}

	public Criterion getCriterion()
	{
		return criterion;
	}

	public void setCriterion(Criterion value)
	{
		this.criterion = value;
	}

	public Student getStudent()
	{
		return student;
	}

	public void setStudent(Student value)
	{
		this.student = value;
	}

	public long getStudentId()
	{
		return studentId;
	}

	public void setStudentId(long studentId)
	{
		this.studentId = studentId;
	}

	public long getTaskId()
	{
		return taskId;
	}

	public void setTaskId(long taskId)
	{
		this.taskId = taskId;
	}

	public long getCriterionId()
	{
		return criterionId;
	}

	public void setCriterionId(long criterionId)
	{
		this.criterionId = criterionId;
	}
}

