package ru.innopolis.crm.task.models.dao.impl;


import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import ru.innopolis.crm.main.models.entity.Criterion;
import ru.innopolis.crm.main.models.entity.Task;
import ru.innopolis.crm.task.models.dao.TaskCriterionDao;
import ru.innopolis.crm.task.models.exceptions.DaoException;
import ru.innopolis.crm.task.services.CriterionService;
import ru.innopolis.crm.task.services.impl.CriterionServiceImpl;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 * Implementation of <tt>TaskCriterionDaoInterface</tt> interface.
 * Can be used to work with <tt>Task</tt> and <tt>Criterion</tt> relation
 *
 * @author Aslan Dudaev
 */
@Repository
public class TaskCriterionDaoImpl implements TaskCriterionDao
{
	/**
	 * Creation of logger
	 */

	private static final Logger logger = Logger.getLogger(TaskCriterionDao.class);

	private DataSource dataSource;

	private static final String INSERT = "insert into task.criterion_to_task(task_id, criterion_id) values(?, ?)";
	private static final CriterionService criterionService = new CriterionServiceImpl();

	/**
	 * Connects <tt>Criterion</tt> to <tt>Task</tt>
	 *
	 * @param task      task to which {@code criterion} must be connected
	 * @param criterion criterion which must be connected to {@code task}
	 * @return <tt>true</tt> if connection went successful
	 */
	@Override
	public boolean connect(Task task, Criterion criterion) throws DaoException
	{
		try
		{
			boolean connected = exec(INSERT, task.getId(), criterion.getId());

			if (connected)
			{
				task.getCriteria().add(criterion);
			}

			return connected;
		}
		catch (SQLException e)
		{
			logger.error(e.getMessage());
			throw new DaoException();
		}
	}

	/**
	 * Deletes <tt>Criterion</tt> and <tt>Task</tt> connection
	 *
	 * @param task      task from which {@code criterion} must be disconnected
	 * @param criterion criterion which must be disconnected from {@code task}
	 * @return <tt>true</tt> if disconnection went successful
	 */
	@Override
	public boolean disconnect(Task task, Criterion criterion) throws DaoException
	{
		try
		{
			boolean deleted = exec("delete from task.criterion_to_task where task_id = ? and criterion_id = ?",
					task.getId(), criterion.getId());
			if (deleted)
			{
				task.getCriteria().remove(criterion);
			}

			return deleted;
		}
		catch (SQLException e)
		{
			logger.error(e.getMessage());
			throw new DaoException();
		}
	}

	/**
	 * Just executes query
	 *
	 * @param query       query to be executed
	 * @param taskId      id of task (cap)
	 * @param criterionId id of criterion (cap2)
	 * @return <tt>true</tt> if execution was successful
	 */
	private boolean exec(String query, long taskId, long criterionId) throws SQLException
	{
		Connection connection = dataSource.getConnection();
		PreparedStatement statement = connection.prepareStatement(query);
		statement.setLong(1, taskId);
		statement.setLong(2, criterionId);
		boolean executed = statement.executeUpdate() > 0;
		statement.close();
		connection.close();
		return executed;
	}

	@Autowired
	public void setDataSource(DataSource dataSource)
	{
		this.dataSource = dataSource;
	}
}
