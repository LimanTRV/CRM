package ru.innopolis.crm.task.models.entity;

import ru.innopolis.crm.main.models.entity.Task;

import java.io.File;
import java.sql.Timestamp;

/**
 * Created by Roman Taranov on 22.04.2017.
 */
public class Download
{
	private long id;
	private Long studentId;
	private Long taskId;
	private String gitUrl;
	private Timestamp downloadDate;
	private String fileName;
	private String downloadUrl;
	private boolean isDeleted = false;
	/**
	 * helpful fields
	 */
	private Task task;
	private boolean isDownloaded;
	private File docFile;

	public long getId()
	{
		return id;
	}

	public void setId(long id)
	{
		this.id = id;
	}

	public Long getStudentId()
	{
		return studentId;
	}

	public void setStudentId(Long studentId)
	{
		this.studentId = studentId;
	}

	public Long getTaskId()
	{
		return taskId;
	}

	public void setTaskId(Long taskId)
	{
		this.taskId = taskId;
	}

	public String getGitUrl()
	{
		return gitUrl;
	}

	public void setGitUrl(String gitUrl)
	{
		this.gitUrl = gitUrl;
	}

	public Timestamp getDownloadDate()
	{
		return downloadDate;
	}

	public void setDownloadDate(Timestamp downloadDate)
	{
		this.downloadDate = downloadDate;
	}

	public String getFileName()
	{
		return fileName;
	}

	public void setFileName(String fileName)
	{
		this.fileName = fileName;
	}

	public String getDownloadUrl()
	{
		return downloadUrl;
	}

	public void setDownloadUrl(String downloadUrl)
	{
		this.downloadUrl = downloadUrl;
	}

	public boolean isDownloaded()
	{
		return isDownloaded;
	}

	public void setDownloaded(boolean downloaded)
	{
		isDownloaded = downloaded;
	}

	public boolean isDeleted()
	{
		return isDeleted;
	}

	public void setDeleted(boolean deleted)
	{
		this.isDeleted = deleted;
	}

	public Task getTask()
	{
		return task;
	}

	public void setTask(Task task)
	{
		this.task = task;
	}

	public File getDocFile()
	{
		return docFile;
	}

	public void setDocFile(File docFile)
	{
		this.docFile = docFile;
	}
}
