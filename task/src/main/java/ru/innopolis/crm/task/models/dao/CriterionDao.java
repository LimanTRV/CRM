package ru.innopolis.crm.task.models.dao;

import java.util.List;

import ru.innopolis.crm.main.models.entity.Criterion;
import ru.innopolis.crm.task.models.exceptions.DaoException;

/**
 * Created by Roman Taranov on 22.04.2017.
 */
public interface CriterionDao
{

	Criterion getById(long id) throws DaoException;

	List<Criterion> getAll() throws DaoException;

	List<Criterion> getByTask(long taskId) throws DaoException;

	boolean create(Criterion criterion) throws DaoException;

	boolean update(Criterion criterion) throws DaoException;

	boolean delete(Criterion criterion) throws DaoException;
}
