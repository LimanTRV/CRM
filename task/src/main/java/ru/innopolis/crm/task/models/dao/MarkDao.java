package ru.innopolis.crm.task.models.dao;

import ru.innopolis.crm.task.models.entity.Mark;
import ru.innopolis.crm.task.models.exceptions.DaoException;

import java.util.List;

/**
 * Created by Roman Taranov on 22.04.2017.
 */
public interface MarkDao
{

	Mark getById(long id) throws DaoException;

	List<Mark> getAll() throws DaoException;

	List<Mark> getAllByGroupAndTask(long groupId, long taskId) throws DaoException;

	long create(Mark mark) throws DaoException;

	List<Mark> getAllByCriterion(long criterionId) throws DaoException;

	int update(Mark mark) throws DaoException;

	int delete(Mark mark) throws DaoException;

}
