package ru.innopolis.crm.task.services;

import ru.innopolis.crm.main.models.entity.Task;
import ru.innopolis.crm.task.services.exceptions.ServiceException;

import java.util.List;

/**
 * Created by Roman Taranov on 24.04.17.
 */
public interface TaskService
{

	List<Task> getByGroupId(long id) throws ServiceException;

	Task getById(long id) throws ServiceException;

	List<Task> getAllByStudentId(long studentId) throws ServiceException;

	;

	boolean delete(Task task) throws ServiceException;

	boolean update(Task task) throws ServiceException;

	boolean updateUrl(String taskStandardUrl, long taskId) throws ServiceException;

	boolean create(Task task) throws ServiceException;

	List<Task> getAllByGroup(long groupId) throws ServiceException;
}