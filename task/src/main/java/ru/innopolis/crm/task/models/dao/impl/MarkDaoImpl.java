package ru.innopolis.crm.task.models.dao.impl;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import ru.innopolis.crm.task.models.dao.MarkDao;
import ru.innopolis.crm.task.models.entity.Mark;
import ru.innopolis.crm.task.models.exceptions.DaoException;

import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Roman Taranov on 22.04.2017.
 */
@SuppressWarnings("Duplicates")
@Repository
public class MarkDaoImpl implements MarkDao
{

	private static final Logger LOG = Logger.getLogger(MarkDaoImpl.class);

	private DataSource dataSource;

	private static final String SELECT_ALL = "SELECT id, student_id, task_id, " +
			"criterion_id, points FROM task.mark WHERE NOT is_deleted";

	private static final String INSERT = "INSERT INTO task.mark (is_deleted, student_id, task_id, " +
			"criterion_id, points) VALUES(?, ?, ?, ?, ?)";

	private static final String UPDATE_BY_ID = "UPDATE task.mark SET is_deleted = ?, student_id = ?, " +
			"task_id = ?, criterion_id = ?, points = ? WHERE id = ?";

	private static final String DELETE_BY_ID = "DELETE FROM task.mark WHERE id = ?";

	@Override
	public Mark getById(long id) throws DaoException
	{
		try
		{
			Connection connection = dataSource.getConnection();
			PreparedStatement statement = connection.prepareStatement(SELECT_ALL + " AND id = ?", Statement.NO_GENERATED_KEYS);
			statement.setLong(1, id);
			ResultSet resultSet = statement.executeQuery();
			if (resultSet.next())
			{
				return createNewInstance(resultSet);
			}
		}
		catch (SQLException e)
		{
			LOG.error("SQLException:" + e.getMessage());
			throw new DaoException();
		}
		return null;
	}

	@Override
	public List<Mark> getAll() throws DaoException
	{
		List<Mark> markList = new ArrayList<>();
		try
		{
			Connection connection = dataSource.getConnection();
			Statement statement = connection.createStatement();

			ResultSet resultSet = statement.executeQuery(SELECT_ALL);
			while (resultSet.next())
			{
				markList.add(createNewInstance(resultSet));
			}
		}
		catch (SQLException e)
		{
			LOG.error("SQLException:" + e.getMessage());
			throw new DaoException();
		}
		return markList;
	}

	@Override
	public List<Mark> getAllByGroupAndTask(long groupId, long taskId) throws DaoException
	{
		List<Mark> markList = new ArrayList<>();
		try
		{
			Connection connection = dataSource.getConnection();
			PreparedStatement statement = connection.prepareStatement("SELECT id, student_id, task_id, criterion_id, points " +
							"FROM task.mark WHERE NOT is_deleted AND task_id = ? AND student_id IN " +
							"(SELECT student_id FROM main.student_to_group WHERE study_group_id = ?)",
					Statement.NO_GENERATED_KEYS);

			statement.setLong(1, taskId);
			statement.setLong(2, groupId);

			ResultSet resultSet = statement.executeQuery();
			while (resultSet.next())
			{
				markList.add(createNewInstance(resultSet));
			}
		}
		catch (SQLException e)
		{
			LOG.error("SQLException:" + e.getMessage());
			throw new DaoException();
		}
		return markList;
	}

	@Override
	public List<Mark> getAllByCriterion(long criterionId) throws DaoException
	{
		List<Mark> markList = new ArrayList<>();
		try
		{
			Connection connection = dataSource.getConnection();
			PreparedStatement statement = connection.prepareStatement(SELECT_ALL + " AND criterion_id = ?",
					Statement.NO_GENERATED_KEYS);

			statement.setLong(1, criterionId);
			ResultSet resultSet = statement.executeQuery();
			while (resultSet.next())
			{
				markList.add(createNewInstance(resultSet));
			}
		}
		catch (SQLException e)
		{
			LOG.error("SQLException:" + e.getMessage());
			throw new DaoException();
		}
		return markList;
	}

	@Override
	public long create(Mark mark) throws DaoException
	{
		long insertedEntityId = -1;
		try
		{
			Connection connection = dataSource.getConnection();
			PreparedStatement insertStatement = connection.prepareStatement(INSERT,
					Statement.RETURN_GENERATED_KEYS);

			insertStatement.setBoolean(1, mark.isDeleted());
			insertStatement.setLong(2, mark.getStudentId());
			insertStatement.setLong(3, mark.getTaskId());
			insertStatement.setLong(4, mark.getCriterionId());
			insertStatement.setLong(5, mark.getPoints());

			insertStatement.executeUpdate();

			try (ResultSet generatedKeys = insertStatement.getGeneratedKeys())
			{
				if (generatedKeys.next())
				{
					insertedEntityId = generatedKeys.getLong(1);
				}
			}
		}
		catch (SQLException e)
		{
			LOG.error("SQLException:" + e.getMessage());
			throw new DaoException();
		}
		return insertedEntityId;
	}

	@Override
	public int update(Mark mark) throws DaoException
	{
		int updatedCount = 0;
		try
		{
			Connection connection = dataSource.getConnection();
			PreparedStatement updateStatement = connection.prepareStatement(UPDATE_BY_ID);

			updateStatement.setBoolean(1, mark.isDeleted());
			updateStatement.setLong(2, mark.getStudentId());
			updateStatement.setLong(3, mark.getTaskId());
			updateStatement.setLong(4, mark.getCriterionId());
			updateStatement.setInt(5, mark.getPoints());

			updateStatement.setLong(6, mark.getId());

			updatedCount = updateStatement.executeUpdate();
		}
		catch (SQLException e)
		{
			LOG.error("SQLException:" + e.getMessage());
			throw new DaoException();
		}
		return updatedCount;
	}

	@Override
	public int delete(Mark mark) throws DaoException
	{
		int deletedCount = 0;
		try
		{
			Connection connection = dataSource.getConnection();
			PreparedStatement deleteStatement = connection.prepareStatement(DELETE_BY_ID,
					Statement.RETURN_GENERATED_KEYS);

			deleteStatement.setLong(1, mark.getId());

			deletedCount = deleteStatement.executeUpdate();
		}
		catch (SQLException e)
		{
			LOG.error("SQLException:" + e.getMessage());
			throw new DaoException();
		}
		return deletedCount;
	}

	private Mark createNewInstance(ResultSet resultSet) throws SQLException
	{

		Mark mark = new Mark();
		mark.setId(resultSet.getLong("id"));
		mark.setPoints(resultSet.getInt("points"));
		mark.setStudentId(resultSet.getLong("student_id"));
		mark.setTaskId(resultSet.getLong("task_id"));
		mark.setCriterionId(resultSet.getLong("criterion_id"));

		return mark;
	}

	@Autowired
	public void setDataSource(DataSource dataSource)
	{
		this.dataSource = dataSource;
	}
}
