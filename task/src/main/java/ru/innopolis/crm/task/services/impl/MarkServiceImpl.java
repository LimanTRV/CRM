package ru.innopolis.crm.task.services.impl;

import org.apache.log4j.Logger;
import ru.innopolis.crm.main.models.entity.Criterion;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.innopolis.crm.main.models.entity.Criterion;
import ru.innopolis.crm.main.models.entity.Student;
import ru.innopolis.crm.main.models.entity.Task;
import ru.innopolis.crm.main.services.StudentService;
import ru.innopolis.crm.task.models.dao.CriterionDao;
import ru.innopolis.crm.task.models.dao.MarkDao;
import ru.innopolis.crm.task.models.dao.TaskDao;
import ru.innopolis.crm.task.models.dao.impl.CriterionDaoImpl;
import ru.innopolis.crm.task.models.dao.impl.MarkDaoImpl;
import ru.innopolis.crm.task.models.dao.impl.TaskDaoImpl;
import ru.innopolis.crm.task.models.entity.Mark;
import ru.innopolis.crm.task.models.exceptions.DaoException;
import ru.innopolis.crm.task.services.MarkService;
import ru.innopolis.crm.task.services.exceptions.ServiceException;

import java.util.*;

/**
 *
 */
@Service
public class MarkServiceImpl implements MarkService
{

	private static final Logger logger = Logger.getLogger(MarkServiceImpl.class);

	private final MarkDao markDao;
	private final CriterionDao criterionDao;
	private final TaskDao taskDao;
	private StudentService studentService;

	@Autowired
	public MarkServiceImpl(MarkDao markDao, CriterionDao criterionDao, TaskDao taskDao, StudentService studentService)
	{
		this.markDao = markDao;
		this.criterionDao = criterionDao;
		this.taskDao = taskDao;
		this.studentService = studentService;
	}

	public Map<Student, List<Mark>> getAllByGroupAndTask(long groupId, long taskId)
			throws ServiceException
	{

		Map<Student, List<Mark>> result = new TreeMap<>(Comparator
				.comparing(s -> s.getUser().getFirstName()));

		Task task = null;
		List<Criterion> taskCriteria = null;
		try
		{
			task = taskDao.getById(taskId);
			taskCriteria = criterionDao.getByTask(taskId);
			taskCriteria.sort(Comparator.comparingLong(Criterion::getId));
			List<Mark> allGroupMarks = markDao.getAllByGroupAndTask(groupId, taskId);

			List<Student> studentsInGroup = studentService.getStudentsByGroupId(groupId);
			studentsInGroup.sort(Comparator.comparingLong(Student::getId));
			for (Student student : studentsInGroup)
			{
				List<Mark> studentMarks = new ArrayList<>();
				for (Criterion criterion : taskCriteria)
				{
					Optional<Mark> markOptional = allGroupMarks.stream()
							.filter(m -> m.getStudentId() == student.getId() &&
									m.getCriterionId() == criterion.getId())
							.findFirst();

					Mark mark = markOptional.orElseGet(Mark::new);

					mark.setStudent(student);
					mark.setStudentId(student.getId());
					mark.setCriterion(criterion);
					mark.setCriterionId(criterion.getId());
					mark.setTask(task);
					mark.setTaskId(task.getId());
					mark.setDeleted(false);
					studentMarks.add(mark);
				}  // end of criterion loop
				result.put(student, studentMarks);
			} // end of students loop
		}
		catch (DaoException e)
		{
			logger.error(e);
			throw new ServiceException();
		}
		return result;
	}

	@Override
	public List<Mark> getAllByCriterion(long criterionId) throws ServiceException
	{
		try
		{
			return markDao.getAllByCriterion(criterionId);
		}
		catch (DaoException e)
		{
			logger.error(e);
			throw new ServiceException();
		}
	}

	@Override
	public long create(Mark mark) throws ServiceException
	{
		try
		{
			return markDao.create(mark);
		}
		catch (DaoException e)
		{
			logger.error(e);
			throw new ServiceException();
		}
	}

	@Override
	public int update(Mark mark) throws ServiceException
	{
		try
		{
			return markDao.update(mark);
		}
		catch (DaoException e)
		{
			logger.error(e);
			throw new ServiceException();
		}
	}

}