package ru.innopolis.crm.task.controllers;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.annotation.RequestParam;
import ru.innopolis.crm.main.models.entity.StudyGroup;
import ru.innopolis.crm.task.services.TaskService;
import ru.innopolis.crm.task.services.exceptions.ServiceException;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by Roman Taranov on 22.04.2017.
 */
@Controller
@PreAuthorize(value = "hasAnyRole('T_V', 'T_E')")
public class GroupTasksController
{
	private static final Logger LOGGER = Logger.getLogger(GroupTasksController.class);
	private final TaskService taskService;

	@Autowired
	public GroupTasksController(TaskService taskService)
	{
		this.taskService = taskService;
	}

	@RequestMapping(value = "/task/list", method = RequestMethod.GET)
	public String getTasksList(Model model, HttpServletRequest request)
	{
		StudyGroup group = (StudyGroup) request.getAttribute("currentGroup");

		try
		{
			model.addAttribute("tasks", taskService.getAllByGroup(group.getId()));
		}
		catch (ServiceException e)
		{
			LOGGER.error(e);
			return "redirect: /error";
		}

		return "/task/groupTasks";
	}

	@RequestMapping(value = "/task/list", method = RequestMethod.POST)
	public String saveStandardTaskUrl(@RequestParam(value = "task_standard_url") String taskStandardUrl,
									  @RequestParam(value = "task_id") long taskId,
									  Model model, HttpServletRequest req)
	{
		StudyGroup group = (StudyGroup) req.getAttribute("currentGroup");

		try
		{
			taskService.updateUrl(taskStandardUrl, taskId);
			model.addAttribute("tasks", taskService.getAllByGroup(group.getId()));
		}
		catch (ServiceException e)
		{
			LOGGER.error(e);
			return "redirect: /error";
		}

		return "/task/groupTasks";
	}
}
