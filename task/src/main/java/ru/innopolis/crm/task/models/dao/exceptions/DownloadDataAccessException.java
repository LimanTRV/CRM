package ru.innopolis.crm.task.models.dao.exceptions;

/**
 * Download Data Access Exception
 * <p>
 * Created by Roman Taranov on 22.04.2017.
 */
public class DownloadDataAccessException extends RuntimeException
{

}
