package ru.innopolis.crm.task.models.dao.impl;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import ru.innopolis.crm.main.models.entity.Criterion;
import ru.innopolis.crm.task.models.dao.CriterionDao;
import ru.innopolis.crm.task.models.exceptions.DaoException;

import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Roman Taranov on 22.04.2017.
 */
@SuppressWarnings("Duplicates")
@Repository
public class CriterionDaoImpl implements CriterionDao
{

	private static final Logger LOG = Logger.getLogger(CriterionDaoImpl.class);

	private static final String SELECT_ALL = "select * from task.criterion where not is_deleted order by id desc";

	private static final String SELECT_ALL_BY_TASK = "SELECT criterion.id AS id, criterion.title AS title, max_points, is_additional, criterion.created AS created " +
			"FROM task.criterion_to_task JOIN task.task ON criterion_to_task.task_id = task.id " +
			"JOIN task.criterion criterion ON criterion_to_task.criterion_id = criterion.id WHERE task.id = ? and not is_deleted";

	private static final String INSERT = "insert into task.criterion(title, " +
			"max_points, is_additional, is_deleted) values(?, ?, ?, false) returning id, created";

	private static final String UPDATE = "update task.criterion set title = ?, max_points = ?, is_additional = ? where id = ?";

	private static final String DELETE_BY_ID = "update task.criterion set is_deleted = true WHERE id = ?";

	private DataSource dataSource;


	/**
	 * Returns <tt>Criterion</tt> element, if it was found by id
	 *
	 * @param id id of <tt>Criterion</tt> to be found
	 */
	@Override
	public Criterion getById(long id) throws DaoException
	{
		Criterion criterion = null;
		try
		{
			Connection connection = dataSource.getConnection();
			Statement statement = connection.createStatement();

			ResultSet resultSet =
					statement.executeQuery("select * from task.criterion where id = " + id);
			while (resultSet.next())
			{
				if (criterion == null)
				{
					criterion = new Criterion();
					criterion.setId(id);
					criterion.setTitle(resultSet.getString("title"));
					criterion.setMaxPoints(resultSet.getInt("max_points"));
					criterion.setAdditional(resultSet.getBoolean("is_additional"));
					criterion.setCreated(resultSet.getTimestamp("created"));
				}
			}
		}
		catch (SQLException e)
		{
			LOG.error("SQLException:" + e.getMessage());
			throw new DaoException();
		}
		return criterion;
	}

	/**
	 * Returns a list of <tt>Criterion</tt> elements
	 */
	@Override
	public List<Criterion> getAll() throws DaoException
	{
		List<Criterion> criterionList = new ArrayList<>();
		try
		{
			Connection connection = dataSource.getConnection();
			PreparedStatement statement = connection.prepareStatement(SELECT_ALL,
					Statement.NO_GENERATED_KEYS);

			ResultSet resultSet = statement.executeQuery();
			while (resultSet.next())
			{
				criterionList.add(createNewInstance(resultSet));
			}
		}
		catch (SQLException e)
		{
			LOG.error("SQLException:" + e.getMessage());
			throw new DaoException();
		}
		return criterionList;
	}

	/**
	 * Saves <tt>Criterion</tt> element to database, adds id and
	 * creation date to {@code criterion} element
	 *
	 * @param criterion element to be saved
	 * @return <tt>true</tt> if element was added
	 */
	@Override
	public boolean create(Criterion criterion) throws DaoException
	{
		try
		{
			Connection connection = dataSource.getConnection();
			PreparedStatement statement = connection.prepareStatement(INSERT);
			statement.setString(1, criterion.getTitle());
			statement.setInt(2, criterion.getMaxPoints());
			statement.setBoolean(3, criterion.isAdditional());
			ResultSet keys = statement.executeQuery();
			boolean inserted = keys.next();
			if (inserted)
			{
				criterion.setId(keys.getLong(1));
				criterion.setCreated(keys.getTimestamp(2));
			}
			return inserted;
		}
		catch (SQLException e)
		{
			LOG.error("SQLException:" + e.getMessage());
			throw new DaoException();
		}
	}

	/**
	 * Updates information about <tt>Criterion</tt> in db
	 *
	 * @param criterion element to be updated
	 * @return <tt>true</tt> if information was updated
	 * @throws SQLException
	 */
	@Override
	public boolean update(Criterion criterion) throws DaoException
	{
		try
		{
			Connection connection = dataSource.getConnection();
			PreparedStatement statement = connection.prepareStatement(UPDATE);
			statement.setString(1, criterion.getTitle());
			statement.setInt(2, criterion.getMaxPoints());
			statement.setBoolean(3, criterion.isAdditional());
			statement.setLong(4, criterion.getId());
			boolean updated = statement.executeUpdate() > 0;
			return updated;
		}
		catch (SQLException e)
		{
			LOG.error("SQLException:" + e.getMessage());
			throw new DaoException();
		}
	}

	/**
	 * Deletes <tt>Criterion</tt> from db, removes id and creation date
	 * from {@code criterion} element
	 *
	 * @param criterion element to be deleted
	 * @return <tt>true</tt> if element was deleted
	 */
	@Override
	public boolean delete(Criterion criterion) throws DaoException
	{
		int deletedCount = 0;
		try
		{
			Connection connection = dataSource.getConnection();
			PreparedStatement deleteStatement = connection.prepareStatement(DELETE_BY_ID,
					Statement.RETURN_GENERATED_KEYS);

			deleteStatement.setLong(1, criterion.getId());

			deletedCount = deleteStatement.executeUpdate();
		}
		catch (SQLException e)
		{
			LOG.error("Всё ещё есть ссылки в других таблицах" + e.getMessage());
			throw new DaoException();
		}
		return deletedCount > 0;
	}

	@Override
	public List<Criterion> getByTask(long taskId) throws DaoException
	{
		List<Criterion> criterionList = new ArrayList<>();
		try
		{
			Connection connection = dataSource.getConnection();
			PreparedStatement statement = connection.prepareStatement(SELECT_ALL_BY_TASK,
					Statement.NO_GENERATED_KEYS);
			statement.setLong(1, taskId);
			ResultSet resultSet = statement.executeQuery();
			while (resultSet.next())
			{
				criterionList.add(createNewInstance(resultSet));
			}
		}
		catch (SQLException e)
		{
			LOG.error(e);
			throw new DaoException();
		}
		return criterionList;
	}

	/**
	 * Creates new <tt>Criterion</tt> element from {@code set}
	 *
	 * @param resultSet a set with data
	 * @return new <tt>Criterion</tt> element or null
	 * @throws SQLException
	 */
	private Criterion createNewInstance(ResultSet resultSet) throws SQLException
	{
		Criterion criterion = new Criterion();
		criterion.setId(resultSet.getLong("id"));
		criterion.setTitle(resultSet.getString("title"));
		criterion.setMaxPoints(resultSet.getInt("max_points"));
		criterion.setAdditional(resultSet.getBoolean("is_additional"));
		criterion.setCreated(resultSet.getTimestamp("created"));
		return criterion;
	}

	@Autowired
	public void setDataSource(DataSource dataSource)
	{
		this.dataSource = dataSource;
	}
}
