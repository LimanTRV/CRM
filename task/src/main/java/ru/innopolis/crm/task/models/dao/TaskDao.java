package ru.innopolis.crm.task.models.dao;

import ru.innopolis.crm.main.models.entity.Task;
import ru.innopolis.crm.task.models.exceptions.DaoException;

import java.util.List;

/**
 * Created by Roman Taranov on 22.04.2017.
 */
public interface TaskDao
{

	Task getById(long id) throws DaoException;

	List<Task> getAllByGroup(long groupId) throws DaoException;

	List<Task> getAllByStudentId(long studentId) throws DaoException;



	boolean create(Task task) throws DaoException;

	boolean update(Task task) throws DaoException;

	boolean updateUrl(String taskStandardUrl, long taskId) throws DaoException;

	boolean delete(Task task) throws DaoException;

}
