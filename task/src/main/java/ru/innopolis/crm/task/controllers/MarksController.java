package ru.innopolis.crm.task.controllers;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import ru.innopolis.crm.main.models.entity.Student;
import ru.innopolis.crm.main.models.entity.StudyGroup;
import ru.innopolis.crm.main.services.StudentService;
import ru.innopolis.crm.task.models.entity.Mark;
import ru.innopolis.crm.task.services.CriterionService;
import ru.innopolis.crm.task.services.MarkService;
import ru.innopolis.crm.task.services.TaskService;
import ru.innopolis.crm.task.services.exceptions.ServiceException;

import javax.servlet.http.HttpServletRequest;
import java.util.Comparator;
import java.util.List;

/**
 * Created by Roman Taranov on 22.04.2017.
 */
@Controller
public class MarksController
{
	private static final Logger LOGGER = Logger.getLogger(MarksController.class);
	private StudentService studentService;
	private MarkService markService;
	private TaskService taskService;
	private CriterionService criterionService;

	@Autowired
	public MarksController(StudentService studentService, MarkService markService, TaskService taskService, CriterionService criterionService)
	{
		this.studentService = studentService;
		this.markService = markService;
		this.taskService = taskService;
		this.criterionService = criterionService;
	}

	@PreAuthorize(value = "hasAnyRole('TM_E', 'TM_V')")
	@RequestMapping(value = "/task/mark", method = RequestMethod.GET)
	public String getTasksList(
			@RequestParam(value = "task", required = true) Long id,
			Model model,
			HttpServletRequest request
	)
	{
		StudyGroup group = (StudyGroup) request.getAttribute("currentGroup");

		List<Student> students = studentService.getStudentsByGroupId(group.getId());
		students.sort(Comparator.comparingLong(Student::getId));
		model.addAttribute("students", students);

		try
		{
			model.addAttribute("criteria", criterionService.getAllByTask(id));
			model.addAttribute("marks", markService.getAllByGroupAndTask(group.getId(), id));
			model.addAttribute("task", taskService.getById(id));

			return "/task/marks";
		}
		catch (ServiceException e)
		{
			LOGGER.error(e.getMessage());
			return "redirect: /error";
		}
	}

	@PreAuthorize(value = "hasRole('TM_E')")
	@RequestMapping(value = "/mark/save", produces = {"application/json; charset=UTF-8"}, method = RequestMethod.POST)
	public ResponseEntity<String> saveMark(
			@RequestParam(value = "id", required = false) Long id,
			@RequestParam(value = "taskId", required = true) Long taskId,
			@RequestParam(value = "studentId", required = true) Long studentId,
			@RequestParam(value = "criterionId", required = true) Long criterionId,
			@RequestParam(value = "points", required = true) Integer points
	)
	{
		Mark mark = new Mark(id, points, studentId, taskId, criterionId);
		try
		{
			if (mark.getId() != 0)
			{
				markService.update(mark);
				return ResponseEntity.ok("{\"result\": true}");
			}
			else
			{
				long createdId = markService.create(mark);
				return ResponseEntity.ok("{\"taskId\": " + String.valueOf(createdId) + "}");
			}
		}
		catch (ServiceException e)
		{
			LOGGER.error(e);
			return ResponseEntity.status(500).body("{\"result\": false}");
		}
	}
}