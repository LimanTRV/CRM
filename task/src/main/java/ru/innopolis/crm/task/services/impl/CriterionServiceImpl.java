package ru.innopolis.crm.task.services.impl;


import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.innopolis.crm.task.models.dao.CriterionDao;
import ru.innopolis.crm.main.models.entity.Criterion;
import ru.innopolis.crm.task.models.exceptions.DaoException;
import ru.innopolis.crm.task.services.CriterionService;
import ru.innopolis.crm.task.services.exceptions.ServiceException;

import java.util.List;

/**
 * Created by Roman Taranov on 21.04.17.
 */
@Service
public class CriterionServiceImpl implements CriterionService
{

	private static final Logger LOG = Logger.getLogger(CriterionServiceImpl.class);

	private CriterionDao criterionDao;

	@Override
	public List<Criterion> getAll() throws ServiceException
	{
		try
		{
			return criterionDao.getAll();
		}
		catch (DaoException e)
		{
			LOG.error("ServiceException:" + e.getMessage());
			throw new ServiceException();
		}
	}

	@Override
	public Criterion getById(long id) throws ServiceException
	{
		try
		{
			return criterionDao.getById(id);
		}
		catch (DaoException e)
		{
			LOG.error("ServiceException:" + e.getMessage());
			throw new ServiceException();
		}
	}

	@Override
	public boolean delete(Criterion task) throws ServiceException
	{
		try
		{
			return criterionDao.delete(task);
		}
		catch (DaoException e)
		{
			LOG.error("ServiceException:" + e.getMessage());
			throw new ServiceException();
		}
	}

	@Override
	public boolean update(Criterion task) throws ServiceException
	{
		try
		{
			return criterionDao.update(task);
		}
		catch (DaoException e)
		{
			LOG.error("ServiceException:" + e.getMessage());
			throw new ServiceException();
		}
	}

	@Override
	public List<Criterion> getAllByTask(long taskId) throws ServiceException
	{
		try
		{
			return criterionDao.getByTask(taskId);
		}
		catch (DaoException e)
		{
			LOG.error("ServiceException:" + e.getMessage());
			throw new ServiceException();
		}
	}

	@Override
	public boolean create(Criterion criterion) throws ServiceException
	{
		try
		{
			return criterionDao.create(criterion);
		}
		catch (DaoException e)
		{
			LOG.error("ServiceException:" + e.getMessage());
			throw new ServiceException();
		}
	}

	@Autowired
	public void setCriterionDao(CriterionDao criterionDao)
	{
		this.criterionDao = criterionDao;
	}
}