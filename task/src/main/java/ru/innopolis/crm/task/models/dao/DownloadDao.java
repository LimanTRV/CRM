package ru.innopolis.crm.task.models.dao;


import ru.innopolis.crm.task.models.entity.Download;
import ru.innopolis.crm.task.models.exceptions.DaoException;

import java.util.List;

/**
 * Created by Roman Taranov on 22.04.2017.
 */
public interface DownloadDao
{

	Download getById(long id) throws DaoException;

	int addDownload(Download download) throws DaoException;

	int deleteDownload(long id) throws DaoException;

	int updateDownload(Download download) throws DaoException;

	List<Download> getTasksByStudentId(long studentId) throws DaoException;

	Download getByStudentAndTask(long studentId, long taskId) throws DaoException;
}
