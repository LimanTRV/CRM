package ru.innopolis.crm.task.services;

import ru.innopolis.crm.main.models.entity.Student;
import ru.innopolis.crm.task.models.entity.Mark;
import ru.innopolis.crm.task.services.exceptions.ServiceException;

import java.util.List;
import java.util.Map;

/**
 * Created by Roman Taranov on 24.04.17.
 */
public interface MarkService
{

	Map<Student, List<Mark>> getAllByGroupAndTask(long groupId, long taskId) throws ServiceException;

	List<Mark> getAllByCriterion(long criterionId) throws ServiceException;

	long create(Mark mark) throws ServiceException;

	int update(Mark mark) throws ServiceException;
}