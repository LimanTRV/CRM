package ru.innopolis.crm.task.models.dao.impl;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import ru.innopolis.crm.main.models.entity.StudyGroup;
import ru.innopolis.crm.main.models.entity.Task;
import ru.innopolis.crm.task.models.dao.TaskDao;
import ru.innopolis.crm.task.models.dao.TaskGroupDao;
import ru.innopolis.crm.task.models.exceptions.DaoException;

import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Service implements DAO interface to insert User objects into DB
 * or get User objects from DB
 *
 * @author Roman Taranov
 */
@Repository
public class TaskDaoImpl implements TaskDao
{

	private DataSource dataSource;

	private TaskGroupDao taskGroupDao = new TaskGroupDaoImpl();

	private static final String INSERT = "insert into task.task(title, description, is_lab, end_date) values(?, ?, ?, ?) returning id, created";

	private static final String UPDATE = "update task.task set title = ?, description = ?, end_date = ? where id = ?";

	private static final String UPDATE_TASK_DOWNLOAD_URL = "update task.task set task_standard_url = ? where id = ?";

	private static final Logger LOG = Logger.getLogger(TaskDaoImpl.class);


	public TaskDaoImpl()
	{
	}

	public TaskDaoImpl(DataSource dataSource)
	{
		this.dataSource = dataSource;
	}

	/**
	 * Creates a <code>Task</code> object from DB by id
	 *
	 * @param id - identificator of object in DB table
	 * @return a new default <code>User</code> object
	 */

	@Override
	public Task getById(long id) throws DaoException
	{
		Task task = new Task();
		try
		{
			Connection connection = dataSource.getConnection();
			Statement statement = connection.createStatement();
			ResultSet set =
					statement.executeQuery("select tt.title, tt.description,tt.is_lab," +
							"tt.created ,tt.end_date,  ttg.study_group_id as group_id " +
							" from task.task tt left join" +
							" task.task_to_group ttg on tt.id = ttg.task_id where tt.id = " + id);
			while (set.next())
			{
				task.setId(id);
				task.setTitle(set.getString("title"));
				task.setDescription(set.getString("description"));
				task.setLab(set.getBoolean("is_lab"));
				task.setCreated(set.getTimestamp("created"));
				task.setEndDate(set.getTimestamp("end_date"));

				Long groupId = set.getLong("group_id");

				if (groupId > 0)
				{
					StudyGroup group = new StudyGroup();
					group.setId(groupId);
					task.setGroup(group);
				}
			}
		}
		catch (SQLException e)
		{
			LOG.error("SQLException:" + e.getMessage());
			throw new DaoException();
		}
		return task;
	}

	/**
	 * Creates a list of <code>Task</code> objects from DB
	 *
	 * @return a new default <code>list User</code> objects
	 */

	@Override
	public List<Task> getAllByGroup(long groupId) throws DaoException
	{
		List<Task> tasksList = new ArrayList<>();
		try (Connection connection = dataSource.getConnection();
			 PreparedStatement statement = connection.prepareStatement("SELECT tt.id AS id, " +
							 "title, description, task_standard_url, is_lab, created, end_date,ttg.study_group_id as group_id  FROM task.task tt " +
							 "JOIN task.task_to_group ttg ON tt.id = ttg.task_id " +
							 "WHERE ttg.study_group_id = ?",
					 Statement.NO_GENERATED_KEYS))
		{

			statement.setLong(1, groupId);

			ResultSet resultSet = statement.executeQuery();
			while (resultSet.next())
			{
				tasksList.add(createTask(resultSet));
			}
		}
		catch (SQLException e)
		{
			LOG.error("SQLException:" + e.getMessage());
			throw new DaoException();
		}
		return tasksList;
	}

	@Override
	public List<Task> getAllByStudentId(long studentId) throws DaoException
	{
		List<Task> tasksList = new ArrayList<>();
		try (Connection connection = dataSource.getConnection();
			 PreparedStatement statement = connection.prepareStatement("SELECT distinct(tsk.id) AS id, " +
							 " title, description, is_lab, created, end_date, task_standard_url, ttg.study_group_id as group_id FROM task.task tsk " +
							 " left join task.task_to_group ttg ON tsk.id = ttg.task_id " +
							 " left join main.student_to_group stg ON ttg.study_group_id = stg.study_group_id " +
							 " WHERE stg.student_id = ?",
					 Statement.NO_GENERATED_KEYS))
		{

			statement.setLong(1, studentId);

			ResultSet resultSet = statement.executeQuery();
			while (resultSet.next())
			{
				tasksList.add(createTask(resultSet));
			}
		}
		catch (SQLException e)
		{
			LOG.error("SQLException:" + e.getMessage());
			throw new DaoException();
		}
		return tasksList;
	}

	/**
	 * Saves <tt>Task</tt> element to database, adds id and
	 * creation date to {@code task} element
	 *
	 * @param task element to be saved
	 * @return <tt>true</tt> if element was added
	 */
	@Override
	public boolean create(Task task) throws DaoException
	{
		try
		{
			Connection connection = dataSource.getConnection();
			PreparedStatement statement = connection.prepareStatement(INSERT);
			statement.setString(1, task.getTitle());
			statement.setString(2, task.getDescription());
			statement.setBoolean(3, task.isLab());
			statement.setTimestamp(4, task.getEndDate());
			ResultSet keys = statement.executeQuery();
			boolean inserted = false;
			if (keys.next())
			{
				inserted = true;
				task.setId(keys.getLong(1));
				task.setCreated(keys.getTimestamp(2));
				taskGroupDao.addTask(task);
			}
			keys.close();
			statement.close();
			connection.close();
			return inserted;
		}
		catch (SQLException e)
		{
			LOG.error(e.getMessage());
			throw new DaoException();
		}
	}

	/**
	 * Updates information about <tt>Task</tt> in db
	 *
	 * @param task element to be updated
	 * @return <tt>true</tt> if information was updated
	 * @throws SQLException
	 */
	@Override
	public boolean update(Task task) throws DaoException
	{
		try
		{
			Connection connection = dataSource.getConnection();
			Task taskFromDB = getById(task.getId());
			if (taskFromDB == null)
			{
				return false;
			}
			PreparedStatement statement = connection.prepareStatement(UPDATE);
			statement.setString(1, task.getTitle());
			statement.setString(2, task.getDescription());
			statement.setTimestamp(3, task.getEndDate());
			statement.setLong(4, task.getId());
			boolean updated = statement.executeUpdate() > 0;
			return updated;
		}
		catch (SQLException e)
		{
			LOG.error(e.getMessage());
			throw new DaoException();
		}
	}

	/**
	 * Updates information about <tt>Task</tt> in db
	 *
	 * @param taskStandardUrl new reference to standard solution
	 * @return <tt>true</tt> if information was updated
	 * @throws SQLException
	 */
	@Override
	public boolean updateUrl(String taskStandardUrl, long taskId) throws DaoException
	{
		try
		{
			Connection connection = dataSource.getConnection();

			PreparedStatement statement = connection.prepareStatement(UPDATE_TASK_DOWNLOAD_URL);
			statement.setString(1, taskStandardUrl);
			statement.setLong(2, taskId);

			boolean updated = statement.executeUpdate() > 0;
			return updated;
		}
		catch (SQLException e)
		{
			LOG.error(e.getMessage());
			throw new DaoException();
		}
	}

	/**
	 * Deletes <tt>Task</tt> from db, removes id and creation date
	 * from {@code task} element
	 *
	 * @param task element to be deleted
	 * @return <tt>true</tt> if element was deleted
	 */
	@Override
	public boolean delete(Task task) throws DaoException
	{
		int deletedCount = 0;
		try
		{
			deleteFromCriterion(task);
			deleteFromGroup(task);

			Connection connection = dataSource.getConnection();
			PreparedStatement dds = connection.prepareStatement("delete from task.task where id = ?",
					Statement.RETURN_GENERATED_KEYS);
			dds.setLong(1, task.getId());
			deletedCount = dds.executeUpdate();
		}
		catch (SQLException e)
		{
			LOG.error("SQLException:" + e.getMessage());
			throw new DaoException();
		}
		return deletedCount > 0;
	}

	public boolean deleteFromCriterion(Task task) throws DaoException
	{
		int deletedCount = 0;
		try
		{
			Connection connection = dataSource.getConnection();
			PreparedStatement ds = connection.prepareStatement("delete from task.criterion_to_task where task_id = ?",
					Statement.RETURN_GENERATED_KEYS);
			ds.setLong(1, task.getId());
			deletedCount = ds.executeUpdate();
		}
		catch (SQLException e)
		{
			LOG.error("SQLException:" + e.getMessage());
			throw new DaoException();
		}
		return deletedCount > 0;
	}

	public boolean deleteFromGroup(Task task) throws DaoException
	{
		int deletedCount = 0;
		try
		{
			Connection connection = dataSource.getConnection();
			PreparedStatement deleteStatement = connection.prepareStatement("delete from task.task_to_group where task_id = ?",
					Statement.RETURN_GENERATED_KEYS);
			deleteStatement.setLong(1, task.getId());
			deletedCount = deleteStatement.executeUpdate();
		}
		catch (SQLException e)
		{
			LOG.error("SQLException:" + e.getMessage());
			throw new DaoException();
		}
		return deletedCount > 0;
	}

	/**
	 * Creates new <tt>Task</tt> element from {@code set}
	 *
	 * @param set a set with data
	 * @return new <tt>Task</tt> element or null
	 * @throws SQLException
	 */
	private Task createTask(ResultSet set) throws SQLException
	{
		Task task = new Task();
		task.setId(set.getLong("id"));
		task.setTitle(set.getString("title"));
		task.setDescription(set.getString("description"));
		task.setTaskStandardUrl(set.getString("task_standard_url"));
		task.setLab(set.getBoolean("is_lab"));
		task.setCreated(set.getTimestamp("created"));
		task.setEndDate(set.getTimestamp("end_date"));

		Long groupId = set.getLong("group_id");

		if (groupId > 0)
		{
			StudyGroup group = new StudyGroup();
			group.setId(groupId);
			task.setGroup(group);
		}
		return task;
	}

	@Autowired
	public void setTaskGroupDao(TaskGroupDao taskGroupDao)
	{
		this.taskGroupDao = taskGroupDao;
	}

	@Autowired
	public void setDataSource(DataSource dataSource)
	{
		this.dataSource = dataSource;
	}
}
