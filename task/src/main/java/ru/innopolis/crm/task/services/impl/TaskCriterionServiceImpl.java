package ru.innopolis.crm.task.services.impl;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.innopolis.crm.main.models.entity.Criterion;
import ru.innopolis.crm.main.models.entity.Task;
import ru.innopolis.crm.task.models.dao.TaskCriterionDao;
import ru.innopolis.crm.task.models.exceptions.DaoException;
import ru.innopolis.crm.task.services.TaskCriterionService;
import ru.innopolis.crm.task.services.exceptions.ServiceException;

/**
 * Created by Roman Taranov on 27.04.2017.
 */
@Service
public class TaskCriterionServiceImpl implements TaskCriterionService
{

	private static final Logger LOG = Logger.getLogger(TaskCriterionServiceImpl.class);

	private TaskCriterionDao taskCriterionDao;

	@Override
	public boolean connect(Task task, Criterion criterion) throws ServiceException
	{
		try
		{
			return taskCriterionDao.connect(task, criterion);
		}
		catch (DaoException e)
		{
			LOG.error("ServiceException:" + e.getMessage());
			throw new ServiceException();
		}
	}

	@Override
	public boolean disconnect(Task task, Criterion criterion) throws ServiceException
	{
		try
		{
			return taskCriterionDao.disconnect(task, criterion);
		}
		catch (DaoException e)
		{
			LOG.error("ServiceException:" + e.getMessage());
			throw new ServiceException();
		}
	}

	@Autowired
	public void setTaskCriterionDao(TaskCriterionDao taskCriterionDao)
	{
		this.taskCriterionDao = taskCriterionDao;
	}
}
