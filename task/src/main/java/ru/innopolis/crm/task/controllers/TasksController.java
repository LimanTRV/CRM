package ru.innopolis.crm.task.controllers;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import ru.innopolis.crm.main.models.entity.Criterion;
import ru.innopolis.crm.main.models.entity.StudyGroup;
import ru.innopolis.crm.main.models.entity.Task;
import ru.innopolis.crm.task.services.CriterionService;
import ru.innopolis.crm.task.services.TaskService;
import ru.innopolis.crm.task.services.exceptions.ServiceException;

import javax.servlet.http.HttpServletRequest;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by Roman Taranov on 22.04.2017.
 */
@Controller
public class TasksController
{
	private static final Logger LOGGER = Logger.getLogger(TasksController.class);
	private static final Gson gson = new GsonBuilder().serializeNulls().setDateFormat("dd MM yyyy").create();
	private final TaskService taskService;
	private final CriterionService criterionService;

	@Autowired
	public TasksController(TaskService taskService, CriterionService criterionService)
	{
		this.taskService = taskService;
		this.criterionService = criterionService;
	}

	@PreAuthorize(value = "hasAnyRole('T_E', 'T_V')")
	@RequestMapping(value = "/task", method = RequestMethod.GET)
	public String getTasksList(Model model, HttpServletRequest request)
	{
		StudyGroup group = (StudyGroup) request.getAttribute("currentGroup");

		try
		{
			List<Criterion> criteria = criterionService.getAll();
			List<Task> tasks = taskService.getByGroupId(group.getId());

			criteria = criteria.stream().sorted((a, b) -> a.isAdditional() ? 1 : -1).collect(Collectors.toList());

			for (Task task : tasks)
			{
				task.setCriteria(criterionService.getAllByTask(task.getId()));
			}

			model.addAttribute("tasks", gson.toJson(tasks));
			model.addAttribute("criteria", gson.toJson(criteria));
		}
		catch (ServiceException e)
		{
			LOGGER.error(e.getMessage());
			return "redirect: /error";
		}

		return "/task/tasks";
	}

	@PreAuthorize(value = "hasRole('T_E')")
	@RequestMapping(value = "/task/add", produces = {"application/json; charset=UTF-8"}, method = RequestMethod.POST)
	public ResponseEntity<String> addTask(
			@RequestParam(value = "isLab", required = true) boolean isLab,
			@RequestParam(value = "date", required = false) String date,
			@RequestParam(value = "title", required = true) String title,
			@RequestParam(value = "description", required = true) String description,
			@RequestParam(value = "groupId", required = true) Long groupId
	)
	{
		Task task = new Task(title, description, isLab);
		StudyGroup group = new StudyGroup();
		group.setId(groupId);
		task.setGroup(group);

		try
		{
			if (date != null)
			{
				task.setEndDate(new Timestamp(new SimpleDateFormat("dd MM yyyy").parse(date).getTime()));
			}
			if (!taskService.create(task))
			{
				LOGGER.error("Неизвестная ошибка при добавлении задания");
				return ResponseEntity.status(500).body("{\"result\": false}");
			}

			return ResponseEntity.ok(gson.toJson(task));
		}
		catch (ServiceException e)
		{
			LOGGER.error(e.getMessage());
			return ResponseEntity.status(500).body("{\"result\": false}");
		}
		catch (ParseException e)
		{
			LOGGER.error(e.getMessage());
			return ResponseEntity.status(500).body("{\"result\": false}");
		}
	}

	@PreAuthorize(value = "hasRole('T_E')")
	@RequestMapping(value = "/task/edit", produces = {"application/json; charset=UTF-8"}, method = RequestMethod.POST)
	public ResponseEntity<String> updateTask(
			@RequestParam(value = "id", required = true) Long id,
			@RequestParam(value = "date", required = false) String date,
			@RequestParam(value = "title", required = true) String title,
			@RequestParam(value = "description", required = true) String description
	)
	{
		try
		{
			Task task = taskService.getById(id);
			task.setTitle(title);
			task.setDescription(description);

			if (date != null)
			{
				task.setEndDate(new Timestamp(new SimpleDateFormat("dd MM yyyy").parse(date).getTime()));
			}

			if (!taskService.update(task))
			{
				LOGGER.error("Неизвестная ошибка при обновлении задания");
				return ResponseEntity.status(500).body("{\"result\": false}");
			}

			task.setCriteria(criterionService.getAllByTask(id));
			return ResponseEntity.ok(gson.toJson(task));
		}
		catch (ServiceException e)
		{
			LOGGER.error(e.getMessage());
			return ResponseEntity.status(500).body("{\"result\": false}");
		}
		catch (ParseException e)
		{
			LOGGER.error(e.getMessage());
			return ResponseEntity.status(500).body("{\"result\": false}");
		}
	}

	@PreAuthorize(value = "hasRole('T_E')")
	@RequestMapping(value = "/task/delete", produces = {"application/json; charset=UTF-8"}, method = RequestMethod.POST)
	public ResponseEntity<String> deleteTask(
			@RequestParam(value = "id", required = true) Long id
	)
	{
		try
		{
			Task task = taskService.getById(id);
			if (!taskService.delete(task))
			{
				LOGGER.error("Неизвестная ошибка при удалении задания");
				return ResponseEntity.status(500).body("{\"result\": false}");
			}
			return ResponseEntity.ok("{\"result\": false}");
		}
		catch (ServiceException e)
		{
			LOGGER.error(e.getMessage());
			return ResponseEntity.status(500).body("{\"result\": false}");
		}
	}

	@PreAuthorize(value = "hasRole('T_E')")
	@RequestMapping(value = "/task/changedate", produces = {"application/json; charset=UTF-8"}, method = RequestMethod.POST)
	public ResponseEntity<String> changeDate(
			@RequestParam(value = "id", required = true) Long id,
			@RequestParam(value = "date", required = false) String date
	)
	{
		try
		{
			Task task = taskService.getById(id);
			task.setEndDate(date == null ? null : new Timestamp(new SimpleDateFormat("dd MM yyyy").parse(date).getTime()));

			if (!taskService.update(task))
			{
				LOGGER.error("Неизвестная ошибка при обновлении задания");
				return ResponseEntity.status(500).body("{\"result\": false}");
			}
			return ResponseEntity.ok("{\"result\": true}");
		}
		catch (ServiceException e)
		{
			LOGGER.error(e.getMessage());
			return ResponseEntity.status(500).body("{\"result\": false}");
		}
		catch (ParseException e)
		{
			LOGGER.error(e.getMessage());
			return ResponseEntity.status(500).body("{\"result\": false}");
		}
	}
}
