package ru.innopolis.crm.task.controllers;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import ru.innopolis.crm.main.models.entity.Criterion;
import ru.innopolis.crm.main.models.entity.Task;
import ru.innopolis.crm.task.models.entity.Mark;
import ru.innopolis.crm.task.services.CriterionService;
import ru.innopolis.crm.task.services.MarkService;
import ru.innopolis.crm.task.services.TaskCriterionService;
import ru.innopolis.crm.task.services.TaskService;
import ru.innopolis.crm.task.services.exceptions.ServiceException;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by Roman Taranov on 22.04.2017.
 */
@Controller
@PreAuthorize(value = "hasRole('T_E')")
public class CriterionController
{

	private TaskService taskService;

	private TaskCriterionService tcService;

	private CriterionService criterionService;

	private MarkService markService;

	private final Gson gson = new GsonBuilder().serializeNulls().setDateFormat("dd MM yyyy").create();

	private static final Logger LOGGER = Logger.getLogger(TasksController.class);

	@RequestMapping(value = "/criteria", produces = {"application/json; charset=UTF-8"}, method = RequestMethod.POST)
	public ResponseEntity<String> createCriterion(
			@RequestParam(value = "title", required = true) String title,
			@RequestParam(value = "max_points", required = false) int max_points,
			@RequestParam(value = "is_additional", required = false) boolean isAdd
	)
	{
		if (max_points == 0 || title.isEmpty())
		{
			return ResponseEntity.status(422).body("{\"result\": false}");
		}
		Criterion criterion = new Criterion(title, max_points, isAdd);
		try
		{
			criterionService.create(criterion);
			return ResponseEntity.ok(gson.toJson(criterion));
		}
		catch (ServiceException e)
		{
			LOGGER.error(e.getMessage());
			return ResponseEntity.status(500).body("{\"result\": false}");
		}
	}

	@RequestMapping(value = "/criteria/edit", produces = {"application/json; charset=UTF-8"}, method = RequestMethod.POST)
	public ResponseEntity<String> updateCriterion(
			@RequestParam(value = "id", required = true) Long id,
			@RequestParam(value = "title", required = true) String title,
			@RequestParam(value = "max_points", required = false) int max_points,
			@RequestParam(value = "is_additional", required = false) boolean isAdd
	)
	{

		if (max_points == 0 || title.isEmpty())
		{
			return ResponseEntity.status(422).body("{\"result\": false}");
		}

		try
		{
			Criterion criterion = criterionService.getById(id);
			if (criterion == null)
			{
				LOGGER.error("Неизвестная ошибка при поиске критерия");
				return ResponseEntity.status(500).body("{\"result\": false}");
			}
			criterion.setTitle(title);
			criterion.setAdditional(isAdd);
			criterion.setMaxPoints(max_points);

			List<Mark> marks = markService.getAllByCriterion(id);
			for (Mark mark : marks)
			{
				if (mark.getPoints() > max_points)
				{
					mark.setPoints(max_points);
					markService.update(mark);
				}
			}

			if (!criterionService.update(criterion))
			{
				LOGGER.error("Неизвестная ошибка при обновлении критерия");
				return ResponseEntity.status(500).body("{\"result\": false}");
			}
			;
			return ResponseEntity.ok(gson.toJson(criterion));
		}
		catch (ServiceException e)
		{
			LOGGER.error("Ошибка" + e.getMessage());
			return ResponseEntity.status(500).body("{\"result\": false}");
		}
	}

	@RequestMapping(value = "/criteria/delete", produces = {"application/json; charset=UTF-8"}, method = RequestMethod.POST)
	public ResponseEntity<String> deleteCriterion(@RequestParam(value = "id", required = true) Long criterionId)
	{
		try
		{
			Criterion task = criterionService.getById(criterionId);
			if (task == null || !criterionService.delete(task))
			{
				LOGGER.error("Неизвестная ошибка при удалении критерия");
				return ResponseEntity.status(422).body("{\"result\": false}");
			}
			return ResponseEntity.ok("{\"result\": true}");
		}
		catch (ServiceException e)
		{
			LOGGER.error("Неизвестная ошибка при удалении критерия");
			return ResponseEntity.status(500).body("{\"result\": false}");
		}
	}

	@RequestMapping(value = "/task/connectcriterion", produces = {"application/json; charset=UTF-8"}, method = RequestMethod.POST)
	public ResponseEntity<String> addCriterionToTask(
			@RequestParam(value = "criterionId[]", required = true) Long[] criterions,
			@RequestParam(value = "taskId", required = true) Long taskId)
	{
		try
		{
			List<Long> dbCriterions = criterionService.getAllByTask(taskId).stream().map(c -> c.getId()).collect(Collectors.toList());
			Task task = taskService.getById(taskId);

			for (long cr : criterions)
			{
				if (dbCriterions.contains(cr))
				{
					continue;
				}
				tcService.connect(task, criterionService.getById(cr));
			}

			for (long cr : dbCriterions)
			{
				if (Arrays.asList(criterions).stream().anyMatch(c -> c == cr))
				{
					continue;
				}
				tcService.disconnect(task, criterionService.getById(cr));
			}
			return ResponseEntity.ok("{\"result\": true}");
		}
		catch (ServiceException e)
		{
			LOGGER.error("Неизвестная ошибка при поиске критерия" + e.getMessage());
			return ResponseEntity.status(500).body("{\"result\": false}");
		}
	}

	@RequestMapping(value = "/criteria/check", produces = {"application/json; charset=UTF-8"}, method = RequestMethod.GET)
	public ResponseEntity<String> checkCriterion(
			@RequestParam(value = "id", required = true) Long id,
			@RequestParam(value = "max", required = true) Integer max
	)
	{
		try
		{
			List<Mark> marks = markService.getAllByCriterion(id);
			for (Mark mark : marks)
			{
				if (mark.getPoints() > max)
				{
					return ResponseEntity.ok("{\"conflicts\": true}");
				}
			}
			return ResponseEntity.ok("{\"conflicts\": false}");
		}
		catch (ServiceException e)
		{
			LOGGER.error("Неизвестная ошибка при поиске критерия" + e.getMessage());
			return ResponseEntity.status(500).body("{\"result\": false}");
		}
	}

	@Autowired
	public void setTcService(TaskCriterionService tcService)
	{
		this.tcService = tcService;
	}

	@Autowired
	public void setTaskService(TaskService taskService)
	{
		this.taskService = taskService;
	}

	@Autowired
	public void setCriterionService(CriterionService criterionService)
	{
		this.criterionService = criterionService;
	}

	@Autowired
	public void setMarkService(MarkService markService)
	{
		this.markService = markService;
	}
}
