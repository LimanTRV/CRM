package ru.innopolis.crm.task.services.impl;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.innopolis.crm.task.models.dao.DownloadDao;
import ru.innopolis.crm.task.models.entity.Download;
import ru.innopolis.crm.task.models.exceptions.DaoException;
import ru.innopolis.crm.task.services.DownloadService;
import ru.innopolis.crm.task.services.exceptions.ServiceException;

import java.util.List;

/**
 * Created by Roman Taranov on 27.04.2017.
 */

@Service
public class DownloadServiceImpl implements DownloadService
{

	private static final Logger LOGGER = Logger.getLogger(DownloadServiceImpl.class);

	private DownloadDao downloadDao;

	@Autowired
	public void setDownloadDao(DownloadDao downloadDao)
	{
		this.downloadDao = downloadDao;
	}

	@Override
	public Download getById(long id)
	{
		try
		{
			return downloadDao.getById(id);
		}
		catch (DaoException e)
		{
			LOGGER.error(e);
			throw new ServiceException();
		}
	}

	@Override
	public Download getByStudentAndTask(long studentId, long taskId) throws ServiceException
	{
		try
		{
			return downloadDao.getByStudentAndTask(studentId, taskId);
		}
		catch (DaoException e)
		{
			LOGGER.error(e);
			throw new ServiceException();
		}
	}

	/**
	 * Добавление объекта загрузки
	 *
	 * @param download
	 * @return
	 */
	@Override
	public int addDownload(Download download)
	{
		try
		{
			return downloadDao.addDownload(download);
		}
		catch (DaoException e)
		{
			LOGGER.error(e);
			throw new ServiceException();
		}
	}

	/**
	 * Удаление
	 *
	 * @param id
	 * @return
	 */
	@Override
	public int deleteDownload(long id)
	{
		try
		{
			return downloadDao.deleteDownload(id);
		}
		catch (DaoException e)
		{
			LOGGER.error(e);
			throw new ServiceException();
		}
	}

	/**
	 * Обновление
	 *
	 * @param download
	 * @return
	 */
	@Override
	public int updateDownload(Download download)
	{
		try
		{
			return downloadDao.updateDownload(download);
		}
		catch (DaoException e)
		{
			LOGGER.error(e);
			throw new ServiceException();
		}
	}

	/**
	 * получить TaskWithDownloadStatus по Ид студента
	 *
	 * @param studentId
	 * @return
	 */
	@Override
	public List<Download> getTasksByStudentId(long studentId)
	{
		try
		{
			return downloadDao.getTasksByStudentId(studentId);
		}
		catch (DaoException e)
		{
			LOGGER.error(e);
			throw new ServiceException();
		}
	}

}
