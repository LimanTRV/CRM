package ru.innopolis.crm.task.models.dao.impl;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import ru.innopolis.crm.task.models.dao.DownloadDao;
import ru.innopolis.crm.task.models.dao.TaskDao;
import ru.innopolis.crm.task.models.entity.Download;
import ru.innopolis.crm.main.models.entity.Task;
import ru.innopolis.crm.task.models.exceptions.DaoException;

import javax.sql.DataSource;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Roman Taranov on 22.04.2017.
 */
@Repository
@SuppressWarnings("Duplicates")
public class DownloadDaoImpl implements DownloadDao
{

	private static final Logger LOGGER = Logger.getLogger(DownloadDaoImpl.class);
	private static final String GET_BY_ID_QUERY = "SELECT * FROM task.download WHERE id = ? AND NOT is_deleted";
	private static final String INSERT_DOWNLOAD = "INSERT INTO task.download (student_id, task_id, git_url, download_date, download_file_name, download_url) VALUES (?, ?, ?, ?, ?, ?)";
	private static final String DELETE_TASK_BY_ID = "UPDATE task.download SET is_deleted = TRUE WHERE id = ?";
	private static final String GET_TASKS_BY_STUDENT_ID = "SELECT * FROM (SELECT id, student_id, task_id, " +
			"git_url, download_date, download_file_name, download_url, is_deleted, row_number() OVER (PARTITION BY task_id ORDER BY download_date DESC )  AS date_in_group " +
			"FROM task.download WHERE is_deleted = FALSE AND student_id = ? ORDER BY task_id, date_in_group DESC) a WHERE date_in_group <= 1;";
	private static final String UPDATE_DOWNLOAD_BY_ID = "UPDATE task.download SET " +
			"student_id = ?, task_id = ?, git_url = ?, download_date = ?, download_file_name = ?, " +
			"download_url = ?, is_deleted = ? WHERE id = ?";
	private static final String DOWNLOAD_BY_STUDENT_AND_TASK = "SELECT * FROM task.download WHERE student_id = ? AND task_id = ? AND NOT is_deleted";

	private DataSource dataSource;
	private TaskDao taskDao;

	@Autowired
	public void setTaskDao(TaskDao taskDao)
	{
		this.taskDao = taskDao;
	}

	@Autowired
	public void setDataSource(DataSource dataSource)
	{
		this.dataSource = dataSource;
	}

	@Override
	public Download getById(long id) throws DaoException
	{
		try
		{
			Connection connection = dataSource.getConnection();
			PreparedStatement statement = connection.prepareStatement(GET_BY_ID_QUERY);
			statement.setLong(1, id);
			ResultSet resultSet = statement.executeQuery();
			if (resultSet.next())
			{
				Download download = new Download();
				download.setId(resultSet.getLong("id"));
				download.setStudentId(resultSet.getLong("student_id"));
				download.setTaskId(resultSet.getLong("task_id"));
				download.setGitUrl(resultSet.getString("git_url"));
				download.setDownloadDate(resultSet.getTimestamp("download_date"));
				download.setFileName(resultSet.getString("download_file_name"));
				download.setDownloadUrl(resultSet.getString("download_url"));
				download.setDeleted(resultSet.getBoolean("is_deleted"));
				return download;
			}
		}
		catch (SQLException e)
		{
			LOGGER.error("An SQL error occurred :" + e.getMessage() + "error code:" + e.getErrorCode());
			throw new DaoException();
		}
		return null;
	}

	@Override
	public Download getByStudentAndTask(long studentId, long taskId) throws DaoException
	{
		try
		{
			Connection connection = dataSource.getConnection();
			PreparedStatement statement = connection.prepareStatement(DOWNLOAD_BY_STUDENT_AND_TASK);

			statement.setLong(1, studentId);
			statement.setLong(2, taskId);

			ResultSet resultSet = statement.executeQuery();
			if (resultSet.next())
			{
				Download download = new Download();
				download.setId(resultSet.getLong("id"));
				download.setStudentId(resultSet.getLong("student_id"));
				download.setTaskId(resultSet.getLong("task_id"));
				download.setGitUrl(resultSet.getString("git_url"));
				download.setDownloadDate(resultSet.getTimestamp("download_date"));
				download.setFileName(resultSet.getString("download_file_name"));
				download.setDownloadUrl(resultSet.getString("download_url"));
				download.setDeleted(resultSet.getBoolean("is_deleted"));
				return download;
			}
		}
		catch (SQLException e)
		{
			LOGGER.error("An SQL error occurred :" + e.getMessage() + "error code:" + e.getErrorCode());
			throw new DaoException();
		}
		return null;
	}

	@Override
	public int addDownload(Download download) throws DaoException
	{
		int result;
		try
		{
			Connection connection = dataSource.getConnection();
			PreparedStatement preparedStatement = null;

			preparedStatement = connection.prepareStatement(INSERT_DOWNLOAD);

			preparedStatement.setLong(1, download.getStudentId());
			preparedStatement.setLong(2, download.getTaskId());
			preparedStatement.setString(3, download.getGitUrl());
			preparedStatement.setTimestamp(4, download.getDownloadDate());
			preparedStatement.setString(5, download.getFileName());
			preparedStatement.setString(6, download.getDownloadUrl());

			result = preparedStatement.executeUpdate();

		}
		catch (SQLException e)
		{
			LOGGER.error("Add <download> in DB failed", e);
			throw new DaoException();
		}

		return result;
	}

	@Override
	public int deleteDownload(long id) throws DaoException
	{
		int deleted;

		try
		{
			Connection connection = dataSource.getConnection();
			PreparedStatement statement = connection.prepareStatement(DELETE_TASK_BY_ID);
			statement.setLong(1, id);

			deleted = statement.executeUpdate();

		}
		catch (SQLException e)
		{
			LOGGER.error("Delete <download> failed", e);
			throw new DaoException();
		}

		return deleted;
	}

	@Override
	public int updateDownload(Download download) throws DaoException
	{

		int updated = 0;
		try
		{
			Connection connection = dataSource.getConnection();

			PreparedStatement preparedStatement = connection.prepareStatement(UPDATE_DOWNLOAD_BY_ID);

			preparedStatement.setLong(1, download.getStudentId());
			preparedStatement.setLong(2, download.getTaskId());
			preparedStatement.setString(3, download.getGitUrl());
			preparedStatement.setTimestamp(4, download.getDownloadDate());
			preparedStatement.setString(5, download.getFileName());
			preparedStatement.setString(6, download.getDownloadUrl());
			preparedStatement.setBoolean(7, download.isDeleted());
			preparedStatement.setLong(8, download.getId());

			updated = preparedStatement.executeUpdate();

		}
		catch (SQLException e)
		{
			LOGGER.error("Update <download> failed", e);
			throw new DaoException();
		}

		return updated;
	}

	@Override
	public List<Download> getTasksByStudentId(long studentId) throws DaoException
	{
		List<Download> downloads = new ArrayList<>(16);

		try
		{
			Connection connection = dataSource.getConnection();

			PreparedStatement preparedStatement = connection.prepareStatement(GET_TASKS_BY_STUDENT_ID);
			preparedStatement.setLong(1, studentId);

			ResultSet resultSet = preparedStatement.executeQuery();

			while (resultSet.next())
			{
				long taskId = resultSet.getLong("task_id");
				Task task = taskDao.getById(taskId);
				Download download = new Download();
				download.setId(resultSet.getLong("id"));
				download.setTaskId(resultSet.getLong("task_id"));
				download.setTask(task);
				download.setDownloadDate(resultSet.getTimestamp("download_date"));
				download.setDownloaded(true);
				download.setGitUrl(resultSet.getString("git_url"));
				download.setFileName(resultSet.getString("download_file_name"));
				downloads.add(download);
			}

		}
		catch (SQLException e)
		{
			LOGGER.error("Getting list students failed!", e);
			throw new DaoException();
		}
		return downloads;
	}
}
