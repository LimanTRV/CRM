package ru.innopolis.crm.task.services;

import ru.innopolis.crm.task.models.entity.Download;
import ru.innopolis.crm.task.services.exceptions.ServiceException;

import java.util.List;

/**
 * Created by Roman Taranov on 27.04.2017.
 */
public interface DownloadService
{

	Download getById(long id) throws ServiceException;

	Download getByStudentAndTask(long studentId, long taskId) throws ServiceException;

	int addDownload(Download download) throws ServiceException;

	int deleteDownload(long id) throws ServiceException;

	int updateDownload(Download download) throws ServiceException;

	List<Download> getTasksByStudentId(long studentId) throws ServiceException;

}
