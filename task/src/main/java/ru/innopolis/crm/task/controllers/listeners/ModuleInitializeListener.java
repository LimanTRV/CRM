package ru.innopolis.crm.task.controllers.listeners;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

/**
 * Created by Roman Taranov on 22.04.2017.
 */
public class ModuleInitializeListener implements ServletContextListener {
    private static final Logger logger = Logger.getLogger(ModuleInitializeListener.class);

    @Override
    public void contextInitialized(ServletContextEvent servletContextEvent) {

    }

    @Override
    public void contextDestroyed(ServletContextEvent servletContextEvent) {

    }
}
