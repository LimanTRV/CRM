package ru.innopolis.crm.task.models.dao.impl;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import ru.innopolis.crm.main.models.dao.impl.StudentGroupDaoImpl;
import ru.innopolis.crm.main.models.entity.Task;
import ru.innopolis.crm.task.models.dao.TaskGroupDao;
import ru.innopolis.crm.task.models.exceptions.DaoException;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 * Created by Roman Taranov on 27.04.2017.
 */
@Repository
public class TaskGroupDaoImpl implements TaskGroupDao
{

	private DataSource dataSource;

	private final Logger LOG = Logger.getLogger(StudentGroupDaoImpl.class);

	private static final String INSERT = "insert into task.task_to_group(task_id, study_group_id) values(?, ?)";

	@Override
	public void addTask(Task task) throws DaoException
	{

		try
		{
			exec(INSERT, task.getId(), task.getGroup().getId());
		}
		catch (SQLException e)
		{
			LOG.error(e.getMessage());
			throw new DaoException();
		}
	}

	private boolean exec(String query, long taskId, long groupId) throws SQLException
	{
		Connection connection = dataSource.getConnection();
		PreparedStatement statement = connection.prepareStatement(query);
		statement.setLong(1, taskId);
		statement.setLong(2, groupId);
		boolean executed = statement.executeUpdate() > 0;
		statement.close();
		connection.close();
		return executed;
	}

	@Autowired
	public void setDataSource(DataSource dataSource)
	{
		this.dataSource = dataSource;
	}
}
