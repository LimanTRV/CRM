package ru.innopolis.crm.task.services;

import ru.innopolis.crm.main.models.entity.Criterion;
import ru.innopolis.crm.main.models.entity.Task;
import ru.innopolis.crm.task.services.exceptions.ServiceException;

/**
 * Created by Roman Taranov on 27.04.2017.
 */
public interface TaskCriterionService
{
	boolean connect(Task task, Criterion criterion) throws ServiceException;

	boolean disconnect(Task task, Criterion criterion) throws ServiceException;
}
