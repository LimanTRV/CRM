package ru.innopolis.crm.task.models.dao;

import ru.innopolis.crm.main.models.entity.Task;
import ru.innopolis.crm.task.models.exceptions.DaoException;

/**
 * Created by Roman Taranov on 22.04.2017.
 */
public interface TaskGroupDao
{
	void addTask(Task task) throws DaoException;
}
