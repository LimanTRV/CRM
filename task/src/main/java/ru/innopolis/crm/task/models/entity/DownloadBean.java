package ru.innopolis.crm.task.models.entity;

/**
 * Created by Roman Taranov on 27.04.2017.
 */
public class DownloadBean
{
	private Long taskId;
	private String gitUrl;
	private String downloadedFile;

	public Long getTaskId()
	{
		return taskId;
	}

	public void setTaskId(Long taskId)
	{
		this.taskId = taskId;
	}

	public String getGitUrl()
	{
		return gitUrl;
	}

	public void setGitUrl(String gitUrl)
	{
		this.gitUrl = gitUrl;
	}

	public String getDownloadedFile()
	{
		return downloadedFile;
	}

	public void setDownloadedFile(String downloadedFile)
	{
		this.downloadedFile = downloadedFile;
	}
}
