package ru.innopolis.crm.task.models.dao;

import ru.innopolis.crm.main.models.entity.Criterion;
import ru.innopolis.crm.main.models.entity.Task;
import ru.innopolis.crm.task.models.exceptions.DaoException;

/**
 * Created by Roman Taranov on 22.04.2017.
 */
public interface TaskCriterionDao
{
	boolean connect(Task task, Criterion criterion) throws DaoException;

	boolean disconnect(Task task, Criterion criterion) throws DaoException;
}
