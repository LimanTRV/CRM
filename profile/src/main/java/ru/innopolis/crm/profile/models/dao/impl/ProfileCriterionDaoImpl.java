package ru.innopolis.crm.profile.models.dao.impl;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import ru.innopolis.crm.profile.models.dao.ProfileCriterionDao;
import ru.innopolis.crm.profile.models.dao.exception.ProfileCriterionDaoException;
import ru.innopolis.crm.profile.models.entity.ProfileCriterion;
import ru.innopolis.crm.profile.services.exception.ProfileCriterionServiceException;

import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Имплементация DAO для ProfileCriterionDao
 * Created by admin on 21.04.2017.
 */
@Repository
public class ProfileCriterionDaoImpl implements ProfileCriterionDao {

    private static final Logger LOGGER = Logger.getLogger(ProfileCriterionDaoImpl.class);

    private static final String SELECT_ALL = "SELECT * FROM profile.questionnaire_criterion_instance WHERE NOT is_deleted";

    private static final String INSERT = "INSERT INTO profile.questionnaire_criterion_instance " +
            "(questionnaire_instance_id, name, max_grade, grade, criterion_type_id, criterion_group_id, " +
            "crm_user_id_edited, edit_date, questionaire_criterion_id, is_deleted) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

    private static final String UPDATE = "UPDATE profile.questionnaire_criterion_instance " +
            "SET questionnaire_instance_id = ?, name = ?, max_grade = ?, grade = ?, criterion_type_id = ?, " +
            "criterion_group_id = ?, crm_user_id_edited = ?, edit_date = ?, questionaire_criterion_id = ?, is_deleted = ? " +
            "WHERE id = ?;";

    private static final String DELETE = "UPDATE profile.questionnaire_criterion_instance " +
            "SET is_deleted = TRUE WHERE id = ?;";

    private DataSource dataSource;

    @Autowired
    public void setDataSource(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    public ProfileCriterion getById(long id) throws ProfileCriterionDaoException {
        ProfileCriterion profileCriterion = null;

        try {
            Connection connection = dataSource.getConnection();
            PreparedStatement statement = connection.prepareStatement(SELECT_ALL + " AND id = ?");
            statement.setLong(1, id);
            ResultSet resultSet = statement.executeQuery();
            if(resultSet.next()){
                profileCriterion = createProfileCriterion(resultSet);
            }
        } catch (SQLException e) {
            LOGGER.error(e);
            throw new ProfileCriterionDaoException();
        }

        return profileCriterion;
    }

    public List<ProfileCriterion> getAll() throws ProfileCriterionDaoException {
        List<ProfileCriterion> list = new ArrayList<ProfileCriterion>();

        try {
            Connection connection = dataSource.getConnection();
            PreparedStatement statement = connection.prepareStatement(SELECT_ALL);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                list.add(createProfileCriterion(resultSet));
            }
        } catch (SQLException e) {
            LOGGER.error(e);
            throw new ProfileCriterionDaoException();
        }

        return list;
    }

    @Override
    public List<ProfileCriterion> getByProfileId(Long profileId) throws ProfileCriterionDaoException {

        List<ProfileCriterion> list = new ArrayList<>();

        try {
            Connection connection = dataSource.getConnection();
            PreparedStatement statement = connection.prepareStatement(SELECT_ALL + " AND questionnaire_instance_id = ?");
            statement.setLong(1, profileId);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                list.add(createProfileCriterion(resultSet));
            }
        } catch (SQLException e) {
            LOGGER.error(e);
            throw new ProfileCriterionDaoException();
        }

        return list;
    }

    public ProfileCriterion save(ProfileCriterion profileCriterion) throws ProfileCriterionDaoException {
        if (profileCriterion == null) {
            return null;
        }

        if(profileCriterion.getId() > 0){
            update(profileCriterion);
        } else {
            long newId = create(profileCriterion);
            profileCriterion.setId(newId);
        }

        return profileCriterion;
    }

    public long create(ProfileCriterion profileCriterion) throws ProfileCriterionDaoException {
        long lastId = 0;

        if (profileCriterion == null) {
            LOGGER.error("ProfileCriterionDao.create(null) called.");
            return lastId;
        }

        if (profileCriterion.getId() > 0){
            LOGGER.error("Tried ProfileCriterionDao.create() object with existing id.");
            return profileCriterion.getId();
        }

        try {
            Connection connection = dataSource.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(INSERT, Statement.RETURN_GENERATED_KEYS);
            preparedStatement.setLong(1, profileCriterion.getProfileId());
            preparedStatement.setString(2, profileCriterion.getName());
            preparedStatement.setLong(3, profileCriterion.getMaxGrade());
            preparedStatement.setLong(4, profileCriterion.getGrade());
            preparedStatement.setLong(5, profileCriterion.getProfileCriterionTypeId());
            preparedStatement.setLong(6, profileCriterion.getProfileCriterionGroupId());
            preparedStatement.setLong(7, profileCriterion.getEditUserId());
            preparedStatement.setTimestamp(8, new java.sql.Timestamp(profileCriterion.getEditDate().getTime()));
            preparedStatement.setLong(9, profileCriterion.getProfileCriterionTemplateId());
            preparedStatement.setBoolean(10, profileCriterion.isDeleted());

            preparedStatement.executeUpdate();
            ResultSet resultSet = preparedStatement.getGeneratedKeys();
            if (resultSet.next()) {
                lastId = resultSet.getInt(1);
            }
        } catch (SQLException e) {
            LOGGER.error(e);
            throw new ProfileCriterionDaoException();
        }
        return lastId;
    }

    public int update(ProfileCriterion profileCriterion) throws ProfileCriterionDaoException {
        int count = 0;

        if (profileCriterion == null) {
            LOGGER.error("ProfileCriterionDao.update(null) called.");
            return count;
        }

        try {
            Connection connection = dataSource.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(UPDATE);
            preparedStatement.setLong(1, profileCriterion.getProfileId());
            preparedStatement.setString(2, profileCriterion.getName());
            preparedStatement.setLong(3, profileCriterion.getMaxGrade());
            preparedStatement.setLong(4, profileCriterion.getGrade());
            preparedStatement.setLong(5, profileCriterion.getProfileCriterionTypeId());
            preparedStatement.setLong(6, profileCriterion.getProfileCriterionGroupId());
            preparedStatement.setLong(7, profileCriterion.getEditUserId());
            preparedStatement.setTimestamp(8, new java.sql.Timestamp(profileCriterion.getEditDate().getTime()));
            preparedStatement.setLong(9, profileCriterion.getProfileCriterionTemplateId());
            preparedStatement.setBoolean(10, profileCriterion.isDeleted());

            preparedStatement.setLong(11, profileCriterion.getId());

            count = preparedStatement.executeUpdate();

        } catch (SQLException e) {
            LOGGER.error(e);
            throw new ProfileCriterionDaoException();
        }
        return count;
    }

    public int delete(ProfileCriterion profileCriterion) throws ProfileCriterionDaoException {
        int count = 0;

        if (profileCriterion == null) {
            LOGGER.error("ProfileCriterionDao.delete(null) called.");
            return count;
        }

        try {
            Connection connection = dataSource.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(DELETE);
            preparedStatement.setLong(1, profileCriterion.getId());
            count = preparedStatement.executeUpdate();
        } catch (SQLException e) {
            LOGGER.error(e);
            throw new ProfileCriterionDaoException();
        }
        return count;
    }

    private ProfileCriterion createProfileCriterion(ResultSet resultSet) throws SQLException {
        return  new ProfileCriterion(
                resultSet.getLong("id"),
                resultSet.getLong("questionnaire_instance_id"),
                resultSet.getString("name"),
                resultSet.getLong("max_grade"),
                resultSet.getLong("grade"),
                resultSet.getLong("criterion_type_id"),
                resultSet.getLong("criterion_group_id"),
                resultSet.getLong("crm_user_id_edited"),
                resultSet.getDate("edit_date"),
                resultSet.getLong("questionaire_criterion_id"),
                resultSet.getBoolean("is_deleted")
        );
    }
}
