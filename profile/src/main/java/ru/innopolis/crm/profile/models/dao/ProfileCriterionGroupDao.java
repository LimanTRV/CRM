package ru.innopolis.crm.profile.models.dao;

import ru.innopolis.crm.profile.models.dao.exception.ProfileCriterionGroupDaoException;
import ru.innopolis.crm.profile.models.entity.ProfileCriterionGroup;

import java.util.List;

/**
 * DAO для сущности ProfileCriterionGroup
 * Created by admin on 21.04.2017.
 */
public interface ProfileCriterionGroupDao {

    ProfileCriterionGroup getById(long id) throws ProfileCriterionGroupDaoException;

    List<ProfileCriterionGroup> getAll() throws ProfileCriterionGroupDaoException;

    ProfileCriterionGroup save(ProfileCriterionGroup entity) throws ProfileCriterionGroupDaoException;

    long create(ProfileCriterionGroup entity) throws ProfileCriterionGroupDaoException;

    int update(ProfileCriterionGroup entity) throws ProfileCriterionGroupDaoException;

    int delete(ProfileCriterionGroup entity) throws ProfileCriterionGroupDaoException;
}
