package ru.innopolis.crm.profile.models.dao.impl;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import ru.innopolis.crm.profile.models.dao.ProfileCriterionTypeDao;
import ru.innopolis.crm.profile.models.dao.exception.ProfileCriterionTypeDaoException;
import ru.innopolis.crm.profile.models.entity.ProfileCriterionType;

import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Имплементация DAO для ProfileCriterionTypeDao
 * Created by admin on 21.04.2017.
 */
@Repository
public class ProfileCriterionTypeDaoImpl implements ProfileCriterionTypeDao {

    private static final Logger LOGGER = Logger.getLogger(ProfileCriterionGroupDaoImpl.class);

    private static final String SELECT_ALL = "SELECT * FROM profile.criterion_type WHERE NOT is_deleted";

    private static final String INSERT = "INSERT INTO profile.criterion_type (name, max_grade, is_deleted) VALUES (?, ?, ?)";

    private static final String UPDATE = "UPDATE profile.criterion_type SET name = ?, max_grade = ?, is_deleted = ? WHERE id = ?;";

    private static final String DELETE = "UPDATE profile.criterion_type SET is_deleted = TRUE WHERE id = ?;";

    private static final String CONTAINS_QUESTIONNAIRE_CRITERION = "select count(id) from profile.questionnaire_criterion " +
            " WHERE criterion_type_id = ? AND NOT is_deleted";

    private static final String CONTAINS_QUESTIONNAIRE_CRITERION_INSTANCE = "select count(id) from profile.questionnaire_criterion_instance " +
            " WHERE criterion_type_id = ? AND NOT is_deleted";

    private DataSource dataSource;

    @Autowired
    public void setDataSource(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    public ProfileCriterionType getById(long id) throws ProfileCriterionTypeDaoException {
        ProfileCriterionType profileCriterionType = null;

        try {
            Connection connection = dataSource.getConnection();
            PreparedStatement statement = connection.prepareStatement(SELECT_ALL + " AND id = ?");

            statement.setLong(1, id);
            ResultSet resultSet = statement.executeQuery();
            if(resultSet.next()){
                profileCriterionType = createProfileCriterionType(resultSet);
            }
        } catch (SQLException e) {
            LOGGER.error(e);
            throw new ProfileCriterionTypeDaoException();
        }

        return profileCriterionType;
    }

    public List<ProfileCriterionType> getAll() throws ProfileCriterionTypeDaoException {
        List<ProfileCriterionType> list = new ArrayList<ProfileCriterionType>();

        try {
            Connection connection = dataSource.getConnection();
            PreparedStatement statement = connection.prepareStatement(SELECT_ALL);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                list.add(createProfileCriterionType(resultSet));
            }
        } catch (SQLException e) {
            LOGGER.error(e);
            throw new ProfileCriterionTypeDaoException();
        }

        return list;
    }

    public ProfileCriterionType save(ProfileCriterionType profileCriterionType) throws ProfileCriterionTypeDaoException {
        if (profileCriterionType == null) {
            return null;
        }

        if(profileCriterionType.getId() > 0){
            update(profileCriterionType);
        } else {
            long newId = create(profileCriterionType);
            profileCriterionType.setId(newId);
        }

        return profileCriterionType;
    }

    public long create(ProfileCriterionType profileCriterionType) throws ProfileCriterionTypeDaoException {
        long lastId = 0;

        if (profileCriterionType == null) {
            LOGGER.error("ProfileCriterionTypeDao.create(null) called.");
            return lastId;
        }

        if (profileCriterionType.getId() > 0){
            LOGGER.error("Tried ProfileCriterionTypeDao.create() object with existing id.");
            return profileCriterionType.getId();
        }

        try {
            Connection connection = dataSource.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(INSERT, Statement.RETURN_GENERATED_KEYS);

            preparedStatement.setString(1, profileCriterionType.getName());
            preparedStatement.setLong(2, profileCriterionType.getMaxGrade());
            preparedStatement.setBoolean(3, profileCriterionType.isDeleted());

            preparedStatement.executeUpdate();
            ResultSet resultSet = preparedStatement.getGeneratedKeys();
            if (resultSet.next()) {
                lastId = resultSet.getInt(1);
            }
        } catch (SQLException e) {
            LOGGER.error(e);
            throw new ProfileCriterionTypeDaoException();
        }
        return lastId;
    }

    public int update(ProfileCriterionType profileCriterionType) throws ProfileCriterionTypeDaoException {
        int count = 0;

        if (profileCriterionType == null) {
            LOGGER.error("ProfileCriterionTypeDao.update(null) called.");
            return count;
        }

        try {
            Connection connection = dataSource.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(UPDATE);

            preparedStatement.setString(1, profileCriterionType.getName());
            preparedStatement.setLong(2, profileCriterionType.getMaxGrade());
            preparedStatement.setBoolean(3, profileCriterionType.isDeleted());
            preparedStatement.setLong(4, profileCriterionType.getId());

            count = preparedStatement.executeUpdate();
        } catch (SQLException e) {
            LOGGER.error(e);
            throw new ProfileCriterionTypeDaoException();
        }
        return count;
    }

    public int delete(ProfileCriterionType profileCriterionType) throws ProfileCriterionTypeDaoException {
        int count = 0;

        if (profileCriterionType == null) {
            LOGGER.error("ProfileCriterionTypeDao.delete(null) called.");
            return count;
        }

        if (isUsedProfileCriterionType(profileCriterionType.getId())){
            LOGGER.error("ProfileCriterionTypeDao.delete() called. Type " + profileCriterionType.getId() + " used.");
            return count;
        }

        try {
            Connection connection = dataSource.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(DELETE);
            preparedStatement.setLong(1, profileCriterionType.getId());
            count = preparedStatement.executeUpdate();
        } catch (SQLException e) {
            LOGGER.error(e);
            throw new ProfileCriterionTypeDaoException();
        }
        return count;
    }

    private ProfileCriterionType createProfileCriterionType(ResultSet resultSet) throws SQLException {
        return  new ProfileCriterionType(
                resultSet.getLong("id"),
                resultSet.getString("name"),
                resultSet.getLong("max_grade"),
                resultSet.getBoolean("is_deleted")
        );
    }

    private boolean isUsedProfileCriterionType(long typeId) throws ProfileCriterionTypeDaoException {
        int count = 0;
        try (Connection connection = dataSource.getConnection()){
            PreparedStatement preparedStatement = connection.prepareStatement(CONTAINS_QUESTIONNAIRE_CRITERION);
            preparedStatement.setLong(1, typeId);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next())
                count = resultSet.getInt(1);

            if (count > 0)
                return true;

            preparedStatement = connection.prepareStatement(CONTAINS_QUESTIONNAIRE_CRITERION_INSTANCE);
            preparedStatement.setLong(1, typeId);
            resultSet = preparedStatement.executeQuery();
            if (resultSet.next())
                count += resultSet.getInt(1);

            resultSet.close();
            preparedStatement.close();
        } catch (SQLException e) {
            LOGGER.error(e);
            throw new ProfileCriterionTypeDaoException();
        }

        return count > 0;
    }
}
