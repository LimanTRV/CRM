package ru.innopolis.crm.profile.models.dao;

import ru.innopolis.crm.profile.models.dao.exception.ProfileCriterionDaoException;
import ru.innopolis.crm.profile.models.entity.ProfileCriterion;

import java.util.LinkedList;
import java.util.List;

/**
 * DAO для сущности ProfileCriterion
 * Created by admin on 21.04.2017.
 */
public interface ProfileCriterionDao {

    ProfileCriterion getById(long id) throws ProfileCriterionDaoException;

    List<ProfileCriterion> getAll() throws ProfileCriterionDaoException;

    ProfileCriterion save(ProfileCriterion entity) throws ProfileCriterionDaoException;

    long create(ProfileCriterion entity) throws ProfileCriterionDaoException;

    int update(ProfileCriterion entity) throws ProfileCriterionDaoException;

    int delete(ProfileCriterion entity) throws ProfileCriterionDaoException;

    List<ProfileCriterion> getByProfileId(Long profileId) throws ProfileCriterionDaoException;
}
