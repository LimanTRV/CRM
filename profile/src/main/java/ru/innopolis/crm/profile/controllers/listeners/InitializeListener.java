package ru.innopolis.crm.profile.controllers.listeners;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

/**
 * Created by admin on 22.04.2017.
 */
public class InitializeListener implements ServletContextListener {
    private static final Logger LOGGER = Logger.getLogger(InitializeListener.class);

    @Override
    public void contextInitialized(ServletContextEvent servletContextEvent) {
        LOGGER.debug("Module [crm-profile] initialized.");
    }

    @Override
    public void contextDestroyed(ServletContextEvent servletContextEvent) {

    }
}
