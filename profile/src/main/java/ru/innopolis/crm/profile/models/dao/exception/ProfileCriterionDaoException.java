package ru.innopolis.crm.profile.models.dao.exception;

/**
 * Exception для ProfileCriterionDao
 * Created by STC-05 Team [Aleksei Lysov] on 04.05.2017.
 */
public class ProfileCriterionDaoException extends Exception {
}
