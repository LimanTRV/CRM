package ru.innopolis.crm.profile.models.dao;


import ru.innopolis.crm.profile.models.dao.exception.TranscriptDaoException;
import ru.innopolis.crm.profile.models.entity.Transcript;

import java.util.Collection;
import java.util.List;

/**
 * DAO для сущности Transcript
 * Created by admin on 21.04.2017.
 */
public interface TranscriptDao {

    Transcript getById(long id) throws TranscriptDaoException;

    List<Transcript> getAll() throws TranscriptDaoException;

    Transcript save(Transcript entity) throws TranscriptDaoException;

    long create(Transcript entity) throws TranscriptDaoException;

    int update(Transcript entity) throws TranscriptDaoException;

    int delete(Transcript entity) throws TranscriptDaoException;

    List<Transcript> getByProfileCriterionTemplateId(long profileCriterionTemplateId) throws TranscriptDaoException;
}
