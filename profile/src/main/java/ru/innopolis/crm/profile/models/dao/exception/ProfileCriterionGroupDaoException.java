package ru.innopolis.crm.profile.models.dao.exception;

/**
 * Exception для ProfileCriterionGroupDao
 * Created by STC-05 Team [Aleksei Lysov] on 04.05.2017.
 */
public class ProfileCriterionGroupDaoException extends Exception {
}
