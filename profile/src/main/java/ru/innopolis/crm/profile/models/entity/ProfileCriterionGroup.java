package ru.innopolis.crm.profile.models.entity;

/**
 * Класс сущность для таблицы profile.criterion_group
 * Данная сущность представляет из себя каталог групп
 * для использования в критериях
 * Created by admin on 21.04.2017.
 */
public class ProfileCriterionGroup implements Comparable<ProfileCriterionGroup> {
    private long id;
    private String name;
    private boolean isDeleted;

    public ProfileCriterionGroup(long id, String name, boolean isDeleted){

        this.id = id;
        this.name = name;
        this.isDeleted = isDeleted;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isDeleted() {
        return isDeleted;
    }

    public void setDeleted(boolean deleted) {
        isDeleted = deleted;
    }

    @Override
    public int compareTo(ProfileCriterionGroup profileCriterionGroup) {
        return this.name.compareTo(profileCriterionGroup.getName());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ProfileCriterionGroup group = (ProfileCriterionGroup) o;

        if (id != group.id) return false;
        if (isDeleted != group.isDeleted) return false;
        return name != null ? name.equals(group.name) : group.name == null;
    }

    @Override
    public int hashCode() {
        int result = (int) (id ^ (id >>> 32));
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (isDeleted ? 1 : 0);
        return result;
    }

    @Override
    public String toString() {
        return "ProfileCriterionGroup{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", isDeleted=" + isDeleted +
                '}';
    }
}
