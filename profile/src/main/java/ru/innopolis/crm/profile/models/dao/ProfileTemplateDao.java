package ru.innopolis.crm.profile.models.dao;

import ru.innopolis.crm.profile.models.dao.exception.ProfileTemplateDaoException;
import ru.innopolis.crm.profile.models.entity.ProfileCriterion;
import ru.innopolis.crm.profile.models.entity.ProfileTemplate;

import java.util.List;

/**
 * DAO для ProfileTemplate
 * Created by admin on 21.04.2017.
 */
public interface ProfileTemplateDao {

    ProfileTemplate getById(long id) throws ProfileTemplateDaoException;

    List<ProfileTemplate> getAll() throws ProfileTemplateDaoException;

    ProfileTemplate save(ProfileTemplate entity) throws ProfileTemplateDaoException;

    long create(ProfileTemplate entity) throws ProfileTemplateDaoException;

    int update(ProfileTemplate entity) throws ProfileTemplateDaoException;

    int delete(ProfileTemplate entity) throws ProfileTemplateDaoException;

    ProfileTemplate getDefault() throws ProfileTemplateDaoException;

    /**
     * Метод сохраняет или обновляет сущность ProfileTemplate вместе со списком критериев
     * Устанавливает только одно значение default
     * @param entity сущность ProfileTemplate
     * @param criterionIds массив id критериев
     * @return id вновь созданной сущности или кол-во обновленных строк
     */
    long saveWithCriterionAndUpdateDefault(ProfileTemplate entity, String[] criterionIds) throws ProfileTemplateDaoException;
}
