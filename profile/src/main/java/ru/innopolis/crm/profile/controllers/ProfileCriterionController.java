package ru.innopolis.crm.profile.controllers;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import ru.innopolis.crm.main.sessionbean.UserBean;
import ru.innopolis.crm.main.utils.InjectLogger;
import ru.innopolis.crm.profile.models.entity.ProfileCriterionGroup;
import ru.innopolis.crm.profile.models.entity.ProfileCriterionTemplate;
import ru.innopolis.crm.profile.models.entity.ProfileCriterionType;
import ru.innopolis.crm.profile.models.entity.Transcript;
import ru.innopolis.crm.profile.services.ProfileCriterionGroupService;
import ru.innopolis.crm.profile.services.ProfileCriterionTemplateService;
import ru.innopolis.crm.profile.services.ProfileCriterionTypeService;
import ru.innopolis.crm.profile.services.TranscriptService;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Date;
import java.util.List;

/**
 * This class realize logic
 * of adding profile criterion template in web page.
 */
@Controller
public class ProfileCriterionController
{
    @InjectLogger(ProfileCriterionController.class)
    private Logger logger;




    private ProfileCriterionGroupService profileCriterionGroupService;

    private ProfileCriterionTypeService profileCriterionTypeService;

    private ProfileCriterionTemplateService profileCriterionTemplateService;

    private TranscriptService transcriptService;

    @Autowired
    private UserBean userBean;



    /* ================================= MAPPING =================================== */

    /**
     * Function build criterion web page.
     * @param model - MVC model.
     * @return - return context of the same page.
     */
    @PreAuthorize(value = "hasRole('PC_E') or hasRole('PC_V')")
    @RequestMapping(value = "/profile/criterion", method = RequestMethod.GET)
    public String getProfileCriterionView(Model model,
                       @RequestParam (value = "delete", required = false)
                                                    String deleteCriterionId)
    {

        ifDeleteCmdDeleteCriterion(deleteCriterionId);

        setCriterionGroupElementsInSelector(model);

        setCriterionTypeElementsInSelector(model);

        addAllCriteriaInView(model);

        return "profile/profile_criterion";
    }



    /**
     * Function set maximum grade for selected profile criterion type.
     * @param selectedTypeIdStr - selected profile criterion type id.
     * @param resp - response object of servlet dispatcher.
     */
    @PreAuthorize(value = "hasRole('PC_E') or hasRole('PC_V')")
    @RequestMapping(value = "/profile/criterion/maxGrade", method = RequestMethod.POST)
    public @ResponseBody void setMaximumGradeInViewAfterTypeSelected(
            @RequestParam (value = "sendSelectedType", required = false)  String selectedTypeIdStr,
            HttpServletResponse resp)
    {
        ifProfileTypeSelectedSetMaxGrade(selectedTypeIdStr, resp);
    }




    /**
     * Function set maximum grade for selected profile criterion type.
     * @param profileCriterionTemplate - {@link ProfileCriterionTemplate} object
     *                                 retrieved through json from web page.
     * @param resp - response object of servlet dispatcher.
     */
    @PreAuthorize(value = "hasRole('PC_E')")
    @RequestMapping(value = "/profile/criterion/addCriterion", method = RequestMethod.POST)
    public @ResponseBody void addProfileCriterionTemplate(
            @RequestBody ProfileCriterionTemplate profileCriterionTemplate,
            HttpServletResponse resp
            )
    {

        long userId = userBean.getUserId();

        profileCriterionTemplate.setCreateUserId(userId);
        profileCriterionTemplate.setEditUserId(userId);
           /*===================*/

        Date editDate = new Date();
        profileCriterionTemplate.setEditDate(editDate);
        long thisCriterionId =
                profileCriterionTemplateService.create(profileCriterionTemplate);




        /* Send id of new {@link ProfileCriterionTemplate} object. */
        String jsonText = "{ \"criterionIdVal\" : "+ thisCriterionId + " }";
        try {
            resp.getWriter().append(jsonText).flush();
        } catch (IOException e) {
            logger.error(e);
        }
    }


    /**
     * Function add transcripts retrieved from web page through json.
     * @param transcriptArr - new transcript added for new
     * {@link ProfileCriterionTemplate} object.
     * @param resp - response object of servlet dispatcher.
     */
    @PreAuthorize(value = "hasRole('PC_E')")
    @RequestMapping(value = "/profile/criterion/addTranscripts", method = RequestMethod.POST)
    public @ResponseBody void addTranscripts(
            @RequestBody Transcript[] transcriptArr,
            HttpServletResponse resp
    )
    {
        for (Transcript transcript:transcriptArr
             ) {
            transcriptService.create(transcript);
        }


        String jsonText = "{ \"success\" : \"success\" }";
        try {
            resp.getWriter().append(jsonText).flush();
        } catch (IOException e) {
            logger.error(e);
        }

    }





    /*==================================================================================*/


    /**
     * Function delete selected {@link ProfileCriterionTemplate} object from
     * data base.
     * @param deleteCriterionId - id of {@link ProfileCriterionTemplate} object,
     *                          that need to delete.
     */
    private void ifDeleteCmdDeleteCriterion(String deleteCriterionId)
    {
        if (deleteCriterionId != null)
        {
            long criterionIdForDelete = Long.parseLong(deleteCriterionId);

            List<Transcript> transcripts = transcriptService
                    .getByProfileCriterionTemplateId(criterionIdForDelete);

            transcripts.forEach(tr->{
                transcriptService.delete(tr);
            });

            ProfileCriterionTemplate profileCriterionTemplate =
                    profileCriterionTemplateService.getById(criterionIdForDelete);
            profileCriterionTemplateService.delete(profileCriterionTemplate);
        }
    }



    /**
     * Function check if selected profile type. If it selected
     * than set and show in view max grade for this type of profile.
     */
    private void ifProfileTypeSelectedSetMaxGrade(String selectedCriterionTypeId, HttpServletResponse resp)
    {
        String jsonText = null;
        if (selectedCriterionTypeId != null)
        {
            int value = Integer.parseInt(selectedCriterionTypeId);
            ProfileCriterionType profileCriterionType = profileCriterionTypeService.getById(value);
            String maxGrade = Long.toString(profileCriterionType.getMaxGrade()) ;
            jsonText = "{ \"maxGrade\" : \""+ maxGrade + "\" }";

            try {
                resp.getWriter().append(jsonText).flush();
            } catch (IOException e) {
                logger.error(e);
            }
        }
    }


    /**
     * Function add all exist criteria in web page.
     * @param model - model.
     */
    private void addAllCriteriaInView(Model model)
    {
        List<ProfileCriterionTemplate> profileCriterionTemplates
                = profileCriterionTemplateService.getAll();
        model.addAttribute("profileCriterionTemplates", profileCriterionTemplates);
    }


    /**
     * Function set criterion groups in selector (view).
     * @param model - model.
     */
    private void setCriterionGroupElementsInSelector(Model model)
    {
        List<ProfileCriterionGroup> profileCriterionGroups =
                profileCriterionGroupService.getAll();
        model.addAttribute("profileCriterionGroups", profileCriterionGroups);
    }


    /**
     * Function set criterion types in selector (view).
     * @param model - model.
     * @return - new servlet request with criterion types.
     */
    private void setCriterionTypeElementsInSelector(Model model)
    {
        List<ProfileCriterionType> profileCriterionTypes =
                profileCriterionTypeService.getAll();

        model.addAttribute("profileCriterionTypes", profileCriterionTypes);
    }




    @Autowired
    public void setProfileCriterionGroupService(ProfileCriterionGroupService profileCriterionGroupService) {
        this.profileCriterionGroupService = profileCriterionGroupService;
    }

    @Autowired
    public void setProfileCriterionTypeService(ProfileCriterionTypeService profileCriterionTypeService) {
        this.profileCriterionTypeService = profileCriterionTypeService;
    }

    @Autowired
    public void setProfileCriterionTemplateService(ProfileCriterionTemplateService profileCriterionTemplateService) {
        this.profileCriterionTemplateService = profileCriterionTemplateService;
    }

    @Autowired
    public void setTranscriptService(TranscriptService transcriptService) {
        this.transcriptService = transcriptService;
    }
}
