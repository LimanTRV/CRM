package ru.innopolis.crm.profile.services;

import javafx.collections.transformation.SortedList;
import ru.innopolis.crm.profile.models.entity.ProfileCriterion;
import ru.innopolis.crm.profile.models.entity.ProfileCriterionGroup;
import ru.innopolis.crm.profile.models.entity.ProfileCriterionType;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.TreeSet;

/**
 * Сервис для работы с сущностью ProfileCriterion
 * Created by admin on 23.04.2017.
 */
public interface ProfileCriterionService {
    HashMap<ProfileCriterionType,TreeSet<ProfileCriterionGroup>> getCriterionTypesAndGroupsById(Long profileId);

    List<ProfileCriterion> getCriteriaWithTranscriptsById(Long profileId);

    List<ProfileCriterion> getCriteriaByProfileId(Long profileId);

    void update(ProfileCriterion profileCriterion);
}
