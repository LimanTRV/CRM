package ru.innopolis.crm.profile.util;

import javax.servlet.http.HttpServletRequest;
import java.nio.charset.StandardCharsets;

/**
 * Класс - помощник для работы с модулем Анкета
 * Created by STC-05 Team [Aleksei Lysov] on 27.04.2017.
 */
public class ProfileHelper {
    /**
     * Проверяем что id корректный
     *
     * @param param строка с id
     * @return boolean
     */
    public static boolean isCorrectId(String param) {
        if ((param != null) && (param.matches("\\d+")))
            return true;
        else
            return false;
    }

    /**
     * Полчаем ранее установленные значения в сессии success или error,
     * удаляем их из сессии и передаем аттрибутом
     *
     * @param req HttpServletRequest
     */
    public static void checkSuccessOrErrorMessage(HttpServletRequest req) {
        if (req.getSession().getAttribute("success") != null) {
            req.setAttribute("success", req.getSession().getAttribute("success"));
            req.getSession().removeAttribute("success");
        }

        if (req.getSession().getAttribute("error") != null) {
            req.setAttribute("error", req.getSession().getAttribute("error"));
            req.getSession().removeAttribute("error");
        }
    }

    /**
     * Запись сообщения в сессию после сохранения
     *
     * @param req       объект HttpServletRequest
     * @param isSuccess тип сообщения
     */
    public static void setSessionAfterSaveData(HttpServletRequest req, boolean isSuccess) {
        if (isSuccess)
            req.getSession().setAttribute("success", "Данные успешно сохранены");
        else
            req.getSession().setAttribute("error", "При сохранении данных произошла ошибка");
    }

    /**
     * Запись сообщения в сессию после сохранения
     *
     * @param req       объект HttpServletRequest
     * @param isSuccess тип сообщения
     */
    public static void setSessionAfterDeleteData(HttpServletRequest req, boolean isSuccess) {
        if (isSuccess)
            req.getSession().setAttribute("success", "Данные успешно удалены");
        else
            req.getSession().setAttribute("error", "Ошибка при удалении данных. Возможно данные отсутствуют или используются в другом месте");
    }

    /**
     * Метод конвертации строки из ISO_8859_1 в UTF_8
     * @param string строка в формате ISO_8859_1
     * @return строка в формате UTF_8
     */
    public static String convertIso88591ToUtf8(String string){
        if (string == null)
            return string;

        byte[] bytes = string.getBytes(StandardCharsets.ISO_8859_1);
        return new String(bytes, StandardCharsets.UTF_8);
    }
}
