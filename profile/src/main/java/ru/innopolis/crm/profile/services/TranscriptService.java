package ru.innopolis.crm.profile.services;

import ru.innopolis.crm.profile.models.entity.ProfileCriterionGroup;
import ru.innopolis.crm.profile.models.entity.Transcript;

import java.util.List;

/**
 * Created by Di on 29.04.2017.
 */
public interface TranscriptService
{
    long create(Transcript entity);

    Transcript getById(long id);

    List<Transcript> getByProfileCriterionTemplateId(long criterionTemplateId);

    int update(Transcript transcript);

    int delete(Transcript transcript);
}
