package ru.innopolis.crm.profile.models.dao.exception;

/**
 * Exception для TranscriptDao
 * Created by STC-05 Team [Aleksei Lysov] on 04.05.2017.
 */
public class TranscriptDaoException extends Exception {
}
