package ru.innopolis.crm.profile.models.dao.exception;

/**
 * Exception для ProfileDao
 * Created by STC-05 Team [Aleksei Lysov] on 04.05.2017.
 */
public class ProfileDaoException extends Exception {
}
