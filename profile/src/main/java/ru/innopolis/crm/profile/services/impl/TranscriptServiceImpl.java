package ru.innopolis.crm.profile.services.impl;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.innopolis.crm.profile.models.dao.TranscriptDao;
import ru.innopolis.crm.profile.models.dao.exception.TranscriptDaoException;
import ru.innopolis.crm.profile.models.entity.Transcript;
import ru.innopolis.crm.profile.services.TranscriptService;
import ru.innopolis.crm.profile.services.exception.TranscriptServiceException;

import java.util.List;

/**
 * Created by Di on 29.04.2017.
 */
@Service
public class TranscriptServiceImpl implements TranscriptService
{
    private static final Logger LOGGER = Logger.getLogger(TranscriptServiceImpl.class);
    private TranscriptDao transcriptDao;

    @Autowired
    public void setTranscriptDao(TranscriptDao transcriptDao) {
        this.transcriptDao = transcriptDao;
    }

    @Override
    public long create(Transcript entity) {
        try {
            return transcriptDao.create(entity);
        } catch (TranscriptDaoException e) {
            LOGGER.error(e);
            throw new TranscriptServiceException();
        }
    }

    @Override
    public Transcript getById(long id) {
        try {
            return transcriptDao.getById(id);
        } catch (TranscriptDaoException e) {
            LOGGER.error(e);
            throw new TranscriptServiceException();
        }
    }

    @Override
    public List<Transcript> getByProfileCriterionTemplateId(long criterionTemplateId) {
        try {
            return transcriptDao.getByProfileCriterionTemplateId(criterionTemplateId);
        } catch (TranscriptDaoException e) {
            LOGGER.error(e);
            throw new TranscriptServiceException();
        }
    }

    @Override
    public int update(Transcript transcript) {
        try {
            return transcriptDao.update(transcript);
        } catch (TranscriptDaoException e) {
            LOGGER.error(e);
            throw new TranscriptServiceException();
        }
    }

    @Override
    public int delete(Transcript transcript) {
        try {
            return transcriptDao.delete(transcript);
        } catch (TranscriptDaoException e) {
            LOGGER.error(e);
            throw new TranscriptServiceException();
        }
    }
}
