package ru.innopolis.crm.profile.services;

import ru.innopolis.crm.profile.models.entity.ProfileCriterionType;

import java.util.List;

/**
 * Сервис для работы с сущностью ProfileCriterionType
 * Created by STC-05 Team [Aleksei Lysov] on 27.04.2017.
 */
public interface ProfileCriterionTypeService {
    /**
     * Метод для получения сущности по id
     *
     * @param id
     * @return объект ProfileCriterionType
     */
    ProfileCriterionType getById(long id);

    /**
     * Метод для получения всех сущностей из БД
     *
     * @return список ProfileCriterionType
     */
    List<ProfileCriterionType> getAll();

    /**
     * Метод сохраняет или обновляет сущность в БД
     *
     * @param entity
     * @return ProfileCriterionType
     */
    ProfileCriterionType save(ProfileCriterionType entity);

    /**
     * Метод дл записи сущности в БД
     *
     * @param entity
     * @return id сохраненой сущности
     */
    long create(ProfileCriterionType entity);

    /**
     * Метод для обновдения сущностив БД
     *
     * @param entity
     * @return количество обновленных строк в БД
     */
    int update(ProfileCriterionType entity);

    /**
     * Метод удаления сущности из БД
     *
     * @param entity
     * @return количество обновленных строк в БД
     */
    int delete(ProfileCriterionType entity);
}
