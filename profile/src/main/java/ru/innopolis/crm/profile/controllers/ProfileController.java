package ru.innopolis.crm.profile.controllers;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import ru.innopolis.crm.main.models.entity.User;
import ru.innopolis.crm.main.services.StudentService;
import ru.innopolis.crm.main.services.UserService;
import ru.innopolis.crm.main.sessionbean.UserBean;
import ru.innopolis.crm.profile.models.entity.*;
import ru.innopolis.crm.profile.services.ProfileCriterionService;
import ru.innopolis.crm.profile.services.ProfileService;
import ru.innopolis.crm.profile.services.ProfileTemplateService;
import ru.innopolis.crm.profile.util.ProfileHelper;

import javax.servlet.http.HttpServletRequest;
import java.util.*;

/**
 * Created by admin on 23.04.2017.
 */
@Controller
public class ProfileController{
    private static final Logger LOGGER = Logger.getLogger(ProfileController.class);
    private ProfileService profileService;
    private ProfileCriterionService profileCriterionService;
    private ProfileTemplateService profileTemplateService;
    private StudentService studentService;
    private UserService userService;
    private UserBean userBean;

    @Autowired
    public void setProfileService(ProfileService profileService) {
        this.profileService = profileService;
    }

    @Autowired
    public void setProfileCriterionService(ProfileCriterionService profileCriterionService) {
        this.profileCriterionService = profileCriterionService;
    }

    @Autowired
    public void setProfileTemplateService(ProfileTemplateService profileTemplateService) {
        this.profileTemplateService = profileTemplateService;
    }

    @Autowired
    public void setStudentService(StudentService studentService) {
        this.studentService = studentService;
    }

    @Autowired
    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    @Autowired
    public void setUserBean(UserBean userBean) {
        this.userBean = userBean;
    }

    @PreAuthorize(value = "hasRole('P_E') or hasRole('P_V')")
    @RequestMapping(value = "/profile", method = RequestMethod.GET)
    public ModelAndView doGet(
            @RequestParam(value = "studentId", required = false) String studentIdParameter){

        ModelAndView model = new ModelAndView();

        long studentId = 1L;
        if (ProfileHelper.isCorrectId(studentIdParameter))
            studentId = Long.parseLong(studentIdParameter);

        Profile profile = profileService.getByStudentId(studentId);
        if (profile == null) {
            profile = profileTemplateService.createFromDefault(studentId);
        }

        User userStudent = studentService.findById(studentId).getUser();
        String studentName = userStudent.getFirstName() + " " + userStudent.getLastName();

        User userTeacher = userService.getById(profile.getEditUserId());
        String teacherName = userTeacher.getFirstName() + " " + userTeacher.getLastName();

        TreeMap<ProfileCriterionType, TreeSet<ProfileCriterionGroup>> profileCriterionTypes =
                new TreeMap<ProfileCriterionType, TreeSet<ProfileCriterionGroup>>(
                        profileCriterionService.getCriterionTypesAndGroupsById(profile.getId()));

        TreeSet<ProfileCriterion> profileCriteria =
                new TreeSet<ProfileCriterion>(
                        profileCriterionService.getCriteriaWithTranscriptsById(profile.getId()));

        model.addObject("templates", profileTemplateService.getAll());
        model.addObject("studentName", studentName);
        model.addObject("teacherName", teacherName);
        model.addObject("studentId", studentId);
        model.addObject("profileName", profile.getName());
        model.addObject("isFinished", profile.isFinished());
        model.addObject("criterionGroupsToTypesMap", profileCriterionTypes);
        model.addObject("profileCriteria", profileCriteria);

        model.setViewName("profile/profile");
        return model;
    }

    @PreAuthorize(value = "hasRole('P_E')")
    @RequestMapping(value = "/profile/add", method = RequestMethod.GET)
    public String createProfile(
            @RequestParam(value = "studentId") Long studentId,
            @RequestParam(value = "group", required = false) String group,
            @RequestParam(value = "template") Long templateId){

        ModelAndView model = new ModelAndView();

        Profile profile = profileService.getByStudentId(studentId);
        if (profile != null) {
            profileService.delete(profile);
        }

        profileTemplateService.createFromTemplate(templateId, studentId);

        return "redirect:/profile?studentId=" + studentId + "&group=" + group;
    }

    @PreAuthorize(value = "hasRole('P_E')")
    @RequestMapping(value = "/profile", method = RequestMethod.POST)
    public String doPost(
            @RequestParam(value = "studentId", required = false) String studentIdParameter,
            @RequestParam(value = "is_finished", required = false) String postIsFinished,
            @RequestParam(value = "group", required = false) String group,
            HttpServletRequest req
    ){

        long studentId = 1L;
        if (ProfileHelper.isCorrectId(studentIdParameter))
            studentId = Long.parseLong(studentIdParameter);

        Profile profile = profileService.getByStudentId(studentId);
        boolean isFinished = profile.isFinished();
        boolean isFinishedOnPage = false;

        if(postIsFinished != null)
            isFinishedOnPage = true;

        long userId = userBean.getUserId();

        profile.setEditUserId(userId);

        if(isFinishedOnPage != isFinished){
            profile.setFinished(isFinishedOnPage);
            profileService.update(profile);
        }


        List<String> paramsList = Collections.list(req.getParameterNames());
        HashMap<Long, Long> criterionValues = new HashMap<Long, Long>();

        for (String parameter :
                paramsList) {
            if(parameter.matches("cr_id_.*")){
                Long criterionId = Long.parseLong(parameter.substring(6));
                Long criterionValue = Long.parseLong(req.getParameter(parameter));

                criterionValues.put(criterionId, criterionValue);
            }
        }

        List<ProfileCriterion> profileCriteria = profileCriterionService.getCriteriaByProfileId(profile.getId());
        for (ProfileCriterion profileCriterion :
                profileCriteria) {
            long grade = profileCriterion.getGrade();

            long gradeOnPage = 0;
            if (criterionValues.containsKey(profileCriterion.getId())){
                gradeOnPage = criterionValues.get(profileCriterion.getId());
            }

            if (grade != gradeOnPage){
                profileCriterion.setGrade(gradeOnPage);
                profileCriterion.setEditDate(new Date());
                profileCriterionService.update(profileCriterion);
            }
        }

        return "redirect:/profile?studentId=" + studentId + "&group=" + group;
    }
}
