package ru.innopolis.crm.profile.models.dao.impl;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import ru.innopolis.crm.profile.models.dao.ProfileCriterionGroupDao;
import ru.innopolis.crm.profile.models.dao.ProfileCriterionTemplateDao;
import ru.innopolis.crm.profile.models.dao.ProfileCriterionTypeDao;
import ru.innopolis.crm.profile.models.dao.exception.ProfileCriterionGroupDaoException;
import ru.innopolis.crm.profile.models.dao.exception.ProfileCriterionTemplateDaoException;
import ru.innopolis.crm.profile.models.dao.exception.ProfileCriterionTypeDaoException;
import ru.innopolis.crm.profile.models.entity.ProfileCriterionTemplate;
import ru.innopolis.crm.profile.services.impl.ProfileCriterionGroupServiceImpl;
import ru.innopolis.crm.profile.services.impl.ProfileCriterionTypeServiceImpl;

import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Имплементация для ProfileCriterionTemplateDao
 * Created by admin on 21.04.2017.
 */
@Repository
public class ProfileCriterionTemplateDaoImpl implements ProfileCriterionTemplateDao {

    private static final Logger LOGGER = Logger.getLogger(ProfileCriterionTemplateDaoImpl.class);

    private static final String SELECT_ALL = "SELECT * FROM profile.questionnaire_criterion WHERE NOT is_deleted";

    private static final String SELECT_BY_TEMPLATE = "SELECT * " +
            "  FROM profile.questionnaire_criterion qc " +
            "       INNER JOIN profile.questionnaire_criterion_to_questionnaire qctq on qc.id = qctq.questionnaire_criterion_id " +
            "WHERE NOT qc.is_deleted and NOT qctq.is_deleted and qctq.questionnaire_id = ?";

    private static final String SELECT_WITHOUT_TEMPLATE = "SELECT *  FROM profile.questionnaire_criterion qc WHERE qc.id" +
            " NOT IN(SELECT DISTINCT qctq.questionnaire_criterion_id FROM profile.questionnaire_criterion_to_questionnaire qctq where NOT qctq.is_deleted AND qctq.questionnaire_id = ?)" +
            " AND NOT qc.is_deleted";

    private static final String INSERT = "INSERT INTO profile.questionnaire_criterion " +
            "(name, description, criterion_type_id, criterion_group_id, crm_user_id_created, " +
            "crm_user_id_edited, edit_date, is_deleted) VALUES (?, ?, ?, ?, ?, ?, ?, ?)";

    private static final String UPDATE = "UPDATE profile.questionnaire_criterion " +
            "SET name = ?, description = ?, criterion_type_id = ?, criterion_group_id = ?, " +
            "crm_user_id_created = ?, crm_user_id_edited = ?, edit_date = ?, is_deleted = ? " +
            "WHERE id = ?;";

    private static final String DELETE = "UPDATE profile.questionnaire_criterion " +
            "SET is_deleted = TRUE WHERE id = ?;";

    private DataSource dataSource;
    private ProfileCriterionGroupDao profileCriterionGroupDao;
    private ProfileCriterionTypeDao profileCriterionTypeDao;

    @Autowired
    public void setDataSource(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Autowired
    public void setProfileCriterionGroupDao(ProfileCriterionGroupDao profileCriterionGroupDao) {
        this.profileCriterionGroupDao = profileCriterionGroupDao;
    }

    @Autowired
    public void setProfileCriterionTypeDao(ProfileCriterionTypeDao profileCriterionTypeDao) {
        this.profileCriterionTypeDao = profileCriterionTypeDao;
    }

    public ProfileCriterionTemplate getById(long id) throws ProfileCriterionTemplateDaoException {
        ProfileCriterionTemplate profileCriterionTemplate = null;

        try {
            Connection connection = dataSource.getConnection();
            PreparedStatement statement = connection.prepareStatement(SELECT_ALL + " AND id = ?");
            statement.setLong(1, id);
            ResultSet resultSet = statement.executeQuery();
            if(resultSet.next()){
                profileCriterionTemplate = createProfileCriterionTemplate(resultSet);
            }
        } catch (SQLException e) {
            LOGGER.error(e);
            throw new ProfileCriterionTemplateDaoException();
        }

        return profileCriterionTemplate;
    }

    public List<ProfileCriterionTemplate> getAll() throws ProfileCriterionTemplateDaoException {
        List<ProfileCriterionTemplate> list = new ArrayList<ProfileCriterionTemplate>();

        try {
            Connection connection = dataSource.getConnection();
            PreparedStatement statement = connection.prepareStatement(SELECT_ALL);

            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                list.add(createProfileCriterionTemplate(resultSet));
            }
        } catch (SQLException e) {
            LOGGER.error(e);
            throw new ProfileCriterionTemplateDaoException();
        }

        return list;
    }

    public ProfileCriterionTemplate save(ProfileCriterionTemplate profileCriterionTemplate) throws ProfileCriterionTemplateDaoException {
        if (profileCriterionTemplate == null) {
            return null;
        }

        if(profileCriterionTemplate.getId() > 0){
            update(profileCriterionTemplate);
        } else {
            long newId = create(profileCriterionTemplate);
            profileCriterionTemplate.setId(newId);
        }

        return profileCriterionTemplate;
    }

    public long create(ProfileCriterionTemplate profileCriterionTemplate) throws ProfileCriterionTemplateDaoException {
        long lastId = 0;

        if (profileCriterionTemplate == null) {
            LOGGER.error("ProfileCriterionTemplateDao.create(null) called.");
            return lastId;
        }

        if (profileCriterionTemplate.getId() > 0){
            LOGGER.error("Tried ProfileCriterionTemplateDao.create() object with existing id.");
            return profileCriterionTemplate.getId();
        }

        try {
            Connection connection = dataSource.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(INSERT, Statement.RETURN_GENERATED_KEYS);

            preparedStatement.setString(1, profileCriterionTemplate.getName());
            preparedStatement.setString(2, profileCriterionTemplate.getDescription());
            preparedStatement.setLong(3, profileCriterionTemplate.getProfileCriterionTypeId());
            preparedStatement.setLong(4, profileCriterionTemplate.getProfileCriterionGroupId());
            preparedStatement.setLong(5, profileCriterionTemplate.getCreateUserId());
            preparedStatement.setLong(6, profileCriterionTemplate.getEditUserId());
            preparedStatement.setTimestamp(7, new java.sql.Timestamp(profileCriterionTemplate.getEditDate().getTime()));
            preparedStatement.setBoolean(8, profileCriterionTemplate.isDeleted());

            preparedStatement.executeUpdate();
            ResultSet resultSet = preparedStatement.getGeneratedKeys();
            if (resultSet.next()) {
                lastId = resultSet.getInt(1);
            }
        } catch (SQLException e) {
            LOGGER.error(e);
            throw new ProfileCriterionTemplateDaoException();
        }
        return lastId;
    }

    public int update(ProfileCriterionTemplate profileCriterionTemplate) throws ProfileCriterionTemplateDaoException {
        int count = 0;

        if (profileCriterionTemplate == null) {
            LOGGER.error("ProfileCriterionTemplateDao.update(null) called.");
            return count;
        }

        try {
            Connection connection = dataSource.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(UPDATE);

            preparedStatement.setString(1, profileCriterionTemplate.getName());
            preparedStatement.setString(2, profileCriterionTemplate.getDescription());
            preparedStatement.setLong(3, profileCriterionTemplate.getProfileCriterionTypeId());
            preparedStatement.setLong(4, profileCriterionTemplate.getProfileCriterionGroupId());
            preparedStatement.setLong(5, profileCriterionTemplate.getCreateUserId());
            preparedStatement.setLong(6, profileCriterionTemplate.getEditUserId());
            preparedStatement.setTimestamp(7, new java.sql.Timestamp(profileCriterionTemplate.getEditDate().getTime()));
            preparedStatement.setBoolean(8, profileCriterionTemplate.isDeleted());
            preparedStatement.setLong(9, profileCriterionTemplate.getId());

            count = preparedStatement.executeUpdate();
        } catch (SQLException e) {
            LOGGER.error(e);
            throw new ProfileCriterionTemplateDaoException();
        }
        return count;
    }

    public int delete(ProfileCriterionTemplate profileCriterionTemplate) throws ProfileCriterionTemplateDaoException {
        int count = 0;

        if (profileCriterionTemplate == null) {
            LOGGER.error("ProfileCriterionTemplateDao.delete(null) called.");
            return count;
        }

        try {
            Connection connection = dataSource.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(DELETE);
            preparedStatement.setLong(1, profileCriterionTemplate.getId());
            count = preparedStatement.executeUpdate();
        } catch (SQLException e) {
            LOGGER.error(e);
            throw new ProfileCriterionTemplateDaoException();
        }
        return count;
    }

    @Override
    public List<ProfileCriterionTemplate> getByProfileTemplateId(long profileTemplateId) throws ProfileCriterionTemplateDaoException {
        return getListProfileCriterionTemplate(SELECT_BY_TEMPLATE, profileTemplateId);
    }

    @Override
    public List<ProfileCriterionTemplate> getListWithoutProfileTemplateId(long profileTemplateId) throws ProfileCriterionTemplateDaoException {
        return getListProfileCriterionTemplate(SELECT_WITHOUT_TEMPLATE, profileTemplateId);
    }

    private List<ProfileCriterionTemplate> getListProfileCriterionTemplate(String sql, long profileTemplateId) throws ProfileCriterionTemplateDaoException {
        List<ProfileCriterionTemplate> list = new ArrayList<>();

        try {
            Connection connection = dataSource.getConnection();
            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setLong(1, profileTemplateId);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                list.add(createProfileCriterionTemplate(resultSet));
            }
        } catch (SQLException e) {
            LOGGER.error(e);
            throw new ProfileCriterionTemplateDaoException();
        }

        return list;
    }

    private ProfileCriterionTemplate createProfileCriterionTemplate(ResultSet resultSet) throws SQLException{
        ProfileCriterionTemplate template = new ProfileCriterionTemplate(
                resultSet.getLong("id"),
                resultSet.getString("name"),
                resultSet.getString("description"),
                resultSet.getLong("criterion_type_id"),
                resultSet.getLong("criterion_group_id"),
                resultSet.getLong("crm_user_id_created"),
                resultSet.getLong("crm_user_id_edited"),
                resultSet.getDate("edit_date"),
                resultSet.getBoolean("is_deleted")
        );

        try {
            template.setProfileCriterionType(profileCriterionTypeDao.getById(template.getProfileCriterionTypeId()));
            template.setProfileCriterionGroup(profileCriterionGroupDao.getById(template.getProfileCriterionGroupId()));
        } catch (Exception e) {
            LOGGER.error(e);
            throw new SQLException(e);
        }
        return template;
    }
}
