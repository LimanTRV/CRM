package ru.innopolis.crm.profile.models.dao;

import ru.innopolis.crm.profile.models.dao.exception.ProfileCriterionTemplateDaoException;
import ru.innopolis.crm.profile.models.entity.ProfileCriterionTemplate;

import java.util.List;

/**
 * DAO для ProfileCriterionTemplate
 * Created by admin on 21.04.2017.
 */
public interface ProfileCriterionTemplateDao {

    ProfileCriterionTemplate getById(long id) throws ProfileCriterionTemplateDaoException;

    List<ProfileCriterionTemplate> getAll() throws ProfileCriterionTemplateDaoException;

    ProfileCriterionTemplate save(ProfileCriterionTemplate entity) throws ProfileCriterionTemplateDaoException;

    long create(ProfileCriterionTemplate entity) throws ProfileCriterionTemplateDaoException;

    int update(ProfileCriterionTemplate entity) throws ProfileCriterionTemplateDaoException;

    int delete(ProfileCriterionTemplate entity) throws ProfileCriterionTemplateDaoException;

    List<ProfileCriterionTemplate> getByProfileTemplateId(long profileTemplateId) throws ProfileCriterionTemplateDaoException;

    /**
     * Метод возвращает список критериев, которые отсутствуют в конкретном шаблоне
     *
     * @param profileTemplateId id шаблона
     * @return список критериев для конкретного шаблона
     */
    List<ProfileCriterionTemplate> getListWithoutProfileTemplateId(long profileTemplateId) throws ProfileCriterionTemplateDaoException;
}
