package ru.innopolis.crm.profile.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import ru.innopolis.crm.profile.models.entity.ProfileCriterionTemplate;
import ru.innopolis.crm.profile.models.entity.ProfileTemplate;
import ru.innopolis.crm.profile.services.ProfileCriterionTemplateService;
import ru.innopolis.crm.profile.services.ProfileTemplateService;
import ru.innopolis.crm.profile.util.ProfileHelper;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

/**
 * Контроллер для обработки страницы по адресу /profile/template
 * Created by STC-05 Team [Aleksei Lysov] on 27.04.2017.
 */
@Controller
@RequestMapping(value = "/profile/template")
public class ProfileTemplateController{

    private ProfileTemplateService templateService;
    private ProfileCriterionTemplateService criterionService;

    @Autowired
    public void setTemplateService(ProfileTemplateService templateService) {
        this.templateService = templateService;
    }

    @Autowired
    public void setCriterionService(ProfileCriterionTemplateService criterionService) {
        this.criterionService = criterionService;
    }

    @PreAuthorize(value = "hasRole('PC_V')")
    @RequestMapping(method = RequestMethod.GET)
    public ModelAndView doGet(
            @RequestParam(value = "edit", required = false) String editId,
            @RequestParam(value = "delete", required = false) String deleteId,
            @RequestParam(value = "group", required = false) String groupId,
            HttpServletRequest req) {

        ModelAndView model = new ModelAndView();
        ProfileHelper.checkSuccessOrErrorMessage(req);

        if (ProfileHelper.isCorrectId(deleteId)) {
            boolean isDelete = deleteOperation(Long.valueOf(deleteId));
            ProfileHelper.setSessionAfterDeleteData(req, isDelete);
            model.setViewName("redirect:/profile/template?group=" + groupId);
            return model;
        }

        if (ProfileHelper.isCorrectId(editId)) {
            initEditOperation(model, Long.valueOf(editId));
        } else {
            initOperation(model);
        }

        List<ProfileTemplate> templates = templateService.getAll();
        model.addObject("templates", templates);

        model.setViewName("profile/profile_template");
        return model;
    }

    @PreAuthorize(value = "hasRole('PC_V')")
    @RequestMapping(method = RequestMethod.POST)
    public String doPost(
            @RequestParam(value = "name") String name,
            @RequestParam(value = "templateId", required = false) String templateId,
            @RequestParam(value = "default", required = false) String defaultOn,
            @RequestParam(value = "group", required = false) String groupId,
            @RequestParam(value = "ids[]", required = false) String[] ids,
            HttpServletRequest req
    ) {

        ProfileTemplate template = null;

        if (ProfileHelper.isCorrectId(templateId))
            template = templateService.getById(Long.valueOf(templateId));

        boolean isSave = saveOperation(template, name, defaultOn, ids);
        ProfileHelper.setSessionAfterSaveData(req, isSave);

        return "redirect:/profile/template?group=" + groupId;
    }

    private void initEditOperation(ModelAndView model, long editId) {
        ProfileTemplate template = templateService.getById(editId);
        if (template != null) {
            model.addObject("editTemplate", template);
            if (template.isDefault())
                model.addObject("defaultChecked", "checked='checked'");
            model.addObject("criterionProfileList", criterionService.getByProfileTemplateId(editId));
            model.addObject("criterionList", criterionService.getListWithoutProfileTemplateId(editId));
        } else {
            initOperation(model);
        }

    }

    private void initOperation(ModelAndView model) {
        model.addObject("criterionList", criterionService.getAll());
        model.addObject("criterionProfileList", new ArrayList<ProfileCriterionTemplate>());
    }

    private boolean deleteOperation(Long deleteId) {
        return templateService.delete(templateService.getById(deleteId)) > 0;
    }

    /**
     * Метод сохраняет или обновляет шаблон.
     * При этом default может быть только один шаблон
     *
     * @param template  Шаблон
     * @param name      Имя
     * @param defaultOn Дефолт
     * @param ids       Массив id критериев
     * @return результат операции
     */
    private boolean saveOperation(ProfileTemplate template, String name, String defaultOn, String[] ids) {
        long countUpdate = 0;
        boolean isDefault = "on".equals(defaultOn);

        if (template == null) {
            template = new ProfileTemplate(0, name, isDefault, false);
        } else {
            template.setName(name);
            template.setDefault(isDefault);
        }

        countUpdate = templateService.saveWithCriterionAndUpdateDefault(template, ids);
        return countUpdate > 0;
    }
}
