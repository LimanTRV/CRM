package ru.innopolis.crm.profile.models.entity;

/**
 * Класс сущность для таблицы profile.questionnaire
 * Данная сущность представляет из себя шаблон анкеты
 * Связь с критериями осуществляется через ProfileCriterionTemplate
 * Created by admin on 21.04.2017.
 */
public class ProfileTemplate {
    private long id;
    private String name;
    private boolean isDefault;
    private boolean isDeleted;

    public ProfileTemplate(long id, String name, boolean isDefault, boolean isDeleted){

        this.id = id;
        this.name = name;
        this.isDefault = isDefault;
        this.isDeleted = isDeleted;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isDefault() {
        return isDefault;
    }

    public void setDefault(boolean aDefault) {
        isDefault = aDefault;
    }

    public boolean isDeleted() {
        return isDeleted;
    }

    public void setDeleted(boolean deleted) {
        isDeleted = deleted;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ProfileTemplate template = (ProfileTemplate) o;

        if (id != template.id) return false;
        if (isDefault != template.isDefault) return false;
        if (isDeleted != template.isDeleted) return false;
        return name != null ? name.equals(template.name) : template.name == null;
    }

    @Override
    public int hashCode() {
        int result = (int) (id ^ (id >>> 32));
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (isDefault ? 1 : 0);
        result = 31 * result + (isDeleted ? 1 : 0);
        return result;
    }

    @Override
    public String toString() {
        return "ProfileTemplate{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", isDefault=" + isDefault +
                ", isDeleted=" + isDeleted +
                '}';
    }
}
