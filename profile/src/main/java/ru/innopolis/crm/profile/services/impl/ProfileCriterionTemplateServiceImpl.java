package ru.innopolis.crm.profile.services.impl;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.innopolis.crm.profile.models.dao.ProfileCriterionTemplateDao;
import ru.innopolis.crm.profile.models.dao.exception.ProfileCriterionTemplateDaoException;
import ru.innopolis.crm.profile.models.entity.ProfileCriterionTemplate;
import ru.innopolis.crm.profile.services.ProfileCriterionTemplateService;
import ru.innopolis.crm.profile.services.exception.ProfileCriterionTemplateServiceException;

import java.util.List;

/**
 * Имплементация для ProfileCriterionTemplateService
 * Created by STC-05 Team [Aleksei Lysov] on 29.04.2017.
 */
@Service
public class ProfileCriterionTemplateServiceImpl implements ProfileCriterionTemplateService {

    private static final Logger LOGGER = Logger.getLogger(ProfileCriterionTemplateServiceImpl.class);
    private ProfileCriterionTemplateDao dao;

    @Autowired
    public void setDao(ProfileCriterionTemplateDao dao) {
        this.dao = dao;
    }

    @Override
    public ProfileCriterionTemplate getById(long id) {
        try {
            return dao.getById(id);
        } catch (ProfileCriterionTemplateDaoException e) {
            LOGGER.error(e);
            throw new ProfileCriterionTemplateServiceException();
        }
    }

    @Override
    public List<ProfileCriterionTemplate> getAll() {
        try {
            return dao.getAll();
        } catch (ProfileCriterionTemplateDaoException e) {
            LOGGER.error(e);
            throw new ProfileCriterionTemplateServiceException();
        }
    }

    @Override
    public ProfileCriterionTemplate save(ProfileCriterionTemplate entity) {
        try {
            return dao.save(entity);
        } catch (ProfileCriterionTemplateDaoException e) {
            LOGGER.error(e);
            throw new ProfileCriterionTemplateServiceException();
        }
    }

    @Override
    public long create(ProfileCriterionTemplate entity) {
        try {
            return dao.create(entity);
        } catch (ProfileCriterionTemplateDaoException e) {
            LOGGER.error(e);
            throw new ProfileCriterionTemplateServiceException();
        }
    }

    @Override
    public int update(ProfileCriterionTemplate entity) {
        try {
            return dao.update(entity);
        } catch (ProfileCriterionTemplateDaoException e) {
            LOGGER.error(e);
            throw new ProfileCriterionTemplateServiceException();
        }
    }

    @Override
    public int delete(ProfileCriterionTemplate entity) {
        try {
            return dao.delete(entity);
        } catch (ProfileCriterionTemplateDaoException e) {
            LOGGER.error(e);
            throw new ProfileCriterionTemplateServiceException();
        }
    }

    @Override
    public List<ProfileCriterionTemplate> getByProfileTemplateId(long profileTemplateId) {
        try {
            return dao.getByProfileTemplateId(profileTemplateId);
        } catch (ProfileCriterionTemplateDaoException e) {
            LOGGER.error(e);
            throw new ProfileCriterionTemplateServiceException();
        }
    }

    @Override
    public List<ProfileCriterionTemplate> getListWithoutProfileTemplateId(long profileTemplateId) {
        try {
            return dao.getListWithoutProfileTemplateId(profileTemplateId);
        } catch (ProfileCriterionTemplateDaoException e) {
            LOGGER.error(e);
            throw new ProfileCriterionTemplateServiceException();
        }
    }
}
