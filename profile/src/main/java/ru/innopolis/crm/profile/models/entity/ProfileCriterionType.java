package ru.innopolis.crm.profile.models.entity;

/**
 * Класс сущность для таблицы profile.criterion_type
 * Данная сущность представляет из себя каталог типов
 * для использования в критериях
 * Created by admin on 21.04.2017.
 */
public class ProfileCriterionType implements Comparable<ProfileCriterionType> {
    private long id;
    private String name;
    private long maxGrade;
    private boolean isDeleted;

    public ProfileCriterionType(long id, String name, long maxGrade, boolean isDeleted){

        this.id = id;
        this.name = name;
        this.maxGrade = maxGrade;
        this.isDeleted = isDeleted;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getMaxGrade() {
        return maxGrade;
    }

    public void setMaxGrade(long maxGrade) {
        this.maxGrade = maxGrade;
    }

    public boolean isDeleted() {
        return isDeleted;
    }

    public void setDeleted(boolean deleted) {
        isDeleted = deleted;
    }

    @Override
    public int compareTo(ProfileCriterionType profileCriterionType) {
        return this.name.compareTo(profileCriterionType.getName());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ProfileCriterionType type = (ProfileCriterionType) o;

        if (id != type.id) return false;
        if (maxGrade != type.maxGrade) return false;
        if (isDeleted != type.isDeleted) return false;
        return name != null ? name.equals(type.name) : type.name == null;
    }

    @Override
    public int hashCode() {
        int result = (int) (id ^ (id >>> 32));
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (int) (maxGrade ^ (maxGrade >>> 32));
        result = 31 * result + (isDeleted ? 1 : 0);
        return result;
    }

    @Override
    public String toString() {
        return "ProfileCriterionType{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", maxGrade=" + maxGrade +
                ", isDeleted=" + isDeleted +
                '}';
    }
}
