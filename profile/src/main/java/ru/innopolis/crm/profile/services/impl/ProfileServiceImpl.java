package ru.innopolis.crm.profile.services.impl;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.innopolis.crm.profile.models.dao.ProfileDao;
import ru.innopolis.crm.profile.models.dao.exception.ProfileDaoException;
import ru.innopolis.crm.profile.models.entity.Profile;
import ru.innopolis.crm.profile.services.ProfileService;
import ru.innopolis.crm.profile.services.exception.ProfileServiceException;

/**
 * Имплементация сервиса ProfileService
 * Created by admin on 22.04.2017.
 */
@Service
public class ProfileServiceImpl implements ProfileService {

    private static final Logger LOGGER = Logger.getLogger(ProfileServiceImpl.class);

    private ProfileDao profileDao;

    @Autowired
    public void setProfileDao(ProfileDao profileDao) {
        this.profileDao = profileDao;
    }

    @Override
    public Profile getById(Long profileId) {
        try {
            return profileDao.getById(profileId);
        } catch (ProfileDaoException e) {
            LOGGER.error(e);
            throw new ProfileServiceException();
        }
    }

    @Override
    public void update(Profile profile) {
        try {
            profileDao.update(profile);
        } catch (ProfileDaoException e) {
            LOGGER.error(e);
            throw new ProfileServiceException();
        }
    }

    @Override
    public Profile getByStudentId(Long studentId) {
        try {
            return profileDao.getByStudentId(studentId);
        } catch (ProfileDaoException e) {
            LOGGER.error(e);
            throw new ProfileServiceException();
        }
    }

    @Override
    public void delete(Profile profile) {
        try {
            profileDao.delete(profile);
        } catch (ProfileDaoException e) {
            LOGGER.error(e);
            throw new ProfileServiceException();
        }
    }
}
