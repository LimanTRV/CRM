package ru.innopolis.crm.profile.services;

import ru.innopolis.crm.profile.models.entity.ProfileCriterionTemplate;

import java.util.List;

/**
 * Сервис для работы с  сущностью ProfileCriterionTemplate
 * Created by STC-05 Team [Aleksei Lysov] on 29.04.2017.
 */
public interface ProfileCriterionTemplateService {
    ProfileCriterionTemplate getById(long id);

    List<ProfileCriterionTemplate> getAll();

    ProfileCriterionTemplate save(ProfileCriterionTemplate entity);

    long create(ProfileCriterionTemplate entity);

    int update(ProfileCriterionTemplate entity);

    int delete(ProfileCriterionTemplate entity);

    List<ProfileCriterionTemplate> getByProfileTemplateId(long profileTemplateId);

    /**
     * Метод возвращает список критериев, которые отсутствуют в конкретном шаблоне
     *
     * @param profileTemplateId id шаблона
     * @return список критериев для конкретного шаблона
     */
    List<ProfileCriterionTemplate> getListWithoutProfileTemplateId(long profileTemplateId);
}
