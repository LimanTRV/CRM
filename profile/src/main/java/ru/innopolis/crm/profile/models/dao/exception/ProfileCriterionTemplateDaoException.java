package ru.innopolis.crm.profile.models.dao.exception;

/**
 * Exception для ProfileCriterionTemplateDao
 * Created by STC-05 Team [Aleksei Lysov] on 04.05.2017.
 */
public class ProfileCriterionTemplateDaoException extends Exception {
}
