package ru.innopolis.crm.profile.services;

import ru.innopolis.crm.profile.models.entity.Profile;
import ru.innopolis.crm.profile.models.entity.ProfileCriterionGroup;
import ru.innopolis.crm.profile.models.entity.ProfileCriterionType;

import java.util.HashMap;
import java.util.LinkedList;

/**
 * Сервис для работы с сущностью Profile
 * Created by admin on 22.04.2017.
 */
public interface ProfileService {
    Profile getById(Long profileId);

    void update(Profile profile);

    Profile getByStudentId(Long studentId);

    void delete(Profile profile);
}
