package ru.innopolis.crm.profile.models.entity;

/**
 * Класс сущность для таблицы profile.questionnaire_instance
 * Created by admin on 21.04.2017.
 */
public class Profile {
    private long id;
    private String name;
    private long studentId;
    private long editUserId;
    private boolean isFinished;
    private boolean isDeleted;

    public Profile(long id, String name, long studentId, long editUserId, boolean isFinished, boolean isDeleted){

        this.id = id;
        this.name = name;
        this.studentId = studentId;
        this.editUserId = editUserId;
        this.isFinished = isFinished;
        this.isDeleted = isDeleted;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getStudentId() {
        return studentId;
    }

    public void setStudentId(long studentId) {
        this.studentId = studentId;
    }

    public long getEditUserId() {
        return editUserId;
    }

    public void setEditUserId(long editUserId) {
        this.editUserId = editUserId;
    }

    public boolean isFinished() {
        return isFinished;
    }

    public void setFinished(boolean finished) {
        isFinished = finished;
    }

    public boolean isDeleted() {
        return isDeleted;
    }

    public void setDeleted(boolean deleted) {
        isDeleted = deleted;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Profile profile = (Profile) o;

        if (id != profile.id) return false;
        if (studentId != profile.studentId) return false;
        if (editUserId != profile.editUserId) return false;
        if (isFinished != profile.isFinished) return false;
        if (isDeleted != profile.isDeleted) return false;
        return name != null ? name.equals(profile.name) : profile.name == null;
    }

    @Override
    public int hashCode() {
        int result = (int) (id ^ (id >>> 32));
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (int) (studentId ^ (studentId >>> 32));
        result = 31 * result + (int) (editUserId ^ (editUserId >>> 32));
        result = 31 * result + (isFinished ? 1 : 0);
        result = 31 * result + (isDeleted ? 1 : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Profile{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", studentId=" + studentId +
                ", editUserId=" + editUserId +
                ", isFinished=" + isFinished +
                ", isDeleted=" + isDeleted +
                '}';
    }
}
