package ru.innopolis.crm.profile.models.dao.impl;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import ru.innopolis.crm.profile.models.dao.ProfileCriterionGroupDao;
import ru.innopolis.crm.profile.models.dao.exception.ProfileCriterionGroupDaoException;
import ru.innopolis.crm.profile.models.entity.ProfileCriterionGroup;

import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Имплементация DAO для ProfileCriterionGroupDao
 * Created by admin on 21.04.2017.
 */
@Repository
public class ProfileCriterionGroupDaoImpl implements ProfileCriterionGroupDao {

    private static final Logger LOGGER = Logger.getLogger(ProfileCriterionGroupDaoImpl.class);

    private static final String SELECT_ALL = "SELECT * FROM profile.criterion_group WHERE NOT is_deleted";

    private static final String INSERT = "INSERT INTO profile.criterion_group (name, is_deleted) VALUES (?, ?)";

    private static final String UPDATE = "UPDATE profile.criterion_group SET name = ?, is_deleted = ? WHERE id = ?;";

    private static final String DELETE = "UPDATE profile.criterion_group SET is_deleted = TRUE WHERE id = ?;";

    private static final String CONTAINS_QUESTIONNAIRE_CRITERION = "select count(id) from profile.questionnaire_criterion " +
            " WHERE criterion_group_id = ? AND NOT is_deleted";

    private static final String CONTAINS_QUESTIONNAIRE_CRITERION_INSTANCE = "select count(id) from profile.questionnaire_criterion_instance " +
            " WHERE criterion_group_id = ? AND NOT is_deleted";

    private DataSource dataSource;

    @Autowired
    public void setDataSource(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    public ProfileCriterionGroup getById(long id) throws ProfileCriterionGroupDaoException {
        ProfileCriterionGroup profileCriterionGroup = null;

        try {
            Connection connection = dataSource.getConnection();
            PreparedStatement statement = connection.prepareStatement(SELECT_ALL + " AND id = ?");
            statement.setLong(1, id);
            ResultSet resultSet = statement.executeQuery();
            if(resultSet.next()){
                profileCriterionGroup = createProfileCriterionGroup(resultSet);
            }
        } catch (SQLException e) {
            LOGGER.error(e);
            throw new ProfileCriterionGroupDaoException();
        }

        return profileCriterionGroup;
    }

    public List<ProfileCriterionGroup> getAll() throws ProfileCriterionGroupDaoException {
        List<ProfileCriterionGroup> list = new ArrayList<>();

        try {
            Connection connection = dataSource.getConnection();
            PreparedStatement statement = connection.prepareStatement(SELECT_ALL);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                list.add(createProfileCriterionGroup(resultSet));
            }
        } catch (SQLException e) {
            LOGGER.error(e);
            throw new ProfileCriterionGroupDaoException();
        }

        return list;
    }

    public ProfileCriterionGroup save(ProfileCriterionGroup profileCriterionGroup) throws ProfileCriterionGroupDaoException {
        if (profileCriterionGroup == null) {
            return null;
        }

        if(profileCriterionGroup.getId() > 0){
            update(profileCriterionGroup);
        } else {
            long newId = create(profileCriterionGroup);
            profileCriterionGroup.setId(newId);
        }

        return profileCriterionGroup;
    }

    public long create(ProfileCriterionGroup profileCriterionGroup) throws ProfileCriterionGroupDaoException {
        long lastId = 0;

        if (profileCriterionGroup == null) {
            LOGGER.error("ProfileCriterionGroupDao.create(null) called.");
            return lastId;
        }

        if (profileCriterionGroup.getId() > 0){
            LOGGER.error("Tried ProfileCriterionGroupDao.create() object with existing id.");
            return profileCriterionGroup.getId();
        }

        try {
            Connection connection = dataSource.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(INSERT, Statement.RETURN_GENERATED_KEYS);

            preparedStatement.setString(1, profileCriterionGroup.getName());
            preparedStatement.setBoolean(2, profileCriterionGroup.isDeleted());

            preparedStatement.executeUpdate();
            ResultSet resultSet = preparedStatement.getGeneratedKeys();
            if (resultSet.next()) {
                lastId = resultSet.getInt(1);
            }
        } catch (SQLException e) {
            LOGGER.error(e);
            throw new ProfileCriterionGroupDaoException();
        }
        return lastId;
    }

    public int update(ProfileCriterionGroup profileCriterionGroup) throws ProfileCriterionGroupDaoException {
        int count = 0;

        if (profileCriterionGroup == null) {
            LOGGER.error("ProfileCriterionGroupDao.update(null) called.");
            return count;
        }

        try {
            Connection connection = dataSource.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(UPDATE);

            preparedStatement.setString(1, profileCriterionGroup.getName());
            preparedStatement.setBoolean(2, profileCriterionGroup.isDeleted());
            preparedStatement.setLong(3, profileCriterionGroup.getId());

            count = preparedStatement.executeUpdate();
        } catch (SQLException e) {
            LOGGER.error(e);
            throw new ProfileCriterionGroupDaoException();
        }
        return count;
    }

    public int delete(ProfileCriterionGroup profileCriterionGroup) throws ProfileCriterionGroupDaoException {
        int count = 0;

        if (profileCriterionGroup == null) {
            LOGGER.error("ProfileCriterionGroupDao.delete(null) called.");
            return count;
        }

        if (isUsedProfileCriterionGroup(profileCriterionGroup.getId())){
            LOGGER.error("ProfileCriterionGroupDao.delete() called. Group " + profileCriterionGroup.getId() + " used.");
            return count;
        }

        try {
            Connection connection = dataSource.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(DELETE);
            preparedStatement.setLong(1, profileCriterionGroup.getId());
            count = preparedStatement.executeUpdate();
        } catch (SQLException e) {
            LOGGER.error(e);
            throw new ProfileCriterionGroupDaoException();
        }
        return count;
    }

    private ProfileCriterionGroup createProfileCriterionGroup(ResultSet resultSet) throws SQLException {
        return  new ProfileCriterionGroup(
                resultSet.getLong("id"),
                resultSet.getString("name"),
                resultSet.getBoolean("is_deleted")
        );
    }

    private boolean isUsedProfileCriterionGroup(long groupId) throws ProfileCriterionGroupDaoException {
        int count = 0;
        try {
            Connection connection = dataSource.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(CONTAINS_QUESTIONNAIRE_CRITERION);
            preparedStatement.setLong(1, groupId);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next())
                count = resultSet.getInt(1);

            if (count > 0)
                return true;

            preparedStatement = connection.prepareStatement(CONTAINS_QUESTIONNAIRE_CRITERION_INSTANCE);
            preparedStatement.setLong(1, groupId);
            resultSet = preparedStatement.executeQuery();
            if (resultSet.next())
                count += resultSet.getInt(1);
        } catch (SQLException e) {
            LOGGER.error(e);
            throw new ProfileCriterionGroupDaoException();
        }

        return count > 0;
    }
}
