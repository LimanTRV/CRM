package ru.innopolis.crm.profile.models.dao.impl;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import ru.innopolis.crm.profile.models.dao.ProfileDao;
import ru.innopolis.crm.profile.models.dao.exception.ProfileDaoException;
import ru.innopolis.crm.profile.models.entity.Profile;

import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Имплементация DAO для ProfileDao
 * Created by admin on 21.04.2017.
 */
@Repository
public class ProfileDaoImpl implements ProfileDao {

    private static final Logger LOGGER = Logger.getLogger(ProfileCriterionGroupDaoImpl.class);

    private static final String SELECT_ALL = "SELECT * FROM profile.questionnaire_instance WHERE NOT is_deleted";

    private static final String INSERT = "INSERT INTO profile.questionnaire_instance " +
            "(name, student_id, edit_user_id, is_finished, is_deleted) VALUES (?, ?, ?, ?, ?);";

    private static final String UPDATE = "UPDATE profile.questionnaire_instance " +
            "SET name = ?, student_id = ?, edit_user_id = ?, is_finished = ?, is_deleted = ? WHERE id = ?;";

    private static final String DELETE = "UPDATE profile.questionnaire_instance SET is_deleted = TRUE WHERE id = ?;";

    private DataSource dataSource;

    @Autowired
    public void setDataSource(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    public Profile getById(long id) throws ProfileDaoException {
        Profile profile = null;

        try {
            Connection connection = dataSource.getConnection();
            PreparedStatement statement = connection.prepareStatement(SELECT_ALL + " AND id = ?");

            statement.setLong(1, id);
            ResultSet resultSet = statement.executeQuery();
            if(resultSet.next()){
                profile = createProfile(resultSet);
            }
        } catch (SQLException e) {
            LOGGER.error(e);
            throw new ProfileDaoException();
        }

        return profile;
    }

    public Profile getByStudentId(long id) throws ProfileDaoException {
        Profile profile = null;

        try {
            Connection connection = dataSource.getConnection();
            PreparedStatement statement = connection.prepareStatement(SELECT_ALL + " AND student_id = ?");
            statement.setLong(1, id);
            ResultSet resultSet = statement.executeQuery();
            if(resultSet.next()){
                profile = createProfile(resultSet);
            }
        } catch (SQLException e) {
            LOGGER.error(e);
            throw new ProfileDaoException();
        }

        return profile;
    }

    public List<Profile> getAll() throws ProfileDaoException {
        List<Profile> list = new ArrayList<>();

        try {
            Connection connection = dataSource.getConnection();
            PreparedStatement statement = connection.prepareStatement(SELECT_ALL);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                list.add(createProfile(resultSet));
            }
        } catch (SQLException e) {
            LOGGER.error(e);
            throw new ProfileDaoException();
        }

        return list;
    }

    public Profile save(Profile profile) throws ProfileDaoException {
        if (profile == null) {
            return null;
        }

        if(profile.getId() > 0){
            update(profile);
        } else {
            long newId = create(profile);
            profile.setId(newId);
        }

        return profile;
    }

    public long create(Profile profile) throws ProfileDaoException {
        long lastId = 0;

        if (profile == null) {
            LOGGER.error("ProfileDao.create(null) called.");
            return lastId;
        }

        if (profile.getId() > 0){
            LOGGER.error("Tried ProfileDao.create() object with existing id.");
            return profile.getId();
        }

        try {
            Connection connection = dataSource.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(INSERT, Statement.RETURN_GENERATED_KEYS);

            preparedStatement.setString(1, profile.getName());
            preparedStatement.setLong(2, profile.getStudentId());
            preparedStatement.setLong(3, profile.getEditUserId());
            preparedStatement.setBoolean(4, profile.isFinished());
            preparedStatement.setBoolean(5, profile.isDeleted());

            preparedStatement.executeUpdate();
            ResultSet resultSet = preparedStatement.getGeneratedKeys();
            if (resultSet.next()) {
                lastId = resultSet.getInt(1);
            }
        } catch (SQLException e) {
            LOGGER.error(e);
            throw new ProfileDaoException();
        }
        return lastId;
    }

    public int update(Profile profile) throws ProfileDaoException {
        int count = 0;

        if (profile == null) {
            LOGGER.error("ProfileDao.update(null) called.");
            return count;
        }

        try {
            Connection connection = dataSource.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(UPDATE);
            preparedStatement.setString(1, profile.getName());
            preparedStatement.setLong(2, profile.getStudentId());
            preparedStatement.setLong(3, profile.getEditUserId());
            preparedStatement.setBoolean(4, profile.isFinished());
            preparedStatement.setBoolean(5, profile.isDeleted());
            preparedStatement.setLong(6, profile.getId());

            count = preparedStatement.executeUpdate();
        } catch (SQLException e) {
            LOGGER.error(e);
            throw new ProfileDaoException();
        }
        return count;
    }

    public int delete(Profile profile) throws ProfileDaoException {
        int count = 0;

        if (profile == null) {
            LOGGER.error("ProfileDao.delete(null) called.");
            return count;
        }

        try {
            Connection connection = dataSource.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(DELETE);
            preparedStatement.setLong(1, profile.getId());
            count = preparedStatement.executeUpdate();
        } catch (SQLException e) {
            LOGGER.error(e);
            throw new ProfileDaoException();
        }
        return count;
    }

    private Profile createProfile(ResultSet resultSet) throws SQLException {
        return  new Profile(
                resultSet.getLong("id"),
                resultSet.getString("name"),
                resultSet.getLong("student_id"),
                resultSet.getLong("edit_user_id"),
                resultSet.getBoolean("is_finished"),
                resultSet.getBoolean("is_deleted")
        );
    }
}
