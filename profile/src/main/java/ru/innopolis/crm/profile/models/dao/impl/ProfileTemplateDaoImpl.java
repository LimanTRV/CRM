package ru.innopolis.crm.profile.models.dao.impl;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import ru.innopolis.crm.profile.models.dao.ProfileTemplateDao;
import ru.innopolis.crm.profile.models.dao.exception.ProfileTemplateDaoException;
import ru.innopolis.crm.profile.models.entity.ProfileTemplate;

import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Имплементация DAO для ProfileTemplateDao
 * Created by admin on 21.04.2017.
 */
@Repository
public class ProfileTemplateDaoImpl implements ProfileTemplateDao {

    private static final Logger LOGGER = Logger.getLogger(ProfileCriterionGroupDaoImpl.class);

    private static final String SELECT_ALL = "SELECT * FROM profile.questionnaire WHERE NOT is_deleted";

    private static final String INSERT = "INSERT INTO profile.questionnaire " +
            "(name, is_default, is_deleted) VALUES (?, ?, ?)";

    private static final String UPDATE = "UPDATE profile.questionnaire " +
            "SET name = ?, is_default = ?, is_deleted = ? WHERE id = ?;";

    private static final String DELETE = "UPDATE profile.questionnaire SET is_deleted = TRUE WHERE id = ?;";

    private static final String SELECT_QUESTIONNAIRE = "SELECT * FROM profile.questionnaire WHERE id != ? AND NOT is_deleted LIMIT 1";

    private DataSource dataSource;

    @Autowired
    public void setDataSource(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    public ProfileTemplate getById(long id) throws ProfileTemplateDaoException {
        ProfileTemplate profileTemplate = null;

        try {
            Connection connection = dataSource.getConnection();
            PreparedStatement statement = connection.prepareStatement(SELECT_ALL + " AND id = ?");
            statement.setLong(1, id);
            ResultSet resultSet = statement.executeQuery();
            if(resultSet.next()){
                profileTemplate = createProfileTemplate(resultSet);
            }
        } catch (SQLException e) {
            LOGGER.error(e);
            throw new ProfileTemplateDaoException();
        }

        return profileTemplate;
    }

    public List<ProfileTemplate> getAll() throws ProfileTemplateDaoException {
        List<ProfileTemplate> list = new ArrayList<>();

        try {
            Connection connection = dataSource.getConnection();
            PreparedStatement statement = connection.prepareStatement(SELECT_ALL);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                list.add(createProfileTemplate(resultSet));
            }
        } catch (SQLException e) {
            LOGGER.error(e);
            throw new ProfileTemplateDaoException();
        }

        return list;
    }

    public ProfileTemplate save(ProfileTemplate profileTemplate) throws ProfileTemplateDaoException {
        if (profileTemplate == null) {
            return null;
        }

        if(profileTemplate.getId() > 0){
            update(profileTemplate);
        } else {
            long newId = create(profileTemplate);
            profileTemplate.setId(newId);
        }

        return profileTemplate;
    }

    public long create(ProfileTemplate profileTemplate) throws ProfileTemplateDaoException {
        long lastId = 0;

        if (profileTemplate == null) {
            LOGGER.error("ProfileTemplateDao.create(null) called.");
            return lastId;
        }

        if (profileTemplate.getId() > 0){
            LOGGER.error("Tried ProfileTemplateDao.create() object with existing id.");
            return profileTemplate.getId();
        }

        try {
            Connection connection = dataSource.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(INSERT, Statement.RETURN_GENERATED_KEYS);

            preparedStatement.setString(1, profileTemplate.getName());
            preparedStatement.setBoolean(2, profileTemplate.isDefault());
            preparedStatement.setBoolean(3, profileTemplate.isDeleted());

            preparedStatement.executeUpdate();
            ResultSet resultSet = preparedStatement.getGeneratedKeys();
            if (resultSet.next()) {
                lastId = resultSet.getInt(1);
            }
        } catch (SQLException e) {
            LOGGER.error(e);
            throw new ProfileTemplateDaoException();
        }
        return lastId;
    }

    public int update(ProfileTemplate profileTemplate) throws ProfileTemplateDaoException {
        int count = 0;

        if (profileTemplate == null) {
            LOGGER.error("ProfileTemplateDao.update(null) called.");
            return count;
        }

        try {
            Connection connection = dataSource.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(UPDATE);
            preparedStatement.setString(1, profileTemplate.getName());
            preparedStatement.setBoolean(2, profileTemplate.isDefault());
            preparedStatement.setBoolean(3, profileTemplate.isDeleted());
            preparedStatement.setLong(4, profileTemplate.getId());

            count = preparedStatement.executeUpdate();
        } catch (SQLException e) {
            LOGGER.error(e);
            throw new ProfileTemplateDaoException();
        }
        return count;
    }

    public int delete(ProfileTemplate profileTemplate) throws ProfileTemplateDaoException {
        int count = 0;

        if (profileTemplate == null) {
            LOGGER.error("ProfileTemplateDao.delete(null) called.");
            return count;
        }

        if (!existOtherProfileTemplate()){
            LOGGER.error("ProfileTemplateDao.delete() called. ProfileTemplate < 2");
            return count;
        }

        try {
            Connection connection = dataSource.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(DELETE);
            preparedStatement.setLong(1, profileTemplate.getId());
            count = preparedStatement.executeUpdate();
        } catch (SQLException e) {
            LOGGER.error(e);
            throw new ProfileTemplateDaoException();
        }

        if (count > 0)
            changeDefaultProfileTemplate(profileTemplate);

        return count;
    }

    @Override
    public ProfileTemplate getDefault() throws ProfileTemplateDaoException {
        ProfileTemplate profileTemplate = null;

        try {
            Connection connection = dataSource.getConnection();
            PreparedStatement statement = connection.prepareStatement(SELECT_ALL + " AND is_default");
            ResultSet resultSet = statement.executeQuery();
            if(resultSet.next()){
                profileTemplate = createProfileTemplate(resultSet);
            }
        } catch (SQLException e) {
            LOGGER.error(e);
            throw new ProfileTemplateDaoException();
        }

        return profileTemplate;
    }

    @Override
    public long saveWithCriterionAndUpdateDefault(ProfileTemplate entity, String[] criterionIds) throws ProfileTemplateDaoException {
        long result = 0;

        if (entity == null) {
            LOGGER.warn("saveWithCriterionAndUpdateDefault: Ошибка сохранения. Передана пустая сущность");
            return 0;
        }

        Connection connection = null;
        try {
            connection = dataSource.getConnection();
            connection.setAutoCommit(false);
            long entityId = 0;
            PreparedStatement preparedStatement = null;

            if (entity.getId() == 0) {
                preparedStatement = connection.prepareStatement(INSERT, Statement.RETURN_GENERATED_KEYS);

                preparedStatement.setString(1, entity.getName());
                preparedStatement.setBoolean(2, entity.isDefault());
                preparedStatement.setBoolean(3, entity.isDeleted());

                preparedStatement.executeUpdate();
                ResultSet resultSet = preparedStatement.getGeneratedKeys();
                if (resultSet.next()) {
                    result = resultSet.getLong(1);
                    entityId = result;
                }
                resultSet.close();
            }else{
                preparedStatement = connection.prepareStatement(UPDATE);
                preparedStatement.setString(1, entity.getName());
                preparedStatement.setBoolean(2, entity.isDefault());
                preparedStatement.setBoolean(3, entity.isDeleted());
                preparedStatement.setLong(4, entity.getId());
                result = preparedStatement.executeUpdate();
                entityId = entity.getId();
            }

            preparedStatement = connection.prepareStatement("UPDATE profile.questionnaire_criterion_to_questionnaire SET is_deleted = TRUE WHERE questionnaire_id = ?");
            preparedStatement.setLong(1, entityId);
            preparedStatement.executeUpdate();

            if (criterionIds != null) {
                for (String criterionId : criterionIds) {
                    preparedStatement = connection.prepareStatement("INSERT INTO profile.questionnaire_criterion_to_questionnaire (questionnaire_criterion_id, questionnaire_id, is_deleted) VALUES (?, ?, ?)");
                    preparedStatement.setLong(1, Long.valueOf(criterionId));
                    preparedStatement.setLong(2, entityId);
                    preparedStatement.setBoolean(3, false);
                    preparedStatement.executeUpdate();
                }
            }

            if (entity.isDefault()){
                preparedStatement = connection.prepareStatement("UPDATE profile.questionnaire SET is_default = FALSE WHERE id != ?");
                preparedStatement.setLong(1, entityId);
                preparedStatement.executeUpdate();
            }

            connection.commit();
        } catch (SQLException e) {
            LOGGER.error(e);
            try {
                connection.rollback();
            } catch (SQLException e1) {
                LOGGER.error(e1);
            }
            throw new ProfileTemplateDaoException();
        }finally {
            try {
                if (connection != null) {
                    connection.setAutoCommit(true);
                    connection.close();
                }
            } catch (SQLException e) {
                LOGGER.error(e);
            }
        }
        return result;
    }

    private ProfileTemplate createProfileTemplate(ResultSet resultSet) throws SQLException {
        return  new ProfileTemplate(
                resultSet.getLong("id"),
                resultSet.getString("name"),
                resultSet.getBoolean("is_default"),
                resultSet.getBoolean("is_deleted")
        );
    }

    private boolean existOtherProfileTemplate() throws ProfileTemplateDaoException {
        int count = 0;
        try {
            Connection connection = dataSource.getConnection();
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery("select count(id) from profile.questionnaire WHERE NOT is_deleted");
            if (resultSet.next())
                count = resultSet.getInt(1);
        } catch (SQLException e) {
            LOGGER.error(e);
            throw new ProfileTemplateDaoException();
        }

        return count > 1;
    }

    private void changeDefaultProfileTemplate(ProfileTemplate profileTemplate) throws ProfileTemplateDaoException {
        if ((profileTemplate == null) || (!profileTemplate.isDefault()))
            return;

        try {
            Connection connection = dataSource.getConnection();
            PreparedStatement statement
                    = connection.prepareStatement(SELECT_QUESTIONNAIRE);
            statement.setLong(1, profileTemplate.getId());

            ProfileTemplate resultTemplate = null;
            ResultSet resultSet = statement.executeQuery();

            if(resultSet.next())
                resultTemplate = createProfileTemplate(resultSet);

            if (resultTemplate != null) {
                resultTemplate.setDefault(true);
                update(resultTemplate);
            }

            resultSet.close();
        } catch (SQLException e) {
            LOGGER.error(e);
            throw new ProfileTemplateDaoException();
        }
    }
}
