package ru.innopolis.crm.profile.controllers;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import ru.innopolis.crm.profile.models.entity.ProfileCriterionGroup;
import ru.innopolis.crm.profile.services.ProfileCriterionGroupService;
import ru.innopolis.crm.profile.util.ProfileHelper;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * Контроллер для обработки страницы по адресу /profile/group
 * Created by STC-05 Team [Aleksei Lysov] on 02.05.2017.
 */
@Controller
@RequestMapping(value = "/profile/group")
public class ProfileGroupController {

    private static final Logger LOGGER = Logger.getLogger(ProfileGroupController.class);
    private ProfileCriterionGroupService profileCriterionGroupService;

    @Autowired
    public void setProfileCriterionGroupService(ProfileCriterionGroupService profileCriterionGroupService) {
        this.profileCriterionGroupService = profileCriterionGroupService;
    }

    @PreAuthorize(value = "hasRole('PC_V')")
    @RequestMapping(method = RequestMethod.GET)
    public ModelAndView doGet(
              @RequestParam(value = "edit", required = false) String editId,
              @RequestParam(value = "delete", required = false) String deleteId,
              @RequestParam(value = "group", required = false) String groupId,
              HttpServletRequest req){

        ModelAndView model = new ModelAndView();
        ProfileHelper.checkSuccessOrErrorMessage(req);

        List<ProfileCriterionGroup> groups = profileCriterionGroupService.getAll();
        model.addObject("profileGroups", groups);

        if (ProfileHelper.isCorrectId(editId))
            initEditOperation(model, Long.valueOf(editId));

        model.setViewName("profile/profile_group");

        if (ProfileHelper.isCorrectId(deleteId)) {
            boolean isDelete = deleteOperation(Long.valueOf(deleteId));
            model.setViewName("redirect:/profile/group?group=" + groupId);
            ProfileHelper.setSessionAfterDeleteData(req, isDelete);
        }

        return model;
    }

    @PreAuthorize(value = "hasRole('PC_V')")
    @RequestMapping(method = RequestMethod.POST)
    public String doPost(
            @RequestParam(value = "name") String name,
            @RequestParam(value = "profileGroupId", required = false) String profileGroupId,
            @RequestParam(value = "group", required = false) String groupId,
            HttpServletRequest req){

        ProfileCriterionGroup group = null;

        if (ProfileHelper.isCorrectId(profileGroupId))
            group = profileCriterionGroupService.getById(Long.valueOf(profileGroupId));

        boolean isSave = saveOperation(group, name);
        ProfileHelper.setSessionAfterSaveData(req, isSave);

        return "redirect:/profile/group?group=" + groupId;
    }


    private void initEditOperation(ModelAndView model, long groupId) {
        ProfileCriterionGroup entity = profileCriterionGroupService.getById(groupId);
        if (entity != null)
            model.addObject("editGroup", entity);
    }

    private boolean deleteOperation(long deleteId) {
        return profileCriterionGroupService.delete(profileCriterionGroupService.getById(deleteId)) > 0;
    }

    private boolean saveOperation(ProfileCriterionGroup group, String name) {
        long countUpdate = 0;

        if (group == null) {
            group = new ProfileCriterionGroup(0, name, false);
            countUpdate = profileCriterionGroupService.create(group);
        } else {
            group.setName(name);
            countUpdate = profileCriterionGroupService.update(group);
        }

        return countUpdate > 0;
    }
}
