package ru.innopolis.crm.profile.services.impl;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.innopolis.crm.main.sessionbean.UserBean;
import ru.innopolis.crm.profile.models.dao.*;
import ru.innopolis.crm.profile.models.dao.exception.*;
import ru.innopolis.crm.profile.models.entity.Profile;
import ru.innopolis.crm.profile.models.entity.ProfileCriterion;
import ru.innopolis.crm.profile.models.entity.ProfileCriterionTemplate;
import ru.innopolis.crm.profile.models.entity.ProfileTemplate;
import ru.innopolis.crm.profile.services.ProfileTemplateService;
import ru.innopolis.crm.profile.services.exception.*;

import java.util.Date;
import java.util.List;

/**
 * Имплементация сервиса ProfileTemplateService
 * Created by admin on 25.04.2017.
 */
@Service
public class ProfileTemplateServiceImpl implements ProfileTemplateService {

    private static final Logger LOGGER = Logger.getLogger(ProfileServiceImpl.class);

    private ProfileTemplateDao profileTemplateDao;
    private ProfileCriterionTemplateDao profileCriterionTemplateDao;
    private ProfileDao profileDao;
    private ProfileCriterionDao profileCriterionDao;
    private ProfileCriterionTypeDao profileCriterionTypeDao;
    private UserBean userBean;


    @Autowired
    public void setProfileTemplateDao(ProfileTemplateDao profileTemplateDao) {
        this.profileTemplateDao = profileTemplateDao;
    }

    @Autowired
    public void setProfileCriterionTemplateDao(ProfileCriterionTemplateDao profileCriterionTemplateDao) {
        this.profileCriterionTemplateDao = profileCriterionTemplateDao;
    }

    @Autowired
    public void setProfileDao(ProfileDao profileDao) {
        this.profileDao = profileDao;
    }

    @Autowired
    public void setProfileCriterionDao(ProfileCriterionDao profileCriterionDao) {
        this.profileCriterionDao = profileCriterionDao;
    }

    @Autowired
    public void setProfileCriterionTypeDao(ProfileCriterionTypeDao profileCriterionTypeDao) {
        this.profileCriterionTypeDao = profileCriterionTypeDao;
    }

    @Autowired
    public void setUserBean(UserBean userBean) {
        this.userBean = userBean;
    }

    @Override
    public Profile createFromDefault(Long studentId) {
        try {
            ProfileTemplate profileTemplate = profileTemplateDao.getDefault();
            return createFromTemplate(profileTemplate.getId(), studentId);
        } catch (ProfileTemplateDaoException e) {
            LOGGER.error(e);
            throw new ProfileTemplateServiceException();
        }
    }

    @Override
    public Profile createFromTemplate(Long templateId, Long studentId) {
        try {
            Profile profile = null;
            profile = profileDao.getByStudentId(studentId);

            if (profile != null) {
                return null;
            }

            long userId = userBean.getUserId();
            ProfileTemplate profileTemplate = profileTemplateDao.getById(templateId);

            profile = new Profile(-1, profileTemplate.getName(), studentId, userId, false, false);
            profile.setId(profileDao.create(profile));

            List<ProfileCriterionTemplate> profileCriterionTemplates = profileCriterionTemplateDao.getByProfileTemplateId(profileTemplate.getId());

            for (ProfileCriterionTemplate profileCriterionTemplate :
                    profileCriterionTemplates) {
                long maxGrade = profileCriterionTypeDao.getById(profileCriterionTemplate.getProfileCriterionTypeId()).getMaxGrade();

                profileCriterionDao.create(new ProfileCriterion(-1, profile.getId(), profileCriterionTemplate.getName(),
                        maxGrade, 0, profileCriterionTemplate.getProfileCriterionTypeId(),
                        profileCriterionTemplate.getProfileCriterionGroupId(), 2, new Date(),
                        profileCriterionTemplate.getId(), false));
            }

            return profile;
        } catch (ProfileDaoException e) {
            LOGGER.error(e);
            throw new ProfileServiceException();
        } catch (ProfileTemplateDaoException e) {
            LOGGER.error(e);
            throw new ProfileTemplateServiceException();
        } catch (ProfileCriterionTemplateDaoException e) {
            LOGGER.error(e);
            throw new ProfileCriterionTemplateServiceException();
        } catch (ProfileCriterionDaoException e) {
            LOGGER.error(e);
            throw new ProfileCriterionServiceException();
        } catch (ProfileCriterionTypeDaoException e) {
            LOGGER.error(e);
            throw new ProfileCriterionTypeServiceException();
        }
    }

    @Override
    public ProfileTemplate getById(long id) {
        try {
            return profileTemplateDao.getById(id);
        } catch (ProfileTemplateDaoException e) {
            LOGGER.error(e);
            throw new ProfileTemplateServiceException();
        }
    }

    @Override
    public List<ProfileTemplate> getAll() {
        try {
            return profileTemplateDao.getAll();
        } catch (ProfileTemplateDaoException e) {
            LOGGER.error(e);
            throw new ProfileTemplateServiceException();
        }
    }

    @Override
    public ProfileTemplate save(ProfileTemplate entity) {
        try {
            return profileTemplateDao.save(entity);
        } catch (ProfileTemplateDaoException e) {
            LOGGER.error(e);
            throw new ProfileTemplateServiceException();
        }
    }

    @Override
    public long create(ProfileTemplate entity) {
        try {
            return profileTemplateDao.create(entity);
        } catch (ProfileTemplateDaoException e) {
            LOGGER.error(e);
            throw new ProfileTemplateServiceException();
        }
    }

    @Override
    public int update(ProfileTemplate entity) {
        try {
            return profileTemplateDao.update(entity);
        } catch (ProfileTemplateDaoException e) {
            LOGGER.error(e);
            throw new ProfileTemplateServiceException();
        }
    }

    @Override
    public int delete(ProfileTemplate entity) {
        try {
            return profileTemplateDao.delete(entity);
        } catch (ProfileTemplateDaoException e) {
            LOGGER.error(e);
            throw new ProfileTemplateServiceException();
        }
    }

    @Override
    public ProfileTemplate getDefault() {
        try {
            return profileTemplateDao.getDefault();
        } catch (ProfileTemplateDaoException e) {
            LOGGER.error(e);
            throw new ProfileTemplateServiceException();
        }
    }

    @Override
    public long saveWithCriterionAndUpdateDefault(ProfileTemplate entity, String[] criterionIds) {
        try {
            return profileTemplateDao.saveWithCriterionAndUpdateDefault(entity, criterionIds);
        } catch (ProfileTemplateDaoException e) {
            LOGGER.error(e);
            throw new ProfileTemplateServiceException();
        }
    }
}
