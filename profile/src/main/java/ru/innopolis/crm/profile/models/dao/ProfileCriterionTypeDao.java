package ru.innopolis.crm.profile.models.dao;

import ru.innopolis.crm.profile.models.dao.exception.ProfileCriterionTypeDaoException;
import ru.innopolis.crm.profile.models.entity.ProfileCriterionType;

import java.util.List;

/**
 * DAO для сущности ProfileCriterionType
 * Created by admin on 21.04.2017.
 */
public interface ProfileCriterionTypeDao {

    ProfileCriterionType getById(long id) throws ProfileCriterionTypeDaoException;

    List<ProfileCriterionType> getAll() throws ProfileCriterionTypeDaoException;

    ProfileCriterionType save(ProfileCriterionType entity) throws ProfileCriterionTypeDaoException;

    long create(ProfileCriterionType entity) throws ProfileCriterionTypeDaoException;

    int update(ProfileCriterionType entity) throws ProfileCriterionTypeDaoException;

    int delete(ProfileCriterionType entity) throws ProfileCriterionTypeDaoException;
}
