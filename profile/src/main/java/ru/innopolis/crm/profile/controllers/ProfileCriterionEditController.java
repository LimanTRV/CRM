package ru.innopolis.crm.profile.controllers;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import ru.innopolis.crm.main.sessionbean.UserBean;
import ru.innopolis.crm.main.utils.InjectLogger;
import ru.innopolis.crm.profile.models.entity.ProfileCriterionGroup;
import ru.innopolis.crm.profile.models.entity.ProfileCriterionTemplate;
import ru.innopolis.crm.profile.models.entity.ProfileCriterionType;
import ru.innopolis.crm.profile.models.entity.Transcript;
import ru.innopolis.crm.profile.services.ProfileCriterionGroupService;
import ru.innopolis.crm.profile.services.ProfileCriterionTemplateService;
import ru.innopolis.crm.profile.services.ProfileCriterionTypeService;
import ru.innopolis.crm.profile.services.TranscriptService;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Date;
import java.util.List;

/**
 * This class realize logic of editing profile criterion template
 * in web page.
 */
@Controller
public class ProfileCriterionEditController
{
    @InjectLogger(ProfileCriterionEditController.class)
    private Logger logger;

    private ProfileCriterionGroupService profileCriterionGroupService;

    private ProfileCriterionTypeService profileCriterionTypeService;

    private ProfileCriterionTemplateService profileCriterionTemplateService;

    private TranscriptService transcriptService;

    @Autowired
    private UserBean userBean;



    /* ================================= MAPPING =================================== */

    /**
     * Function build criterion web page.
     * @param model - MVC model.
     * @return - return context of the same page.
     */
    @PreAuthorize(value = "hasRole('PC_V') or hasRole('PC_E')")
    @RequestMapping(value = "/profile/criterion/edit", method = RequestMethod.GET)
    public String getProfileCriterionView(Model model,
                                          @RequestParam(value = "edit", required = false)
                                                  String profileCriterionTemplateId)
    {

        setCriterionGroupElementsInSelector(model);

        setCriterionTypeElementsInSelector(model);

        fillDataInViewForEditMode(model, profileCriterionTemplateId);

        return "profile/profile_criterion_edit";
    }


    /**
     * Function edit {@link ProfileCriterionTemplate} object from view
     * in data base.
     * @param profileCriterionTemplate - {@link ProfileCriterionTemplate} object from web.
     * @param resp - servlet dispatcher response.
     */
    @PreAuthorize(value = "hasRole('PC_E')")
    @RequestMapping(value = "/profile/criterion/edit/editCriterion",
                                                        method = RequestMethod.POST)
    public @ResponseBody void editProfileCriterionTemplate(
                                      @RequestBody ProfileCriterionTemplate profileCriterionTemplate,
                                      HttpServletResponse resp)
    {

        ProfileCriterionTemplate profileCriterionTemplateOriginal
                                    = profileCriterionTemplateService
                                            .getById(profileCriterionTemplate.getId());
        profileCriterionTemplate
                .setCreateUserId(profileCriterionTemplateOriginal.getCreateUserId());


        long userId = userBean.getUserId();

        profileCriterionTemplate.setEditUserId(userId);
        /*===================*/

        Date editDate = new Date();
        profileCriterionTemplate.setEditDate(editDate);
        profileCriterionTemplateService.update(profileCriterionTemplate);




        /* Send id of new {@link ProfileCriterionTemplate} object. */
        String jsonText = "{ \"criterionIdVal\" : "+ profileCriterionTemplate.getId() + " }";
        try {
            resp.getWriter().append(jsonText).flush();
        } catch (IOException e) {
            logger.error(e);
        }


    }


    /**
     * Function edit {@link Transcript} object from view
     * in data base. If there are new transcripts than add those transcripts
     * to data base.
     * @param transcriptArr - array of {@link Transcript} objects from web.
     * @param resp - servlet dispatcher response.
     */
    @PreAuthorize(value = "hasRole('PC_E')")
    @RequestMapping(value = "/profile/criterion/edit/addEditTranscripts",
            method = RequestMethod.POST)
    public @ResponseBody void editTranscripts(@RequestBody Transcript[] transcriptArr,
                                     HttpServletResponse resp)
    {
        for (Transcript transcript:transcriptArr
                ) {
            /* if id =0 it is new transcript added by user. */
            if (transcript.getId() == 0)
            {
                transcriptService.create(transcript);

            }
            // If id > 0 than it is old transcript so update it.
            if (transcript.getId() > 0)
            {
                transcriptService.update(transcript);
            }
        }

        String jsonText = "{ \"success\" : \"success\" }";
        try {
            resp.getWriter().append(jsonText).flush();
        } catch (IOException e) {
            logger.error(e);
        }
    }


    /**
     * Function delete selected transcript from web page command.
     * @param transcriptIdForDelete - id of transcript that need to delete.
     * @param resp - servlet dispatcher response.
     */
    @PreAuthorize(value = "hasRole('PC_E')")
    @RequestMapping(value = "/profile/criterion/edit/deleteTranscript",
            method = RequestMethod.POST)
    public @ResponseBody void deleteTranscript(
                            @RequestParam (value = "transcriptIdForDelete",required = false)
                                                           String transcriptIdForDelete,
            HttpServletResponse resp)
    {
        if (transcriptIdForDelete != null)
        {
            long transcriptId = Long.parseLong(transcriptIdForDelete);

            Transcript transcriptOriginal = transcriptService.getById(transcriptId);

            transcriptService.delete(transcriptOriginal);

            String jsonText = "{ \"success\" : \"success\" }";
            try {
                resp.getWriter().append(jsonText).flush();
            } catch (IOException e) {
                logger.error(e);
            }
        }
    }



    /*==================================================================================*/

    /**
     * Function fill all fields in view with edited criterion values.
     */
    private void fillDataInViewForEditMode(Model model, String profileCriterionTemplateId)
    {

        if (profileCriterionTemplateId != null)
        {
            long criterionTemplateId = Long.parseLong(profileCriterionTemplateId);

            ProfileCriterionTemplate profileCriterionTemplate =
                    profileCriterionTemplateService.getById(criterionTemplateId);

            List<Transcript> transcriptsList = transcriptService
                    .getByProfileCriterionTemplateId(profileCriterionTemplate.getId());

            ProfileCriterionType profileCriterionType =
                    profileCriterionTypeService
                            .getById(profileCriterionTemplate.getProfileCriterionTypeId());

            model.addAttribute("maxGradeVal", profileCriterionType.getMaxGrade());

            model.addAttribute("profileCriterionTemplate", profileCriterionTemplate);

            model.addAttribute("transcripts", transcriptsList);
        }

    }


    /**
     * Function set criterion groups in selector (view).
     * @param model - model.
     */
    private void setCriterionGroupElementsInSelector(Model model)
    {
        List<ProfileCriterionGroup> profileCriterionGroups =
                profileCriterionGroupService.getAll();
        model.addAttribute("profileCriterionGroups", profileCriterionGroups);
    }


    /**
     * Function set criterion types in selector (view).
     * @param model - model.
     * @return - new servlet request with criterion types.
     */
    private void setCriterionTypeElementsInSelector(Model model)
    {
        List<ProfileCriterionType> profileCriterionTypes =
                profileCriterionTypeService.getAll();

        model.addAttribute("profileCriterionTypes", profileCriterionTypes);
    }







    @Autowired
    public void setProfileCriterionGroupService(ProfileCriterionGroupService profileCriterionGroupService) {
        this.profileCriterionGroupService = profileCriterionGroupService;
    }

    @Autowired
    public void setProfileCriterionTypeService(ProfileCriterionTypeService profileCriterionTypeService) {
        this.profileCriterionTypeService = profileCriterionTypeService;
    }

    @Autowired
    public void setProfileCriterionTemplateService(ProfileCriterionTemplateService profileCriterionTemplateService) {
        this.profileCriterionTemplateService = profileCriterionTemplateService;
    }

    @Autowired
    public void setTranscriptService(TranscriptService transcriptService) {
        this.transcriptService = transcriptService;
    }

}
