package ru.innopolis.crm.profile.services.impl;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.innopolis.crm.profile.models.dao.ProfileCriterionDao;
import ru.innopolis.crm.profile.models.dao.ProfileCriterionGroupDao;
import ru.innopolis.crm.profile.models.dao.ProfileCriterionTypeDao;
import ru.innopolis.crm.profile.models.dao.TranscriptDao;
import ru.innopolis.crm.profile.models.dao.exception.ProfileCriterionDaoException;
import ru.innopolis.crm.profile.models.dao.exception.ProfileCriterionGroupDaoException;
import ru.innopolis.crm.profile.models.dao.exception.ProfileCriterionTypeDaoException;
import ru.innopolis.crm.profile.models.dao.exception.TranscriptDaoException;
import ru.innopolis.crm.profile.models.entity.ProfileCriterion;
import ru.innopolis.crm.profile.models.entity.ProfileCriterionGroup;
import ru.innopolis.crm.profile.models.entity.ProfileCriterionType;
import ru.innopolis.crm.profile.services.ProfileCriterionService;
import ru.innopolis.crm.profile.services.exception.ProfileCriterionGroupServiceException;
import ru.innopolis.crm.profile.services.exception.ProfileCriterionServiceException;
import ru.innopolis.crm.profile.services.exception.ProfileCriterionTypeServiceException;
import ru.innopolis.crm.profile.services.exception.TranscriptServiceException;

import java.util.*;

/**
 * Имплементация сервиса ProfileCriterionService
 * Created by admin on 23.04.2017.
 */
@Service
public class ProfileCriterionServiceImpl implements ProfileCriterionService {

    private static final Logger LOGGER = Logger.getLogger(ProfileCriterionServiceImpl.class);

    public ProfileCriterionDao profileCriterionDao;
    public ProfileCriterionGroupDao profileCriterionGroupDao;
    public ProfileCriterionTypeDao profileCriterionTypeDao;
    public TranscriptDao transcriptDao;

    @Autowired
    public void setProfileCriterionDao(ProfileCriterionDao profileCriterionDao) {
        this.profileCriterionDao = profileCriterionDao;
    }

    @Autowired
    public void setProfileCriterionGroupDao(ProfileCriterionGroupDao profileCriterionGroupDao) {
        this.profileCriterionGroupDao = profileCriterionGroupDao;
    }

    @Autowired
    public void setProfileCriterionTypeDao(ProfileCriterionTypeDao profileCriterionTypeDao) {
        this.profileCriterionTypeDao = profileCriterionTypeDao;
    }

    @Autowired
    public void setTranscriptDao(TranscriptDao transcriptDao) {
        this.transcriptDao = transcriptDao;
    }

    @Override
    public HashMap<ProfileCriterionType, TreeSet<ProfileCriterionGroup>> getCriterionTypesAndGroupsById(Long profileId) {
        try {
            List<ProfileCriterion> criteria = profileCriterionDao.getByProfileId(profileId);
            HashMap<Long, LinkedList<Long>> typeGroupFilter = new HashMap<Long, LinkedList<Long>>();

            for (ProfileCriterion criterion :
                    criteria) {
                if (typeGroupFilter.containsKey(criterion.getProfileCriterionTypeId())) {
                    if (!typeGroupFilter.get(criterion.getProfileCriterionTypeId()).contains(criterion.getProfileCriterionGroupId())) {
                        typeGroupFilter.get(criterion.getProfileCriterionTypeId()).add(criterion.getProfileCriterionGroupId());
                    }
                } else {
                    LinkedList<Long> groups = new LinkedList<Long>();
                    groups.add(criterion.getProfileCriterionGroupId());
                    typeGroupFilter.put(criterion.getProfileCriterionTypeId(), groups);
                }
            }

            HashMap<ProfileCriterionType, TreeSet<ProfileCriterionGroup>> profileCriterionTypes = new HashMap<ProfileCriterionType, TreeSet<ProfileCriterionGroup>>();

            for (Map.Entry<Long, LinkedList<Long>> entry :
                    typeGroupFilter.entrySet()) {

                TreeSet<ProfileCriterionGroup> profileCriterionGroups = new TreeSet<ProfileCriterionGroup>();
                for (Long profileCriterionGroupId :
                        entry.getValue()) {
                    profileCriterionGroups.add(profileCriterionGroupDao.getById(profileCriterionGroupId));
                }

                profileCriterionTypes.put(profileCriterionTypeDao.getById(entry.getKey()), profileCriterionGroups);
            }
            return profileCriterionTypes;
        } catch (ProfileCriterionGroupDaoException e) {
            LOGGER.error(e);
            throw new ProfileCriterionGroupServiceException();
        } catch (ProfileCriterionDaoException e) {
            LOGGER.error(e);
            throw new ProfileCriterionServiceException();
        } catch (ProfileCriterionTypeDaoException e) {
            LOGGER.error(e);
            throw new ProfileCriterionTypeServiceException();
        }
    }

    @Override
    public List<ProfileCriterion> getCriteriaWithTranscriptsById(Long profileId) {
        try {
            List<ProfileCriterion> profileCriteria = profileCriterionDao.getByProfileId(profileId);

            for (ProfileCriterion criterion :
                    profileCriteria) {
                criterion.getTranscripts().addAll(transcriptDao.getByProfileCriterionTemplateId(criterion.getProfileCriterionTemplateId()));
            }
            return profileCriteria;
        } catch (ProfileCriterionDaoException e) {
            LOGGER.error(e);
            throw new ProfileCriterionServiceException();
        } catch (TranscriptDaoException e) {
            LOGGER.error(e);
            throw new TranscriptServiceException();
        }
    }

    @Override
    public List<ProfileCriterion> getCriteriaByProfileId(Long profileId) {
        try {
            return profileCriterionDao.getByProfileId(profileId);
        } catch (ProfileCriterionDaoException e) {
            LOGGER.error(e);
            throw new ProfileCriterionServiceException();
        }
    }

    @Override
    public void update(ProfileCriterion profileCriterion) {
        try {
            profileCriterionDao.update(profileCriterion);
        } catch (ProfileCriterionDaoException e) {
            LOGGER.error(e);
            throw new ProfileCriterionServiceException();
        }
    }
}
