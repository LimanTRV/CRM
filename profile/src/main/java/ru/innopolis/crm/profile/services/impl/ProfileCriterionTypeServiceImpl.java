package ru.innopolis.crm.profile.services.impl;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.innopolis.crm.profile.models.dao.ProfileCriterionTypeDao;
import ru.innopolis.crm.profile.models.dao.exception.ProfileCriterionTypeDaoException;
import ru.innopolis.crm.profile.models.entity.ProfileCriterionType;
import ru.innopolis.crm.profile.services.ProfileCriterionTypeService;
import ru.innopolis.crm.profile.services.exception.ProfileCriterionTypeServiceException;

import java.util.List;

/**
 * Имплементация сервиса ProfileCriterionTypeService
 * Created by STC-05 Team [Aleksei Lysov] on 27.04.2017.
 */
@Service
public class ProfileCriterionTypeServiceImpl implements ProfileCriterionTypeService {

    private static final Logger LOGGER = Logger.getLogger(ProfileCriterionTypeServiceImpl.class);
    private ProfileCriterionTypeDao typeDao;

    @Autowired
    public void setTypeDao(ProfileCriterionTypeDao typeDao) {
        this.typeDao = typeDao;
    }

    @Override
    public ProfileCriterionType getById(long id) {

        try {
            return typeDao.getById(id);
        } catch (ProfileCriterionTypeDaoException e) {
            LOGGER.error(e);
            throw new ProfileCriterionTypeServiceException();
        }
    }

    @Override
    public List<ProfileCriterionType> getAll() {
        try {
            return typeDao.getAll();
        } catch (ProfileCriterionTypeDaoException e) {
            LOGGER.error(e);
            throw new ProfileCriterionTypeServiceException();
        }
    }

    @Override
    public ProfileCriterionType save(ProfileCriterionType entity) {
        try {
            return typeDao.save(entity);
        } catch (ProfileCriterionTypeDaoException e) {
            LOGGER.error(e);
            throw new ProfileCriterionTypeServiceException();
        }
    }

    @Override
    public long create(ProfileCriterionType entity) {
        try {
            return typeDao.create(entity);
        } catch (ProfileCriterionTypeDaoException e) {
            LOGGER.error(e);
            throw new ProfileCriterionTypeServiceException();
        }
    }

    @Override
    public int update(ProfileCriterionType entity) {
        try {
            return typeDao.update(entity);
        } catch (ProfileCriterionTypeDaoException e) {
            LOGGER.error(e);
            throw new ProfileCriterionTypeServiceException();
        }
    }

    @Override
    public int delete(ProfileCriterionType entity) {
        try {
            return typeDao.delete(entity);
        } catch (ProfileCriterionTypeDaoException e) {
            LOGGER.error(e);
            throw new ProfileCriterionTypeServiceException();
        }
    }
}
