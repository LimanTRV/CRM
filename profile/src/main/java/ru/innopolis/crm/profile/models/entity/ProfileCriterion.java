package ru.innopolis.crm.profile.models.entity;

import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;

/**
 * Класс сущность для таблицы profile.questionnaire_criterion_instance
 * Created by admin on 21.04.2017.
 */
public class ProfileCriterion implements Comparable<ProfileCriterion>{
    private long id;
    private long profileId;
    private String name;
    private long maxGrade;
    private long grade;
    private long profileCriterionTypeId;
    private long profileCriterionGroupId;
    private long editUserId;
    private Date editDate;
    private long profileCriterionTemplateId;
    private boolean isDeleted;
    private LinkedList<Transcript> transcripts = new LinkedList<Transcript>();
    private ProfileCriterionType profileCriterionType;
    private ProfileCriterionGroup profileCriterionGroup;


    public ProfileCriterion(long id, long profileId, String name, long maxGrade, long grade,
                            long profileCriterionTypeId, long profileCriterionGroupId,
                            long editUserId, Date editDate, long profileCriterionTemplateId, boolean isDeleted){

        this.id = id;
        this.profileId = profileId;
        this.name = name;
        this.maxGrade = maxGrade;
        this.grade = grade;
        this.profileCriterionTypeId = profileCriterionTypeId;
        this.profileCriterionGroupId = profileCriterionGroupId;
        this.editUserId = editUserId;
        this.editDate = editDate;
        this.profileCriterionTemplateId = profileCriterionTemplateId;
        this.isDeleted = isDeleted;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getProfileId() {
        return profileId;
    }

    public void setProfileId(long profileId) {
        this.profileId = profileId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getMaxGrade() {
        return maxGrade;
    }

    public void setMaxGrade(long maxGrade) {
        this.maxGrade = maxGrade;
    }

    public long getGrade() {
        return grade;
    }

    public void setGrade(long grade) {
        this.grade = grade;
    }

    public long getProfileCriterionTypeId() {
        return profileCriterionTypeId;
    }

    public void setProfileCriterionTypeId(long profileCriterionTypeId) {
        this.profileCriterionTypeId = profileCriterionTypeId;
    }

    public long getProfileCriterionGroupId() {
        return profileCriterionGroupId;
    }

    public void setProfileCriterionGroupId(long profileCriterionGroupId) {
        this.profileCriterionGroupId = profileCriterionGroupId;
    }

    public long getEditUserId() {
        return editUserId;
    }

    public void setEditUserId(long editUserId) {
        this.editUserId = editUserId;
    }

    public Date getEditDate() {
        return editDate;
    }

    public void setEditDate(Date editDate) {
        this.editDate = editDate;
    }

    public boolean isDeleted() {
        return isDeleted;
    }

    public void setDeleted(boolean deleted) {
        isDeleted = deleted;
    }

    public long getProfileCriterionTemplateId() {
        return profileCriterionTemplateId;
    }

    public void setProfileCriterionTemplateId(long profileCriterionTemplateId) {
        this.profileCriterionTemplateId = profileCriterionTemplateId;
    }

    public LinkedList<Transcript> getTranscripts() {
        return transcripts;
    }

    @Override
    public int compareTo(ProfileCriterion profileCriterion) {
        return this.name.compareTo(profileCriterion.getName());
    }

    public ProfileCriterionType getProfileCriterionType() {
        return profileCriterionType;
    }

    public void setProfileCriterionType(ProfileCriterionType profileCriterionType) {
        this.profileCriterionType = profileCriterionType;
    }

    public ProfileCriterionGroup getProfileCriterionGroup() {
        return profileCriterionGroup;
    }

    public void setProfileCriterionGroup(ProfileCriterionGroup profileCriterionGroup) {
        this.profileCriterionGroup = profileCriterionGroup;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ProfileCriterion criterion = (ProfileCriterion) o;

        if (id != criterion.id) return false;
        if (profileId != criterion.profileId) return false;
        if (maxGrade != criterion.maxGrade) return false;
        if (grade != criterion.grade) return false;
        if (profileCriterionTypeId != criterion.profileCriterionTypeId) return false;
        if (profileCriterionGroupId != criterion.profileCriterionGroupId) return false;
        if (editUserId != criterion.editUserId) return false;
        if (profileCriterionTemplateId != criterion.profileCriterionTemplateId) return false;
        if (isDeleted != criterion.isDeleted) return false;
        if (name != null ? !name.equals(criterion.name) : criterion.name != null) return false;
        if (editDate != null ? !editDate.equals(criterion.editDate) : criterion.editDate != null) return false;
        return transcripts != null ? transcripts.equals(criterion.transcripts) : criterion.transcripts == null;
    }

    @Override
    public int hashCode() {
        int result = (int) (id ^ (id >>> 32));
        result = 31 * result + (int) (profileId ^ (profileId >>> 32));
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (int) (maxGrade ^ (maxGrade >>> 32));
        result = 31 * result + (int) (grade ^ (grade >>> 32));
        result = 31 * result + (int) (profileCriterionTypeId ^ (profileCriterionTypeId >>> 32));
        result = 31 * result + (int) (profileCriterionGroupId ^ (profileCriterionGroupId >>> 32));
        result = 31 * result + (int) (editUserId ^ (editUserId >>> 32));
        result = 31 * result + (editDate != null ? editDate.hashCode() : 0);
        result = 31 * result + (int) (profileCriterionTemplateId ^ (profileCriterionTemplateId >>> 32));
        result = 31 * result + (isDeleted ? 1 : 0);
        result = 31 * result + (transcripts != null ? transcripts.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "ProfileCriterion{" +
                "id=" + id +
                ", profileId=" + profileId +
                ", name='" + name + '\'' +
                ", maxGrade=" + maxGrade +
                ", grade=" + grade +
                ", profileCriterionTypeId=" + profileCriterionTypeId +
                ", profileCriterionGroupId=" + profileCriterionGroupId +
                ", editUserId=" + editUserId +
                ", editDate=" + editDate +
                ", profileCriterionTemplateId=" + profileCriterionTemplateId +
                ", isDeleted=" + isDeleted +
                ", transcripts=" + transcripts +
                ", profileCriterionType=" + profileCriterionType +
                ", profileCriterionGroup=" + profileCriterionGroup +
                '}';
    }
}
