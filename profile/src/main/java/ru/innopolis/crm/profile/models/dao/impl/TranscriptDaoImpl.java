package ru.innopolis.crm.profile.models.dao.impl;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import ru.innopolis.crm.profile.models.dao.TranscriptDao;
import ru.innopolis.crm.profile.models.dao.exception.TranscriptDaoException;
import ru.innopolis.crm.profile.models.entity.Transcript;

import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Имплементация DAO для TranscriptDao
 * Created by admin on 21.04.2017.
 */
@Repository
public class TranscriptDaoImpl implements TranscriptDao {

    private static final Logger LOGGER = Logger.getLogger(ProfileCriterionTemplateDaoImpl.class);

    private static final String SELECT_ALL = "SELECT * FROM profile.transcript WHERE NOT is_deleted";

    private static final String INSERT = "INSERT INTO profile.transcript " +
            "(questionnaire_criterion_id, min_value, max_value, description, text_value, is_deleted) " +
            "VALUES (?, ?, ?, ?, ?, ?)";

    private static final String UPDATE = "UPDATE profile.transcript " +
            "SET questionnaire_criterion_id = ?, min_value = ?, max_value = ?, description = ?, " +
            "text_value = ?, is_deleted = ? WHERE id = ?;";

    private static final String DELETE = "UPDATE profile.transcript " +
            "SET is_deleted = TRUE WHERE id = ?;";

    private DataSource dataSource;

    @Autowired
    public void setDataSource(DataSource dataSource) {
        this.dataSource = dataSource;
    }


    public Transcript getById(long id) throws TranscriptDaoException {
        Transcript transcript = null;

        try {
            Connection connection = dataSource.getConnection();
            PreparedStatement statement = connection.prepareStatement(SELECT_ALL + " AND id = ?");
            statement.setLong(1, id);
            ResultSet resultSet = statement.executeQuery();
            if(resultSet.next()){
                transcript = createTranscript(resultSet);
            }
        } catch (SQLException e) {
            LOGGER.error(e);
            throw new TranscriptDaoException();
        }

        return transcript;
    }

    @Override
    public List<Transcript> getByProfileCriterionTemplateId(long profileCriterionTemplateId) throws TranscriptDaoException {
        List<Transcript> list = new ArrayList<Transcript>();

        try {
            Connection connection = dataSource.getConnection();
            PreparedStatement statement = connection.prepareStatement(SELECT_ALL + " AND questionnaire_criterion_id = ?");
            statement.setLong(1, profileCriterionTemplateId);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                list.add(createTranscript(resultSet));
            }
        } catch (SQLException e) {
            LOGGER.error(e);
            throw new TranscriptDaoException();
        }

        return list;
    }

    public List<Transcript> getAll() throws TranscriptDaoException {
        List<Transcript> list = new ArrayList<>();

        try {
            Connection connection = dataSource.getConnection();
            PreparedStatement statement = connection.prepareStatement(SELECT_ALL);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                list.add(createTranscript(resultSet));
            }
        } catch (SQLException e) {
            LOGGER.error(e);
            throw new TranscriptDaoException();
        }

        return list;
    }

    public Transcript save(Transcript transcript) throws TranscriptDaoException {
        if (transcript == null) {
            return null;
        }

        if(transcript.getId() > 0){
            update(transcript);
        } else {
            long newId = create(transcript);
            transcript.setId(newId);
        }

        return transcript;
    }

    public long create(Transcript transcript) throws TranscriptDaoException {
        long lastId = 0;

        if (transcript == null) {
            LOGGER.error("TranscriptDao.create(null) called.");
            return lastId;
        }

        if (transcript.getId() > 0){
            LOGGER.error("Tried TranscriptDao.create() object with existing id.");
            return transcript.getId();
        }

        try {
            Connection connection = dataSource.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(INSERT, Statement.RETURN_GENERATED_KEYS);

            preparedStatement.setLong(1, transcript.getProfileCriterionTemplateId());
            preparedStatement.setLong(2, transcript.getMinGrade());
            preparedStatement.setLong(3, transcript.getMaxGrade());
            preparedStatement.setString(4, transcript.getDescription());
            preparedStatement.setString(5, transcript.getTextValue());
            preparedStatement.setBoolean(6, transcript.isDeleted());

            preparedStatement.executeUpdate();
            ResultSet resultSet = preparedStatement.getGeneratedKeys();
            if (resultSet.next()) {
                lastId = resultSet.getInt(1);
            }
        } catch (SQLException e) {
            LOGGER.error(e);
            throw new TranscriptDaoException();
        }
        return lastId;
    }

    public int update(Transcript transcript) throws TranscriptDaoException {
        int count = 0;

        if (transcript == null) {
            LOGGER.error("TranscriptDao.update(null) called.");
            return count;
        }

        try {
            Connection connection = dataSource.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(UPDATE);
            preparedStatement.setLong(1, transcript.getProfileCriterionTemplateId());
            preparedStatement.setLong(2, transcript.getMinGrade());
            preparedStatement.setLong(3, transcript.getMaxGrade());
            preparedStatement.setString(4, transcript.getDescription());
            preparedStatement.setString(5, transcript.getTextValue());
            preparedStatement.setBoolean(6, transcript.isDeleted());
            preparedStatement.setLong(7, transcript.getId());

            count = preparedStatement.executeUpdate();
        } catch (SQLException e) {
            LOGGER.error(e);
            throw new TranscriptDaoException();
        }
        return count;
    }

    public int delete(Transcript transcript) throws TranscriptDaoException {
        int count = 0;

        if (transcript == null) {
            LOGGER.error("TranscriptDao.delete(null) called.");
            return count;
        }

        try {
            Connection connection = dataSource.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(DELETE);
            preparedStatement.setLong(1, transcript.getId());

            count = preparedStatement.executeUpdate();

        } catch (SQLException e) {
            LOGGER.error(e);
            throw new TranscriptDaoException();
        }
        return count;
    }

    private Transcript createTranscript(ResultSet resultSet) throws SQLException {
        return  new Transcript(
                resultSet.getLong("id"),
                resultSet.getLong("questionnaire_criterion_id"),
                resultSet.getLong("min_value"),
                resultSet.getLong("max_value"),
                resultSet.getString("description"),
                resultSet.getString("text_value"),
                resultSet.getBoolean("is_deleted")
        );
    }
}
