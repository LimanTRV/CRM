package ru.innopolis.crm.profile.models.entity;

/**
 * Класс сущность для таблицы profile.transcript
 * Created by admin on 21.04.2017.
 */
public class Transcript {
    private long id;
    private long profileCriterionTemplateId;
    private long minGrade;
    private long maxGrade;
    private String description;
    private String textValue;
    private boolean isDeleted;


    public Transcript() {
    }

    public Transcript(long id, long profileCriterionTemplateId, long minGrade, long maxGrade,
                      String description, String textValue, boolean isDeleted){

        this.id = id;
        this.profileCriterionTemplateId = profileCriterionTemplateId;
        this.minGrade = minGrade;
        this.maxGrade = maxGrade;
        this.description = description;
        this.textValue = textValue;
        this.isDeleted = isDeleted;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getProfileCriterionTemplateId() {
        return profileCriterionTemplateId;
    }

    public void setProfileCriterionTemplateId(long profileCriterionTemplateId) {
        this.profileCriterionTemplateId = profileCriterionTemplateId;
    }

    public long getMinGrade() {
        return minGrade;
    }

    public void setMinGrade(long minGrade) {
        this.minGrade = minGrade;
    }

    public long getMaxGrade() {
        return maxGrade;
    }

    public void setMaxGrade(long maxGrade) {
        this.maxGrade = maxGrade;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getTextValue() {
        return textValue;
    }

    public void setTextValue(String textValue) {
        this.textValue = textValue;
    }

    public boolean isDeleted() {
        return isDeleted;
    }

    public void setDeleted(boolean deleted) {
        isDeleted = deleted;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Transcript that = (Transcript) o;

        if (id != that.id) return false;
        if (profileCriterionTemplateId != that.profileCriterionTemplateId) return false;
        if (minGrade != that.minGrade) return false;
        if (maxGrade != that.maxGrade) return false;
        if (isDeleted != that.isDeleted) return false;
        if (description != null ? !description.equals(that.description) : that.description != null) return false;
        return textValue != null ? textValue.equals(that.textValue) : that.textValue == null;
    }

    @Override
    public int hashCode() {
        int result = (int) (id ^ (id >>> 32));
        result = 31 * result + (int) (profileCriterionTemplateId ^ (profileCriterionTemplateId >>> 32));
        result = 31 * result + (int) (minGrade ^ (minGrade >>> 32));
        result = 31 * result + (int) (maxGrade ^ (maxGrade >>> 32));
        result = 31 * result + (description != null ? description.hashCode() : 0);
        result = 31 * result + (textValue != null ? textValue.hashCode() : 0);
        result = 31 * result + (isDeleted ? 1 : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Transcript{" +
                "id=" + id +
                ", profileCriterionTemplateId=" + profileCriterionTemplateId +
                ", minGrade=" + minGrade +
                ", maxGrade=" + maxGrade +
                ", description='" + description + '\'' +
                ", textValue='" + textValue + '\'' +
                ", isDeleted=" + isDeleted +
                '}';
    }
}
