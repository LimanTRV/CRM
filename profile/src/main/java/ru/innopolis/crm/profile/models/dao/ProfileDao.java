package ru.innopolis.crm.profile.models.dao;

import ru.innopolis.crm.profile.models.dao.exception.ProfileDaoException;
import ru.innopolis.crm.profile.models.entity.Profile;

import java.util.List;

/**
 * DAO для сущности Profile
 * Created by admin on 21.04.2017.
 */
public interface ProfileDao {

    Profile getById(long id) throws ProfileDaoException;

    Profile getByStudentId(long id) throws ProfileDaoException;

    List<Profile> getAll() throws ProfileDaoException;

    Profile save(Profile entity) throws ProfileDaoException;

    long create(Profile entity) throws ProfileDaoException;

    int update(Profile entity) throws ProfileDaoException;

    int delete(Profile entity) throws ProfileDaoException;
}
