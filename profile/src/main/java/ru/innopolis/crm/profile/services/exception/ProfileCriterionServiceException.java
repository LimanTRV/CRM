package ru.innopolis.crm.profile.services.exception;

/**
 * Exception для ProfileCriterionService
 * Created by STC-05 Team [Aleksei Lysov] on 04.05.2017.
 */
public class ProfileCriterionServiceException extends RuntimeException {
}
