package ru.innopolis.crm.profile.services;

import ru.innopolis.crm.profile.models.entity.Profile;
import ru.innopolis.crm.profile.models.entity.ProfileCriterion;
import ru.innopolis.crm.profile.models.entity.ProfileTemplate;

import java.util.List;

/**
 * Сервис для работы с сущностью ProfileTemplate
 * Created by admin on 25.04.2017.
 */
public interface ProfileTemplateService {
    Profile createFromDefault(Long studentId);

    Profile createFromTemplate(Long templateId, Long studentId);

    ProfileTemplate getById(long id);

    List<ProfileTemplate> getAll();

    ProfileTemplate save(ProfileTemplate entity);

    long create(ProfileTemplate entity);

    int update(ProfileTemplate entity);

    int delete(ProfileTemplate entity);

    ProfileTemplate getDefault();

    long saveWithCriterionAndUpdateDefault(ProfileTemplate entity, String[] criterionIds);
}
