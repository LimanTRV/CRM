package ru.innopolis.crm.profile.services.impl;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.innopolis.crm.profile.models.dao.ProfileCriterionGroupDao;
import ru.innopolis.crm.profile.models.dao.exception.ProfileCriterionGroupDaoException;
import ru.innopolis.crm.profile.models.entity.ProfileCriterionGroup;
import ru.innopolis.crm.profile.services.ProfileCriterionGroupService;
import ru.innopolis.crm.profile.services.exception.ProfileCriterionGroupServiceException;

import java.util.List;

/**
 * Имплементация сервиса ProfileCriterionGroupService
 * Created by STC-05 Team [Aleksei Lysov] on 27.04.2017.
 */
@Service
public class ProfileCriterionGroupServiceImpl implements ProfileCriterionGroupService {

    private static final Logger LOGGER = Logger.getLogger(ProfileCriterionGroupServiceImpl.class);
    private ProfileCriterionGroupDao groupDao;

    @Autowired
    public void setGroupDao(ProfileCriterionGroupDao groupDao) {
        this.groupDao = groupDao;
    }

    @Override
    public ProfileCriterionGroup getById(long id) {
        try {
            return groupDao.getById(id);
        } catch (ProfileCriterionGroupDaoException e) {
            LOGGER.error(e);
            throw new ProfileCriterionGroupServiceException();
        }
    }

    @Override
    public List<ProfileCriterionGroup> getAll() {
        try {
            return groupDao.getAll();
        } catch (ProfileCriterionGroupDaoException e) {
            LOGGER.error(e);
            throw new ProfileCriterionGroupServiceException();
        }
    }

    @Override
    public ProfileCriterionGroup save(ProfileCriterionGroup entity) {
        try {
            return groupDao.save(entity);
        } catch (ProfileCriterionGroupDaoException e) {
            LOGGER.error(e);
            throw new ProfileCriterionGroupServiceException();
        }
    }

    @Override
    public long create(ProfileCriterionGroup entity) {
        try {
            return groupDao.create(entity);
        } catch (ProfileCriterionGroupDaoException e) {
            LOGGER.error(e);
            throw new ProfileCriterionGroupServiceException();
        }
    }

    @Override
    public int update(ProfileCriterionGroup entity) {
        try {
            return groupDao.update(entity);
        } catch (ProfileCriterionGroupDaoException e) {
            LOGGER.error(e);
            throw new ProfileCriterionGroupServiceException();
        }
    }

    @Override
    public int delete(ProfileCriterionGroup entity) {
        try {
            return groupDao.delete(entity);
        } catch (ProfileCriterionGroupDaoException e) {
            LOGGER.error(e);
            throw new ProfileCriterionGroupServiceException();
        }
    }
}
