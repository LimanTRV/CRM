package ru.innopolis.crm.profile.controllers;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import ru.innopolis.crm.profile.models.entity.ProfileCriterionType;
import ru.innopolis.crm.profile.services.ProfileCriterionTypeService;
import ru.innopolis.crm.profile.services.impl.ProfileCriterionTypeServiceImpl;
import ru.innopolis.crm.profile.util.ProfileHelper;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * Контроллер для обработки страницы по адресу /profile/type
 * Created by STC-05 Team [Aleksei Lysov] on 26.04.2017.
 */
@Controller
@RequestMapping(value = "/profile/type")
public class ProfileTypeController{

    private ProfileCriterionTypeService typeService;
    private static final Logger LOGGER = Logger.getLogger(ProfileTypeController.class);

    @Autowired
    public void setTypeService(ProfileCriterionTypeService typeService) {
        this.typeService = typeService;
    }

    @PreAuthorize(value = "hasRole('PC_E') or hasRole('PC_V')")
    @RequestMapping(method = RequestMethod.GET)
    public ModelAndView doGet(
            @RequestParam(value = "edit", required = false) String editId,
            @RequestParam(value = "delete", required = false) String deleteId,
            @RequestParam(value = "group", required = false) String groupId,
            HttpServletRequest req){

        ModelAndView model = new ModelAndView();
        ProfileHelper.checkSuccessOrErrorMessage(req);

        List<ProfileCriterionType> types = typeService.getAll();
        model.addObject("types", types);

        if (ProfileHelper.isCorrectId(editId))
            initEditOperation(model, Long.valueOf(editId));

        model.setViewName("profile/profile_type");

        if (ProfileHelper.isCorrectId(deleteId)) {
            boolean isDelete = deleteOperation(Long.valueOf(deleteId));
            ProfileHelper.setSessionAfterDeleteData(req, isDelete);
            model.setViewName("redirect:/profile/type?group=" + groupId);
        }

        return model;
    }

    @PreAuthorize(value = "hasRole('PC_V')")
    @RequestMapping(method = RequestMethod.POST)
    public String doPost(
            @RequestParam(value = "name") String name,
            @RequestParam(value = "max") String max,
            @RequestParam(value = "typeId", required = false) String typeId,
            @RequestParam(value = "group", required = false) String groupId,
            HttpServletRequest req ){

        ProfileCriterionType type = null;

        if (ProfileHelper.isCorrectId(typeId))
            type = typeService.getById(Long.valueOf(typeId));

        boolean isSave = saveOperation(type, name, Long.valueOf(max));
        ProfileHelper.setSessionAfterSaveData(req, isSave);

        return "redirect:/profile/type?group=" + groupId;
    }

    private void initEditOperation(ModelAndView model, long editId) {
        ProfileCriterionType editType = typeService.getById(editId);
        if (editType != null)
            model.addObject("editType", editType);
    }

    private boolean deleteOperation(long deleteId) {
        return typeService.delete(typeService.getById(deleteId)) > 0;
    }

    private boolean saveOperation(ProfileCriterionType type, String name, long max) {
        long countUpdate = 0;

        if (type == null) {
            type = new ProfileCriterionType(0, name, max, false);
            countUpdate = typeService.create(type);
        } else {
            type.setName(name);
            type.setMaxGrade(max);
            countUpdate = typeService.update(type);
        }

        return countUpdate > 0;
    }
}
