package ru.innopolis.crm.profile.services;

import ru.innopolis.crm.profile.models.entity.ProfileCriterionGroup;

import java.util.List;

/**
 * Сервис для работы с сущностью ProfileCriterionGroup
 * Created by STC-05 Team [Aleksei Lysov] on 27.04.2017.
 */
public interface ProfileCriterionGroupService {
    /**
     * Метод для получения сущности по id
     *
     * @param id
     * @return объект ProfileCriterionGroup
     */
    ProfileCriterionGroup getById(long id);

    /**
     * Метод для получения всех сущностей из БД
     *
     * @return список ProfileCriterionGroup
     */
    List<ProfileCriterionGroup> getAll();

    /**
     * Метод сохраняет или обновляет сущность в БД
     *
     * @param entity
     * @return ProfileCriterionGroup
     */
    ProfileCriterionGroup save(ProfileCriterionGroup entity);

    /**
     * Метод дл записи сущности в БД
     *
     * @param entity
     * @return id сохраненой сущности
     */
    long create(ProfileCriterionGroup entity);

    /**
     * Метод для обновдения сущностив БД
     *
     * @param entity
     * @return количество обновленных строк в БД
     */
    int update(ProfileCriterionGroup entity);

    /**
     * Метод удаления сущности из БД
     *
     * @param entity
     * @return количество обновленных строк в БД
     */
    int delete(ProfileCriterionGroup entity);
}
