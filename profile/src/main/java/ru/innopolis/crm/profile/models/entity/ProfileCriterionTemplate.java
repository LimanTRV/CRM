package ru.innopolis.crm.profile.models.entity;

import java.util.Date;

/**
 * Класс сущность для таблицы profile.questionnaire_criterion
 * Данная сущность представляет из себя связь шаблона анкеты с критериями анкеты
 * Created by admin on 21.04.2017.
 */
public class ProfileCriterionTemplate {
    private long id;
    private String name;
    private String description;
    private long profileCriterionTypeId;
    private long profileCriterionGroupId;
    private long createUserId;
    private long editUserId;
    private Date editDate;
    private boolean isDeleted;
    private ProfileCriterionType profileCriterionType;
    private ProfileCriterionGroup profileCriterionGroup;

    public ProfileCriterionTemplate() {
    }

    public ProfileCriterionTemplate(long id, String name, String description,
                                    long profileCriterionTypeId, long profileCriterionGroupId,
                                    long createUserId, long editUserId, Date editDate, boolean isDeleted){

        this.id = id;
        this.name = name;
        this.description = description;
        this.profileCriterionTypeId = profileCriterionTypeId;
        this.profileCriterionGroupId = profileCriterionGroupId;
        this.createUserId = createUserId;
        this.editUserId = editUserId;
        this.editDate = editDate;
        this.isDeleted = isDeleted;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public long getProfileCriterionTypeId() {
        return profileCriterionTypeId;
    }

    public void setProfileCriterionTypeId(long profileCriterionTypeId) {
        this.profileCriterionTypeId = profileCriterionTypeId;
    }

    public long getProfileCriterionGroupId() {
        return profileCriterionGroupId;
    }

    public void setProfileCriterionGroupId(long profileCriterionGroupId) {
        this.profileCriterionGroupId = profileCriterionGroupId;
    }

    public long getCreateUserId() {
        return createUserId;
    }

    public void setCreateUserId(long createUserId) {
        this.createUserId = createUserId;
    }

    public long getEditUserId() {
        return editUserId;
    }

    public void setEditUserId(long editUserId) {
        this.editUserId = editUserId;
    }

    public Date getEditDate() {
        return editDate;
    }

    public void setEditDate(Date editDate) {
        this.editDate = editDate;
    }

    public boolean isDeleted() {
        return isDeleted;
    }

    public void setDeleted(boolean deleted) {
        isDeleted = deleted;
    }

    public ProfileCriterionType getProfileCriterionType() {
        return profileCriterionType;
    }

    public void setProfileCriterionType(ProfileCriterionType profileCriterionType) {
        this.profileCriterionType = profileCriterionType;
    }

    public ProfileCriterionGroup getProfileCriterionGroup() {
        return profileCriterionGroup;
    }

    public void setProfileCriterionGroup(ProfileCriterionGroup profileCriterionGroup) {
        this.profileCriterionGroup = profileCriterionGroup;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ProfileCriterionTemplate that = (ProfileCriterionTemplate) o;

        if (id != that.id) return false;
        if (profileCriterionTypeId != that.profileCriterionTypeId) return false;
        if (profileCriterionGroupId != that.profileCriterionGroupId) return false;
        if (createUserId != that.createUserId) return false;
        if (editUserId != that.editUserId) return false;
        if (isDeleted != that.isDeleted) return false;
        if (name != null ? !name.equals(that.name) : that.name != null) return false;
        if (description != null ? !description.equals(that.description) : that.description != null) return false;
        return editDate != null ? editDate.equals(that.editDate) : that.editDate == null;
    }

    @Override
    public int hashCode() {
        int result = (int) (id ^ (id >>> 32));
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (description != null ? description.hashCode() : 0);
        result = 31 * result + (int) (profileCriterionTypeId ^ (profileCriterionTypeId >>> 32));
        result = 31 * result + (int) (profileCriterionGroupId ^ (profileCriterionGroupId >>> 32));
        result = 31 * result + (int) (createUserId ^ (createUserId >>> 32));
        result = 31 * result + (int) (editUserId ^ (editUserId >>> 32));
        result = 31 * result + (editDate != null ? editDate.hashCode() : 0);
        result = 31 * result + (isDeleted ? 1 : 0);
        return result;
    }

    @Override
    public String toString() {
        return "ProfileCriterionTemplate{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", profileCriterionTypeId=" + profileCriterionTypeId +
                ", profileCriterionGroupId=" + profileCriterionGroupId +
                ", createUserId=" + createUserId +
                ", editUserId=" + editUserId +
                ", editDate=" + editDate +
                ", isDeleted=" + isDeleted +
                ", profileCriterionType=" + profileCriterionType +
                ", profileCriterionGroup=" + profileCriterionGroup +
                '}';
    }
}
