INSERT INTO profile.criterion_type (id, name, is_deleted, max_grade) VALUES (1, 'Технические навыки', false, 5);
INSERT INTO profile.criterion_type (id, name, is_deleted, max_grade) VALUES (2, 'Личностные характеристики', false, 3);

INSERT INTO profile.criterion_group (id, name, is_deleted) VALUES (1, 'Группа1', false);
INSERT INTO profile.criterion_group (id, name, is_deleted) VALUES (2, 'Группа2', false);
INSERT INTO profile.criterion_group (id, name, is_deleted) VALUES (3, 'Группа3', false);
INSERT INTO profile.criterion_group (id, name, is_deleted) VALUES (4, 'Группа4', false);

INSERT INTO profile.questionnaire_instance (id, name, student_id, is_deleted, edit_user_id, is_finished) VALUES (1, 'Анкета студента: Иван Иванов', 1, false, 2, false);

INSERT INTO profile.questionnaire_criterion (id, name, description, criterion_type_id, criterion_group_id, crm_user_id_created, crm_user_id_edited, edit_date, is_deleted)
VALUES (1, 'Навык1', 'Навык1', 1, 1, 2, null, null, false);
INSERT INTO profile.questionnaire_criterion (id, name, description, criterion_type_id, criterion_group_id, crm_user_id_created, crm_user_id_edited, edit_date, is_deleted)
VALUES (2, 'Навык2', 'Навык2', 1, 1, 2, null, null, false);
INSERT INTO profile.questionnaire_criterion (id, name, description, criterion_type_id, criterion_group_id, crm_user_id_created, crm_user_id_edited, edit_date, is_deleted)
VALUES (3, 'Навык3', 'Навык3', 1, 1, 2, null, null, false);
INSERT INTO profile.questionnaire_criterion (id, name, description, criterion_type_id, criterion_group_id, crm_user_id_created, crm_user_id_edited, edit_date, is_deleted)
VALUES (4, 'Навык4', 'Навык4', 1, 1, 2, null, null, false);
INSERT INTO profile.questionnaire_criterion (id, name, description, criterion_type_id, criterion_group_id, crm_user_id_created, crm_user_id_edited, edit_date, is_deleted)
VALUES (5, 'Навык5', 'Навык5', 1, 2, 2, null, null, false);
INSERT INTO profile.questionnaire_criterion (id, name, description, criterion_type_id, criterion_group_id, crm_user_id_created, crm_user_id_edited, edit_date, is_deleted)
VALUES (6, 'Навык6', 'Навык6', 1, 2, 2, null, null, false);
INSERT INTO profile.questionnaire_criterion (id, name, description, criterion_type_id, criterion_group_id, crm_user_id_created, crm_user_id_edited, edit_date, is_deleted)
VALUES (7, 'Характеристика1', 'Характеристика1', 2, 3, 2, null, null, false);
INSERT INTO profile.questionnaire_criterion (id, name, description, criterion_type_id, criterion_group_id, crm_user_id_created, crm_user_id_edited, edit_date, is_deleted)
VALUES (8, 'Характеристика2', 'Характеристика2', 2, 3, 2, null, null, false);
INSERT INTO profile.questionnaire_criterion (id, name, description, criterion_type_id, criterion_group_id, crm_user_id_created, crm_user_id_edited, edit_date, is_deleted)
VALUES (9, 'Характеристика3', 'Характеристика3', 2, 3, 2, null, null, false);
INSERT INTO profile.questionnaire_criterion (id, name, description, criterion_type_id, criterion_group_id, crm_user_id_created, crm_user_id_edited, edit_date, is_deleted)
VALUES (10, 'Характеристика4', 'Характеристика4', 2, 4, 2, null, null, false);
INSERT INTO profile.questionnaire_criterion (id, name, description, criterion_type_id, criterion_group_id, crm_user_id_created, crm_user_id_edited, edit_date, is_deleted)
VALUES (11, 'Характеристика5', 'Характеристика5', 2, 4, 2, null, null, false);
INSERT INTO profile.questionnaire_criterion (id, name, description, criterion_type_id, criterion_group_id, crm_user_id_created, crm_user_id_edited, edit_date, is_deleted)
VALUES (12, 'Характеристика6', 'Характеристика6', 2, 4, 2, null, null, false);

INSERT INTO profile.transcript (questionnaire_criterion_id, min_value, max_value, description, text_value, is_deleted)
VALUES (1, 1, 1, 'Описание балла 1 в Навыке1', 'Детализация балла 1 в Навыке1', false);
INSERT INTO profile.transcript (questionnaire_criterion_id, min_value, max_value, description, text_value, is_deleted)
VALUES (1, 2, 2, 'Описание балла 2 в Навыке1', 'Детализация балла 2 в Навыке1', false);
INSERT INTO profile.transcript (questionnaire_criterion_id, min_value, max_value, description, text_value, is_deleted)
VALUES (1, 3, 3, 'Описание балла 3 в Навыке1', 'Детализация балла 3 в Навыке1', false);
INSERT INTO profile.transcript (questionnaire_criterion_id, min_value, max_value, description, text_value, is_deleted)
VALUES (1, 4, 4, 'Описание балла 4 в Навыке1', 'Детализация балла 4 в Навыке1', false);
INSERT INTO profile.transcript (questionnaire_criterion_id, min_value, max_value, description, text_value, is_deleted)
VALUES (1, 5, 5, 'Описание балла 5 в Навыке1', 'Детализация балла 5 в Навыке1', false);

INSERT INTO profile.transcript (questionnaire_criterion_id, min_value, max_value, description, text_value, is_deleted)
VALUES (2, 1, 3, 'Описание баллов 1-3 в Навыке2', 'Детализация баллов 1-3 в Навыке2', false);
INSERT INTO profile.transcript (questionnaire_criterion_id, min_value, max_value, description, text_value, is_deleted)
VALUES (2, 4, 5, 'Описание баллов 4-5 в Навыке2', 'Детализация баллов 4-5 в Навыке2', false);

INSERT INTO profile.transcript (questionnaire_criterion_id, min_value, max_value, description, text_value, is_deleted)
VALUES (3, 1, 5, 'Описание баллов 1-5 в Навыке3', 'Детализация баллов 1-5 в Навыке3', false);

INSERT INTO profile.transcript (questionnaire_criterion_id, min_value, max_value, description, text_value, is_deleted)
VALUES (4, 1, 5, 'Описание баллов 1-5 в Навыке4', 'Детализация баллов 1-5 в Навыке4', false);

INSERT INTO profile.transcript (questionnaire_criterion_id, min_value, max_value, description, text_value, is_deleted)
VALUES (5, 1, 5, 'Описание баллов 1-5 в Навыке5', 'Детализация баллов 1-5 в Навыке5', false);

INSERT INTO profile.transcript (questionnaire_criterion_id, min_value, max_value, description, text_value, is_deleted)
VALUES (6, 1, 5, 'Описание баллов 1-5 в Навыке6', 'Детализация баллов 1-5 в Навыке6', false);

INSERT INTO profile.transcript (questionnaire_criterion_id, min_value, max_value, description, text_value, is_deleted)
VALUES (7, 1, 1, 'Описание балла 1 в Характеристике1', 'Детализация балла 1 в Характеристике1', false);
INSERT INTO profile.transcript (questionnaire_criterion_id, min_value, max_value, description, text_value, is_deleted)
VALUES (7, 2, 2, 'Описание балла 2 в Характеристике1', 'Детализация балла 2 в Характеристике1', false);
INSERT INTO profile.transcript (questionnaire_criterion_id, min_value, max_value, description, text_value, is_deleted)
VALUES (7, 3, 3, 'Описание балла 3 в Характеристике1', 'Детализация балла 3 в Характеристике1', false);

INSERT INTO profile.transcript (questionnaire_criterion_id, min_value, max_value, description, text_value, is_deleted)
VALUES (8, 1, 1, 'Описание балла 1 в Характеристике2', 'Детализация балла 1 в Характеристике2', false);
INSERT INTO profile.transcript (questionnaire_criterion_id, min_value, max_value, description, text_value, is_deleted)
VALUES (8, 2, 2, 'Описание балла 2 в Характеристике2', 'Детализация балла 2 в Характеристике2', false);
INSERT INTO profile.transcript (questionnaire_criterion_id, min_value, max_value, description, text_value, is_deleted)
VALUES (8, 3, 3, 'Описание балла 3 в Характеристике2', 'Детализация балла 3 в Характеристике2', false);

INSERT INTO profile.transcript (questionnaire_criterion_id, min_value, max_value, description, text_value, is_deleted)
VALUES (9, 1, 5, 'Описание баллов 1-3 в Характеристике3', 'Детализация баллов 1-3 в Характеристике3', false);

INSERT INTO profile.transcript (questionnaire_criterion_id, min_value, max_value, description, text_value, is_deleted)
VALUES (10, 1, 5, 'Описание баллов 1-3 в Характеристике4', 'Детализация баллов 1-3 в Характеристике4', false);

INSERT INTO profile.transcript (questionnaire_criterion_id, min_value, max_value, description, text_value, is_deleted)
VALUES (11, 1, 5, 'Описание баллов 1-3 в Характеристике5', 'Детализация баллов 1-3 в Характеристике5', false);

INSERT INTO profile.transcript (questionnaire_criterion_id, min_value, max_value, description, text_value, is_deleted)
VALUES (12, 1, 5, 'Описание баллов 1-3 в Характеристике6', 'Детализация баллов 1-3 в Характеристике6', false);


INSERT INTO profile.questionnaire_criterion_instance (id, questionnaire_instance_id, name, max_grade, grade, criterion_type_id, criterion_group_id, crm_user_id_edited, edit_date, questionaire_criterion_id, is_deleted)
VALUES (1, 1, 'Навык1', 5, 0, 1, 1, 2, null, 1, false);
INSERT INTO profile.questionnaire_criterion_instance (id, questionnaire_instance_id, name, max_grade, grade, criterion_type_id, criterion_group_id, crm_user_id_edited, edit_date, questionaire_criterion_id, is_deleted)
VALUES (2, 1, 'Навык2', 5, 0, 1, 1, 2, null, 2, false);
INSERT INTO profile.questionnaire_criterion_instance (id, questionnaire_instance_id, name, max_grade, grade, criterion_type_id, criterion_group_id, crm_user_id_edited, edit_date, questionaire_criterion_id, is_deleted)
VALUES (3, 1, 'Навык3', 5, 0, 1, 1, 2, null, 3, false);
INSERT INTO profile.questionnaire_criterion_instance (id, questionnaire_instance_id, name, max_grade, grade, criterion_type_id, criterion_group_id, crm_user_id_edited, edit_date, questionaire_criterion_id, is_deleted)
VALUES (4, 1, 'Навык4', 5, 0, 1, 1, 2, null, 4, false);
INSERT INTO profile.questionnaire_criterion_instance (id, questionnaire_instance_id, name, max_grade, grade, criterion_type_id, criterion_group_id, crm_user_id_edited, edit_date, questionaire_criterion_id, is_deleted)
VALUES (5, 1, 'Навык5', 5, 0, 1, 2, 2, null, 5, false);
INSERT INTO profile.questionnaire_criterion_instance (id, questionnaire_instance_id, name, max_grade, grade, criterion_type_id, criterion_group_id, crm_user_id_edited, edit_date, questionaire_criterion_id, is_deleted)
VALUES (6, 1, 'Навык6', 5, 0, 1, 2, 2, null, 6, false);
INSERT INTO profile.questionnaire_criterion_instance (id, questionnaire_instance_id, name, max_grade, grade, criterion_type_id, criterion_group_id, crm_user_id_edited, edit_date, questionaire_criterion_id, is_deleted)
VALUES (7, 1, 'Характеристика1', 3, 0, 2, 3, 2, null, 7, false);
INSERT INTO profile.questionnaire_criterion_instance (id, questionnaire_instance_id, name, max_grade, grade, criterion_type_id, criterion_group_id, crm_user_id_edited, edit_date, questionaire_criterion_id, is_deleted)
VALUES (8, 1, 'Характеристика2', 3, 0, 2, 3, 2, null, 8, false);
INSERT INTO profile.questionnaire_criterion_instance (id, questionnaire_instance_id, name, max_grade, grade, criterion_type_id, criterion_group_id, crm_user_id_edited, edit_date, questionaire_criterion_id, is_deleted)
VALUES (9, 1, 'Характеристика3', 3, 0, 2, 3, 2, null, 9, false);
INSERT INTO profile.questionnaire_criterion_instance (id, questionnaire_instance_id, name, max_grade, grade, criterion_type_id, criterion_group_id, crm_user_id_edited, edit_date, questionaire_criterion_id, is_deleted)
VALUES (10, 1, 'Характеристика4', 3, 0, 2, 4, 2, null, 10, false);
INSERT INTO profile.questionnaire_criterion_instance (id, questionnaire_instance_id, name, max_grade, grade, criterion_type_id, criterion_group_id, crm_user_id_edited, edit_date, questionaire_criterion_id, is_deleted)
VALUES (11, 1, 'Характеристика5', 3, 0, 2, 4, 2, null, 11, false);
INSERT INTO profile.questionnaire_criterion_instance (id, questionnaire_instance_id, name, max_grade, grade, criterion_type_id, criterion_group_id, crm_user_id_edited, edit_date, questionaire_criterion_id, is_deleted)
VALUES (12, 1, 'Характеристика6', 3, 0, 2, 4, 2, null, 12, false);


INSERT INTO profile.questionnaire (id, name, is_default, is_deleted) VALUES (1, 'Шаблон анкеты', true, false);

INSERT INTO profile.questionnaire_criterion_to_questionnaire (id, questionnaire_criterion_id, questionnaire_id, is_deleted) VALUES (1, 1, 1, true);
INSERT INTO profile.questionnaire_criterion_to_questionnaire (id, questionnaire_criterion_id, questionnaire_id, is_deleted) VALUES (2, 2, 1, true);
INSERT INTO profile.questionnaire_criterion_to_questionnaire (id, questionnaire_criterion_id, questionnaire_id, is_deleted) VALUES (3, 3, 1, true);
INSERT INTO profile.questionnaire_criterion_to_questionnaire (id, questionnaire_criterion_id, questionnaire_id, is_deleted) VALUES (4, 4, 1, true);
INSERT INTO profile.questionnaire_criterion_to_questionnaire (id, questionnaire_criterion_id, questionnaire_id, is_deleted) VALUES (5, 5, 1, true);
INSERT INTO profile.questionnaire_criterion_to_questionnaire (id, questionnaire_criterion_id, questionnaire_id, is_deleted) VALUES (6, 6, 1, true);
INSERT INTO profile.questionnaire_criterion_to_questionnaire (id, questionnaire_criterion_id, questionnaire_id, is_deleted) VALUES (7, 7, 1, true);
INSERT INTO profile.questionnaire_criterion_to_questionnaire (id, questionnaire_criterion_id, questionnaire_id, is_deleted) VALUES (8, 8, 1, true);
INSERT INTO profile.questionnaire_criterion_to_questionnaire (id, questionnaire_criterion_id, questionnaire_id, is_deleted) VALUES (9, 9, 1, true);
INSERT INTO profile.questionnaire_criterion_to_questionnaire (id, questionnaire_criterion_id, questionnaire_id, is_deleted) VALUES (10, 10, 1, true);
INSERT INTO profile.questionnaire_criterion_to_questionnaire (id, questionnaire_criterion_id, questionnaire_id, is_deleted) VALUES (11, 11, 1, true);
INSERT INTO profile.questionnaire_criterion_to_questionnaire (id, questionnaire_criterion_id, questionnaire_id, is_deleted) VALUES (12, 12, 1, true);