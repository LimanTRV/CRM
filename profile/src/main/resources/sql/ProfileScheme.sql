DROP SCHEMA public;
CREATE SCHEMA profile;

CREATE TABLE profile.criterion_type
(
  id serial NOT NULL,
  name character varying(255),
  max_grade integer NOT NULL,
  is_deleted boolean NOT NULL default FALSE,
  CONSTRAINT pk_criterion_type PRIMARY KEY (id)
);

CREATE TABLE profile.criterion_group
(
  id serial NOT NULL,
  name character varying(255),
  is_deleted boolean NOT NULL default FALSE,
  CONSTRAINT pk_criterion_group PRIMARY KEY (id)
);

CREATE TABLE profile.questionnaire_criterion
(
  id serial NOT NULL,
  name character varying(255),
  description character varying(2000) COLLATE pg_catalog."default",
  criterion_type_id integer NOT NULL,
  criterion_group_id integer NOT NULL,
  crm_user_id_created integer NOT NULL,
  crm_user_id_edited integer,
  edit_date timestamp without time zone,
  is_deleted boolean NOT NULL default FALSE,
  CONSTRAINT pk_questionnaire_criterion PRIMARY KEY (id),
  CONSTRAINT fk_criterion_group_to_questionnaire_instance FOREIGN KEY (criterion_group_id)
  REFERENCES profile.criterion_group (id) MATCH SIMPLE
  ON UPDATE NO ACTION
  ON DELETE NO ACTION,
  CONSTRAINT fk_criterion_type_to_questionnaire_criterion FOREIGN KEY (criterion_type_id)
  REFERENCES profile.criterion_type (id) MATCH SIMPLE
  ON UPDATE NO ACTION
  ON DELETE NO ACTION,
  CONSTRAINT fk_crm_user_to_questionnaire_criterion FOREIGN KEY (crm_user_id_created)
  REFERENCES main.crm_user (id) MATCH SIMPLE
  ON UPDATE NO ACTION
  ON DELETE NO ACTION,
  CONSTRAINT fk_crm_user_to_questionnaire_criterion2 FOREIGN KEY (crm_user_id_edited)
  REFERENCES main.crm_user (id) MATCH SIMPLE
  ON UPDATE NO ACTION
  ON DELETE NO ACTION
);

CREATE TABLE profile.transcript
(
  id serial NOT NULL,
  questionnaire_criterion_id integer NOT NULL,
  min_value integer NOT NULL,
  max_value integer NOT NULL,
  description character varying(2000) COLLATE pg_catalog."default",
  text_value character varying(2000) COLLATE pg_catalog."default",
  is_deleted boolean NOT NULL default FALSE,
  CONSTRAINT pk_transcript PRIMARY KEY (id),
  CONSTRAINT fk_question_criterion_to_transcript FOREIGN KEY (questionnaire_criterion_id)
  REFERENCES profile.questionnaire_criterion (id) MATCH SIMPLE
  ON UPDATE NO ACTION
  ON DELETE NO ACTION
);

CREATE TABLE profile.questionnaire
(
  id serial NOT NULL,
  name character varying(255),
  is_default boolean NOT NULL default FALSE,
  is_deleted boolean NOT NULL default FALSE,
  CONSTRAINT pk_questionnaire PRIMARY KEY (id)
);

CREATE TABLE profile.questionnaire_criterion_to_questionnaire
(
  id serial NOT NULL,
  questionnaire_criterion_id integer NOT NULL,
  questionnaire_id integer NOT NULL,
  is_deleted boolean NOT NULL default FALSE,
  CONSTRAINT pk_quest_criterion_to_questionnaire PRIMARY KEY (id),
  CONSTRAINT fk_quest_criterion_to_quest_quest_criterion FOREIGN KEY (questionnaire_criterion_id)
  REFERENCES profile.questionnaire_criterion(id) MATCH SIMPLE
  ON UPDATE NO ACTION
  ON DELETE NO ACTION,
  CONSTRAINT fk_quest_criterion_to_quest_questionnaire FOREIGN KEY (questionnaire_id)
  REFERENCES profile.questionnaire (id) MATCH SIMPLE
  ON UPDATE NO ACTION
  ON DELETE NO ACTION
);

CREATE TABLE profile.questionnaire_instance
(
  id serial NOT NULL,
  name character varying(255),
  student_id integer NOT NULL,
  edit_user_id integer NOT NULL,
  is_finished boolean NOT NULL,
  is_deleted boolean NOT NULL default FALSE,
  CONSTRAINT pk_quest_instance PRIMARY KEY (id),
  CONSTRAINT fk_crm_user_to_quest_instance FOREIGN KEY (edit_user_id)
  REFERENCES main.crm_user (id) MATCH SIMPLE
  ON UPDATE NO ACTION
  ON DELETE NO ACTION,
  CONSTRAINT fk_student_to_quest_instance FOREIGN KEY (student_id)
  REFERENCES main.student (id) MATCH SIMPLE
  ON UPDATE NO ACTION
  ON DELETE NO ACTION
);

CREATE TABLE profile.questionnaire_criterion_instance
(
  id serial NOT NULL,
  questionnaire_instance_id integer NOT NULL,
  name character varying(255),
  max_grade integer NOT NULL,
  grade integer,
  criterion_type_id integer NOT NULL,
  criterion_group_id integer NOT NULL,
  crm_user_id_edited integer,
  edit_date timestamp without time zone,
  questionaire_criterion_id INTEGER NOT NULL,
  is_deleted boolean NOT NULL default FALSE,
  CONSTRAINT pk_quest_criterion_instance PRIMARY KEY (id),
  CONSTRAINT fk_criterion_group_to_quest_criterion_instance FOREIGN KEY (criterion_group_id)
  REFERENCES profile.criterion_group (id) MATCH SIMPLE
  ON UPDATE NO ACTION
  ON DELETE NO ACTION,
  CONSTRAINT fk_criterion_type_to_quest_criterion_instance FOREIGN KEY (criterion_type_id)
  REFERENCES profile.criterion_type (id) MATCH SIMPLE
  ON UPDATE NO ACTION
  ON DELETE NO ACTION,
  CONSTRAINT fk_crm_user_to_quest_criterion_instance FOREIGN KEY (crm_user_id_edited)
  REFERENCES main.crm_user (id) MATCH SIMPLE
  ON UPDATE NO ACTION
  ON DELETE NO ACTION,
  CONSTRAINT fk_quest_instance_to_quest_criterion_instance FOREIGN KEY (questionnaire_instance_id)
  REFERENCES profile.questionnaire_instance (id) MATCH SIMPLE
  ON UPDATE NO ACTION
  ON DELETE NO ACTION,
  CONSTRAINT fk_quest_criterion_to_quest_criterion_instance FOREIGN KEY (questionaire_criterion_id)
  REFERENCES profile.questionnaire_criterion (id) MATCH SIMPLE
  ON UPDATE NO ACTION
  ON DELETE NO ACTION
);