package ru.innopolis.crm.lesson.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.ModelAndView;
import ru.innopolis.crm.lesson.models.entity.Journal;
import ru.innopolis.crm.lesson.models.entity.Lesson;
import ru.innopolis.crm.lesson.services.JournalService;
import ru.innopolis.crm.lesson.services.LessonService;
import ru.innopolis.crm.main.models.entity.Student;
import ru.innopolis.crm.main.services.StudentService;
import ru.innopolis.crm.main.sessionbean.UserBean;

import javax.servlet.http.HttpSession;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Контролер, обрабатывающий запросы от студента
 */
@Controller
@PreAuthorize(value = "hasRole('LVS_V')")
@SessionAttributes({"tokenSuccess", "tokenError"})
@RequestMapping("/lessons/")
public class StudentLessonController
{

	private StudentService studentService;
	private LessonService lessonService;
	private JournalService journalService;
	private UserBean userBean;

	@Autowired
	public void setUserBean(UserBean userBean)
	{
		this.userBean = userBean;
	}

	@RequestMapping(value = "/view/student/", method = RequestMethod.GET)
	public ModelAndView viewLessonForStudent(@RequestParam("id") long lessonId, HttpSession session, SessionStatus status)
	{
		ModelAndView modelAndView = new ModelAndView();

		Object successAttr = session.getAttribute("tokenSuccess");
		if (successAttr != null)
		{
			session.removeAttribute("tokenSuccess");
		}

		Object errorAttr = session.getAttribute("tokenError");
		if (errorAttr != null)
		{
			session.removeAttribute("tokenError");
		}
		status.setComplete();

		Lesson lesson = lessonService.getLesson(lessonId);

		modelAndView.addObject("student_id", userBean.getStudentId());

		List<Student> studentList = studentService.getStudentsByGroupId(lesson.getStudyGroupId());
		Map<Long, Student> studentsByJournal = journalService.getStudentsByJoutnal(lesson.getId());

		String[][] students = null;
		if (studentList != null)
		{
			students = new String[studentList.size()][3];
			for (int i = 0; i < studentList.size(); i++)
			{
				Student student = studentList.get(i);
				String journal = studentsByJournal.containsKey(student.getId()) ? "1" : "0";
				students[i] = new String[]{"" + student.getId(), student.getUser().getLastName() + " " + student.getUser().getFirstName(), journal};
			}
		}

		modelAndView.addObject("lesson", lesson);
		modelAndView.addObject("students", students);
		modelAndView.addObject("journal", studentsByJournal.size() + " / " + studentList.size());

		modelAndView.setViewName("forward:/lesson/view_student.jsp");

		return modelAndView;
	}

	@RequestMapping(value = "/view/student/", method = RequestMethod.POST)
	public ModelAndView confirmJournal(@RequestParam("id") long lessonId,
									   @RequestParam(value = "token", required = false) String token,
									   HttpSession session)
	{

		long studentId = userBean.getStudentId();
		ModelAndView modelAndView = new ModelAndView();

		Lesson lesson = lessonService.getLesson(lessonId);

		if ((token != null) && (token.equals(lesson.getToken())))
		{

			long tokenExpirationMillis = -1;
			if (lesson.getTokenExpiration() != null)
			{
				tokenExpirationMillis = lesson.getTokenExpiration().getTime() - new Date().getTime();
			}

			if (tokenExpirationMillis > 0)
			{
				Journal journal = journalService.getByLessonIdAndStudentId(lessonId, studentId);
				if (journal == null)
				{
					journal = new Journal(0, lessonId, lesson, studentId,
							studentService.findById(studentId), new Timestamp(new Date().getTime()), false);
					journalService.addJournal(journal);
				}
				else
				{
					journal.setCheckTime(new Timestamp(new Date().getTime()));
					journalService.updateJournal(journal);
				}
				session.setAttribute("tokenSuccess", "Посещение занятия успешно подтверждено");
			}
			else
			{
				session.setAttribute("tokenError", "Время ввода токена истекло");
			}
		}
		else
		{
			session.setAttribute("tokenError", "Некорректный токен");
		}

		modelAndView.setViewName("redirect:/lessons/view/student/?id=" + studentId);

		return modelAndView;
	}

	@Autowired
	public void setLessonService(LessonService lessonService)
	{
		this.lessonService = lessonService;
	}

	@Autowired
	public void setJournalService(JournalService journalService)
	{
		this.journalService = journalService;
	}

	@Autowired
	public void setStudentService(StudentService studentService)
	{
		this.studentService = studentService;
	}
}
