package ru.innopolis.crm.lesson.services;


import ru.innopolis.crm.lesson.models.entity.Lesson;
import ru.innopolis.crm.lesson.services.exceptions.ServiceException;

import java.util.List;

/**
 * Created by Roman Taranov on 22.04.2017.
 */
public interface LessonService
{
	List<Lesson> findByGroupId(long groupId) throws ServiceException;

	List<Lesson> getAllLessons() throws ServiceException;

	Lesson getLesson(Long id) throws ServiceException;

	int addLesson(Lesson obj) throws ServiceException;

	int updateLesson(Lesson obj) throws ServiceException;

	Boolean deleteLesson(Long id) throws ServiceException;
}
