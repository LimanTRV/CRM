package ru.innopolis.crm.lesson.models.dao.impl;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import ru.innopolis.crm.lesson.models.dao.LessonDao;
import ru.innopolis.crm.lesson.models.entity.Lesson;
import ru.innopolis.crm.lesson.models.exceptions.DaoException;
import ru.innopolis.crm.main.models.dao.GroupDao;


import javax.sql.DataSource;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Lessons DAO implementation
 */
@Repository
public class LessonDaoImpl implements LessonDao
{

	private static final Logger LOGGER = Logger.getLogger(LessonDao.class);

	private static final String GET_ALL_LESSONS = "SELECT * FROM lesson.lesson WHERE NOT is_deleted;";
	private static final String GET_LESSON_BY_GROUP_ID = "SELECT * FROM lesson.lesson where study_group_id = %d and not is_deleted;";
	private static final String GET_LESSON = "SELECT * FROM lesson.lesson where id = %d and not is_deleted;";
	private static final String ADD_LESSON = "INSERT INTO lesson.lesson (study_group_id, lesson_date, room, topic, description, lesson_comment, token, token_expiration, is_deleted) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)";
	private static final String UPDATE_LESSON = "UPDATE lesson.lesson SET study_group_id = ?, lesson_date = ?, room = ?, topic = ?, description = ?, lesson_comment = ?, token = ?, token_expiration = ?  WHERE id = %d";
	private static final String DELETE_LESSON = "UPDATE lesson.lesson SET is_deleted = true where id = %d";

	private DataSource dataSource;

	private GroupDao groupDao;

	@Autowired
	public void setDataSource(DataSource dataSource)
	{
		this.dataSource = dataSource;
	}

	@Autowired
	public void setGroupDao(GroupDao groupDao)
	{
		this.groupDao = groupDao;
	}

	/**
	 * Getter for property 'allLessons'.
	 *
	 * @return Value for property 'allLessons'.
	 */
	@Override
	public List<Lesson> getAllLessons() throws DaoException
	{
		List<Lesson> result = new ArrayList<>(16);
		Connection connection = null;

		try
		{
			connection = dataSource.getConnection();
			Statement statement = connection.createStatement();
			ResultSet resultSet = statement.executeQuery(GET_ALL_LESSONS);

			while (resultSet.next())
			{
				Long studyGroupId = resultSet.getLong("study_group_id");
				result.add(new Lesson(
						resultSet.getLong("id"),
						groupDao.findById(studyGroupId),
						studyGroupId,
						resultSet.getTimestamp("lesson_date"),
						resultSet.getString("room"),
						resultSet.getString("topic"),
						resultSet.getString("description"),
						resultSet.getString("lesson_comment"),
						resultSet.getString("token"),
						resultSet.getTimestamp("token_expiration"),
						false));
			}
		}
		catch (SQLException e)
		{
			LOGGER.error(e);
			throw new DaoException();
		}

		return result;
	}

	@Override
	public List<Lesson> getLessonsByGroupId(long groupId) throws DaoException
	{
		List<Lesson> result = new ArrayList<>(16);
		Connection connection = null;

		try
		{
			connection = dataSource.getConnection();
			Statement statement = connection.createStatement();
			ResultSet resultSet = statement.executeQuery(String.format(GET_LESSON_BY_GROUP_ID, groupId));

			while (resultSet.next())
			{
				Long studyGroupId = resultSet.getLong("study_group_id");
				result.add(new Lesson(
						resultSet.getLong("id"),
						groupDao.findById(studyGroupId),
						studyGroupId,
						resultSet.getTimestamp("lesson_date"),
						resultSet.getString("room"),
						resultSet.getString("topic"),
						resultSet.getString("description"),
						resultSet.getString("lesson_comment"),
						resultSet.getString("token"),
						resultSet.getTimestamp("token_expiration"),
						false
				));
			}
		}
		catch (SQLException e)
		{
			LOGGER.error(e);
			throw new DaoException();
		}

		return result;
	}

	@Override
	public Lesson getLesson(Long id) throws DaoException
	{
		Lesson result = null;
		Connection connection = null;

		try
		{
			connection = dataSource.getConnection();
			Statement statement = connection.createStatement();
			ResultSet resultSet = statement.executeQuery(String.format(GET_LESSON, id));

			if (resultSet.next())
			{
				Long studyGroupId = resultSet.getLong("study_group_id");
				result = new Lesson(
						resultSet.getLong("id"),
						groupDao.findById(studyGroupId),
						studyGroupId,
						resultSet.getTimestamp("lesson_date"),
						resultSet.getString("room"),
						resultSet.getString("topic"),
						resultSet.getString("description"),
						resultSet.getString("lesson_comment"),
						resultSet.getString("token"),
						resultSet.getTimestamp("token_expiration"),
						false
				);
			}
		}
		catch (SQLException e)
		{
			LOGGER.error(e);
			throw new DaoException();
		}

		return result;
	}

	@Override
	public int addLesson(Lesson obj)
	{
		int inserted = 0;
		Connection connection = null;

		try
		{
			connection = dataSource.getConnection();
			PreparedStatement ps = connection.prepareStatement(ADD_LESSON);
			ps.setLong(1, obj.getStudyGroupId());
			ps.setTimestamp(2, obj.getLessonDate());
			ps.setString(3, obj.getRoom());
			ps.setString(4, obj.getTopic());
			ps.setString(5, obj.getDescription());
			ps.setString(6, obj.getLessonComment());
			ps.setString(7, obj.getToken());
			ps.setTimestamp(8, obj.getTokenExpiration());
			ps.setBoolean(9, false);

			inserted = ps.executeUpdate();
		}
		catch (SQLException e)
		{
			LOGGER.error(e);
			// TODO: 02.05.2017 throw exception
		}

		return inserted;
	}

	@Override
	public int updateLesson(Lesson obj) throws DaoException
	{
		int updated = 0;
		Connection connection = null;

		try
		{
			connection = dataSource.getConnection();
			PreparedStatement ps = connection.prepareStatement(String.format(UPDATE_LESSON, obj.getId()));
			ps.setLong(1, obj.getStudyGroupId());
			ps.setTimestamp(2, obj.getLessonDate());
			ps.setString(3, obj.getRoom());
			ps.setString(4, obj.getTopic());
			ps.setString(5, obj.getDescription());
			ps.setString(6, obj.getLessonComment());
			ps.setString(7, obj.getToken());
			ps.setTimestamp(8, obj.getTokenExpiration());

			updated = ps.executeUpdate();
		}
		catch (SQLException e)
		{
			LOGGER.error(e);
			throw new DaoException();
		}

		return updated;
	}

	@Override
	public Boolean deleteLesson(Long id) throws DaoException
	{
		int deleted = 0;
		Connection connection = null;

		try
		{
			connection = dataSource.getConnection();
			Statement statement = connection.createStatement();
			deleted = statement.executeUpdate(String.format(DELETE_LESSON, id));
		}
		catch (SQLException e)
		{
			LOGGER.error(e);
			throw new DaoException();
		}

		return deleted > 0;
	}

}