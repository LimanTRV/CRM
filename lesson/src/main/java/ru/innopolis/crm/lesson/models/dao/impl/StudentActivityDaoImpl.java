package ru.innopolis.crm.lesson.models.dao.impl;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import ru.innopolis.crm.lesson.models.dao.LessonDao;
import ru.innopolis.crm.lesson.models.dao.StudentActivityDao;
import ru.innopolis.crm.lesson.models.entity.StudentActivity;
import ru.innopolis.crm.lesson.models.exceptions.DaoException;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

/**
 * Student activity DAO implementation
 */
@Repository
@SuppressWarnings("Duplicates")
public class StudentActivityDaoImpl implements StudentActivityDao
{

	private static final Logger LOGGER = Logger.getLogger(StudentActivityDaoImpl.class);

	private static final String ALL_ACTIVITIES_QUERY = "SELECT * FROM lesson.student_activity";

	private static final String UPDATE_ACTIVITY_BY_ID = "UPDATE lesson.student_activity SET " +
			"student_id = ?, lesson_id = ?, activity_grade = ?, activity_comment = ? WHERE id = ?";

	private static final String DELETE_ACTIVITY_BY_ID_FMT = "UPDATE lesson.student_activity SET is_deleted = true WHERE id = %d";

	private static final String QUERY_ACTIVITY_BY_LESSON_FMT = "SELECT * FROM lesson.student_activity WHERE lesson_id = %d AND NOT is_deleted";

	private static final String QUERY_ACTIVITY_BY_STUDENT_ID_FMT = "SELECT * FROM lesson.student_activity where student_id = %d AND NOT is_deleted";

	private static final String QUERY_ACTIVITY_BY_STUDENT_AND_LESSON_FMT = "SELECT * FROM lesson.student_activity where student_id = %d AND lesson_id = %d";

	private static final String INSERT_STUDENT_ACTIVITY = "INSERT INTO lesson.student_activity (student_id, " +
			"lesson_id, activity_grade, activity_comment, is_deleted) VALUES (?, ?, ?, ?, FALSE)";

	private DataSource dataSource;

	private LessonDao lessonDao;


	@Override
	public List<StudentActivity> getAllStudentActivities() throws DaoException
	{
		List<StudentActivity> result = new ArrayList<>(16);
		Connection connection = null;
		try
		{
			connection = dataSource.getConnection();
			Statement statement = connection.createStatement();
			ResultSet resultSet = statement.executeQuery(ALL_ACTIVITIES_QUERY);
			while (resultSet.next())
			{
				StudentActivity studentActivity = createNewInstance(resultSet);
				result.add(studentActivity);
			}
		}
		catch (SQLException e)
		{
			LOGGER.error(e);
			throw new DaoException();
		}

		return result;
	}

	@Override
	public StudentActivity getStudentActivity(long id) throws DaoException
	{
		StudentActivity studentActivity = null;
		Connection connection = null;
		try
		{
			connection = dataSource.getConnection();
			Statement statement = connection.createStatement();
			ResultSet resultSet = statement.executeQuery(String.format(QUERY_ACTIVITY_BY_STUDENT_ID_FMT, id));
			if (resultSet.next())
			{
				studentActivity = createNewInstance(resultSet);
			}
		}
		catch (SQLException e)
		{
			LOGGER.error("An SQL error occurred :" + e.getMessage() + ", error code:" + e.getErrorCode());
			throw new DaoException();
		}
		return studentActivity;
	}

	@Override
	public int addStudentActivity(StudentActivity obj) throws DaoException
	{
		Connection connection = null;
		int inserted = 0;
		try
		{
			connection = dataSource.getConnection();
			PreparedStatement statement = connection.prepareStatement(INSERT_STUDENT_ACTIVITY);
			statement.setLong(1, obj.getStudentId());
			statement.setLong(2, obj.getLessonId());
			statement.setInt(3, obj.getActivityPoints());
			statement.setString(4, obj.getActivityComment());
			inserted = statement.executeUpdate();
		}
		catch (SQLException e)
		{
			LOGGER.error("An SQL error occurred :" + e.getMessage() + ", error code:" + e.getErrorCode());
			throw new DaoException();
		}
		return inserted;
	}

	@Override
	public int updateStudentActivity(StudentActivity obj) throws DaoException
	{
		Connection connection = null;
		int inserted = 0;
		try
		{
			connection = dataSource.getConnection();
			PreparedStatement statement = connection.prepareStatement(UPDATE_ACTIVITY_BY_ID);

			statement.setLong(1, obj.getStudentId());
			statement.setLong(2, obj.getLessonId());
			statement.setInt(3, obj.getActivityPoints());
			statement.setString(4, obj.getActivityComment());
			statement.setLong(5, obj.getId());
			inserted = statement.executeUpdate();

		}
		catch (SQLException e)
		{
			LOGGER.error("An SQL error occurred :" + e.getMessage() + ", error code:" + e.getErrorCode());
			throw new DaoException();
		}
		return inserted;
	}

	@Override
	public boolean deleteStudentActivity(StudentActivity obj) throws DaoException
	{
		Connection connection = null;
		int result = 0;
		try
		{
			connection = dataSource.getConnection();
			Statement statement = connection.createStatement();
			result = statement.executeUpdate(String.format(DELETE_ACTIVITY_BY_ID_FMT, obj.getId()));

		}
		catch (SQLException e)
		{
			LOGGER.error("An SQL error occurred :" + e.getMessage() + ", error code:" + e.getErrorCode());
			throw new DaoException();
		}
		return result > 0;
	}

	@Override
	public Map<Long, StudentActivity> getStudentActivitiesByLessonId(long lessonId) throws DaoException
	{
		Map<Long, StudentActivity> map = new HashMap<>();
		Connection connection = null;
		try
		{
			connection = dataSource.getConnection();
			Statement statement = connection.createStatement();
			ResultSet resultSet = statement.executeQuery(String.format(QUERY_ACTIVITY_BY_LESSON_FMT, lessonId));

			while (resultSet.next())
			{
				map.put(resultSet.getLong("student_id"), createNewInstance(resultSet));
			}

		}
		catch (SQLException e)
		{
			LOGGER.error("An SQL error occurred :" + e.getMessage() + ", error code:" + e.getErrorCode());
			throw new DaoException();
		}
		return map;
	}

	@Override
	public StudentActivity getStudentActivityByStudentAndLesson(long studentId, long lessonId) throws DaoException
	{
		Connection connection = null;
		StudentActivity studentActivity = null;

		try
		{
			connection = dataSource.getConnection();
			Statement statement = connection.createStatement();
			ResultSet resultSet = statement.executeQuery(String
					.format(QUERY_ACTIVITY_BY_STUDENT_AND_LESSON_FMT, studentId, lessonId));

			if (resultSet.next())
			{
				studentActivity = createNewInstance(resultSet);
			}
		}
		catch (SQLException e)
		{
			LOGGER.error("An SQL error occurred :" + e.getMessage() + ", error code:" + e.getErrorCode());
			throw new DaoException();
		}
		return studentActivity;
	}

	@Autowired
	public void setLessonDao(LessonDao lessonDao)
	{
		this.lessonDao = lessonDao;
	}

	@Autowired
	public void setDataSource(DataSource dataSource)
	{
		this.dataSource = dataSource;
	}

	private StudentActivity createNewInstance(ResultSet resultSet) throws SQLException, DaoException
	{
		return new StudentActivity(
				resultSet.getLong("id"),
				null,
				resultSet.getLong("student_id"),
				lessonDao.getLesson(resultSet.getLong("lesson_id")),
				resultSet.getLong("lesson_id"),
				resultSet.getInt("activity_grade"),
				resultSet.getString("activity_comment")
		);
	}
}
