package ru.innopolis.crm.lesson.models.dao;

import ru.innopolis.crm.lesson.models.entity.Lesson;
import ru.innopolis.crm.lesson.models.exceptions.DaoException;

import java.util.List;

/**
 * DAO for Lessons
 */
public interface LessonDao
{
	List<Lesson> getAllLessons() throws DaoException;

	List<Lesson> getLessonsByGroupId(long groupId) throws DaoException;

	Lesson getLesson(Long id) throws DaoException;

	int addLesson(Lesson obj) throws DaoException;

	int updateLesson(Lesson obj) throws DaoException;

	Boolean deleteLesson(Long id) throws DaoException;
}
