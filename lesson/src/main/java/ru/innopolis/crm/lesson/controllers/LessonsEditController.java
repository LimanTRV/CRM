package ru.innopolis.crm.lesson.controllers;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

import ru.innopolis.crm.lesson.models.entity.Lesson;
import ru.innopolis.crm.lesson.services.LessonService;
import ru.innopolis.crm.main.models.entity.StudyGroup;
import ru.innopolis.crm.main.services.GroupService;

/**
 * Контролер отвечающий за редактирование занятий
 */
@Controller
@PreAuthorize(value = "hasRole('L_E')")
@RequestMapping("/lessons/")
public class LessonsEditController
{

	private static final String STATUS_OK = "{ \"status\" : \"OK\" }";
	private static final String STATUS_ERROR = "{ \"status\" : \"Error\" }";

	private static final Logger LOGGER = Logger.getLogger(LessonsEditController.class);

	private LessonService lessonService;
	private GroupService groupService;

	@RequestMapping(value = "/new/", method = RequestMethod.GET)
	public ModelAndView forwardToAddLesson(@RequestParam("group") long groupId)
	{
		ModelAndView modelAndView = new ModelAndView();

		StudyGroup group = groupService.findById(groupId);

		// TODO: 03.05.2017 убрать, когда заработает меню
		modelAndView.addObject("currentGroup", group);

		modelAndView.setViewName("forward:/lesson/edit.jsp");
		return modelAndView;
	}

	@RequestMapping(value = "/edit/", method = RequestMethod.GET)
	public ModelAndView forwardToEditLesson(@RequestParam(value = "group") long groupId,
											@RequestParam(value = "id") long lessonId)
	{

		ModelAndView modelAndView = new ModelAndView();

		StudyGroup group = groupService.findById(groupId);

		// TODO: 03.05.2017 убрать, когда заработает меню
		modelAndView.addObject("currentGroup", group);


		Lesson lesson = lessonService.getLesson(lessonId);
		modelAndView.addObject("id", lessonId);
		modelAndView.addObject("date", lesson.getLessonDate()
				.toLocalDateTime().format(DateTimeFormatter.ISO_LOCAL_DATE));
		modelAndView.addObject("room", lesson.getRoom());
		modelAndView.addObject("topic", lesson.getTopic());
		modelAndView.addObject("description", lesson.getDescription());

		modelAndView.setViewName("forward:/lesson/edit.jsp");

		return modelAndView;
	}

	@RequestMapping(value = "/delete/", method = RequestMethod.GET)
	public ModelAndView deleteLesson(@RequestParam(value = "group") long groupId,
									 @RequestParam(value = "id") long lessonId)
	{

		ModelAndView modelAndView = new ModelAndView();

		if (lessonService.deleteLesson(lessonId))
		{
			modelAndView.setViewName("redirect:/lessons/?group=" + groupId);
		}
		else
		{
			LOGGER.warn("Couldn't delete lesson with id #" + lessonId);
			modelAndView.setViewName("redirect:/lessons/?group=" + groupId);
		}

		return modelAndView;
	}

	@RequestMapping(value = "/new/", method = RequestMethod.POST)
	@ResponseBody
	public String saveLesson(@RequestParam(value = "group") long groupId,
							 @RequestParam(value = "room") String room,
							 @RequestParam(value = "topic") String topic,
							 @RequestParam(value = "description") String description,
							 @RequestParam(value = "less_date") String lessonDate)
	{


		LocalDate localDate = LocalDate.parse(lessonDate, DateTimeFormatter.ISO_LOCAL_DATE);

		Lesson lesson = new Lesson(0, null,
				groupId, Timestamp.valueOf(LocalDateTime.of(localDate, LocalTime.MIN)), room, topic,
				description, null, null, null, false);

		int inserted = lessonService.addLesson(lesson);

		if (inserted > 0)
		{
			return STATUS_OK;

		}
		else
		{
			LOGGER.error("Error during adding new lesson");
			return STATUS_ERROR;
		}
	}

	@RequestMapping(value = "/edit/", method = RequestMethod.POST)
	@ResponseBody
	public String updateLesson(@RequestParam(value = "id") long id,
							   @RequestParam(value = "group", required = false) long groupId,
							   @RequestParam(value = "room") String room,
							   @RequestParam(value = "topic") String topic,
							   @RequestParam(value = "description") String description,
							   @RequestParam(value = "less_date") String lessonDate)
	{

		Lesson lesson = lessonService.getLesson(id);

		LocalDate localDate = LocalDate.parse(lessonDate, DateTimeFormatter.ISO_LOCAL_DATE);
		lesson.setLessonDate(Timestamp.valueOf(LocalDateTime.of(localDate, LocalTime.MIN)));
		lesson.setRoom(room);
		lesson.setTopic(topic);
		lesson.setDescription(description);

		int updated = lessonService.updateLesson(lesson);
		if (updated > 0)
		{
			return STATUS_OK;
		}
		else
		{
			LOGGER.error("Error during adding new lesson");
			return STATUS_ERROR;
		}
	}

	@Autowired
	public void setLessonService(LessonService lessonService)
	{
		this.lessonService = lessonService;
	}

	@Autowired
	public void setGroupService(GroupService groupService)
	{
		this.groupService = groupService;
	}
}
