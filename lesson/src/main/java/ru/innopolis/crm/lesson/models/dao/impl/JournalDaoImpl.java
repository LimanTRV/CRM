package ru.innopolis.crm.lesson.models.dao.impl;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import ru.innopolis.crm.lesson.models.dao.JournalDao;
import ru.innopolis.crm.lesson.models.dao.LessonDao;
import ru.innopolis.crm.lesson.models.entity.Journal;
import ru.innopolis.crm.lesson.models.exceptions.DaoException;
import ru.innopolis.crm.main.models.dao.StudentDao;
import ru.innopolis.crm.main.models.entity.Student;
import ru.innopolis.crm.main.models.entity.User;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

/**
 * Dao for log of students visiting.
 */
@Repository
public class JournalDaoImpl implements JournalDao
{

	private static final Logger LOGGER = Logger.getLogger(JournalDaoImpl.class);
	private static final String QUERY_ALL_JOURNALS = "SELECT * FROM lesson.journal WHERE not is_deleted";
	private static final String QUERY_JOURNAL_BY_ID_FMT = "SELECT * FROM lesson.journal WHERE id = %d and not is_deleted";
	private static final String INSERT_JOURNAL = "INSERT INTO lesson.journal (lesson_id, student_id, time_check, is_deleted) VALUES (?, ?, ?, ?)";
	private static final String UPDATE_JOURNAL_BY_ID = "UPDATE lesson.journal SET lesson_id = ?, student_id = ?, time_check = ? WHERE id = ?";
	private static final String DELETE_JOURNAL_BY_ID = "UPDATE lesson.journal SET is_deleted = true where id = ?";
	private static final String QUERY_STUDENTS_JOURNAL_BY_LESSON_ID = "SELECT std.id, std.is_deleted, std.crm_user_id, " +
			" usr.login, usr.user_password, usr.first_name, usr.last_name, usr.email, usr.is_active, usr.is_deleted " +
			"FROM main.student std LEFT JOIN main.crm_user usr ON std.crm_user_id = usr.id LEFT JOIN lesson.journal jrn " +
			"ON std.id = jrn.student_id WHERE jrn.lesson_id = ?";
	private static final String QUERY_JOURNAL_BY_LESSON_AND_STUDENT = "SELECT * FROM lesson.journal WHERE lesson_id = ? AND student_id = ?";

	private DataSource dataSource;

	private LessonDao lessonDao;
	private StudentDao studentDao;


	@Override
	public List<Journal> getAllJournals() throws DaoException
	{
		List<Journal> result = new ArrayList<>();

		try
		{
			Connection connection = dataSource.getConnection();
			Statement statement = connection.createStatement();
			ResultSet resultSet = statement.executeQuery(QUERY_ALL_JOURNALS);
			while (resultSet.next())
			{
				result.add(newInstance(resultSet));
			}

		}
		catch (SQLException e)
		{
			LOGGER.error("An SQL error occurred :" + e.getMessage() + ", error code:" + e.getErrorCode());
			throw new DaoException();
		}

		return result;
	}

	@Override
	public Journal getJournal(long id) throws DaoException
	{
		Journal journal = null;

		try
		{
			Connection connection = dataSource.getConnection();
			Statement statement = connection.createStatement();
			ResultSet resultSet = statement
					.executeQuery(String.format(QUERY_JOURNAL_BY_ID_FMT, id));
			if (resultSet.next())
			{
				journal = newInstance(resultSet);
			}
		}
		catch (SQLException e)
		{
			LOGGER.error("An SQL error occurred :" + e.getMessage() + ", error code:" + e.getErrorCode());
			throw new DaoException();
		}
		return journal;
	}

	@Override
	public int addJournal(Journal journal) throws DaoException
	{
		int inserted = 0;
		try
		{
			Connection connection = dataSource.getConnection();
			PreparedStatement ps = connection.prepareStatement(INSERT_JOURNAL);

			ps.setLong(1, journal.getLessonId());
			ps.setLong(2, journal.getStudentId());
			ps.setTimestamp(3, journal.getCheckTime());
			ps.setBoolean(4, false);
			inserted = ps.executeUpdate();
		}
		catch (SQLException e)
		{
			LOGGER.error("An SQL error occurred :" + e.getMessage() + ", error code:" + e.getErrorCode());
			throw new DaoException();
		}

		return inserted;
	}

	@Override
	public int updateJournal(Journal journal) throws DaoException
	{
		int updated = 0;
		try
		{
			Connection connection = dataSource.getConnection();
			PreparedStatement ps = connection.prepareStatement(UPDATE_JOURNAL_BY_ID);
			ps.setLong(1, journal.getLessonId());
			ps.setLong(2, journal.getStudentId());
			ps.setTimestamp(3, journal.getCheckTime());
			ps.setLong(4, journal.getId());

			updated = ps.executeUpdate();
		}
		catch (SQLException e)
		{
			LOGGER.error("An SQL error occurred :" + e.getMessage() + ", error code:" + e.getErrorCode());
			throw new DaoException();
		}

		return updated;
	}

	@Override
	public int deleteJournal(long id) throws DaoException
	{
		int updated = 0;
		try
		{
			Connection connection = dataSource.getConnection();
			PreparedStatement ps = connection.prepareStatement(DELETE_JOURNAL_BY_ID);
			ps.setLong(1, id);

			updated = ps.executeUpdate();
		}
		catch (SQLException e)
		{
			LOGGER.error("An SQL error occurred :" + e.getMessage() + ", error code:" + e.getErrorCode());
			throw new DaoException();
		}
		return updated;
	}

	@Override
	public Map<Long, Student> getStudentsByJoutnal(long lessonId) throws DaoException
	{
		Map<Long, Student> studentMap = new HashMap<>();
		try
		{
			Connection connection = dataSource.getConnection();
			PreparedStatement preparedStatement = connection.prepareStatement(QUERY_STUDENTS_JOURNAL_BY_LESSON_ID);
			preparedStatement.setLong(1, lessonId);

			ResultSet resultSet = preparedStatement.executeQuery();
			while (resultSet.next())
			{
				Student student = new Student();
				student.setId(resultSet.getLong(1));
				student.setDeleted(resultSet.getBoolean(2));
				User user = new User();
				user.setId(resultSet.getLong(3));
				user.setLogin(resultSet.getString(4));
				user.setPassword(resultSet.getString(5));
				user.setFirstName(resultSet.getString(6));
				user.setLastName(resultSet.getString(7));
				user.setEmail(resultSet.getString(8));
				user.setActive(resultSet.getBoolean(9));
				user.setDeleted(resultSet.getBoolean(10));
				student.setUser(user);
				studentMap.put(student.getId(), student);
			}
		}
		catch (SQLException e)
		{
			LOGGER.error("An SQL error occurred :" + e.getMessage() + ", error code:" + e.getErrorCode());
			throw new DaoException();
		}
		return studentMap;
	}

	@Override
	public Journal getByLessonIdAndStudentId(long lessonId, long studentId) throws DaoException
	{
		Journal journal = null;
		try
		{
			Connection connection = dataSource.getConnection();
			PreparedStatement preparedStatement = connection.prepareStatement(QUERY_JOURNAL_BY_LESSON_AND_STUDENT);
			preparedStatement.setLong(1, lessonId);
			preparedStatement.setLong(2, studentId);
			ResultSet resultSet = preparedStatement.executeQuery();
			resultSet.next();
			if (resultSet.getRow() > 0)
			{
				journal = new Journal(resultSet.getLong(1), resultSet.getLong(2),
						lessonDao.getLesson(lessonId), resultSet.getLong(3),
						studentDao.getStudent(studentId), resultSet.getTimestamp(4),
						resultSet.getBoolean(5));
			}
		}
		catch (SQLException e)
		{
			LOGGER.error("An SQL error occurred :" + e.getMessage() + ", error code:" + e.getErrorCode());
			throw new DaoException();
		}
		return journal;
	}

	@Autowired
	public void setDataSource(DataSource dataSource)
	{
		this.dataSource = dataSource;
	}

	@Autowired
	public void setLessonDao(LessonDao lessonDao)
	{
		this.lessonDao = lessonDao;
	}

	@Autowired
	public void setStudentDao(StudentDao studentDao)
	{
		this.studentDao = studentDao;
	}

	private Journal newInstance(ResultSet resultSet) throws SQLException, DaoException
	{
		long lessonId = resultSet.getLong("lesson_id");

		return new Journal(resultSet.getLong("id"), lessonId,
				lessonDao.getLesson(lessonId),
				resultSet.getLong("student_id"), null,
				resultSet.getTimestamp("time_check"), false);
	}
}
