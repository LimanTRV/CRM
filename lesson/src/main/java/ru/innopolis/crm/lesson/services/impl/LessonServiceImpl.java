package ru.innopolis.crm.lesson.services.impl;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ru.innopolis.crm.lesson.models.dao.LessonDao;
import ru.innopolis.crm.lesson.models.entity.Lesson;
import ru.innopolis.crm.lesson.models.exceptions.DaoException;
import ru.innopolis.crm.lesson.services.LessonService;
import ru.innopolis.crm.lesson.services.exceptions.ServiceException;

import java.util.List;

/**
 * Created by Roman Taranov on 22.04.2017.
 */
@Service
public class LessonServiceImpl implements LessonService
{

	private static final Logger LOGGER = Logger.getLogger(LessonServiceImpl.class);

	@Autowired
	private LessonDao lessonDao;

	@Override
	public List<Lesson> findByGroupId(long groupId)
	{
		try
		{
			return lessonDao.getLessonsByGroupId(groupId);
		}
		catch (DaoException e)
		{
			LOGGER.error(e);
			throw new ServiceException();
		}
	}

	@Override
	public List<Lesson> getAllLessons()
	{
		try
		{
			return lessonDao.getAllLessons();
		}
		catch (DaoException e)
		{
			LOGGER.error(e);
			throw new ServiceException();
		}
	}

	@Override
	public Lesson getLesson(Long id)
	{
		try
		{
			return lessonDao.getLesson(id);
		}
		catch (DaoException e)
		{
			LOGGER.error(e);
			throw new ServiceException();
		}
	}

	@Override
	public int addLesson(Lesson obj)
	{
		try
		{
			return lessonDao.addLesson(obj);
		}
		catch (DaoException e)
		{
			LOGGER.error(e);
			throw new ServiceException();
		}
	}

	@Override
	public int updateLesson(Lesson obj)
	{
		try
		{
			return lessonDao.updateLesson(obj);
		}
		catch (DaoException e)
		{
			LOGGER.error(e);
			throw new ServiceException();
		}
	}

	@Override
	public Boolean deleteLesson(Long id)
	{
		try
		{
			return lessonDao.deleteLesson(id);
		}
		catch (DaoException e)
		{
			LOGGER.error(e);
			throw new ServiceException();
		}
	}
}
