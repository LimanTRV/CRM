package ru.innopolis.crm.lesson.models.entity;

import ru.innopolis.crm.main.models.entity.StudyGroup;

import java.sql.Timestamp;
import java.util.Objects;

/**
 * Lesson
 */
public class Lesson
{
	private long id;
	private StudyGroup studyGroup;
	private Long studyGroupId;
	private Timestamp lessonDate;
	private String room;
	private String topic;
	private String description;
	private String lessonComment;
	private String token;
	private Timestamp tokenExpiration;
	private Boolean isDeleted;

	public Lesson(long id, StudyGroup studyGroup, Long studyGroupId, Timestamp lessonDate, String room, String topic, String description, String lessonComment, String token, Timestamp tokenExpiration, Boolean isDeleted)
	{
		this.id = id;
		this.studyGroup = studyGroup;
		this.studyGroupId = studyGroupId;
		this.lessonDate = lessonDate;
		this.room = room;
		this.topic = topic;
		this.description = description;
		this.lessonComment = lessonComment;
		this.token = token;
		this.tokenExpiration = tokenExpiration;
		this.isDeleted = isDeleted;
	}

	/**
	 * Getter for property 'id'.
	 *
	 * @return Value for property 'id'.
	 */
	public long getId()
	{
		return id;
	}

	/**
	 * Setter for property 'id'.
	 *
	 * @param id Value to set for property 'id'.
	 */
	public void setId(long id)
	{
		this.id = id;
	}

	/**
	 * Getter for property 'studyGroup'.
	 *
	 * @return Value for property 'studyGroup'.
	 */
	public StudyGroup getStudyGroup()
	{
		return studyGroup;
	}

	/**
	 * Setter for property 'studyGroup'.
	 *
	 * @param studyGroup Value to set for property 'studyGroup'.
	 */
	public void setStudyGroup(StudyGroup studyGroup)
	{
		this.studyGroup = studyGroup;
	}

	/**
	 * Getter for property 'studyGroupId'.
	 *
	 * @return Value for property 'studyGroupId'.
	 */
	public Long getStudyGroupId()
	{
		return studyGroupId;
	}

	/**
	 * Setter for property 'studyGroupId'.
	 *
	 * @param studyGroupId Value to set for property 'studyGroupId'.
	 */
	public void setStudyGroupId(Long studyGroupId)
	{
		this.studyGroupId = studyGroupId;
	}

	/**
	 * Getter for property 'lessonDate'.
	 *
	 * @return Value for property 'lessonDate'.
	 */
	public Timestamp getLessonDate()
	{
		return lessonDate;
	}

	/**
	 * Setter for property 'lessonDate'.
	 *
	 * @param lessonDate Value to set for property 'lessonDate'.
	 */
	public void setLessonDate(Timestamp lessonDate)
	{
		this.lessonDate = lessonDate;
	}

	/**
	 * Getter for property 'room'.
	 *
	 * @return Value for property 'room'.
	 */
	public String getRoom()
	{
		return room;
	}

	/**
	 * Setter for property 'room'.
	 *
	 * @param room Value to set for property 'room'.
	 */
	public void setRoom(String room)
	{
		this.room = room;
	}

	/**
	 * Getter for property 'topic'.
	 *
	 * @return Value for property 'topic'.
	 */
	public String getTopic()
	{
		return topic;
	}

	/**
	 * Setter for property 'topic'.
	 *
	 * @param topic Value to set for property 'topic'.
	 */
	public void setTopic(String topic)
	{
		this.topic = topic;
	}

	/**
	 * Getter for property 'description'.
	 *
	 * @return Value for property 'description'.
	 */
	public String getDescription()
	{
		return description;
	}

	/**
	 * Setter for property 'description'.
	 *
	 * @param description Value to set for property 'description'.
	 */
	public void setDescription(String description)
	{
		this.description = description;
	}

	/**
	 * Getter for property 'lessonComment'.
	 *
	 * @return Value for property 'lessonComment'.
	 */
	public String getLessonComment()
	{
		return lessonComment;
	}

	/**
	 * Setter for property 'lessonComment'.
	 *
	 * @param lessonComment Value to set for property 'lessonComment'.
	 */
	public void setLessonComment(String lessonComment)
	{
		this.lessonComment = lessonComment;
	}

	/**
	 * Getter for property 'token'.
	 *
	 * @return Value for property 'token'.
	 */
	public String getToken()
	{
		return token;
	}

	/**
	 * Setter for property 'token'.
	 *
	 * @param token Value to set for property 'token'.
	 */
	public void setToken(String token)
	{
		this.token = token;
	}

	/**
	 * Getter for property 'tokenExpiration'.
	 *
	 * @return Value for property 'tokenExpiration'.
	 */
	public Timestamp getTokenExpiration()
	{
		return tokenExpiration;
	}

	/**
	 * Setter for property 'tokenExpiration'.
	 *
	 * @param tokenExpiration Value to set for property 'tokenExpiration'.
	 */
	public void setTokenExpiration(Timestamp tokenExpiration)
	{
		this.tokenExpiration = tokenExpiration;
	}

	public Boolean getDeleted()
	{
		return isDeleted;
	}

	public void setDeleted(Boolean deleted)
	{
		isDeleted = deleted;
	}

	@Override
	public boolean equals(Object o)
	{
		if (this == o)
		{
			return true;
		}
		if (o == null || getClass() != o.getClass())
		{
			return false;
		}
		Lesson lesson = (Lesson) o;
		return id == lesson.id &&
				Objects.equals(studyGroup, lesson.studyGroup) &&
				Objects.equals(studyGroupId, lesson.studyGroupId) &&
				Objects.equals(lessonDate, lesson.lessonDate) &&
				Objects.equals(room, lesson.room) &&
				Objects.equals(topic, lesson.topic) &&
				Objects.equals(description, lesson.description) &&
				Objects.equals(lessonComment, lesson.lessonComment) &&
				Objects.equals(token, lesson.token) &&
				Objects.equals(tokenExpiration, lesson.tokenExpiration) &&
				Objects.equals(isDeleted, lesson.isDeleted);
	}

	@Override
	public int hashCode()
	{
		return Objects.hash(id, studyGroup, studyGroupId, lessonDate, room, topic, description, lessonComment, token, tokenExpiration, isDeleted);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String toString()
	{
		return "Lesson{" +
				"studyGroupId=" + studyGroupId +
				", lessonDate=" + lessonDate +
				", room='" + room + '\'' +
				", topic='" + topic + '\'' +
				", description='" + description + '\'' +
				", lessonComment='" + lessonComment + '\'' +
				", token='" + token + '\'' +
				", tokenExpiration=" + tokenExpiration +
				'}';
	}
}
