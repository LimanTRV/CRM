package ru.innopolis.crm.lesson.utils;

import java.util.Random;

/**
 * Генератор токенов
 */
public class TokenGenerator
{

	private static final String DICT = "ABCDEFGHIJKLMNOPQRSTUVWXYZ" +
			"abcdefghijklmnopqrstuvwxyz" + "1234567890";

	private static final Random RANDOM = new Random();

	private TokenGenerator()
	{
	}

	/**
	 * Генерирует новый токен заданной длины
	 *
	 * @param tokenSize длина токена
	 * @return новый токен
	 */
	public static String generateToken(int tokenSize)
	{
		StringBuilder result = new StringBuilder();

		for (int i = 0; i < tokenSize; i++)
		{
			result.append(DICT.charAt(RANDOM.nextInt(DICT.length())));
		}
		return result.toString();
	}
}
