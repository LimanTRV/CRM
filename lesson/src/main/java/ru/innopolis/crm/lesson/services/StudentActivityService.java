package ru.innopolis.crm.lesson.services;

import ru.innopolis.crm.lesson.models.entity.StudentActivity;
import ru.innopolis.crm.lesson.services.exceptions.ServiceException;

import java.util.List;
import java.util.Map;

/**
 * Created by STC-05 Team [Roman Taranov] on 22.04.2017.
 */
public interface StudentActivityService
{
	List<StudentActivity> getAllStudentActivities() throws ServiceException;

	StudentActivity getStudentActivity(long id) throws ServiceException;

	int addStudentActivity(StudentActivity obj) throws ServiceException;

	int updateStudentActivity(StudentActivity obj) throws ServiceException;

	boolean deleteStudentActivity(StudentActivity obj) throws ServiceException;

	/**
	 * Метод для получения активности студентов на конкретном занятии
	 *
	 * @param lessonId ид заняти
	 * @return Map<Ид студента, Активность студента>
	 */
	Map<Long, StudentActivity> getStudentActivitiesByLessonId(long lessonId) throws ServiceException;

	StudentActivity getStudentActivityByStudentAndLesson(long studentId, long lessonId) throws ServiceException;
}
