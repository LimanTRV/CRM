package ru.innopolis.crm.lesson.services;

import ru.innopolis.crm.lesson.models.entity.Journal;
import ru.innopolis.crm.lesson.services.exceptions.ServiceException;
import ru.innopolis.crm.main.models.entity.Student;

import java.util.List;
import java.util.Map;

/**
 * Created by STC-05 Team [Roman Taranov] on 25.04.2017.
 */
public interface JournalService {
    List<Journal> getAllJournals() throws ServiceException;

    Journal getJournal(long id) throws ServiceException;

    int addJournal(Journal journal) throws ServiceException;

    int updateJournal(Journal journal) throws ServiceException;

    int deleteJournal(long id) throws ServiceException;

    Map<Long, Student> getStudentsByJoutnal(long lessonId) throws ServiceException;

    Journal getByLessonIdAndStudentId(long lessonId, long studentId) throws ServiceException;
}
