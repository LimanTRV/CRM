package ru.innopolis.crm.lesson.models.entity;

import ru.innopolis.crm.main.models.entity.Student;

import java.util.Objects;

/**
 * Activity of students on lessons.
 */
public class StudentActivity
{
	private long id;
	private Student student;
	private Long studentId;
	private Lesson lesson;
	private Long lessonId;
	private Integer activityPoints;
	private String activityComment;
	private Boolean isDeleted;

	public StudentActivity(long id, Student student, Long studentId, Lesson lesson, Long lessonId, Integer activityPoints, String activityComment)
	{
		this.id = id;
		this.student = student;
		this.studentId = studentId;
		this.lesson = lesson;
		this.lessonId = lessonId;
		this.activityPoints = activityPoints;
		this.activityComment = activityComment;
	}

	/**
	 * Getter for property 'id'.
	 *
	 * @return Value for property 'id'.
	 */
	public long getId()
	{
		return id;
	}

	/**
	 * Setter for property 'id'.
	 *
	 * @param id Value to set for property 'id'.
	 */
	public void setId(long id)
	{
		this.id = id;
	}

	/**
	 * Getter for property 'student'.
	 *
	 * @return Value for property 'student'.
	 */
	public Student getStudent()
	{
		return student;
	}

	/**
	 * Setter for property 'student'.
	 *
	 * @param student Value to set for property 'student'.
	 */
	public void setStudent(Student student)
	{
		this.student = student;
	}

	/**
	 * Getter for property 'studentId'.
	 *
	 * @return Value for property 'studentId'.
	 */
	public Long getStudentId()
	{
		return studentId;
	}

	/**
	 * Setter for property 'studentId'.
	 *
	 * @param studentId Value to set for property 'studentId'.
	 */
	public void setStudentId(Long studentId)
	{
		this.studentId = studentId;
	}

	/**
	 * Getter for property 'lesson'.
	 *
	 * @return Value for property 'lesson'.
	 */
	public Lesson getLesson()
	{
		return lesson;
	}

	/**
	 * Setter for property 'lesson'.
	 *
	 * @param lesson Value to set for property 'lesson'.
	 */
	public void setLesson(Lesson lesson)
	{
		this.lesson = lesson;
	}

	/**
	 * Getter for property 'lessonId'.
	 *
	 * @return Value for property 'lessonId'.
	 */
	public Long getLessonId()
	{
		return lessonId;
	}

	/**
	 * Setter for property 'lessonId'.
	 *
	 * @param lessonId Value to set for property 'lessonId'.
	 */
	public void setLessonId(Long lessonId)
	{
		this.lessonId = lessonId;
	}

	/**
	 * Getter for property 'activityPoints'.
	 *
	 * @return Value for property 'activityPoints'.
	 */
	public Integer getActivityPoints()
	{
		return activityPoints;
	}

	/**
	 * Setter for property 'activityPoints'.
	 *
	 * @param activityPoints Value to set for property 'activityPoints'.
	 */
	public void setActivityPoints(Integer activityPoints)
	{
		this.activityPoints = activityPoints;
	}

	/**
	 * Getter for property 'activityComment'.
	 *
	 * @return Value for property 'activityComment'.
	 */
	public String getActivityComment()
	{
		return activityComment;
	}

	/**
	 * Setter for property 'activityComment'.
	 *
	 * @param activityComment Value to set for property 'activityComment'.
	 */
	public void setActivityComment(String activityComment)
	{
		this.activityComment = activityComment;
	}

	/**
	 * Getter for property 'deleted'.
	 *
	 * @return Value for property 'deleted'.
	 */
	public Boolean getDeleted()
	{
		return isDeleted;
	}

	/**
	 * Setter for property 'deleted'.
	 *
	 * @param deleted Value to set for property 'deleted'.
	 */
	public void setDeleted(Boolean deleted)
	{
		isDeleted = deleted;
	}

	@Override
	public boolean equals(Object o)
	{
		if (this == o)
		{
			return true;
		}
		if (o == null || getClass() != o.getClass())
		{
			return false;
		}
		StudentActivity that = (StudentActivity) o;
		return id == that.id &&
				Objects.equals(student, that.student) &&
				Objects.equals(studentId, that.studentId) &&
				Objects.equals(lesson, that.lesson) &&
				Objects.equals(lessonId, that.lessonId) &&
				Objects.equals(activityPoints, that.activityPoints) &&
				Objects.equals(activityComment, that.activityComment) &&
				Objects.equals(isDeleted, that.isDeleted);
	}

	@Override
	public int hashCode()
	{
		return Objects.hash(id, student, studentId, lesson, lessonId, activityPoints, activityComment, isDeleted);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String toString()
	{
		return "StudentActivity{" +
				"id=" + id +
				", student=" + student +
				", studentId=" + studentId +
				", lesson=" + lesson +
				", lessonId=" + lessonId +
				", activityPoints=" + activityPoints +
				", activityComment='" + activityComment + '\'' +
				'}';
	}

}
