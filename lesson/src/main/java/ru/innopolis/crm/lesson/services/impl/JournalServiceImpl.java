package ru.innopolis.crm.lesson.services.impl;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ru.innopolis.crm.lesson.models.dao.JournalDao;
import ru.innopolis.crm.lesson.models.dao.impl.JournalDaoImpl;
import ru.innopolis.crm.lesson.models.entity.Journal;
import ru.innopolis.crm.lesson.models.exceptions.DaoException;
import ru.innopolis.crm.lesson.services.JournalService;
import ru.innopolis.crm.lesson.services.exceptions.ServiceException;
import ru.innopolis.crm.main.models.entity.Student;

import java.util.List;
import java.util.Map;

/**
 * Created by STC-05 Team [Roman Taranov] on 25.04.2017.
 */
@Service
public class JournalServiceImpl implements JournalService
{

	private static final Logger LOGGER = Logger.getLogger(JournalServiceImpl.class);

	private JournalDao journalDao;

	@Override
	public List<Journal> getAllJournals()
	{
		try
		{

			return journalDao.getAllJournals();

		}
		catch (DaoException e)
		{
			LOGGER.error(e);
			throw new ServiceException();
		}
	}

	@Override
	public Journal getJournal(long id)
	{
		try
		{

			return journalDao.getJournal(id);

		}
		catch (DaoException e)
		{
			LOGGER.error(e);
			throw new ServiceException();
		}
	}

	@Override
	public int addJournal(Journal journal)
	{
		try
		{

			return journalDao.addJournal(journal);

		}
		catch (DaoException e)
		{
			LOGGER.error(e);
			throw new ServiceException();
		}
	}

	@Override
	public int updateJournal(Journal journal)
	{
		try
		{

			return journalDao.addJournal(journal);

		}
		catch (DaoException e)
		{
			LOGGER.error(e);
			throw new ServiceException();
		}
	}

	@Override
	public int deleteJournal(long id)
	{
		try
		{

			return journalDao.deleteJournal(id);

		}
		catch (DaoException e)
		{
			LOGGER.error(e);
			throw new ServiceException();
		}
	}

	@Override
	public Map<Long, Student> getStudentsByJoutnal(long lessonId)
	{
		try
		{

			return journalDao.getStudentsByJoutnal(lessonId);

		}
		catch (DaoException e)
		{
			LOGGER.error(e);
			throw new ServiceException();
		}
	}

	@Override
	public Journal getByLessonIdAndStudentId(long lessonId, long studentId)
	{
		try
		{

			return journalDao.getByLessonIdAndStudentId(lessonId, studentId);

		}
		catch (DaoException e)
		{
			LOGGER.error(e);
			throw new ServiceException();
		}
	}

	@Autowired
	public void setJournalDao(JournalDao journalDao)
	{
		this.journalDao = journalDao;
	}
}
