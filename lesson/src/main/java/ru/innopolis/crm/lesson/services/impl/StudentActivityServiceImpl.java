package ru.innopolis.crm.lesson.services.impl;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ru.innopolis.crm.lesson.models.dao.StudentActivityDao;
import ru.innopolis.crm.lesson.models.entity.StudentActivity;
import ru.innopolis.crm.lesson.models.exceptions.DaoException;
import ru.innopolis.crm.lesson.services.StudentActivityService;
import ru.innopolis.crm.lesson.services.exceptions.ServiceException;

import java.util.List;
import java.util.Map;

/**
 * Created by STC-05 Team [Roman Taranov] on 22.04.2017.
 */
@Service
public class StudentActivityServiceImpl implements StudentActivityService
{

	private static final Logger LOGGER = Logger.getLogger(StudentActivityServiceImpl.class);

	private StudentActivityDao studentActivityDao;

	@Override
	public List<StudentActivity> getAllStudentActivities()
	{
		return null;
	}

	@Override
	public StudentActivity getStudentActivity(long id)
	{
		return null;
	}

	@Override
	public int addStudentActivity(StudentActivity obj)
	{
		try
		{
			return studentActivityDao.addStudentActivity(obj);
		}
		catch (DaoException e)
		{
			LOGGER.error(e);
			throw new ServiceException();
		}
	}

	@Override
	public int updateStudentActivity(StudentActivity obj)
	{
		try
		{
			return studentActivityDao.updateStudentActivity(obj);
		}
		catch (DaoException e)
		{
			LOGGER.error(e);
			throw new ServiceException();
		}
	}

	@Override
	public boolean deleteStudentActivity(StudentActivity obj)
	{
		return false;
	}

	@Override
	public Map<Long, StudentActivity> getStudentActivitiesByLessonId(long lessonId)
	{
		try
		{
			return studentActivityDao.getStudentActivitiesByLessonId(lessonId);
		}
		catch (DaoException e)
		{
			LOGGER.error(e);
			throw new ServiceException();
		}
	}

	@Override
	public StudentActivity getStudentActivityByStudentAndLesson(long studentId, long lessonId)
	{
		try
		{
			return studentActivityDao.getStudentActivityByStudentAndLesson(studentId, lessonId);
		}
		catch (DaoException e)
		{
			LOGGER.error(e);
			throw new ServiceException();
		}
	}

	@Autowired
	public void setStudentActivityDao(StudentActivityDao studentActivityDao)
	{
		this.studentActivityDao = studentActivityDao;
	}
}
