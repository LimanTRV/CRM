package ru.innopolis.crm.lesson.models.dao;

import ru.innopolis.crm.lesson.models.entity.Journal;
import ru.innopolis.crm.lesson.models.exceptions.DaoException;
import ru.innopolis.crm.main.models.entity.Student;

import java.util.List;
import java.util.Map;

/**
 * DAO для Journal
 * Created by Roman Taranov on 17.04.2017.
 */
public interface JournalDao
{

	List<Journal> getAllJournals() throws DaoException;

	Journal getJournal(long id) throws DaoException;

	int addJournal(Journal journal) throws DaoException;

	int updateJournal(Journal journal) throws DaoException;

	int deleteJournal(long id) throws DaoException;

	Map<Long, Student> getStudentsByJoutnal(long lessonId) throws DaoException;

	Journal getByLessonIdAndStudentId(long lessonId, long studentId) throws DaoException;
}
