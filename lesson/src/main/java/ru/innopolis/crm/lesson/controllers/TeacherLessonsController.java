package ru.innopolis.crm.lesson.controllers;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import java.sql.Timestamp;
import java.util.*;
import java.util.stream.Collectors;

import ru.innopolis.crm.lesson.models.entity.Lesson;
import ru.innopolis.crm.lesson.models.entity.StudentActivity;
import ru.innopolis.crm.lesson.services.JournalService;
import ru.innopolis.crm.lesson.services.LessonService;
import ru.innopolis.crm.lesson.services.StudentActivityService;
import ru.innopolis.crm.lesson.utils.TokenGenerator;
import ru.innopolis.crm.main.models.entity.Student;
import ru.innopolis.crm.main.models.entity.StudyGroup;
import ru.innopolis.crm.main.services.GroupService;
import ru.innopolis.crm.main.services.StudentService;
import ru.innopolis.crm.main.sessionbean.UserBean;


/**
 *
 */
@Controller
@RequestMapping("/lessons/")
public class TeacherLessonsController
{

	private static final String STATUS_OK = "{ \"status\" : \"OK\" }";
	private static final String STATUS_ERROR = "{ \"status\" : \"Error\" }";

	private StudentService studentService;
	private GroupService groupService;
	private LessonService lessonService;
	private JournalService journalService;
	private StudentActivityService studentActivityService;
	private UserBean userBean;

	@Autowired
	public void setUserBean(UserBean userBean)
	{
		this.userBean = userBean;
	}

	@PreAuthorize(value = "hasRole('L_V')")
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public ModelAndView getLessonsList(@RequestAttribute(value = "currentGroup", required = false) StudyGroup group)
	{

		ModelAndView modelAndView = new ModelAndView();

		if (userBean.getStudentId() == 0)
		{
			modelAndView.addObject("lessons", lessonService.findByGroupId(group.getId()));
			modelAndView.addObject("currentGroup", group);
		}
		else
		{
			List<Long> groupIds = studentService.getStudentGroups(userBean.getStudentId());
			List<Lesson> lessons = new ArrayList<>();
			for (Long groupId : groupIds)
			{
				lessons.addAll(lessonService.findByGroupId(groupId));
			}

			modelAndView.addObject("lessons", lessons.stream().sorted(Comparator.comparingLong(a -> ((Lesson) a).getId()).reversed()).collect(Collectors.toList()));
		}

		modelAndView.setViewName("/lesson/list");

		return modelAndView;
	}

	@PreAuthorize(value = "hasRole('LV_E')")
	@RequestMapping(value = "/view/", method = RequestMethod.GET)
	public ModelAndView viewLessonForTeacher(@RequestParam("id") long lessonId,
											 @RequestParam("group") long groupId,
											 @RequestParam(value = "token", required = false) String token)
	{

		ModelAndView modelAndView = new ModelAndView();

		Lesson lesson = lessonService.getLesson(lessonId);

		if (token != null)
		{
			lesson.setToken(TokenGenerator.generateToken(5));
			Calendar calendar = new GregorianCalendar();
			calendar.add(Calendar.MINUTE, 3);

			lesson.setTokenExpiration(new Timestamp(calendar.getTime().getTime()));
			lessonService.updateLesson(lesson);
			modelAndView.setViewName("redirect:/lessons/view/?group=" + groupId + "&id=" + lesson.getId());
			return modelAndView;
		}

		long tokenExpirationSeconds = -1;
		if (lesson.getTokenExpiration() != null)
		{
			long milliseconds = lesson.getTokenExpiration().getTime() - new Date().getTime();
			tokenExpirationSeconds = milliseconds / 1000;
			tokenExpirationSeconds = (tokenExpirationSeconds > 0) ? tokenExpirationSeconds : 0;
		}

		List<Student> studentList = studentService.getStudentsByGroupId(lesson.getStudyGroupId());
		Map<Long, Student> studentsByJournal = journalService.getStudentsByJoutnal(lesson.getId());
		Map<Long, StudentActivity> studentsActivity = studentActivityService.getStudentActivitiesByLessonId(lesson.getId());

		String[][] students = null;
		if (studentList != null)
		{
			students = new String[studentList.size()][5];
			for (int i = 0; i < studentList.size(); i++)
			{
				Student student = studentList.get(i);
				String journal = studentsByJournal.containsKey(student.getId()) ? "1" : "0";

				String grade = "";
				String comment = "";

				if (studentsActivity.containsKey(student.getId()))
				{
					grade = String.valueOf(studentsActivity.get(student.getId())
							.getActivityPoints());
					comment = studentsActivity.get(student.getId()).
							getActivityComment();
				}

				String fullName = student.getUser().getLastName() + " " + student.getUser().getFirstName();

				students[i] = new String[]{String.valueOf(student.getId()), fullName, grade, comment, journal};
			}
		}

		modelAndView.addObject("id", lesson.getId());
		modelAndView.addObject("lesson", lesson);
		modelAndView.addObject("studyGroup", groupService.findById(groupId));

		if (token == null)
		{
			token = lesson.getToken();
		}

		modelAndView.addObject("token_expiration", tokenExpirationSeconds);
		modelAndView.addObject("token", token);
		modelAndView.addObject("students", students);
		modelAndView.addObject("journal", studentsByJournal.size() + " / " + studentList.size());

		modelAndView.setViewName("forward:/lesson/view.jsp");
		return modelAndView;
	}

	@PreAuthorize(value = "hasRole('LV_E')")
	@ResponseBody
	@RequestMapping(value = "/view/comment/", method = RequestMethod.POST)
	public String updateLessonComment(@RequestParam("id") long lessonId,
									  @RequestParam("group") long groupId,
									  @RequestParam(value = "value", required = false) String comment)
	{

		Lesson lesson = lessonService.getLesson(lessonId);
		lesson.setLessonComment(comment);

		int updated = lessonService.updateLesson(lesson);

		return updated > 0 ? STATUS_OK : STATUS_ERROR;
	}

	@PreAuthorize(value = "hasRole('LV_E')")
	@ResponseBody
	@RequestMapping(value = "/view/activity/", method = RequestMethod.POST)
	public String updateStudentActivityComment(@RequestParam("id") long lessonId,
											   @RequestParam("group") long groupId,
											   @RequestParam("student") long studentId,
											   @RequestParam(value = "activityComment", required = false) String activityComment)
	{

		StudentActivity studentActivity = studentActivityService
				.getStudentActivityByStudentAndLesson(studentId, lessonId);

		int updated = 0;
		if (studentActivity == null)
		{
			if (!StringUtils.isEmpty(activityComment))
			{
				studentActivity = new StudentActivity(0, null, studentId, null,
						lessonId, 0, activityComment);
				updated = studentActivityService.addStudentActivity(studentActivity);
			}
		}
		else
		{
			studentActivity.setActivityComment(activityComment);
			updated = studentActivityService.updateStudentActivity(studentActivity);
		}

		return updated > 0 ? STATUS_OK : STATUS_ERROR;
	}

	@PreAuthorize(value = "hasRole('LV_E')")
	@ResponseBody
	@RequestMapping(value = "/view/points/", method = RequestMethod.POST)
	public String updateStudentActivityPoints(@RequestParam("id") long lessonId,
											  @RequestParam("group") long groupId,
											  @RequestParam("student") long studentId,
											  @RequestParam(value = "activityPoints") int activityPoints)
	{


		StudentActivity studentActivity = studentActivityService
				.getStudentActivityByStudentAndLesson(studentId, lessonId);

		int updated = 0;
		if (studentActivity == null)
		{
			studentActivity = new StudentActivity(0, null, studentId, null,
					lessonId, activityPoints, null);
			updated = studentActivityService.addStudentActivity(studentActivity);
		}
		else
		{
			studentActivity.setActivityPoints(activityPoints);
			updated = studentActivityService.updateStudentActivity(studentActivity);
		}

		return updated > 0 ? STATUS_OK : STATUS_ERROR;
	}

	@Autowired
	public void setLessonService(LessonService lessonService)
	{
		this.lessonService = lessonService;
	}

	@Autowired
	public void setJournalService(JournalService journalService)
	{
		this.journalService = journalService;
	}

	@Autowired
	public void setStudentActivityService(StudentActivityService studentActivityService)
	{
		this.studentActivityService = studentActivityService;
	}

	@Autowired
	public void setStudentService(StudentService studentService)
	{
		this.studentService = studentService;
	}

	@Autowired
	public void setGroupService(GroupService groupService)
	{
		this.groupService = groupService;
	}
}
