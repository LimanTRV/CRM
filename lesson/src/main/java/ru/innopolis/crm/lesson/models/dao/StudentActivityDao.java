package ru.innopolis.crm.lesson.models.dao;

import ru.innopolis.crm.lesson.models.entity.StudentActivity;
import ru.innopolis.crm.lesson.models.exceptions.DaoException;

import java.util.List;
import java.util.Map;

/**
 * DAO for student activities
 */
public interface StudentActivityDao
{

	List<StudentActivity> getAllStudentActivities() throws DaoException;

	StudentActivity getStudentActivity(long id) throws DaoException;

	int addStudentActivity(StudentActivity obj) throws DaoException;

	int updateStudentActivity(StudentActivity obj) throws DaoException;

	boolean deleteStudentActivity(StudentActivity obj) throws DaoException;

	/**
	 * Метод для получения активности студентов на конкретном занятии
	 *
	 * @param lessonId ид заняти
	 * @return Map<Ид студента, Активность студента>
	 */
	Map<Long, StudentActivity> getStudentActivitiesByLessonId(long lessonId) throws DaoException;

	StudentActivity getStudentActivityByStudentAndLesson(long studentId, long lessonId) throws DaoException;
}