package ru.innopolis.crm.lesson.models.entity;

import ru.innopolis.crm.main.models.entity.Student;

import java.sql.Timestamp;
import java.util.Objects;

/**
 * Log of students visiting
 */
public class Journal
{
	private long id;
	private long lessonId;
	private Lesson lesson;
	private long studentId;
	private Student student;
	private Timestamp checkTime;
	private boolean isDeleted;

	public Journal(long id, long lessonId, Lesson lesson, long studentId, Student student, Timestamp checkTime, boolean isDeleted)
	{
		this.id = id;
		this.lessonId = lessonId;
		this.lesson = lesson;
		this.studentId = studentId;
		this.student = student;
		this.checkTime = checkTime;
		this.isDeleted = isDeleted;
	}

	/**
	 * Getter for property 'id'.
	 *
	 * @return Value for property 'id'.
	 */
	public long getId()
	{
		return id;
	}

	/**
	 * Setter for property 'id'.
	 *
	 * @param id Value to set for property 'id'.
	 */
	public void setId(long id)
	{
		this.id = id;
	}

	/**
	 * Getter for property 'lessonId'.
	 *
	 * @return Value for property 'lessonId'.
	 */
	public long getLessonId()
	{
		return lessonId;
	}

	/**
	 * Setter for property 'lessonId'.
	 *
	 * @param lessonId Value to set for property 'lessonId'.
	 */
	public void setLessonId(long lessonId)
	{
		this.lessonId = lessonId;
	}

	/**
	 * Getter for property 'studentId'.
	 *
	 * @return Value for property 'studentId'.
	 */
	public long getStudentId()
	{
		return studentId;
	}

	/**
	 * Setter for property 'studentId'.
	 *
	 * @param studentId Value to set for property 'studentId'.
	 */
	public void setStudentId(long studentId)
	{
		this.studentId = studentId;
	}

	/**
	 * Getter for property 'checkTime'.
	 *
	 * @return Value for property 'checkTime'.
	 */
	public Timestamp getCheckTime()
	{
		return checkTime;
	}

	/**
	 * Setter for property 'checkTime'.
	 *
	 * @param checkTime Value to set for property 'checkTime'.
	 */
	public void setCheckTime(Timestamp checkTime)
	{
		this.checkTime = checkTime;
	}

	/**
	 * Getter for property 'deleted'.
	 *
	 * @return Value for property 'deleted'.
	 */
	public boolean isDeleted()
	{
		return isDeleted;
	}

	/**
	 * Setter for property 'deleted'.
	 *
	 * @param deleted Value to set for property 'deleted'.
	 */
	public void setDeleted(boolean deleted)
	{
		this.isDeleted = deleted;
	}

	/**
	 * Getter for property 'lesson'.
	 *
	 * @return Value for property 'lesson'.
	 */
	public Lesson getLesson()
	{
		return lesson;
	}

	/**
	 * Setter for property 'lesson'.
	 *
	 * @param lesson Value to set for property 'lesson'.
	 */
	public void setLesson(Lesson lesson)
	{
		this.lesson = lesson;
	}

	/**
	 * Getter for property 'student'.
	 *
	 * @return Value for property 'student'.
	 */
	public Student getStudent()
	{
		return student;
	}

	/**
	 * Setter for property 'student'.
	 *
	 * @param student Value to set for property 'student'.
	 */
	public void setStudent(Student student)
	{
		this.student = student;
	}

	@Override
	public boolean equals(Object o)
	{
		if (this == o)
		{
			return true;
		}
		if (o == null || getClass() != o.getClass())
		{
			return false;
		}
		Journal journal = (Journal) o;
		return id == journal.id &&
				lessonId == journal.lessonId &&
				studentId == journal.studentId &&
				isDeleted == journal.isDeleted &&
				Objects.equals(lesson, journal.lesson) &&
				Objects.equals(student, journal.student) &&
				Objects.equals(checkTime, journal.checkTime);
	}

	@Override
	public int hashCode()
	{
		return Objects.hash(id, lessonId, lesson, studentId, student, checkTime, isDeleted);
	}

	@Override
	public String toString()
	{
		return "Journal{" +
				"id=" + id +
				", lessonId=" + lessonId +
				", lesson=" + lesson +
				", studentId=" + studentId +
				", student=" + student +
				", checkTime=" + checkTime +
				", isDeleted=" + isDeleted +
				'}';
	}
}
